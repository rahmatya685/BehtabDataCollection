package ir.zngis.behtabdatacollection

import android.net.Uri
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.TestSubscriber
import ir.zngis.behtabdatacollection.baseBinding.BindableBoolean
import ir.zngis.behtabdatacollection.baseBinding.BindableDouble
import ir.zngis.behtabdatacollection.baseBinding.BindableInt
import ir.zngis.behtabdatacollection.baseBinding.BindableString
import ir.zngis.behtabdatacollection.repository.dataModel.Attachment
import ir.zngis.behtabdatacollection.repository.remote.EntityService
import ir.zngis.behtabdatacollection.repository.remote.deserializer.*
import ir.zngis.behtabdatacollection.repository.remote.serializer.*
import ir.zngis.behtabdatacollection.util.EnumUtil
import junit.framework.Assert.assertNotNull
import okhttp3.MediaType
import okhttp3.OkHttpClient
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import okhttp3.RequestBody
import okhttp3.MultipartBody
import java.io.File


@RunWith(AndroidJUnit4::class)
class UploadTest {


    var api: EntityService? = null

    @Before
    fun prepareRetrofit() {
        val client = OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS).build()

        val gsonBuilder = GsonBuilder()


        val myGson = gsonBuilder.create()

        val context = InstrumentationRegistry.getContext()

        val retrofit = Retrofit.Builder().baseUrl("http://192.168.1.2:8080")
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(myGson))
                .build()

        api = retrofit.create(EntityService::class.java)

    }

    @Test
    fun uploadFile() {


        api?.let { entityService ->


            val file = File("/sdcard/Android/data/ir.zngis.behtabdatacollection/files/Pictures/behtab_20181208_221736_-507060078.jpg")

            val reqFile = RequestBody.create(MediaType.parse("image/*"), file)
            val body = MultipartBody.Part.createFormData("file", file.getName(), reqFile)


            val attachment = Attachment()
            attachment.category = "Test"
            attachment.comments = "daka"

            val gs= Gson()
            val json = gs.toJson(attachment)


           val category=  RequestBody.create(
                    okhttp3.MultipartBody.FORM, json)

//            val attachmentReceived = entityService.uploadAttachment("7895d2d2-cfc0-4d58-bba6-3196618fe2ad", body,category).blockingFirst()
//
//            assertNotNull(attachmentReceived)
//            val testSubscriber = entityService.uploadAttachment("7895d2d2-cfc0-4d58-bba6-3196618fe2ad",body).test()
//
//            testSubscriber.awaitTerminalEvent()
//
//
//
//            testSubscriber.assertValue {
//                assertNotNull(it)
//
//                true
//            }

        }

    }


    @NonNull
    private fun createPartFromString(descriptionString: String): RequestBody {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString)
    }


}