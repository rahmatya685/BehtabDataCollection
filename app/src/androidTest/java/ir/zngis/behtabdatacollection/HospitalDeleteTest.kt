package ir.zngis.behtabdatacollection

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import ir.zngis.behtabdatacollection.repository.local.LocalDb
import ir.zngis.behtabdatacollection.repository.local.dao.HospitalDao
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class HospitalDeleteTest {

    private lateinit var hospitalDao: HospitalDao

    private lateinit var db: LocalDb


    @Before
    fun initDb() {
        db = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(), LocalDb::class.java).allowMainThreadQueries().build()
        hospitalDao = db.hospitalDao()
    }

    @After
    @Throws
    fun after() {
        db.close()
    }


    @Test
    fun deleteHospitalTest() {


        val hospitals =  hospitalDao.getHospitals().blockingFirst()

        hospitalDao.delete(hospitals.first())


    }


}