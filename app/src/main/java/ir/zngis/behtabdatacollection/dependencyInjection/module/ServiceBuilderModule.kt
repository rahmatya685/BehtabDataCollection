package ir.zngis.behtabdatacollection.dependencyInjection.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ir.zngis.behtabdatacollection.task.GetDataService
import ir.zngis.behtabdatacollection.task.SyncDataService


@Module
abstract class ServiceBuilderModule {

    @ContributesAndroidInjector()
    abstract fun contributeSyncDataService(): SyncDataService

    @ContributesAndroidInjector
    abstract fun contributeGetDataService(): GetDataService
}