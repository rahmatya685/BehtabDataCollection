package ir.zngis.behtabdatacollection.dependencyInjection;


import android.app.Application;
import androidx.lifecycle.ViewModelProvider;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ir.zngis.behtabdatacollection.R;
import ir.zngis.behtabdatacollection.baseBinding.BindableBoolean;
import ir.zngis.behtabdatacollection.baseBinding.BindableDouble;
import ir.zngis.behtabdatacollection.baseBinding.BindableInt;
import ir.zngis.behtabdatacollection.baseBinding.BindableString;
import ir.zngis.behtabdatacollection.repository.DataManager;
import ir.zngis.behtabdatacollection.repository.DataManagerImpl;
import ir.zngis.behtabdatacollection.repository.local.DataPopulator;
import ir.zngis.behtabdatacollection.repository.local.LocalDb;
import ir.zngis.behtabdatacollection.repository.local.LocalRepository;
import ir.zngis.behtabdatacollection.repository.local.LocalRepositoryImpl;
import ir.zngis.behtabdatacollection.repository.local.MigrationFromV1_ToV2;
import ir.zngis.behtabdatacollection.repository.local.MigrationFromV2_ToV3;
import ir.zngis.behtabdatacollection.repository.local.dao.AttachmentDao;
import ir.zngis.behtabdatacollection.repository.local.dao.BuildingDao;
import ir.zngis.behtabdatacollection.repository.local.dao.CandidateLocationDao;
import ir.zngis.behtabdatacollection.repository.local.dao.ClinicalUnitDao;
import ir.zngis.behtabdatacollection.repository.local.dao.EvaluationDao;
import ir.zngis.behtabdatacollection.repository.local.dao.HospitalCommentsDao;
import ir.zngis.behtabdatacollection.repository.local.dao.HospitalDao;
import ir.zngis.behtabdatacollection.repository.local.dao.HospitalEntranceDao;
import ir.zngis.behtabdatacollection.repository.local.dao.NonClinicalUnitDao;
import ir.zngis.behtabdatacollection.repository.local.dao.QuestionCategoryDao;
import ir.zngis.behtabdatacollection.repository.local.dao.QuestionDao;
import ir.zngis.behtabdatacollection.repository.local.dao.SettingDao;
import ir.zngis.behtabdatacollection.repository.local.dao.SurgeryDepartmentDao;
import ir.zngis.behtabdatacollection.repository.local.dao.UnitCategoryDao;
import ir.zngis.behtabdatacollection.repository.local.dao.UnitSubCategoryDao;
import ir.zngis.behtabdatacollection.repository.remote.AnnotationExclusionStrategy;
import ir.zngis.behtabdatacollection.repository.remote.deserializer.BindableBooleanDeserializer;
import ir.zngis.behtabdatacollection.repository.remote.deserializer.BooleanDeserializer;
import ir.zngis.behtabdatacollection.repository.remote.deserializer.EntityStatusDeserializer;
import ir.zngis.behtabdatacollection.repository.remote.deserializer.HealthLocationTypeDeserializer;
import ir.zngis.behtabdatacollection.repository.remote.deserializer.PointDeserializer;
import ir.zngis.behtabdatacollection.repository.remote.deserializer.PolygonDeserializer;
import ir.zngis.behtabdatacollection.repository.remote.serializer.BindableBooleanSerializer;
import ir.zngis.behtabdatacollection.repository.remote.deserializer.BindableDoubleDeserializer;
import ir.zngis.behtabdatacollection.repository.remote.serializer.BindableDoubleSerializer;
import ir.zngis.behtabdatacollection.repository.remote.deserializer.BindableIntDeserializer;
import ir.zngis.behtabdatacollection.repository.remote.serializer.BindableIntSerializer;
import ir.zngis.behtabdatacollection.repository.remote.deserializer.BindableStringDeserializer;
import ir.zngis.behtabdatacollection.repository.remote.serializer.BindableStringSerializer;
import ir.zngis.behtabdatacollection.repository.remote.EntityService;
import ir.zngis.behtabdatacollection.repository.remote.EntityApi;
import ir.zngis.behtabdatacollection.repository.remote.EntityApiImpl;
import ir.zngis.behtabdatacollection.repository.remote.serializer.BooleanSerializer;
import ir.zngis.behtabdatacollection.repository.remote.serializer.EntityStatusSerializer;
import ir.zngis.behtabdatacollection.repository.remote.serializer.HealthLocationTypeSerializer;
import ir.zngis.behtabdatacollection.repository.remote.serializer.PointSerializer;
import ir.zngis.behtabdatacollection.repository.remote.serializer.PolygonSerializer;
import ir.zngis.behtabdatacollection.util.EnumUtil;
import ir.zngis.behtabdatacollection.util.GeneralUtil;
import ir.zngis.geojson.Point;
import ir.zngis.geojson.Polygon;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(subcomponents = ViewModelSubComponent.class)
public class AppModule {

    @Provides
    @Singleton
    public LocalDb providesAppDatabase(Application application) {

        String dbFileDirectory = GeneralUtil.INSTANCE.createDbFolder();

        LocalDb db = Room.databaseBuilder(application, LocalDb.class, dbFileDirectory + File.separator + LocalDb.DB_NAME).allowMainThreadQueries()
                .addCallback(new DataPopulator(application))
                .setJournalMode(RoomDatabase.JournalMode.AUTOMATIC)
                .addMigrations(new MigrationFromV1_ToV2(), new MigrationFromV2_ToV3() )
                .build();

        return db;
    }


    @Provides
    @Singleton
    public AttachmentDao providesAttachmentDao(LocalDb localDb) {
        return localDb.attachmentDao();
    }

    @Provides
    @Singleton
    public BuildingDao providesBuildingDao(LocalDb localDb) {
        return localDb.buildingDao();
    }


    @Provides
    @Singleton
    public CandidateLocationDao providesCandidateLocationDao(LocalDb localDb) {
        return localDb.candidateLocationDao();
    }

    @Provides
    @Singleton
    public ClinicalUnitDao providesClinicalUnitDao(LocalDb localDb) {
        return localDb.clinicalUnitDao();
    }


    @Provides
    @Singleton
    public EvaluationDao providesEvaluationDao(LocalDb localDb) {
        return localDb.evaluationDao();
    }


    @Provides
    @Singleton
    public HospitalDao providesHospitalDao(LocalDb localDb) {
        return localDb.hospitalDao();
    }


    @Provides
    @Singleton
    public HospitalEntranceDao providesHospitalEntranceDao(LocalDb localDb) {
        return localDb.hospitalEntranceDao();
    }

    @Provides
    @Singleton
    public NonClinicalUnitDao providesNonClinicalUnit(LocalDb localDb) {
        return localDb.nonClinicalUnitDao();
    }

    @Provides
    @Singleton
    public SettingDao provideSettingDao(LocalDb localDb) {
        return localDb.settingDao();
    }


    @Provides
    @Singleton
    public QuestionDao providesQuestionDao(LocalDb localDb) {
        return localDb.questionDao();
    }


    @Provides
    @Singleton
    public SurgeryDepartmentDao providesSurgeryDepartmentDao(LocalDb localDb) {
        return localDb.surgeryDepartmentDao();
    }


    @Provides
    @Singleton
    public UnitCategoryDao providesUnitCategory(LocalDb localDb) {
        return localDb.unitCategoryDao();
    }

    @Provides
    @Singleton
    public UnitSubCategoryDao providesUnitSubCategoryDao(LocalDb localDb) {
        return localDb.unitSubCategoryDao();
    }

    @Provides
    @Singleton
    public QuestionCategoryDao providesQuestionCategoryDao(LocalDb localDb) {

        return localDb.questionCategoryDao();
    }

    @Provides
    @Singleton
    public HospitalCommentsDao provideHospitalCommentsDao(LocalDb localDb) {
        return localDb.hospitalCommentsDao();
    }


    @Provides
    @Singleton
    public LocalRepository providesTbClassSource(Application application,
                                                 LocalDb localDb,
                                                 AttachmentDao attachmentDao,
                                                 BuildingDao buildingDao,
                                                 CandidateLocationDao candidateLocationDao,
                                                 ClinicalUnitDao clinicalUnitDao,
                                                 EvaluationDao evaluationDao,
                                                 HospitalDao hospitalDao,
                                                 HospitalEntranceDao hospitalEntranceDao,
                                                 NonClinicalUnitDao nonClinicalUnitDao,
                                                 QuestionDao questionDao,
                                                 SurgeryDepartmentDao surgeryDepartmentDao,
                                                 UnitCategoryDao unitCategoryDao,
                                                 UnitSubCategoryDao unitSubCategoryDao,
                                                 QuestionCategoryDao questionCategoryDao,
                                                 SettingDao settingDao,
                                                 HospitalCommentsDao hospitalCommentsDao
    ) {
        return new LocalRepositoryImpl(application, localDb, attachmentDao, buildingDao,
                candidateLocationDao,
                clinicalUnitDao,
                evaluationDao,
                hospitalDao,
                hospitalEntranceDao,
                nonClinicalUnitDao,
                questionDao,
                surgeryDepartmentDao,
                unitCategoryDao,
                unitSubCategoryDao,
                questionCategoryDao,
                settingDao,
                hospitalCommentsDao);
    }


    @Provides
    @Singleton
    public Retrofit provideRetrofit(Application context) {

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS).build();

        GsonBuilder gsonBuilder = new GsonBuilder();

        // Adding custom deserializers
        gsonBuilder.registerTypeAdapter(BindableBoolean.class, new BindableBooleanSerializer());
        gsonBuilder.registerTypeAdapter(BindableBoolean.class, new BindableBooleanDeserializer());
        gsonBuilder.registerTypeAdapter(BindableString.class, new BindableStringSerializer());
        gsonBuilder.registerTypeAdapter(BindableInt.class, new BindableIntSerializer());
        gsonBuilder.registerTypeAdapter(BindableDouble.class, new BindableDoubleSerializer());
        gsonBuilder.registerTypeAdapter(BindableString.class, new BindableStringDeserializer());
        gsonBuilder.registerTypeAdapter(BindableInt.class, new BindableIntDeserializer());
        gsonBuilder.registerTypeAdapter(BindableDouble.class, new BindableDoubleDeserializer());
        gsonBuilder.registerTypeAdapter(Polygon.class, new PolygonDeserializer());
        gsonBuilder.registerTypeAdapter(Polygon.class, new PolygonSerializer());
        gsonBuilder.registerTypeAdapter(Point.class, new PointDeserializer());
        gsonBuilder.registerTypeAdapter(Point.class, new PointSerializer());
        gsonBuilder.registerTypeAdapter(EnumUtil.EntityStatus.class, new EntityStatusSerializer());
        gsonBuilder.registerTypeAdapter(EnumUtil.EntityStatus.class, new EntityStatusDeserializer());
        gsonBuilder.registerTypeAdapter(EnumUtil.HealthLocationType.class, new HealthLocationTypeSerializer());
        gsonBuilder.registerTypeAdapter(EnumUtil.HealthLocationType.class, new HealthLocationTypeDeserializer());
        gsonBuilder.registerTypeAdapter(Boolean.class, new BooleanDeserializer());
        gsonBuilder.registerTypeAdapter(Boolean.class, new BooleanSerializer());

        gsonBuilder.addSerializationExclusionStrategy(new AnnotationExclusionStrategy());
        gsonBuilder.setLenient();
        Gson myGson = gsonBuilder.create();

        return new Retrofit.Builder().baseUrl(context.getString(R.string.baseUrl))
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(myGson))
                .build();

    }

    @Provides
    @Singleton
    public EntityService provideEntityService(Retrofit retrofit) {
        return retrofit.create(EntityService.class);
    }

    @Provides
    @Singleton
    public EntityApi provideEntityApi(EntityService entityService, Application application) {
        return new EntityApiImpl(entityService, application);
    }

    @Provides
    @Singleton
    public DataManager provideDataManager(EntityApi entityApi, LocalRepository localRepository, Application application, EntityService entityService) {
        return new DataManagerImpl(localRepository, entityApi, entityService, application);
    }

    @Singleton
    @Provides
    ViewModelProvider.Factory provideViewModelFactory(
            ViewModelSubComponent.Builder viewModelSubComponent) {

        return new ProjectViewModelFactory(viewModelSubComponent.build());
    }


}
