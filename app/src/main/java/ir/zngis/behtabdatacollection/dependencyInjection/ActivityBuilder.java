package ir.zngis.behtabdatacollection.dependencyInjection;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ir.zngis.behtabdatacollection.view.ui.LoginActivity;
import ir.zngis.behtabdatacollection.view.ui.MainActivity;
import ir.zngis.behtabdatacollection.view.ui.SplashActivity;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = FragmentBuilder.class)
    abstract MainActivity contributeSurveyActivity();


    @ContributesAndroidInjector(modules = FragmentBuilder.class)
    abstract LoginActivity contributeLoginActivity();

    @ContributesAndroidInjector(modules = FragmentBuilder.class)
    abstract SplashActivity contributeSplashActivity();
}
