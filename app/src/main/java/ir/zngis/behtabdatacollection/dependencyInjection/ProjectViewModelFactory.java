package ir.zngis.behtabdatacollection.dependencyInjection;


import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;

import java.util.Map;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import ir.zngis.behtabdatacollection.viewModel.AnswerVm;
import ir.zngis.behtabdatacollection.viewModel.AttachmentsVm;
import ir.zngis.behtabdatacollection.viewModel.BuildingVm;
import ir.zngis.behtabdatacollection.viewModel.HospitalDepVm;
import ir.zngis.behtabdatacollection.viewModel.HospitalEntranceVm;
import ir.zngis.behtabdatacollection.viewModel.UserVm;
import ir.zngis.behtabdatacollection.viewModel.MapVm;
import ir.zngis.behtabdatacollection.viewModel.QuestionVm;
import ir.zngis.behtabdatacollection.viewModel.SurveyActivityVm;

@Singleton
public class ProjectViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final ArrayMap<Class, Callable<? extends AndroidViewModel>> creators;

    @Inject
    public ProjectViewModelFactory(ViewModelSubComponent viewModelSubComponent) {
        this.creators = new ArrayMap<>();
        creators.put(SurveyActivityVm.class, viewModelSubComponent::surveyActivityVm);
        creators.put(HospitalDepVm.class, viewModelSubComponent::hospitalDepVm);
        creators.put(QuestionVm.class, viewModelSubComponent::questionVm);
        creators.put(MapVm.class,viewModelSubComponent::mapVm);
        creators.put(BuildingVm.class,viewModelSubComponent::buildingVm);
        creators.put(AnswerVm.class,viewModelSubComponent::answerVm);
        creators.put(AttachmentsVm.class,viewModelSubComponent::attachmentsVm);
        creators.put(UserVm.class,viewModelSubComponent::loginVm);
        creators.put(HospitalEntranceVm.class,viewModelSubComponent::hospitalEntranceVm);

    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        Callable<? extends AndroidViewModel> callable = creators.get(modelClass);
        if (callable == null) {
            for (Map.Entry<Class, Callable<? extends AndroidViewModel>> classCallableEntry : creators.entrySet()) {
                if (modelClass.isAssignableFrom(classCallableEntry.getKey())) {
                    callable = classCallableEntry.getValue();
                }
            }
        }
        if (callable == null) {
            throw new IllegalArgumentException("Unknown model class " + modelClass);
        }
        try {
            return (T) callable.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
