package ir.zngis.behtabdatacollection.dependencyInjection;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ir.zngis.behtabdatacollection.repository.dataModel.HospitalEntrance;
import ir.zngis.behtabdatacollection.view.ui.AnswerFragment;
import ir.zngis.behtabdatacollection.view.ui.BuildingFragment;
import ir.zngis.behtabdatacollection.view.ui.BuildingListFragment;
import ir.zngis.behtabdatacollection.view.ui.CandidateLocationFragment;
import ir.zngis.behtabdatacollection.view.ui.HospitalEntranceFragment;
import ir.zngis.behtabdatacollection.view.ui.QuestionFragment;
import ir.zngis.behtabdatacollection.view.ui.QuestionsFragment;
import ir.zngis.behtabdatacollection.view.ui.SimpleEvaluationListFragment;
import ir.zngis.behtabdatacollection.view.ui.AddableEvaluationListFragment;
import ir.zngis.behtabdatacollection.view.ui.HospitalDepartmentFragment;
import ir.zngis.behtabdatacollection.view.ui.MapFragment;
import ir.zngis.behtabdatacollection.view.ui.ModulesFragment;
import ir.zngis.behtabdatacollection.view.ui.GeneralModuleFragment;
import ir.zngis.behtabdatacollection.view.ui.HospitalInfoFragment;
import ir.zngis.behtabdatacollection.view.ui.HospitalDepartmentListFragment;
import ir.zngis.behtabdatacollection.view.ui.HospitalsContainerFragment;
import ir.zngis.behtabdatacollection.view.ui.AttachmentsFragment;
import ir.zngis.behtabdatacollection.view.ui.SearchQuestionsFragment;

@Module
public abstract class FragmentBuilder {

    @ContributesAndroidInjector
    abstract ModulesFragment contributeHospitalFragment();

    @ContributesAndroidInjector
    abstract HospitalsContainerFragment contributeHospitalsFragment();

    @ContributesAndroidInjector
    abstract HospitalInfoFragment contributeHospitalInfoFragment();

    @ContributesAndroidInjector
    abstract HospitalDepartmentListFragment contributeHospitalUnitsFragment();


    @ContributesAndroidInjector
    abstract GeneralModuleFragment providHospitalGeneralInfoFragment();


    @ContributesAndroidInjector
    abstract HospitalDepartmentFragment hospitalDepartmentFragment();

    @ContributesAndroidInjector
    abstract AddableEvaluationListFragment evaluateFragment();

    @ContributesAndroidInjector
    abstract MapFragment mapFragment();

    @ContributesAndroidInjector
    abstract BuildingListFragment buildingListFragment();

    @ContributesAndroidInjector
    abstract AnswerFragment answerFragment();

    @ContributesAndroidInjector
    abstract SearchQuestionsFragment searchQuestionsFragment();

    @ContributesAndroidInjector
    abstract BuildingFragment buildingFragment();

    @ContributesAndroidInjector
    abstract CandidateLocationFragment candidateLocationFragment();

    @ContributesAndroidInjector
    abstract AttachmentsFragment picFragment();

    @ContributesAndroidInjector
    abstract SimpleEvaluationListFragment hazardEvaluationListFragment();

    @ContributesAndroidInjector
    abstract QuestionsFragment questionsFragment();

    @ContributesAndroidInjector
    abstract QuestionFragment questionFragment();

    @ContributesAndroidInjector
    abstract HospitalEntranceFragment hospitalEntranceFragment();

}
