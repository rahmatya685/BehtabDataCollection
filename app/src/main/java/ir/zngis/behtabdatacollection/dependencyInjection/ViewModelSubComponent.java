package ir.zngis.behtabdatacollection.dependencyInjection;


import dagger.Subcomponent;
import ir.zngis.behtabdatacollection.repository.dataModel.HospitalEntrance;
import ir.zngis.behtabdatacollection.viewModel.AnswerVm;
import ir.zngis.behtabdatacollection.viewModel.AttachmentsVm;
import ir.zngis.behtabdatacollection.viewModel.BuildingVm;
import ir.zngis.behtabdatacollection.viewModel.HospitalDepVm;
import ir.zngis.behtabdatacollection.viewModel.HospitalEntranceVm;
import ir.zngis.behtabdatacollection.viewModel.UserVm;
import ir.zngis.behtabdatacollection.viewModel.MapVm;
import ir.zngis.behtabdatacollection.viewModel.QuestionVm;
import ir.zngis.behtabdatacollection.viewModel.SurveyActivityVm;

@Subcomponent
public interface ViewModelSubComponent {

    @Subcomponent.Builder
    interface Builder {
        ViewModelSubComponent build();
    }

    SurveyActivityVm surveyActivityVm();

    HospitalDepVm hospitalDepVm();

    QuestionVm questionVm();

    MapVm mapVm();

    BuildingVm buildingVm();

    AnswerVm answerVm();

    AttachmentsVm attachmentsVm();

    UserVm loginVm();

    HospitalEntranceVm hospitalEntranceVm();
}
