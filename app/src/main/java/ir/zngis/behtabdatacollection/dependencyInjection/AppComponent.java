package ir.zngis.behtabdatacollection.dependencyInjection;


import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjection;
import ir.zngis.behtabdatacollection.dependencyInjection.module.ServiceBuilderModule;
import ir.zngis.behtabdatacollection.util.BehtabApp;


@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class,
        ServiceBuilderModule.class
})
public interface AppComponent {


    @Component.Builder
    interface Builder{
        @BindsInstance Builder application(Application application);
        AppComponent build();

    }
    void inject(BehtabApp applicationInit);
}
