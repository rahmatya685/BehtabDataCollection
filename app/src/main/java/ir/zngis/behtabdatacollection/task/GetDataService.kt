package ir.zngis.behtabdatacollection.task

import android.annotation.SuppressLint
import android.app.IntentService
import android.content.Intent
import android.os.Bundle
import android.os.ResultReceiver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ir.zngis.behtabdatacollection.repository.DataManager
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.PreferencesUtil
import javax.inject.Inject

class GetDataService : IntentService(GetDataService::class.java.simpleName) {

    @Inject
    lateinit var dataManager: DataManager

    var resultReceiver: ResultReceiver? = null


    @SuppressLint("CheckResult")
    override fun onHandleIntent(intent: Intent?) {

        intent?.let { intent ->
            resultReceiver = intent.getParcelableExtra(Constants.GET_DATA_RECIVER_KEY)
        }

//         dataManager.getDataFromRemote( ).observeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
//                { t ->
//                     resultReceiver?.send(t, null)
//                },
//                { t: Throwable ->
//                    var bundle=Bundle()
//                    bundle.putString(Constants.ERROR,t.message)
//                    resultReceiver?.send(Constants.ERROR_CODE,bundle )
//                }
//        )
    }


}