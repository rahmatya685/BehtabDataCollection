package ir.zngis.behtabdatacollection.task

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.IBinder
import dagger.android.AndroidInjection
import ir.zngis.behtabdatacollection.repository.DataManager
import javax.inject.Inject
import android.widget.Chronometer
import ir.zngis.behtabdatacollection.R
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import android.util.AndroidException
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ir.zngis.behtabdatacollection.util.*
import java.lang.Exception


class SyncDataService : Service() {


    var mNotificationBuilder: NotificationCompat.Builder? = null


    private var mChronometer: Chronometer? = null



    @Inject
    lateinit var dataManager: DataManager

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (null == intent || null == intent.action) {
            return Service.START_STICKY
        } else {
            when (intent.action) {
                Constants.ACTION_START_SYNC_DATA_SERVICE -> {

                    mChronometer = Chronometer(this)

                    mChronometer?.start()

                    showNotification(getString(R.string.msg_progress_sending_info_server))

                    dataManager.syncData()?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
                            ?.subscribe({
                                it?.let {
                                    if (it.contentEquals(Constants.SYNC_ATTACHMENTS_STATUS_OK)) {

                                        val msg = getString(R.string.msg_info_success_sending_attachments_to_server)

                                        ToastUtil.ShowToast(this, msg, EnumUtil.ToastActionState.Info, Toast.LENGTH_LONG)
                                        showFinishSyncByNotification(msg)

                                    } else if (it.contentEquals(Constants.SYNC_STATUS_OK)) {
                                        val msg = getString(R.string.msg_info_success_sending_info_to_server)

                                        ToastUtil.ShowToast(this, msg, EnumUtil.ToastActionState.Success, Toast.LENGTH_LONG)
                                        showFinishSyncByNotification(msg)
                                        showNotification(getString(R.string.msg_progress_sending_info_server))

                                        startAttachmentsUpload()

                                    } else if (it.contains("User not found")) {

                                        val msg = getString(R.string.error_msg_user_profile_not_found)

                                        ToastUtil.ShowToast(this, msg, EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                                        showErrorByNotification(getString(R.string.app_name), msg)
                                    } else {


                                        showErrorByNotification(getString(R.string.app_name), it)
                                        DialogUtil.showInfo(this, it)

                                        stopSelf()
                                    }
                                }
                            }, {
                                it.message?.let {
                                    showErrorByNotification(getString(R.string.msg_error_sending_info_2_server), it)
                                }
                                stopSelf()
                            })

                }
                // Constants.ACTION_STOP_SYNC_DATA_SERVICE -> sendData()
            }
            return Service.START_STICKY

        }
    }

    @SuppressLint("CheckResult")
    private fun startAttachmentsUpload() {

        var notification : Notification? = null
        dataManager.syncInsertedAttachments().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { notification= showNotification(getString(R.string.msg_info_uploading_attachments)) }
                .subscribe(
                        { t ->
                            when (t.first) {
                                Constants.SYNC_ATTACHMENTS_STATUS_OK -> {
                                    val msg = getString(R.string.msg_info_success_sending_attachments_to_server)

                                    ToastUtil.ShowToast(this, msg, EnumUtil.ToastActionState.Info, Toast.LENGTH_LONG)
                                    showFinishSyncByNotification(msg)
                                    stopSelf()
                                }
                                Constants.SYNC_STATUS_PROGRESS -> {
                                    showNotification(t.second)
                                }
                                Constants.NO_ATTACHMENT_FOUND_FOR_UPLOAD->{
                                    showFinishSyncByNotification(getString(R.string.msg_info_no_attachment_found_for_uplaod))
                                    stopSelf()
                                }
                                else -> {
                                    showErrorByNotification(t.first,t.second)
                                    stopSelf()
                                }
                            }
                        },
                        { t: Throwable? ->
                            showErrorByNotification(getString(R.string.msg_error_sending_info_2_server), t.toString())
                            stopSelf()
                        }

                )


    }


    override fun onDestroy() {
        super.onDestroy()
        mChronometer?.stop()

        mChronometer = null

    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }


    private fun showNotification(title: String):Notification {
//        val notificationIntent = Intent(this, MainActivity::class.java)
//        notificationIntent.action = Constants.GeodbDownload.START_MAIN_ACTIVITY_ACTION
//        notificationIntent.putExtra(Constants.GeodbDownload.MapServiceName,mapServiceName)

//        notificationIntent.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT or Intent.FLAG_ACTIVITY_CLEAR_TASK
//        pendingIntent = PendingIntent.getActivity(this, 0,
//                notificationIntent, 0)


        val channelId =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    createNotificationChannel("my_service", "My Background Service")
                } else {
                    // If earlier version channel ID is not used
                    // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                    ""
                }

        mNotificationBuilder = NotificationCompat.Builder(this, channelId)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(title)
                .setSmallIcon(R.drawable.ic_cloud_upload_black_24dp)
//                .setContentIntent(pendingIntent)
                .setOngoing(true)
        mNotificationBuilder?.setLargeIcon(GeneralUtil.drawableToBitmap(ContextCompat.getDrawable(this, R.mipmap.ic_launcher_round)!!))

        var notification = mNotificationBuilder?.build()
        startForeground(Constants.FOREGROUND_SERVICE, notification)
        return notification!!
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_HIGH)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    private fun showErrorByNotification(title: String, error: String) {
        val channelId =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    createNotificationChannel("my_service", "My Background Service")
                } else {
                    // If earlier version channel ID is not used
                    // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                    ""
                }

        val mBuilder = NotificationCompat.Builder(this, channelId)
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.drawable.ic_error_black_24dp)
                .setContentTitle(title)
                .setContentText(error)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setAutoCancel(true)

        mBuilder!!.setLargeIcon(GeneralUtil.drawableToBitmap(ContextCompat.getDrawable(this, R.mipmap.ic_launcher_round)!!))


        val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        //mBuilder.setSound(alarmSound)
        //mBuilder.setVibrate(longArrayOf(500, 1000, 500, 1000))
        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // notificationID allows you to update the notification later on. //
        mNotificationManager.notify(Constants.FINISH_NOTIFICATION_ID, mBuilder.build())


        try {
            Crashlytics.logException(Throwable(title + "\n" + error));

        } catch (e: Exception) {
        }
    }

    fun showFinishSyncByNotification(finishText: String) {

        val channelId =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    createNotificationChannel("my_service", "My Background Service")
                } else {
                    // If earlier version channel ID is not used
                    // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                    ""
                }

        val mBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_cloud_done_black_24dp)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(finishText)
                .setAutoCancel(true)
        mBuilder!!.setLargeIcon(GeneralUtil.drawableToBitmap(ContextCompat.getDrawable(this, R.mipmap.ic_launcher_round)!!))


        val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        //mBuilder.setSound(alarmSound)
        // mBuilder.setVibrate(longArrayOf(1000, 1000))

        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // notificationID allows you to update the notification later on. //
        mNotificationManager.notify(Constants.FINISH_NOTIFICATION_ID, mBuilder.build())
    }

    companion object {

        const val NOTIFICAION_ID = 54

    }
}