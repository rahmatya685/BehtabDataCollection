package ir.zngis.behtabdatacollection.task

import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver

class GetDataServiceReceiver(val handler: Handler,var callback:Callback) : ResultReceiver(handler) {
    

    
    override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
        super.onReceiveResult(resultCode, resultData)
        callback.onReceiveResult(resultCode, resultData)
    }
    
    
    interface Callback{
        fun  onReceiveResult(resultCode: Int, resultData: Bundle?)
    }


}