package ir.zngis.behtabdatacollection.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ir.zngis.behtabdatacollection.repository.dataModel.BaseClass
import ir.zngis.behtabdatacollection.repository.dataModel.UnitCategoryWithSub
import ir.zngis.behtabdatacollection.repository.DataManager

abstract class BaseVm (var localRepo: DataManager, application: Application): AndroidViewModel(application) {

    private var categories: LiveData<List<UnitCategoryWithSub>> = MutableLiveData()


    init {
        categories = localRepo.getUnitCategories()
    }


    fun updateEntity(hospital: BaseClass) {
        localRepo.updateEntity(hospital)

    }
    fun insertEntity(hospital: BaseClass) {
        localRepo.insertEntity(hospital)

    }
    fun deleteEntity(hospital: BaseClass) {
        localRepo.deleteEntity(hospital)

    }

    fun getUnitCategories(): LiveData<List<UnitCategoryWithSub>> {
        return categories
    }

}