package ir.zngis.behtabdatacollection.viewModel

import android.app.Application
import com.google.android.gms.maps.model.Polygon
import com.google.android.gms.maps.model.Polyline
import ir.zngis.behtabdatacollection.repository.DataManager
import ir.zngis.behtabdatacollection.util.Constants
import javax.inject.Inject

class MapVm @Inject constructor(
        localRepo: DataManager, application: Application
) : BaseVm(localRepo, application) {

    var mHospitalBoudry: Polygon? = null


    fun shouldControlLocation(): Boolean {
        val setting = localRepo.getSetting(Constants.KEY_SETTING_SHOULD_CONTROL_LOCATION)

        return if (setting == null) {
            false
        } else {
            setting.value == "true"
        }

    }


    fun getDistanceToFeature(): Double {

        val setting = localRepo.getSetting(Constants.KEY_SETTING_USER_DISTANCE_TO_FEATURE)
        return setting?.value?.toDouble() ?: 500.0
    }

    fun setHospitalGeom(geom: Polygon) {
        this.mHospitalBoudry = geom
    }

    fun getHospitalBoudry(): Polygon? {
        return mHospitalBoudry
    }


}