package ir.zngis.behtabdatacollection.viewModel

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ir.zngis.behtabdatacollection.repository.dataModel.Hospital
import ir.zngis.behtabdatacollection.repository.DataManager
import javax.inject.Inject

class SurveyActivityVm @Inject constructor(
        localRepo: DataManager, application: Application
) : BaseVm(localRepo, application) {

    private var hospitals: MutableLiveData<Array<Hospital>> = MutableLiveData()

    var app: Application = application


    @SuppressLint("CheckResult")
    fun getHospitals(): MutableLiveData<Array<Hospital>>? {
        localRepo.getHospitals().subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ t ->
            hospitals.value = t
        }, { t: Throwable ->
            t.printStackTrace()
        })
        return hospitals
    }
}