package ir.zngis.behtabdatacollection.viewModel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ir.zngis.behtabdatacollection.repository.dataModel.BaseClass
import ir.zngis.behtabdatacollection.repository.dataModel.Building
import ir.zngis.behtabdatacollection.repository.DataManager
import javax.inject.Inject

class HospitalDepVm @Inject constructor(
        localRepo: DataManager, application: Application
) : BaseVm(localRepo, application) {


    private var units: MutableLiveData<  List<BaseClass>> = MutableLiveData()

//    init {
//        localRepo.getUnits().subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ t ->
//            units.value = t
//        }, { t: Throwable ->
//
//        })
//
//    }

    fun getUnits( hospitalId:Int): MutableLiveData<List<BaseClass>> {
        localRepo.getUnits(hospitalId).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ t ->
            units.value = t
        }, { t: Throwable ->

        })
        return units
    }

    fun getBuildings(hospitalId:Int):List<Building>{
        return localRepo.getBuildings(hospitalId)
    }

    fun getBuilding(id:String):Building?{
        return localRepo.findBuilding(id)
    }





}