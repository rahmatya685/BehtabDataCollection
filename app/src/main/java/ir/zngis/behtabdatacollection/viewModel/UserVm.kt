package ir.zngis.behtabdatacollection.viewModel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ir.zngis.behtabdatacollection.repository.DataManager
import ir.zngis.behtabdatacollection.repository.dataModel.Hospital
import ir.zngis.behtabdatacollection.repository.dataModel.User
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.view.callback.DataWrapper
import retrofit2.Response
import javax.inject.Inject

class UserVm @Inject constructor(
        val repository: DataManager, application: Application
) : BaseVm(repository, application) {

    private val subscriptions = CompositeDisposable()


    private var user: MutableLiveData<DataWrapper<User>> = MutableLiveData()

    private var loggedOutStatus: MutableLiveData<DataWrapper<Boolean>> = MutableLiveData()



    fun login(userName: String, password: String, mImei: String): MutableLiveData<DataWrapper<User>> {
        user.value =null
        val subscription = repository.login(userName, password,mImei).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { t ->
                    Constants.ERROR_SERVER_IS_NOT_AVAILABLE
                    user.value = DataWrapper(null, t)
                },
                { t: Throwable ->
                    user.value = DataWrapper(t, null)
            }
        )
        subscriptions.add(subscription)
        return user
    }

    fun getUserInfo(): MutableLiveData<DataWrapper<User>> {
        val subscription = repository.getUserInfo().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { t ->
                    user.value = DataWrapper(null, t)
                }, { t: Throwable ->
            user.value = DataWrapper(t, null)
        }
        )
        subscriptions.add(subscription)

        return user
    }


    override fun onCleared() {
        super.onCleared()
        subscriptions.clear()
    }

    fun logOut(mImei: String): MutableLiveData<DataWrapper<Boolean>> {
        val subscription = repository.logout(mImei).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { t ->
                    loggedOutStatus.value = DataWrapper(null, t)
                }, { t: Throwable ->
            loggedOutStatus.value = DataWrapper(t, null)
        }
        )
        subscriptions.add(subscription)

        return loggedOutStatus
    }

    fun getRemoteData(): Observable<Response<List<Hospital>>>? {
        return localRepo.getDataFromRemote()
    }
}