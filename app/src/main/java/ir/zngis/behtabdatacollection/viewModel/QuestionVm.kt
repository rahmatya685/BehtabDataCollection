package ir.zngis.behtabdatacollection.viewModel

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ir.zngis.behtabdatacollection.repository.DataManager
import ir.zngis.behtabdatacollection.repository.dataModel.*
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.GeneralUtil
import javax.inject.Inject

class QuestionVm @Inject constructor(localRepo: DataManager, application: Application) : BaseVm(localRepo, application) {


    private var questions: MutableLiveData<List<QuestionCategoryWithQuestion>> = MutableLiveData()

    private var evaluations: MutableLiveData<List<Any>> = MutableLiveData()

    private var questionsWithEvaluation4Hazard: MutableLiveData<List<Any>> = MutableLiveData()


    private var questionsWithCategoryAndEvaluations: MutableLiveData<List<Question>> = MutableLiveData()


    @SuppressLint("CheckResult")
    fun getQuestionsWithCategoryAndEvaluations(moduleName: String, building: Building?, unit: Any?): MutableLiveData<List<Question>> {
        localRepo.getQuestionsWithCategoryAndEvaluations(moduleName, building, unit).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { t: List<Question>? ->
                            questionsWithCategoryAndEvaluations.postValue(t)
                        },
                        { t: Throwable? ->

                        })

        return questionsWithCategoryAndEvaluations
    }


    @SuppressLint("CheckResult")
    fun getQuestionsWithCategoriesAsFlatList(moduleName: String, building: Building?, unit: Any?, currentLanguage: String): MutableLiveData<List<QuestionCategoryWithQuestion>> {

        val modoulNum = GeneralUtil.getModuleNumber(moduleName)


        localRepo.getQuestionCategoryWithQuestionAndEvaluations(moduleName, building, unit).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ t ->

            var counterCat = 0

            t.forEach {

                when (currentLanguage) {
                    Constants.PERSIAN -> {
                        it.questionCategory.number = "${counterCat + 1}-$modoulNum"
                    }
                    Constants.ENGLISH -> {
                        it.questionCategory.number = "$modoulNum-${counterCat + 1}"
                    }
                }

                var counterQuestion = 0

                it.questions.forEach {

                    when (currentLanguage) {
                        Constants.PERSIAN -> {
                            it.number = "${counterQuestion + 1}-${counterCat + 1}-$modoulNum"
                        }
                        Constants.ENGLISH -> {
                            it.number = "$modoulNum-${counterCat + 1}-${counterQuestion + 1}"
                        }
                    }

                    counterQuestion++
                }
                counterCat++
            }
            questions.value = t

        }, { t: Throwable ->

        })

        return questions
    }

    fun getUnitEvaluations(moduleName: String, unit: Any): MutableLiveData<List<Any>> {

        when (unit) {
            is SurgeryDepartment -> {
                localRepo.getEvaluations4SurgeryDepartment(moduleName, unit.rowGuid).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ t ->
                    evaluations.value = t
                }, { t: Throwable ->

                })
            }
            is NonClinicalUnit -> {
                localRepo.getEvaluations4NonClinicalUnit(moduleName, unit.rowGuid).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ t ->
                    evaluations.value = t
                }, { t: Throwable ->

                })
            }
            is ClinicalUnit -> {
                localRepo.getEvaluations4ClinicalUnit(moduleName, unit.rowGuid).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ t ->
                    evaluations.value = t
                }, { t: Throwable ->

                })
            }
        }


        return evaluations
    }

    @SuppressLint("CheckResult")
    fun getBuildingEvaluations(moduleName: String, buildingId: String): MutableLiveData<List<Any>> {

        localRepo.getEvaluations4Building(moduleName, buildingId).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ t ->
            evaluations.value = t
        }, { t: Throwable ->

        })

        return evaluations
    }

    @SuppressLint("CheckResult")
    fun getQuestionsWithEvaluation4Hazard(buildingId: String, moduleName: String): MutableLiveData<List<Any>> {

        localRepo.getQuestionsWithEvaluation4Hazard(buildingId, moduleName).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: List<Any> ->
                    questionsWithEvaluation4Hazard.value = t

                }, { t: Throwable -> })


        return questionsWithEvaluation4Hazard
    }


}