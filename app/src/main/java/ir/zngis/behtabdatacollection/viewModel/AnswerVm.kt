package ir.zngis.behtabdatacollection.viewModel

import android.app.Application
import ir.zngis.behtabdatacollection.repository.DataManager
import ir.zngis.behtabdatacollection.repository.dataModel.Evaluation
import javax.inject.Inject

class AnswerVm @Inject constructor(
        localRepo: DataManager, application: Application
) : BaseVm(localRepo, application) {

    fun evaluationExists(evaluation: Evaluation):Boolean{
        return localRepo.evaluationExists(evaluation)
    }

    fun deleteAttachments4Feature(rowGuid: String) {
        localRepo.deleteAttachments4Feature(rowGuid)

    }
}