package ir.zngis.behtabdatacollection.viewModel

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ir.zngis.behtabdatacollection.repository.dataModel.Attachment
import ir.zngis.behtabdatacollection.repository.DataManager
import ir.zngis.behtabdatacollection.util.EnumUtil
import java.io.File
import javax.inject.Inject

class AttachmentsVm @Inject constructor(
        localRepo: DataManager, application: Application
) : BaseVm(localRepo, application) {

    private var attachments: MutableLiveData<List<Attachment>> = MutableLiveData()


    fun deleteAttachments(deleteQueue: MutableList<Attachment>) {
         deleteQueue.forEach { localRepo.setDeleteStatus(it) }
    }

    fun addAttachments(files: MutableList<File>, category: String, linkedId: String) {

        files.forEach { file ->

            val attachment = Attachment.Creator().setFile(file).setCategoty(category).setLinkId(linkedId).build()

            localRepo.insertEntity(attachment)

        }


    }

    fun updateAttachment(attachment: Attachment){
        localRepo.updateEntity(attachment)
    }


    @SuppressLint("CheckResult")
    fun getAttachments(category: String, linkedId: String): MutableLiveData<List<Attachment>> {
        localRepo.getAttachments(category, linkedId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { t: List<Attachment> ->
                    attachments.value = t
                },
                { t ->

                })

        return attachments
    }
}