package ir.zngis.behtabdatacollection.viewModel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ir.zngis.behtabdatacollection.repository.dataModel.BuildingWithUnits
import ir.zngis.behtabdatacollection.repository.DataManager
import javax.inject.Inject

class BuildingVm @Inject constructor(
        localRepo: DataManager, application: Application
) : BaseVm(localRepo, application) {

    private var buildingWithEvaluations: MutableLiveData<List<BuildingWithUnits>> = MutableLiveData()


    fun getBuildings(hospitalId: Int,module:String): MutableLiveData<List<BuildingWithUnits>> {
        localRepo.getBuildingsFlowable(hospitalId,module)?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe({ t ->
            buildingWithEvaluations.value = t
        }, { t: Throwable ->
        })

        return buildingWithEvaluations
    }


}