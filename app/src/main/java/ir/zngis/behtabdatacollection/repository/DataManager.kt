package ir.zngis.behtabdatacollection.repository

import androidx.lifecycle.LiveData
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import ir.zngis.behtabdatacollection.repository.dataModel.*
import retrofit2.Response
import kotlin.Pair

interface DataManager {

    fun getHospitals( ): Flowable<Array<Hospital>>
//    fun getHospitalsWitComments( ): Flowable<Array<HospitalWithComment>>

    fun getHospitalCommentByHospital(hospitalId: Int): Single<List<HospitalComments>>


    fun getDataFromRemote( ):Observable<Response<List<Hospital>>>?
    fun syncData(): Observable<String?>?
     fun syncInsertedAttachments(): Observable<Pair<String,String>>

    fun getUnitCategories(): LiveData<List<UnitCategoryWithSub>>
    fun updateEntity(baseClass: BaseClass)
    fun insertEntity(baseClass: BaseClass)
    fun deleteEntity(baseClass: BaseClass)
    fun deleteEntityPermanently(baseClass: BaseClass)
    fun setDeleteStatus(baseClass: BaseClass)

    fun getUnits(hospitalId: Int): Flowable<List<BaseClass>>
    fun getQuestionCategoryWithQuestionAndEvaluations(moduleName: String, building: Building?, unit: Any?): Flowable<List<QuestionCategoryWithQuestion>>
    fun getQuestion(questionId: Int): Question


    fun getBuildings(hospitalId: Int): List<Building>
    fun getCandiadateLocations(hospitalId: Int): List<CandidateLocation>
    fun getHospitalEntrances(hospitalId: Int): List<HospitalEntrance>

    fun getLastBuilding(): Building?
    fun getLastCandidateLocation(): CandidateLocation?
    fun getLastHospitalEntrance(): HospitalEntrance?
    fun getLastEvaluation(): Evaluation?

    fun findHospitalEntrance(id: Int): HospitalEntrance?
    fun findCandidateLocation(id: Int): CandidateLocation?
    fun findBuilding(id: Int): Building?
    fun findHospitalById(id:Int):Hospital?

    fun getEvaluations4SurgeryDepartment(moduleName: String, unitId: String):  Flowable<List<Any>>
    fun getEvaluations4NonClinicalUnit(moduleName: String, unitId: String):  Flowable<List<Any>>
    fun getEvaluations4ClinicalUnit(moduleName: String, unitId: String):  Flowable<List<Any>>

    fun getBuildingsFlowable(hospitalId: Int, moduleName: String): Flowable<List<BuildingWithUnits>>?
    fun getAttachments(category: String, linkedId: String):Flowable<List<Attachment>>

    fun getQuestionsWithEvaluation4Hazard(buildingId: String, moduleName: String): Flowable<List<Any>>
    fun getEvaluations4Building(moduleName: String, buildingId: String): Flowable<List<Any>>


    fun login(userName: String, password: String, mImei: String):Single<User>
    fun logout( mImei: String):Single<Boolean>

    fun getUserInfo():Single<User>
    fun evaluationExists(evaluation: Evaluation): Boolean
    fun findBuilding(id: String): Building?
    fun deleteAttachments4Feature(rowGuid: String)
    fun getQuestionsWithCategoryAndEvaluations(moduleName: String, building: Building?, unit: Any?): Flowable<List<Question>>
    fun getAllSettings(): List<Setting>

    fun getSetting(key:String):Setting?
}