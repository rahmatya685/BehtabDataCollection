package ir.zngis.behtabdatacollection.repository.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.edsab.pm.dao.BaseDao
import io.reactivex.Single
import ir.zngis.behtabdatacollection.repository.dataModel.HospitalComments


@Dao
interface HospitalCommentsDao:BaseDao<HospitalComments> {

    @Query("select * from HospitalComments where HOSPITAL_ID = :hospitalId")
    fun getCommentByHospital(hospitalId:Int):Single<List<HospitalComments>>


    @Query("select * from HospitalComments")
    fun getAllComments():Single<List<HospitalComments>>
}