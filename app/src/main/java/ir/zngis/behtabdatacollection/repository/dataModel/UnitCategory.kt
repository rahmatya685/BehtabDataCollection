package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore

@Entity(tableName = "UNIT_CATEGORY")
class UnitCategory(
        @ColumnInfo(name = "TITLE_EN")
        var titleEn: String = "",
        @ColumnInfo(name = "TITLE_FA")
        var titleFa: String = "",
        @ColumnInfo(name = "TYPE")
        var type: String = ""
) : BaseClass() {


        @Ignore
        var unitSubCategories:List<UnitSubCategory> = mutableListOf()

}