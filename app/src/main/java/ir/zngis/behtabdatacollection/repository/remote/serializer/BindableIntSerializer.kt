package ir.zngis.behtabdatacollection.repository.remote.serializer

import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import ir.zngis.behtabdatacollection.baseBinding.BindableInt
import java.lang.reflect.Type

class BindableIntSerializer:JsonSerializer<BindableInt> {
    override fun serialize(src: BindableInt?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        var element= JsonPrimitive(-1)
        src?.let {
            element = JsonPrimitive(it.value)
        }
        return element
    }
}