package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import ir.zngis.behtabdatacollection.baseBinding.BindableString


@Entity(tableName = "QUESTION",

        foreignKeys = [
            ForeignKey(entity = QuestionCategory::class, parentColumns = ["ID"], childColumns = ["CATEGORY_ID"])
        ])
class Question(
        @ColumnInfo(name = "CATEGORY_ID")
        var categoryId: Int = -1,
        @ColumnInfo(name = "QUESTION_TITLE")
        var questionTitleFa: String = "",
        @ColumnInfo(name = "QUESTION_TITLE_ENG")
        var questionTitleEn: String = "",
        @ColumnInfo(name = "HELP_LINK")
        var helpLink: String = "",
        @ColumnInfo(name = "WEIGHT", typeAffinity = ColumnInfo.REAL)
        var weight: Double? = null,
        @ColumnInfo(name = "BRIEF_DESCRIPTION")
        var briefDescriptionEn: String? = null,
        @ColumnInfo(name = "BRIEF_DESCRIPTION_ENG")
        var briefDescriptionFa: String? = null,

        @ColumnInfo(name = "HOSPITAL")
        var used4Hospital: Boolean = true,

        @ColumnInfo(name = "HEALTH_HOUSE")
        var used4HealthHouse: Boolean = false

) : BaseClass() {

    @Ignore
    var questionCategory: QuestionCategory = QuestionCategory()


    @Ignore
    var evaluations: MutableList<Evaluation> = mutableListOf()

    @Ignore
    var number: String? = ""
}