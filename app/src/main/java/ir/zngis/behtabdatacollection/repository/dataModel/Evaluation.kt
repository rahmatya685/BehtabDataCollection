package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.*
import com.google.gson.Gson
import ir.zngis.behtabdatacollection.baseBinding.BindableBoolean
import ir.zngis.behtabdatacollection.baseBinding.BindableInt
import ir.zngis.behtabdatacollection.baseBinding.BindableString
import ir.zngis.behtabdatacollection.util.GeneralUtil

@Entity(tableName = "EVALUATION", foreignKeys = [
    ForeignKey(entity = Question::class, parentColumns = ["ID"], childColumns = ["QUESTION_ID"]),
    ForeignKey(entity = SurgeryDepartment::class, parentColumns = ["ROW_GUID"], childColumns = ["SURGERY_DEPARTMENT_ID"], onDelete = ForeignKey.CASCADE, onUpdate = ForeignKey.NO_ACTION),
    ForeignKey(entity = NonClinicalUnit::class, parentColumns = ["ROW_GUID"], childColumns = ["NON_CLINICAL_UNIT_ID"], onDelete = ForeignKey.CASCADE, onUpdate = ForeignKey.NO_ACTION),
    ForeignKey(entity = ClinicalUnit::class, parentColumns = ["ROW_GUID"], childColumns = ["CLINICAL_UNIT_ID"], onDelete = ForeignKey.CASCADE, onUpdate = ForeignKey.NO_ACTION),
    ForeignKey(entity = Building::class, parentColumns = ["ROW_GUID"], childColumns = ["BUILDING_ID"], onDelete = ForeignKey.CASCADE, onUpdate = ForeignKey.NO_ACTION)
])
class Evaluation(
        @ColumnInfo(name = "HAS_OBSERVED", typeAffinity = ColumnInfo.INTEGER)
        var hasObserved: BindableBoolean? = BindableBoolean(false),
        @ColumnInfo(name = "RISK_LEVEL", typeAffinity = ColumnInfo.INTEGER)
        var riskLevel: Int? = null,
        @ColumnInfo(name = "COMMENT")
        var comment: BindableString? = BindableString(""),
        @ColumnInfo(name = "SURGERY_DEPARTMENT_ID", typeAffinity = ColumnInfo.TEXT)
        var surgeryDepartmentId: String? = null,
        @ColumnInfo(name = "NON_CLINICAL_UNIT_ID", typeAffinity = ColumnInfo.TEXT)
        var nonClinicalUnitId: String? = null,
        @ColumnInfo(name = "CLINICAL_UNIT_ID", typeAffinity = ColumnInfo.TEXT)
        var clinicalUnitId: String? = null,
        @ColumnInfo(name = "QUESTION_ID")
        var questionId: Int = -1,
        @ColumnInfo(name = "BUILDING_ID", typeAffinity = ColumnInfo.TEXT)
        var buildingId: String? = null,
        @ColumnInfo(name = "ROW_GUID", typeAffinity = ColumnInfo.TEXT)
        var rowGuid: String = GeneralUtil.generateGuid()

) : BaseClass(),Cloneable {


    @Ignore
    var buildingByBuildingId: Building? = null


    @Ignore
    var nonClinicalUnitByNonClinicalUnitId: NonClinicalUnit? = null

    @Ignore
    var clinicalUnitByClinicalUnitId: ClinicalUnit? = null

    @Ignore
    var surgeryDepartmentBySurgeryDepartmentId: SurgeryDepartment? = null


    @Ignore
    var questionByQuestionId: Question? = null


    public override fun clone(): Evaluation {
        var cloned =  super.clone() as Evaluation

        cloned.comment = this.comment

        cloned.riskLevel = this.riskLevel

        cloned.hasObserved = this.hasObserved


        return cloned
    }


    override fun toString(): String {
        return Gson().toJson(this)
    }

}