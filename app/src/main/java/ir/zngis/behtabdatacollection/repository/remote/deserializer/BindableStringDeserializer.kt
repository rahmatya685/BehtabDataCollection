package ir.zngis.behtabdatacollection.repository.remote.deserializer

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import ir.zngis.behtabdatacollection.baseBinding.BindableString
import java.lang.reflect.Type

class BindableStringDeserializer : JsonDeserializer<BindableString> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): BindableString {
        var retVal = BindableString("")

        json?.let {
            retVal = BindableString(json.asString)
        }

        return retVal
    }
}