package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import android.os.Parcelable
import ir.zngis.behtabdatacollection.repository.remote.Exclude
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.util.GeneralUtil
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
open class BaseClass : Parcelable {
    @ColumnInfo(name = "ID")
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0


    @ColumnInfo(name = "STATUS", typeAffinity = ColumnInfo.INTEGER)
    var status: EnumUtil.EntityStatus? = null

    @ColumnInfo(name = "MODIFY_DATE", typeAffinity = ColumnInfo.TEXT)
    @Exclude
    var modifyDate:Date? = GeneralUtil.Now()
}