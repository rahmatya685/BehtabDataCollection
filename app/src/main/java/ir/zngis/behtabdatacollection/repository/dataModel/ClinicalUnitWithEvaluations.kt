package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Relation

@Entity
class ClinicalUnitWithEvaluations(
        @Embedded var clinicalUnit: ClinicalUnit    = ClinicalUnit(),
        @Relation(
                parentColumn = "ID",
                entityColumn = "CLINICAL_UNIT_ID"
        ) var evaluations: MutableList<Evaluation> = mutableListOf()
) {

}