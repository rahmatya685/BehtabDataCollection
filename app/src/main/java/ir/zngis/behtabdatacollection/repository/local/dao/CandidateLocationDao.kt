package ir.zngis.behtabdatacollection.repository.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.edsab.pm.dao.BaseDao
import ir.zngis.behtabdatacollection.repository.dataModel.CandidateLocation

@Dao
interface CandidateLocationDao :BaseDao<CandidateLocation> {


    @Query("select * from CANDIDATE_LOCATION  where STATUS != 1 order by ID desc limit 1")
    fun getLastCandidateLocation(): CandidateLocation?

    @Query("SELECT * FROM CANDIDATE_LOCATION  WHERE HOSPITAL_ID  = :hospitalId")
    fun getCandiadateLocations(hospitalId: Int): List<CandidateLocation>


    @Query("SELECT * FROM CANDIDATE_LOCATION")
    fun getCandiadateLocations(): List<CandidateLocation>

    @Query("SELECT * FROM CANDIDATE_LOCATION  WHERE ID = :id")
    fun findCandidateLocation(id:Int):CandidateLocation?

}