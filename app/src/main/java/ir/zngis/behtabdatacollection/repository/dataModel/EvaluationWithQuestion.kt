package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class EvaluationWithQuestion() : Parcelable {
    @Embedded
    var evaluation: Evaluation? = null


    @ColumnInfo(name = "CATEGORY_ID")
    var categoryId: Int = -1
    @ColumnInfo(name = "QUESTION_TITLE")
    var questionTitleFa: String = ""
    @ColumnInfo(name = "QUESTION_TITLE_ENG")
    var questionTitleEn: String = ""

    @ColumnInfo(name = "HELP_LINK")
    var helpLink: String = ""
    @ColumnInfo(name = "WEIGHT", typeAffinity = ColumnInfo.REAL)
    var weight: Double? = null
    @ColumnInfo(name = "BRIEF_DESCRIPTION")
    var briefDescriptionEn: String? = null
    @ColumnInfo(name = "BRIEF_DESCRIPTION_ENG")
    var briefDescriptionFa: String? = null

    @ColumnInfo(name = "Q_ID")
    var questionId: Int = -1

    @Ignore
    var rowNum: Int = -1
}