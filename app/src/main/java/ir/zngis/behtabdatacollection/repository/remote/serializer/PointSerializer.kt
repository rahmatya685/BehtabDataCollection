package ir.zngis.behtabdatacollection.repository.remote.serializer

import com.google.gson.*
import ir.zngis.geojson.Point
import java.lang.reflect.Type

class PointSerializer : JsonSerializer<Point> {

    override fun serialize(src: Point?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement? {
        var element: JsonObject? = null

        src?.let {
            val json = it.toJSON()

            val parser = JsonParser()
            element = parser.parse(json.toString()).asJsonObject

        }
        return element
    }
}