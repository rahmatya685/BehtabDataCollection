package ir.zngis.behtabdatacollection.repository.remote


import io.reactivex.Observable
import io.reactivex.Single
import ir.zngis.behtabdatacollection.repository.dataModel.*
import ir.zngis.behtabdatacollection.util.Constants
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*
import kotlin.Pair

interface EntityService {


    @GET("users/deviceLogin")
    fun login(@Query("userName") userName: String, @Query("password") password: String, @Query("setupId") installationId: String, @Query("imei")mImei:String): Call<ResponseBody>


    @POST("protected/users/current")
    fun getUserInfo(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String): Call<User>


    @GET("protected/users/logoutdevice")
    fun logout(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String,@Query("imei")mImei:String): Call<ResponseBody>

    @GET("protected/users/logoutdevice")
    fun logoutSingle(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String,@Query("imei")mImei:String): Single<Boolean>

    @GET("protected/hospitals4Mobile")
    fun getHospitals(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String): Observable<Response<List<Hospital>>>


    @POST("protected/questions")
    fun getQuestions(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String): Observable<List<Question>>


    @POST("protected/qcategories")
    fun getQuestionCategories(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String): Observable<List<QuestionCategory>>


    @POST("protected/unit_categories")
    fun getUintCategories(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String): Observable<List<UnitCategory>>


    @POST("protected/hospitals/{hospitalid}/buildings/addAll")
    fun postBuildings(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String, @Body buildings: List<Building>, @Path("hospitalid", encoded = true) hospitalid: Int): Call<List<Building>>

    @POST("protected/hospitals/{hospitalid}/buildings/addAll")
    fun postBuildings2(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String, @Body buildings: List<Building>, @Path("hospitalid", encoded = true) hospitalid: Int): Observable<List<Building>>


    @POST("protected/hospitals/{hospitalid}/buildings/delete")
    fun deleteBuilding(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String, @Body buildingIds: List<String>, @Path("hospitalid", encoded = true) hospitalid: Int): Call<Boolean>


    @POST("protected/hospitals/{hospitalid}/buildings/{buildingId}/surgerydepartment/addAll")
    fun postSurgerDepartments(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String,
                              @Body buildings: List<SurgeryDepartment>,
                              @Path("hospitalid", encoded = true) hospitalid: Int,
                              @Path("buildingId", encoded = true) buildingId: String
    ): Call<List<SurgeryDepartment>>

    @POST("protected/hospitals/{hospitalid}/buildings/{buildingId}/surgerydepartment/delete")
    fun deleteSurgerDepartments(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String,
                                @Body ids: List<String>,
                                @Path("hospitalid", encoded = true) hospitalid: Int,
                                @Path("buildingId", encoded = true) buildingId: String
    ): Call<Boolean>


    @POST("protected/hospitals/{hospitalid}/buildings/{buildingId}/clinicalunit/addAll")
    fun postClinicalUnit(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String,
                         @Body entites: List<ClinicalUnit>,
                         @Path("hospitalid", encoded = true) hospitalid: Int,
                         @Path("buildingId", encoded = true) buildingId: String
    ): Call<List<ClinicalUnit>>

    @POST("protected/hospitals/{hospitalid}/buildings/{buildingId}/clinicalunit/delete")
    fun deleteClinicalUnit(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String,
                           @Body ids: List<String>,
                           @Path("hospitalid", encoded = true) hospitalid: Int,
                           @Path("buildingId", encoded = true) buildingId: String
    ): Call<Boolean>


    @POST("protected/hospitals/{hospitalid}/buildings/{buildingId}/nonclinicalunit/addAll")
    fun postNonClinicalUnit(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String,
                            @Body entites: List<NonClinicalUnit>,
                            @Path("hospitalid", encoded = true) hospitalid: Int,
                            @Path("buildingId", encoded = true) buildingId: String
    ): Call<List<NonClinicalUnit>>

    @POST("protected/hospitals/{hospitalid}/buildings/{buildingId}/nonclinicalunit/delete")
    fun deleteNonClinicalUnit(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String,
                              @Body ids: List<String>,
                              @Path("hospitalid", encoded = true) hospitalid: Int,
                              @Path("buildingId", encoded = true) buildingId: String
    ): Call<Boolean>


    @POST("protected/evaluation/addAll")
    fun postEvaluations(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String,
                        @Body evaluations: List<Evaluation>): Call<List<Evaluation>>

    @POST("protected/evaluation/delete")
    fun deleteEvaluation(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String,
                         @Body ids: List<String>
    ): Call<ResponseBody>

    @POST("protected/buildings/getAllBuildings4User")
    fun getBuildings(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String): Observable<List<Building>>


    @POST("protected/surgerydepartment/getAllclinicalunits4User")
    fun getAllSurgeryDeps4User(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String): Observable<List<SurgeryDepartment>>


    @POST("protected/nonclinicalunit/getAllNonClinicalUnits4User")
    fun getAllNonClinicalUnits4User(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String): Observable<List<NonClinicalUnit>>


    @POST("protected/clinicalunit/getAllclinicalunits4User")
    fun getAllClinicalUnits4User(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String): Observable<List<ClinicalUnit>>

    @POST("protected/evaluations")
    fun getAllEvaluations4User(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String): Observable<List<Evaluation>>


    @POST("protected/hospitals/updateAll")
    fun postHospitals(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String, @Body hospitals: Array<Hospital>): Observable<Response<List<Hospital>>  >


    @Multipart
    @POST("protected/attachments/uploadFile")
    fun uploadAttachment(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String,
                         @Part file: MultipartBody.Part,
                         @Part("category") category: RequestBody,
                         @Part("linkedId") linkedId: RequestBody,
                         @Part("RowGUID") rowGUID: RequestBody,
                         @Part("comment") comment: RequestBody,
                         @Part("extension") extension: RequestBody
    ): Single<Attachment>


    @POST("protected/attachments/deleteFiles")
    fun deleteAttachments(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String,
                          @Body guids: List<String>): Observable<List<Pair<String, Boolean>>>


    @POST("protected/attachments")
    fun getAttachments(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String): Observable<List<Attachment>>

    @GET("protected/attachments/downloadFile/{fileName}")
    fun downloadFile(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String, @Path("fileName", encoded = true) fileName: String): Single<ResponseBody>


    @POST("protected/setting/getAllMobileAppSetting")
    fun getSettings(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String): Observable<List<Setting>>


    //-===============CandidateLocation=================================
    @POST("protected/hospitals/{hospitalid}/candidatelocations/addAll")
    fun postCandidateLocations(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String, @Body buildings: List<CandidateLocation>, @Path("hospitalid", encoded = true) hospitalid: Int): Call<List<CandidateLocation>>


    @POST("protected/hospitals/{hospitalid}/candidatelocations/delete")
    fun deleteCandidateLocation(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String, @Body buildingIds: List<String>, @Path("hospitalid", encoded = true) hospitalid: Int): Call<Boolean>


    @POST("protected/candidatelocations/getAllCandidatelocations4User")
    fun getCandidateLocations(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String): Observable<List<CandidateLocation>>


    //=========================entrances===================================
    @POST("protected/hospitals/{hospitalid}/entrances/addAll")
    fun postEntrances(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String, @Body buildings: List<HospitalEntrance>, @Path("hospitalid", encoded = true) hospitalid: Int): Call<List<HospitalEntrance>>


    @POST("protected/hospitals/{hospitalid}/entrances/delete")
    fun deleteEntrances(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String, @Body buildingIds: List<String>, @Path("hospitalid", encoded = true) hospitalid: Int): Call<Boolean>


    @POST("protected/entrances/getAllEntrances4User")
    fun getEntrances(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String): Observable<List<HospitalEntrance>>


    //=========================Hospital Comments===================================

    @POST("protected/hospitalcomments/getAllComments4User")
    fun getHospitalComments(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String): Observable<List<HospitalComments>>


    @POST("protected/hospitalcomments/addAll")
    fun postHospitalComments(@Header(Constants.AUTHORIZATION_LABEL) Authorization: String, @Body comments: List<HospitalComments>): Call<List<HospitalComments>>


}