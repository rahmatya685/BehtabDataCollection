package ir.zngis.behtabdatacollection.repository.remote.deserializer

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import ir.zngis.behtabdatacollection.util.EnumUtil
import java.lang.reflect.Type

class EntityStatusDeserializer :JsonDeserializer<EnumUtil.EntityStatus> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): EnumUtil.EntityStatus {
        var retVal = EnumUtil.EntityStatus.INSERT

        json?.let {
            retVal = EnumUtil.EntityStatus.fromInteger(json.asInt)!!
        }

        return retVal
    }
}