package ir.zngis.behtabdatacollection.repository.remote.deserializer

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import ir.zngis.behtabdatacollection.baseBinding.BindableDouble
import java.lang.reflect.Type

class BindableDoubleDeserializer : JsonDeserializer<BindableDouble> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): BindableDouble {
        var retVal = BindableDouble(-1.0)
        json?.let {
            retVal = BindableDouble(json.asDouble)
        }
        return retVal
    }
}