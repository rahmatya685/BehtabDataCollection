package ir.zngis.behtabdatacollection.repository.remote

class HttpError {

    var timestamp:String=""
    var status:String = ""
    var error:String = ""
    var message:String = ""
    var path:String = ""
}