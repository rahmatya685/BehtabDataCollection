package ir.zngis.behtabdatacollection.repository.remote.serializer

import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import ir.zngis.behtabdatacollection.baseBinding.BindableBoolean
import java.lang.reflect.Type

class BindableBooleanSerializer:JsonSerializer<BindableBoolean> {
    override fun serialize(src: BindableBoolean?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        var element=JsonPrimitive(false)
        src?.let {
            element = JsonPrimitive(it.get())
        }
        return element
    }
}