package ir.zngis.behtabdatacollection.repository.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.edsab.pm.dao.BaseDao
import io.reactivex.Flowable
import io.reactivex.Single
import ir.zngis.behtabdatacollection.repository.dataModel.Hospital
import ir.zngis.behtabdatacollection.repository.dataModel.HospitalComments

@Dao
interface HospitalDao:BaseDao<Hospital> {

    @Query("select * from HOSPITAL where STATUS != 1")
    fun getHospitals(): Flowable<Array<Hospital>>

    @Query("select * from HOSPITAL  ")
    fun getHospitalsWitDeletedItems(): Array<Hospital>


//    @Query("select * from HOSPITAL,HOSPITALCOMMENTS where HOSPITALCOMMENTS.HOSPITAL_ID = HOSPITAL.ID")
//    fun getHospitalsWithComment():Flowable<Array<HospitalWithComment>>

    @Query("select * from HOSPITAL where id  = :id")
    fun findHospitalById(id: Int): Hospital?
}