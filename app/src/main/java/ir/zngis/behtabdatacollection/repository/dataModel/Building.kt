package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.*

 import ir.zngis.behtabdatacollection.baseBinding.BindableString
import ir.zngis.behtabdatacollection.util.GeneralUtil
import ir.zngis.geojson.Polygon


@Entity(tableName = "BUILDING", foreignKeys = [
    ForeignKey(entity = Hospital::class, parentColumns = ["ID"], childColumns = ["HOSPITAL_ID"], onDelete = ForeignKey.CASCADE)

],indices = [
Index("ROW_GUID",unique = true)
])
class Building(
        @ColumnInfo(name = "NAME")
        var name: BindableString = BindableString(""),
        @ColumnInfo(name = "COMMENT")
        var comment: BindableString? = BindableString(""),
        @ColumnInfo(name = "GEOM", typeAffinity = ColumnInfo.TEXT)
        var geom: Polygon? = null,
        @ColumnInfo(name = "HOSPITAL_ID", typeAffinity = ColumnInfo.INTEGER)
        var hospitalId: Int = -1,
        @Ignore
        var countUnVisited: Int = 0,
        @Ignore
        var countVisited: Int = 0,
        @Ignore
        var countRemainedQuestions: Int = 0,

        @ColumnInfo(name = "ROW_GUID", typeAffinity = ColumnInfo.TEXT)
        var rowGuid: String = GeneralUtil.generateGuid()

) : BaseClass(), Cloneable {


    public override fun clone(): Building {
        var cloned = super.clone() as Building

        cloned.hospitalId =  this.hospitalId

        cloned.geom = this.geom

        cloned.name = this.name

        cloned.comment = this.comment


        return cloned
    }



}