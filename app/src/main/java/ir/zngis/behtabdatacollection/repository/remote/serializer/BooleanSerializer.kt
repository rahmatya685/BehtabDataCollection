package ir.zngis.behtabdatacollection.repository.remote.serializer

import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import java.io.Serializable
import java.lang.reflect.Type

class BooleanSerializer:JsonSerializer<Boolean> {
    override fun serialize(src: Boolean?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        var element= JsonPrimitive(1)
        src?.let {
            element = JsonPrimitive(it)
        }
        return element
    }
}