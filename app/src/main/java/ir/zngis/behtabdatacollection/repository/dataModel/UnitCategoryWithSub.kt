package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Relation


@Entity
class UnitCategoryWithSub(
        @Embedded var unitCategory: UnitCategory = UnitCategory(""),
        @Relation(
                parentColumn = "ID",
                entityColumn = "CATEGORY_ID"
        ) var unitSubCategories: MutableList<UnitSubCategory> = mutableListOf()
) {

}