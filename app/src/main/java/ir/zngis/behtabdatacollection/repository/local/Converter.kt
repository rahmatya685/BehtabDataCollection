package ir.zngis.behtabdatacollection.repository.local

import androidx.room.TypeConverter
import android.net.Uri
import ir.zngis.behtabdatacollection.baseBinding.BindableBoolean
import ir.zngis.behtabdatacollection.baseBinding.BindableDouble
import ir.zngis.behtabdatacollection.baseBinding.BindableInt
import ir.zngis.behtabdatacollection.baseBinding.BindableString
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.geojson.LineString
import ir.zngis.geojson.Point
import ir.zngis.geojson.Polygon
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by rahmati on 2/16/2018.
 */
class Converter {

//    @TypeConverter
//    fun toDate(dateLong: Long?): Date {
//        if (dateLong== null){
//            return GeneralUtil.Now()
//        }else{
//            return Date(dateLong)
//        }
//    }
//
//    @TypeConverter
//    fun fromDate(date: Date): Long {
//        if (date==null){
//            return -1
//        }else{
//            return date.time
//        }
//    }

    @TypeConverter
    fun toPoint(string: String?): Point? {
        return if (string.isNullOrEmpty()) {
            null
        } else {
            Point(JSONObject(string))
        }
    }

    @TypeConverter
    fun fromPoint(polygon: Point?): String? {
        return polygon?.toJSON()?.toString()
    }

    @TypeConverter
    fun toPolygon(string: String?): Polygon? {
        return if (string.isNullOrEmpty()) {
            return null
        } else {
            Polygon(JSONObject(string))
        }
    }

    @TypeConverter
    fun fromPolygon(polygon: Polygon?): String? {
        return polygon?.toJSON()?.toString()

    }

    @TypeConverter
    fun toLineString(string: String?): LineString? {
        return if (string.isNullOrEmpty()) {
            return null
        } else {
            LineString(JSONObject(string))
        }
    }

    @TypeConverter
    fun fromPolygon(polygon: LineString?): String? {
        return polygon?.toJSON()?.toString()

    }


    @TypeConverter
    fun toDate(dateString: String?): Date? {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)

        if (dateString == null) {
            return null
        }
        return format.parse(dateString)
    }

    @TypeConverter
    fun fromDate(date: Date?): String? {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        if (date == null) {
            return null
        } else
            return format.format(date)
    }

    @TypeConverter
    fun toBindableString(string: String?): BindableString? {
        return if (string == null) {
            BindableString("")
        } else {
            BindableString(string)
        }
    }

    @TypeConverter
    fun fromBindableString(bindableString: BindableString?): String? {
        return if (bindableString == null) {
            ""
        } else {
            bindableString.get()
        }
    }

    @TypeConverter
    fun toBindableDouble(value: Double?): BindableDouble? {
        return if (value == null) {
            return BindableDouble(-1.0)
        } else {
            return BindableDouble(value)
        }
    }

    @TypeConverter
    fun fromBindableDouble(value: BindableDouble?): Double? {
        return value?.get() ?: -1.0
    }


    @TypeConverter
    fun toBindableInt(value: Int?): BindableInt? {
        return if (value == null) {
            return BindableInt(-1)
        } else {
            return BindableInt(value)
        }
    }

    @TypeConverter
    fun fromBindableBoolean(value: BindableBoolean?): Int? {
        return if (value == null) {
            null
        } else {
            if (value.get()) {
                1
            } else {
                0
            }
        }
    }


    @TypeConverter
    fun toBindableBoolean(value: Int?): BindableBoolean? {
        return if (value == null) {
            return null
        } else {
            return if (value == 0) {
                BindableBoolean(false)
            } else {
                BindableBoolean(true)
            }
        }
    }

    @TypeConverter
    fun fromBindableInt(v: BindableInt?): Int? {
        return v?.value ?: -1
    }

    //Uri
    @TypeConverter
    fun toUri(value: String?) = Uri.parse(value)

    @TypeConverter
    fun fromUri(value: Uri?) = value.toString()


    @TypeConverter
    fun toEntityStatus(value: Int): EnumUtil.EntityStatus? {
        return EnumUtil.EntityStatus.fromInteger(value)
    }

    @TypeConverter
    fun fromEntityStatus(value: EnumUtil.EntityStatus?): Int {

        return if (value == null) {
            -1
        } else {
            EnumUtil.EntityStatus.toInteger(value)
        }

    }

    @TypeConverter
    fun toHealthLocationType(value: Int): EnumUtil.HealthLocationType {
        return EnumUtil.HealthLocationType.toHealthLocationType(value)
    }

    @TypeConverter
    fun fromHealthLocationType(value: EnumUtil.HealthLocationType?): Int {
        return if (value == null) {
            -1
        } else {
            EnumUtil.HealthLocationType.toInt(value)
        }
    }
}