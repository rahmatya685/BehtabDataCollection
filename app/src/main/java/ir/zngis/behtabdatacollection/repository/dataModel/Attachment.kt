package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import android.net.Uri
import android.os.Parcelable
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.util.GeneralUtil
import kotlinx.android.parcel.Parcelize
import org.w3c.dom.Comment
import java.io.File

@Entity(tableName = "ATTACHMENT")
data class Attachment(
        @ColumnInfo(name = "COMMENT", typeAffinity = ColumnInfo.TEXT)
        var comments: String  = "",
        @ColumnInfo(name = "ROW_GUID", typeAffinity = ColumnInfo.TEXT)
        var rowGuid: String = GeneralUtil.generateGuid()

) : BaseClass(), Parcelable, Cloneable {

    @ColumnInfo(name = "LINK_ID", typeAffinity = ColumnInfo.TEXT)
    var linkId: String = ""

    @ColumnInfo(name = "NAME", typeAffinity = ColumnInfo.TEXT)
    var fileName: String = ""

    @ColumnInfo(name = "EXTENSION", typeAffinity = ColumnInfo.TEXT)
    var extension: String = ""

    @ColumnInfo(name = "PATH", typeAffinity = ColumnInfo.TEXT)
    var path: String  = ""
        set(value) {
            field = if (value == null) "" else value
        }


    @Ignore
    var isSelected: Boolean = false

    @ColumnInfo(name = "CATEGORY")
    var category: String = ""

    @ColumnInfo(name = "FILE_DOWNLOAD_URI")
    var fileDownloadUri: String = ""

    private constructor(creator: Creator) : this() {
        this.category = creator.category
        this.extension = creator.extension
        this.linkId = creator.linkId
        this.fileName = creator.fileName
        this.path = creator.path.path
        this.path.let {
            if (File(it).exists()) {
                fileDownloadStatus = EnumUtil.FileDownloadStatus.Exist
            } else {
                fileDownloadStatus = EnumUtil.FileDownloadStatus.Download
            }
        }
    }

    @Ignore
    var fileDownloadStatus: EnumUtil.FileDownloadStatus? = null


    class Creator {
        var linkId: String = ""
            private set
        var category: String = ""
            private set
        var comments: String = ""
            private set
        var extension: String = ""
            private set
        var fileName: String = ""
            private set
        var path: Uri = Uri.parse("")
            private set

        fun setCategoty(category: String) = apply {
            this.category = category
        }

        fun setLinkId(linkedId: String) = apply {
            this.linkId = linkedId
        }

        fun setComment(comment: String) = apply {
            this.comments = comment
        }

        fun setFile(file: File) = apply {
            extension = file.extension
            fileName = file.name
            path = Uri.fromFile(file)

        }

        fun build() = Attachment(this)

    }


    public override fun clone(): Attachment {
        return super.clone() as Attachment
    }


}