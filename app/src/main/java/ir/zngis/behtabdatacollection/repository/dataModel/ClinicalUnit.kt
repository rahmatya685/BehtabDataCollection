package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.*
import ir.zngis.behtabdatacollection.baseBinding.BindableInt
import ir.zngis.behtabdatacollection.baseBinding.BindableString
import ir.zngis.behtabdatacollection.util.GeneralUtil

@Entity(tableName = "CLINICAL_UNIT", foreignKeys = [
    ForeignKey(entity = Building::class, parentColumns = ["ROW_GUID"], childColumns = ["BUILDING_ID"],onDelete =ForeignKey.CASCADE)

],indices = [
    Index("ROW_GUID",unique = true)
])
class ClinicalUnit(
        @ColumnInfo(name = "CATEGORY", typeAffinity = ColumnInfo.INTEGER)
        var category: BindableInt = BindableInt(-1),
        @ColumnInfo(name = "NAME", typeAffinity = ColumnInfo.INTEGER)
        var name: BindableInt = BindableInt(-1),
        @ColumnInfo(name = "NORMAL_CAPACITY", typeAffinity = ColumnInfo.INTEGER)
        var normalCapacity: BindableInt? = BindableInt(-1),
        @ColumnInfo(name = "MAX_CAPACITY", typeAffinity = ColumnInfo.INTEGER)
        var maxCapacity: BindableInt? = BindableInt(-1),
        @ColumnInfo(name = "PREDICTED_STAFF_COUNT", typeAffinity = ColumnInfo.INTEGER)
        var predictedStaffCount: BindableInt? = BindableInt(-1),
        @ColumnInfo(name = "REAL_STAFF_COUNT", typeAffinity = ColumnInfo.INTEGER)
        var realStaffCount: BindableInt? = BindableInt(-1),
        @ColumnInfo(name = "ISOLATED_BED_COUNT", typeAffinity = ColumnInfo.INTEGER)
        var isolatedBedCount: BindableInt? = BindableInt(-1),
        @ColumnInfo(name = "COMMENT")
        var comment: BindableString? = BindableString(""),
        @ColumnInfo(name = "BUILDING_ID", typeAffinity = ColumnInfo.TEXT)
        var buildingId: BindableString? = BindableString(""),
        @Ignore
        var nameTemp:String? = null,

        @Ignore
        var typeTemp:String?= null,

        @Ignore
        var countUnVisited: Int? = 0,
        @Ignore
        var countVisited: Int? = 0,
        @Ignore
        var countRemainedQuestions:Int = 0,
        @ColumnInfo(name = "ROW_GUID", typeAffinity = ColumnInfo.TEXT)
        var rowGuid: String = GeneralUtil.generateGuid()

) : BaseClass(), Cloneable {


    @Ignore
    var unitCategoryByCategory:UnitCategory? = null

    @Ignore
    var unitSubCategoryByName:UnitSubCategory? =null

    @Ignore
    var buildingByBuildingId:Building? =null


    public override fun clone(): ClinicalUnit {
        var cloned = super.clone() as ClinicalUnit
        buildingId?.let {
            cloned.buildingId = it.clone() as BindableString
        }
        cloned.category = category.clone() as BindableInt
        cloned.name = name.clone() as BindableInt
        comment?.let {
            cloned.comment = it.clone() as BindableString
        }
        predictedStaffCount?.let {
            cloned.predictedStaffCount = it.clone() as BindableInt
        }
        realStaffCount?.let {
            cloned.realStaffCount = it.clone() as BindableInt
        }
        isolatedBedCount?.let {
            cloned.isolatedBedCount = it.clone() as BindableInt
        }
        maxCapacity?.let {
            cloned.maxCapacity = it.clone() as BindableInt
        }
        normalCapacity?.let {
            cloned.normalCapacity = it.clone() as BindableInt
        }

        return cloned
    }
}