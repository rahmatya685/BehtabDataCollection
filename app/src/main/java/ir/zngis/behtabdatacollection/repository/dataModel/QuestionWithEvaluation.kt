package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Relation
import android.os.Parcelable
import ir.zngis.behtabdatacollection.baseBinding.BindableBoolean
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
class QuestionWithEvaluation : Parcelable {

    @Embedded
    lateinit var question: Question

    @Relation(
            parentColumn = "ID",
            entityColumn = "QUESTION_ID"
    ) var questions: MutableList<Evaluation> = mutableListOf()

}