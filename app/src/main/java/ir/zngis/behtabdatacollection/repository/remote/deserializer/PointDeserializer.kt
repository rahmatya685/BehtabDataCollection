package ir.zngis.behtabdatacollection.repository.remote.deserializer

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import ir.zngis.geojson.Point
import org.json.JSONObject
import java.lang.reflect.Type

class PointDeserializer : JsonDeserializer<Point> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Point {
        var retVal = Point()

        json?.let {
            retVal = Point(JSONObject(json.toString()))
        }

        return retVal
    }
}