package ir.zngis.behtabdatacollection.repository.dataModel

class User {
    val id = 0
    val name: String? = null
    val pass: String? = null
    val userName: String? = null
    val token: String? = null
    val authorities: Collection<UserRole>? = null
}