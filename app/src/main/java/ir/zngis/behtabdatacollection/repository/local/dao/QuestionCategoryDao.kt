package ir.zngis.behtabdatacollection.repository.local.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.annotation.NonNull
import com.edsab.pm.dao.BaseDao
import io.reactivex.Flowable
import ir.zngis.behtabdatacollection.repository.dataModel.QuestionCategory
import ir.zngis.behtabdatacollection.repository.dataModel.QuestionCategoryWithQuestion
import ir.zngis.behtabdatacollection.util.EnumUtil


@Dao
interface QuestionCategoryDao : BaseDao<QuestionCategory> {

    @Query("SELECT * FROM QUESTION_CATEGORY WHERE MODULE_NAME = :moduleName  ")
    fun getCategoriesWithQuestion(moduleName: String ): Flowable<List<QuestionCategoryWithQuestion>>


    @Query("SELECT * FROM QUESTION_CATEGORY ")
    fun getCategories(): Flowable<List<QuestionCategory>>
}