package ir.zngis.behtabdatacollection.repository.remote.serializer

import com.google.gson.*
import ir.zngis.geojson.Polygon
import ir.zngis.geojson.util.JSONUtils
import java.lang.reflect.Type
import com.google.gson.JsonObject
import com.google.gson.JsonParser



class PolygonSerializer : JsonSerializer<Polygon> {

    override fun serialize(src: Polygon?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement? {
        var element: JsonObject? = null

        src?.let {
            val json = it.toJSON()

            val parser = JsonParser()
            element = parser.parse(json.toString()).asJsonObject

        }
        return element
    }
}