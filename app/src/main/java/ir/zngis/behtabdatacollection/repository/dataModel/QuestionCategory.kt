package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore

@Entity(tableName = "QUESTION_CATEGORY")
class QuestionCategory(@ColumnInfo(name = "MODULE_NAME")
                       var moduleName: String = "",
                       @ColumnInfo(name = "CATEGORY_TITLE")
                       var categoryTitleFa: String = "",
                       @ColumnInfo(name = "CATEGORY_TITLE_ENG")
                       var categoryTitleEn: String = "") : BaseClass() {



    @Ignore
    var number:String? = ""

    @Ignore
    var countUnVisited: Int = 0
    @Ignore
    var countVisited: Int = 0
    @Ignore
    var countRemainedQuestions: Int = 0
}