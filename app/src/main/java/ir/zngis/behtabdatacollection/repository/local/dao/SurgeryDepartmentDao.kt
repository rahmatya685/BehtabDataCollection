package ir.zngis.behtabdatacollection.repository.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.edsab.pm.dao.BaseDao
import io.reactivex.Flowable
import ir.zngis.behtabdatacollection.repository.dataModel.Pair
import ir.zngis.behtabdatacollection.repository.dataModel.SurgeryDepartment

@Dao
interface SurgeryDepartmentDao : BaseDao<SurgeryDepartment> {

    @Query("SELECT * FROM SURGERY_DEPARTMENT  WHERE  ROW_GUID = :id")
    fun findSurgeryDepartment(id:String):SurgeryDepartment?

    @Query("SELECT * FROM SURGERY_DEPARTMENT WHERE BUILDING_ID  IN (select BUILDING.ROW_GUID from BUILDING where BUILDING.HOSPITAL_ID = :hospitalId)")
    fun getSurgeryDepartments(hospitalId: Int): Flowable<List<SurgeryDepartment>>


    @Query("select EVALUATION.SURGERY_DEPARTMENT_ID as first, count(distinct(EVALUATION.QUESTION_ID))   as second  from EVALUATION inner join ( select *,SURGERY_DEPARTMENT.ROW_GUID as S_ID from SURGERY_DEPARTMENT inner join BUILDING on SURGERY_DEPARTMENT.BUILDING_ID = BUILDING.ROW_GUID where BUILDING.HOSPITAL_ID  = :hospitalId) as T   on T.S_ID  =EVALUATION.SURGERY_DEPARTMENT_ID  AND EVALUATION.QUESTION_ID IN ( select QUESTION.ID from QUESTION inner join QUESTION_CATEGORY on QUESTION_CATEGORY.ID =QUESTION.CATEGORY_ID where QUESTION_CATEGORY.MODULE_NAME  = :moduleName) and EVALUATION.HAS_OBSERVED == :observeStatus  group by EVALUATION.SURGERY_DEPARTMENT_ID")
    fun getCountEvaluations(hospitalId: Int, moduleName: String,observeStatus:Int): Flowable<List<Pair>>

    @Query("SELECT * FROM SURGERY_DEPARTMENT ")
    fun getSurgeryDepartments( ):   List<SurgeryDepartment>

    @Query("SELECT * FROM SURGERY_DEPARTMENT where SURGERY_DEPARTMENT.STATUS = :status")
    fun getAllSyncedSurgeryDepartments(status:Int):   Flowable<List<SurgeryDepartment>>


    @Query("SELECT * FROM SURGERY_DEPARTMENT WHERE BUILDING_ID = :buildingId")
    fun getSurgeryDepartmentsByBuilding(buildingId: String): Flowable<List<SurgeryDepartment>>
//    @Query("select  count( distinct EVALUATION.QUESTION_ID )  from EVALUATION where EVALUATION.QUESTION_ID in (   select QUESTION.ID FROM QUESTION join QUESTION_CATEGORY on QUESTION.CATEGORY_ID = QUESTION_CATEGORY.ID  where QUESTION_CATEGORY.MODULE_NAME = :module ) and EVALUATION.SURGERY_DEPARTMENT_ID = :unitId and EVALUATION.HAS_OBSERVED =:hasObserved")
//    fun getEvaluationCount(module:String, unitId:Int,hasObserved:Int):Int
}