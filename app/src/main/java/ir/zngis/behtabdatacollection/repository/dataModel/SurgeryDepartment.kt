package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.*
import ir.zngis.behtabdatacollection.baseBinding.BindableInt
import ir.zngis.behtabdatacollection.baseBinding.BindableString
import ir.zngis.behtabdatacollection.util.GeneralUtil

@Entity(tableName = "SURGERY_DEPARTMENT", foreignKeys = [
    ForeignKey(entity = Building::class, parentColumns = ["ROW_GUID"], childColumns = ["BUILDING_ID"], onDelete = ForeignKey.CASCADE)

],indices = [
    Index("ROW_GUID",unique = true)
])
class SurgeryDepartment(
        @ColumnInfo(name = "NAME")
        var name: BindableInt = BindableInt(-1),
        @ColumnInfo(name = "CATEGORY", typeAffinity = ColumnInfo.INTEGER)
        var category: BindableInt = BindableInt(-1),
        @ColumnInfo(name = "NUM_BEDS_NORM_CONDITION", typeAffinity = ColumnInfo.INTEGER)
        var numBedsNormCondition: BindableInt? = BindableInt(-1),
        @ColumnInfo(name = "NUM_BEDS_CRISIS_CONDITION", typeAffinity = ColumnInfo.INTEGER)
        var numBedsCrisisCondition: BindableInt? = BindableInt(-1),
        @ColumnInfo(name = "COMMENT")
        var comment: BindableString? = BindableString(""),
        @ColumnInfo(name = "BUILDING_ID", typeAffinity = ColumnInfo.TEXT)
        var buildingId: BindableString? = BindableString(""),
        @Ignore
        var nameTemp: String? = null,
        @Ignore
        var countUnVisited: Int? = 0,
        @Ignore
        var countVisited: Int? = 0,
        @Ignore
        var countRemainedQuestions:Int = 0,
        @ColumnInfo(name = "ROW_GUID", typeAffinity = ColumnInfo.TEXT)
        var rowGuid: String = GeneralUtil.generateGuid()


) : BaseClass(), Cloneable {

    public override fun clone(): SurgeryDepartment {
        var cloned = super.clone() as SurgeryDepartment
        buildingId?.let {
            cloned.buildingId = it.clone() as BindableString
        }
        numBedsNormCondition?.let {
            cloned.numBedsNormCondition = it.clone() as BindableInt
        }
        cloned.name = name.clone() as BindableInt
        comment?.let {
            cloned.comment = it.clone() as BindableString
        }
        numBedsCrisisCondition?.let {
            cloned.numBedsCrisisCondition = it.clone() as BindableInt
        }
        cloned.category = category.clone() as BindableInt

        return cloned
    }
}