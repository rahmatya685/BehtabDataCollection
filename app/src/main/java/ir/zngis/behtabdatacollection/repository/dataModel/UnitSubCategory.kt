package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(tableName = "UNIT_SUB_CATEGORY",
        foreignKeys =
        [
            ForeignKey(entity = UnitCategory::class,
                    parentColumns = ["ID"],
                    childColumns = ["CATEGORY_ID"])
        ]
)
class UnitSubCategory(
        @ColumnInfo(name = "TITLE_EN")
        var titleEn: String = "",
        @ColumnInfo(name = "TITLE_FA")
        var titleFa: String = "",
        @ColumnInfo(name = "CATEGORY_ID")
        var categoryId: Int = -1
) : BaseClass() {
}