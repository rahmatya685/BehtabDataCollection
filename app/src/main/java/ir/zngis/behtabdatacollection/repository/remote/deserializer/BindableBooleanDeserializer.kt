package ir.zngis.behtabdatacollection.repository.remote.deserializer

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import ir.zngis.behtabdatacollection.baseBinding.BindableBoolean
import java.lang.reflect.Type

class BindableBooleanDeserializer:JsonDeserializer<BindableBoolean> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): BindableBoolean {
        var retVal = BindableBoolean(false)
         json?.let {
             retVal.set(it.asBoolean)
         }

        return  retVal
    }
}