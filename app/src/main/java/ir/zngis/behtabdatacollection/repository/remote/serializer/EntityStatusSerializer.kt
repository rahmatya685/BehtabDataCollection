package ir.zngis.behtabdatacollection.repository.remote.serializer

import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import ir.zngis.behtabdatacollection.util.EnumUtil
import java.lang.reflect.Type

class EntityStatusSerializer : JsonSerializer<EnumUtil.EntityStatus> {

    override fun serialize(src: EnumUtil.EntityStatus?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        var element = JsonPrimitive(1)
        src?.let {
            element = JsonPrimitive(EnumUtil.EntityStatus.toInteger(it))
        }
        return element
    }
}