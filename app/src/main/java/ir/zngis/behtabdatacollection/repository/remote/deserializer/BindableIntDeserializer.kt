package ir.zngis.behtabdatacollection.repository.remote.deserializer

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import ir.zngis.behtabdatacollection.baseBinding.BindableInt
import java.lang.reflect.Type

class BindableIntDeserializer:JsonDeserializer<BindableInt> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): BindableInt {
        var retVal = BindableInt(-1)

        json?.let {
            retVal = BindableInt(json.asInt)
        }

        return retVal
    }
}