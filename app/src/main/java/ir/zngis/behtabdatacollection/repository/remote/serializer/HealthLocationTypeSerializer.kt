package ir.zngis.behtabdatacollection.repository.remote.serializer

import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import ir.zngis.behtabdatacollection.repository.dataModel.Hospital
import ir.zngis.behtabdatacollection.util.EnumUtil
import java.lang.reflect.Type

class HealthLocationTypeSerializer:JsonSerializer<EnumUtil.HealthLocationType> {
    override fun serialize(src: EnumUtil.HealthLocationType?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        var element = JsonPrimitive(1)
        src?.let {
            element = JsonPrimitive(EnumUtil.HealthLocationType.toInt(it))
        }
        return element
    }
}