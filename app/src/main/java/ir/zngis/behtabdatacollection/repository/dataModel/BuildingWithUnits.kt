package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Relation

@Entity
class BuildingWithUnits(
        @Embedded var building: Building = Building(),
        @Relation(
                parentColumn = "ROW_GUID",
                entityColumn = "BUILDING_ID"
        ) var surgeryDepartments: MutableList<SurgeryDepartment> = mutableListOf(),
        @Relation(
                parentColumn = "ROW_GUID",
                entityColumn = "BUILDING_ID"
        ) var nonClinicalUnits: MutableList<NonClinicalUnit> = mutableListOf(),
        @Relation(
                parentColumn = "ROW_GUID",
                entityColumn = "BUILDING_ID"
        ) var clinicalUnits: MutableList<ClinicalUnit> = mutableListOf()
) {
}