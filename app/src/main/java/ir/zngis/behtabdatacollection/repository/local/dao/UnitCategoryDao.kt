package ir.zngis.behtabdatacollection.repository.local.dao

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Query
import com.edsab.pm.dao.BaseDao
import io.reactivex.Flowable
import io.reactivex.Single
import ir.zngis.behtabdatacollection.repository.dataModel.UnitCategory
import ir.zngis.behtabdatacollection.repository.dataModel.UnitCategoryWithSub
import ir.zngis.behtabdatacollection.repository.dataModel.UnitSubCategory

@Dao
interface UnitCategoryDao : BaseDao<UnitCategory> {

    @Query("SELECT * FROM UNIT_CATEGORY")
    fun getUnitCategories(): LiveData<List<UnitCategoryWithSub>>


    @Query("SELECT * FROM UNIT_CATEGORY")
    fun getUnitCategories2(): Flowable<List<UnitCategoryWithSub>>


}