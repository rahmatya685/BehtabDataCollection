package ir.zngis.behtabdatacollection.repository.local

import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.room.migration.Migration
import android.util.Log

class MigrationFromV1_ToV2 :Migration(1,2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        try {
            database.execSQL("alter table  `HOSPITAL` add column `CENTER_POINT` TEXT;")
        }catch (e:Exception){
            Log.e("",e.message)
        }
    }
}