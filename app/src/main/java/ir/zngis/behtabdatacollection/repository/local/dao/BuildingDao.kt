package ir.zngis.behtabdatacollection.repository.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.edsab.pm.dao.BaseDao
import io.reactivex.Flowable
import ir.zngis.behtabdatacollection.repository.dataModel.Building
import ir.zngis.behtabdatacollection.repository.dataModel.BuildingWithUnits

@Dao
interface BuildingDao:BaseDao<Building> {


    @Query("SELECT * FROM BUILDING  WHERE HOSPITAL_ID  = :hospitalId")
    fun getBuildings(hospitalId:Int):List<Building>

    @Query("SELECT * FROM BUILDING    ")
    fun getBuildings():List<Building>



    @Query("SELECT * FROM BUILDING  WHERE HOSPITAL_ID  = :hospitalId")
    fun getBuildingsFlowables(hospitalId:Int):Flowable<List<BuildingWithUnits>>


    @Query("select  count( distinct EVALUATION.QUESTION_ID )  from EVALUATION where EVALUATION.QUESTION_ID in (   select QUESTION.ID FROM QUESTION join QUESTION_CATEGORY on QUESTION.CATEGORY_ID = QUESTION_CATEGORY.ID  where QUESTION_CATEGORY.MODULE_NAME = :module ) and EVALUATION.BUILDING_ID = :buildingId and EVALUATION.HAS_OBSERVED =:hasObserved")
    fun getEvaluationCount(module:String, buildingId:String,hasObserved:Int):Int


    @Query("select * from BUILDING order by ID desc limit 1")
    fun getLastBuilding(): Building?


    @Query("SELECT * FROM BUILDING  WHERE  ID = :id")
    fun findBuilding(id:Int):Building?

    @Query("SELECT * FROM BUILDING  WHERE  ROW_GUID = :id")
    fun findBuilding(id:String):Building?

}