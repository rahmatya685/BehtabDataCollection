package ir.zngis.behtabdatacollection.repository.remote.serializer

import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import ir.zngis.behtabdatacollection.baseBinding.BindableDouble
import java.lang.reflect.Type

class BindableDoubleSerializer :JsonSerializer<BindableDouble> {
    override fun serialize(src: BindableDouble?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        var element= JsonPrimitive(-1.0)
        src?.let {
            element = JsonPrimitive(it.get())
        }
        return element
    }
}