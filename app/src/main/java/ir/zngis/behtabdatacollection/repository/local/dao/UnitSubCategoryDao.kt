package ir.zngis.behtabdatacollection.repository.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.edsab.pm.dao.BaseDao
import io.reactivex.Flowable
import ir.zngis.behtabdatacollection.repository.dataModel.UnitSubCategory

@Dao
interface UnitSubCategoryDao:BaseDao<UnitSubCategory> {

    @Query("SELECT * FROM UNIT_SUB_CATEGORY")
    fun getUnitSubCategories (): Flowable<List<UnitSubCategory>>
}