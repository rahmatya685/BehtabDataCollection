package ir.zngis.behtabdatacollection.repository

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.LiveData
import android.net.Uri
import com.crashlytics.android.Crashlytics
import io.reactivex.*
import io.reactivex.Observable
import ir.zngis.behtabdatacollection.repository.dataModel.*
import ir.zngis.behtabdatacollection.repository.local.LocalRepository
import ir.zngis.behtabdatacollection.repository.remote.EntityApi
import ir.zngis.behtabdatacollection.repository.remote.EntityService
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.util.GeneralUtil
import ir.zngis.behtabdatacollection.util.PreferencesUtil
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import java.io.File
import java.lang.Exception
import java.lang.StringBuilder
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.Pair


@Singleton
class DataManagerImpl @Inject constructor(
        val localRepository: LocalRepository,
        val remoteRepository: EntityApi,
        private val entityService: EntityService,
        val application: Application
) : DataManager {


    override fun syncData(): Observable<String?>? {

        val userIdentifier = PreferencesUtil.getInteger(Constants.USER_IDENTIFIER, application)

        val hospitals = localRepository.getHospitals().blockingFirst()

        val syncErrors = mutableListOf<Triple<String, Int, String?>>()

        val sendInfo = remoteRepository.postHospitals(hospitals).map {

            if (it.isSuccessful) {

                try {

                    hospitals.forEach { hospital ->

                        val isUserSurveyor = hospital.surveyorUserId == userIdentifier

                        if (isUserSurveyor) {

                            syncBuildings(hospital, syncErrors)

                            syncHospitalUnits(hospital, syncErrors)

                            syncEntrances(hospital, syncErrors)

                            syncCandidateLocations(hospital, syncErrors)

                            syncHospitalComments(syncErrors, hospital)
                        }
                    }

                    syncEvaluations(syncErrors)

                    val attachments = localRepository.getAllAttachments()

                    syncDeletedAttachments(attachments)


                } catch (e: Exception) {
                    syncErrors.add(Triple(Hospital::class.java.simpleName, Constants.STATUS_ERROR, e.message))
                } finally {
                    val stringBuilder = StringBuilder()
                    stringBuilder.append(syncErrors.map { it.third })

                    if (syncErrors.isNotEmpty()) {
                        Throwable(stringBuilder.toString())
                        Crashlytics.log("SyncErrors:$stringBuilder")
                    }


                }
                Constants.SYNC_STATUS_OK
            } else {
                val error = it.errorBody()?.string()
                error
            }

        }.doOnError { t ->
            syncErrors.add(Triple(Hospital::class.java.simpleName, Constants.STATUS_ERROR, t.message))
        }


        return sendInfo
    }


    @SuppressLint("CheckResult")
    private fun syncDeletedAttachments(attachments: List<Attachment>) {

        val guids = attachments.filter { it.status == EnumUtil.EntityStatus.DELETE }.map { it.rowGuid }

        if (guids.isEmpty())
            return

        remoteRepository.deleteAttachments(guids).subscribe({ deleteResult: List<Pair<String, Boolean>> ->

            deleteResult.forEach { pair ->
                val att = attachments.find { it.rowGuid.contentEquals(pair.first) }
                att?.let {
                    localRepository.deleteEntity(it)
                }
            }


        }, { t: Throwable ->


        })


    }

    @SuppressLint("CheckResult")
    override fun syncInsertedAttachments(): Observable<Pair<String, String>> {

        val attachments = localRepository.getAllAttachments()

        val insertedAttachments = attachments.filter { it.status == EnumUtil.EntityStatus.INSERT }

        val uploadRequests = mutableListOf<Single<Attachment>>()

        insertedAttachments.forEach { attachment: Attachment ->

            val file = File(attachment.path)

            if (file.exists()) {

                val mimeType = GeneralUtil.getMimeType(Uri.fromFile(File(attachment.path)), application)

                val reqFile = RequestBody.create(MediaType.parse(mimeType), file)
                val body = MultipartBody.Part.createFormData("file", file.name, reqFile)

                val category = RequestBody.create(MediaType.parse("text/plain"), attachment.category)
                val linkedId = RequestBody.create(MediaType.parse("text/plain"), attachment.linkId.toString())
                val rowGuid = RequestBody.create(MediaType.parse("text/plain"), attachment.rowGuid)
                val comment = RequestBody.create(MediaType.parse("text/plain"), attachment.comments)
                val extension = RequestBody.create(MediaType.parse("text/plain"), attachment.extension)


                val uploadRequest = remoteRepository.uploadAttachment(body, category, linkedId, rowGuid, comment, extension)

                uploadRequests.add(uploadRequest)

            }
        }

        if (uploadRequests.isEmpty()) {
            return Observable.fromCallable {
                Pair(Constants.NO_ATTACHMENT_FOUND_FOR_UPLOAD, "")
            }
        }

        var counter = 0

        return Single.concat(uploadRequests).map {
            val currentAttacheent = insertedAttachments[counter]
            currentAttacheent.status = EnumUtil.EntityStatus.SYNCED
            localRepository.updateEntity(currentAttacheent)
            counter++

            if (counter == uploadRequests.size)
                Pair(Constants.SYNC_ATTACHMENTS_STATUS_OK, "")
            else
                Pair(Constants.SYNC_STATUS_PROGRESS, "Sending $counter of ${uploadRequests.size} Attachments")
        }.toObservable()
    }

    private fun syncHospitalComments(syncResult: MutableList<Triple<String, Int, String?>>, hospital: Hospital) {

        val entites = localRepository.getCommentByHospital(hospital.id).blockingGet()

        val insertedEntites = entites.filter { it.status == EnumUtil.EntityStatus.INSERT }

        if (insertedEntites.isNotEmpty()) {

            var call = entityService.postHospitalComments(getToken(), insertedEntites)
            val response = call.execute()
            if (response.isSuccessful) {

                insertedEntites.forEach {
                    it.status = EnumUtil.EntityStatus.SYNCED
                    localRepository.updateEntity(it)
                }

            } else {
                response.errorBody()?.let {
                    syncResult.add(Triple(HospitalComments::class.java.simpleName, Constants.STATUS_ERROR, it.string()))
                }
            }

        }

    }


    private fun syncEntrances(hospital: Hospital, syncResult: MutableList<Triple<String, Int, String?>>) {

        var entites = localRepository.getHospitalEntrances(hospital.id)

        val insertedEntites = entites.filter { it.status == EnumUtil.EntityStatus.INSERT }
        val deletedEntites = entites.filter { it.status == EnumUtil.EntityStatus.DELETE }

        if (insertedEntites.isNotEmpty()) {

            var call = entityService.postEntrances(getToken(), insertedEntites, hospital.id)
            val response = call.execute()
            if (response.isSuccessful) {

                insertedEntites.forEach {
                    it.status = EnumUtil.EntityStatus.SYNCED
                    localRepository.updateEntity(it)
                }

            } else {
                response.errorBody()?.let {
                    syncResult.add(Triple(HospitalEntrance::class.java.simpleName, Constants.STATUS_ERROR, it.string()))
                }
            }

        }

        if (deletedEntites.isNotEmpty()) {
            val ids = deletedEntites.map { it.rowGuid }
            val callDelete = entityService.deleteEntrances(getToken(), ids, hospital.id)
            val response = callDelete.execute()
            if (response.isSuccessful) {

                deletedEntites.forEach {
                    localRepository.deleteEntity(it)
                }
            } else {
                response.errorBody()?.let {
                    syncResult.add(Triple(HospitalEntrance::class.java.simpleName, Constants.STATUS_ERROR, it.string()))

                }
            }
        }

    }

    private fun syncCandidateLocations(hospital: Hospital, syncResult: MutableList<Triple<String, Int, String?>>) {

        var entites = localRepository.getCandiadateLocations(hospital.id)

        val insertedEntites = entites.filter { it.status == EnumUtil.EntityStatus.INSERT }
        val deletedEntites = entites.filter { it.status == EnumUtil.EntityStatus.DELETE }


        if (insertedEntites.isNotEmpty()) {

            var call = entityService.postCandidateLocations(getToken(), insertedEntites, hospital.id)
            val response = call.execute()
            if (response.isSuccessful) {

                insertedEntites.forEach {
                    it.status = EnumUtil.EntityStatus.SYNCED
                    localRepository.updateEntity(it)
                }

            } else {
                response.errorBody()?.let {
                    syncResult.add(Triple(CandidateLocation::class.java.simpleName, Constants.STATUS_ERROR, it.string()))
                }
            }

        }

        if (deletedEntites.isNotEmpty()) {
            val ids = deletedEntites.map { it.rowGuid }
            val callDelete = entityService.deleteCandidateLocation(getToken(), ids, hospital.id)
            val response = callDelete.execute()
            if (response.isSuccessful) {

                deletedEntites.forEach {
                    localRepository.deleteEntity(it)
                }
            } else {
                response.errorBody()?.let {
                    syncResult.add(Triple(HospitalEntrance::class.java.simpleName, Constants.STATUS_ERROR, it.string()))

                }
            }
        }

    }

    private fun syncBuildings(hospital: Hospital, syncResult: MutableList<Triple<String, Int, String?>>) {

        val buildings = getBuildings(hospital.id)

        val insertedBuildings = buildings.filter { it.status != EnumUtil.EntityStatus.DELETE }
        val deletedBuildings = buildings.filter { it.status == EnumUtil.EntityStatus.DELETE }

        try {
            if (insertedBuildings.isNotEmpty()) {

                val call = remoteRepository.postBuildings(insertedBuildings, hospital.id)
                val response = call.execute()
                if (response.isSuccessful) {

                    insertedBuildings.forEach {

                        it.status = EnumUtil.EntityStatus.SYNCED
                        localRepository.updateEntity(it)
                    }

                } else {
                    response.errorBody()?.let {
                        syncResult.add(Triple(Hospital::class.java.simpleName, Constants.STATUS_ERROR, it.string()))
                    }
                }

            }

            if (deletedBuildings.isNotEmpty()) {
                val ids = deletedBuildings.map { it.rowGuid }
                val callDelete = remoteRepository.deleteBuilding(ids, hospital.id)
                val response = callDelete.execute()
                if (response.isSuccessful) {

                    deletedBuildings.forEach {
                        localRepository.deleteEntity(it)
                    }
                } else {
                    response.errorBody()?.let {
                        syncResult.add(Triple(Hospital::class.java.simpleName, Constants.STATUS_ERROR, it.string()))

                    }
                }
            }

        } catch (e: Exception) {
            syncResult.add(Triple(Building::class.java.simpleName, Constants.STATUS_ERROR, e.message))
        }


    }

    private fun syncHospitalUnits(hospital: Hospital, syncResult: MutableList<Triple<String, Int, String?>>) {

        val buildings = getBuildings(hospital.id).filter { it.status != EnumUtil.EntityStatus.DELETE }


        val unit2UpdateAfterSync = mutableListOf<BaseClass>()
        val unit2DeleteAfterSync = mutableListOf<BaseClass>()


        val unitCats = localRepository.getUnitCategories2().blockingFirst()

        try {
            syncClinicalDepartments(hospital, buildings, unit2UpdateAfterSync, unit2DeleteAfterSync, unitCats, syncResult)
        } catch (e: Exception) {
            syncResult.add(Triple(ClinicalUnit::class.java.simpleName, Constants.STATUS_ERROR, e.message))
        }

        try {
            syncNonClinicalDepartments(hospital, buildings, unit2UpdateAfterSync, unit2DeleteAfterSync, unitCats, syncResult)
        } catch (e: Exception) {
            syncResult.add(Triple(NonClinicalUnit::class.java.simpleName, Constants.STATUS_ERROR, e.message))

        }


        try {
            syncSurgeryDepartments(hospital, buildings, unit2UpdateAfterSync, unit2DeleteAfterSync, syncResult)
        } catch (e: Exception) {
            syncResult.add(Triple(SurgeryDepartment::class.java.simpleName, Constants.STATUS_ERROR, e.message))
        }


        unit2DeleteAfterSync.forEach {
            localRepository.deleteEntity(it)
        }

        unit2UpdateAfterSync.forEach {
            it.status = EnumUtil.EntityStatus.SYNCED
            localRepository.updateEntity(it)
        }

    }

    private fun syncNonClinicalDepartments(hospital: Hospital, insertedBuildings: List<Building>, unit2UpdateAfterSync: MutableList<BaseClass>, unit2DeleteAfterSync: MutableList<BaseClass>, unitCats: List<UnitCategoryWithSub>, syncResult: MutableList<Triple<String, Int, String?>>) {

        val surgeryDepartments = localRepository.getNonClinicalUnit(hospital.id).blockingFirst()

        try {
            insertedBuildings.forEach { building: Building ->

                val insertedEntities = surgeryDepartments.filter { unit ->
                    unit.buildingId!!.get() == building.rowGuid && unit.status != EnumUtil.EntityStatus.DELETE
                }

                insertedEntities.forEach { item ->
                    item.buildingByBuildingId = building

                    val unitCat = unitCats.find { it.unitCategory.id == item.category.value }

                    unitCat?.let { unitCategoryWithSub ->

                        item.unitCategoryByCategory = unitCategoryWithSub.unitCategory

                        val subCategory = unitCategoryWithSub.unitSubCategories.find { it.id == item.name.value }

                        subCategory?.let {
                            item.unitSubCategoryByName = subCategory
                        }

                    }
                }

                if (insertedEntities.isNotEmpty()) {
                    var callPostData = remoteRepository.postNonClinicalUnit(insertedEntities, hospital.id, building.rowGuid)
                    val response = callPostData.execute()
                    if (!response.isSuccessful) {
                        response.errorBody()?.let {
                            throw Throwable(it.string())
                        }
                    } else {
                        response.body()?.let {
                            if (it.isNotEmpty())
                                unit2UpdateAfterSync.addAll(insertedEntities)
                        }
                    }
                }

                val deletedEntities = surgeryDepartments.filter { unit ->
                    unit.buildingId!!.get() == building.rowGuid && unit.status == EnumUtil.EntityStatus.DELETE
                }

                if (deletedEntities.isNotEmpty()) {
                    var callPostData = remoteRepository.deleteNonClinicalUnit(deletedEntities.map { it.rowGuid }, hospital.id, building.rowGuid)
                    val response = callPostData.execute()
                    if (!response.isSuccessful) {
                        response.errorBody()?.let {
                            throw Throwable(it.string())
                        }
                    } else {
                        response.body()?.let {
                            unit2DeleteAfterSync.addAll(deletedEntities)
                        }

                    }
                }

            }

        } catch (e: Exception) {
            syncResult.add(Triple(NonClinicalUnit::class.java.simpleName, Constants.STATUS_ERROR, e.message))
        }
    }

    private fun syncClinicalDepartments(hospital: Hospital, insertedBuildings: List<Building>, unit2UpdateAfterSync: MutableList<BaseClass>, unit2DeleteAfterSync: MutableList<BaseClass>, unitCats: List<UnitCategoryWithSub>, syncResult: MutableList<Triple<String, Int, String?>>) {

        val surgeryDepartments = localRepository.getClinicalUnit(hospital.id).blockingFirst()

        try {
            insertedBuildings.forEach { building: Building ->

                val insertedEntities = surgeryDepartments.filter { unit ->
                    unit.buildingId!!.get() == building.rowGuid && unit.status != EnumUtil.EntityStatus.DELETE
                }


                insertedEntities.forEach { item ->
                    item.buildingByBuildingId = building

                    val unitCat = unitCats?.find { it.unitCategory.id == item.category.value }

                    unitCat?.let { unitCategoryWithSub ->
                        item.unitCategoryByCategory = unitCategoryWithSub.unitCategory

                        val subCategory = unitCategoryWithSub.unitSubCategories.find { it.id == item.name.value }

                        subCategory?.let {
                            item.unitSubCategoryByName = subCategory
                        }

                    }
                }

                if (insertedEntities.isNotEmpty()) {
                    var callPostData = remoteRepository.postClinicalUnit(insertedEntities, hospital.id, building.rowGuid)
                    val response = callPostData.execute()
                    if (!response.isSuccessful) {
                        response.errorBody()?.let {
                            throw Throwable(it.string())
                        }
                    } else {
                        response.body()?.let {
                            if (it.isNotEmpty())
                                unit2UpdateAfterSync.addAll(insertedEntities)
                        }
                    }
                }

                val deletedEntities = surgeryDepartments.filter { unit ->
                    unit.buildingId!!.get() == building.rowGuid && unit.status == EnumUtil.EntityStatus.DELETE
                }

                if (deletedEntities.isNotEmpty()) {
                    var callPostData = remoteRepository.deleteClinicalUnit(deletedEntities.map { it.rowGuid }, hospital.id, building.rowGuid)
                    val response = callPostData.execute()
                    if (!response.isSuccessful) {
                        response.errorBody()?.let {
                            throw Throwable(it.string())
                        }
                    } else {
                        response.body()?.let {
                            unit2DeleteAfterSync.addAll(deletedEntities)
                        }

                    }
                }

            }

        } catch (e: Exception) {
            syncResult.add(Triple(ClinicalUnit::class.java.simpleName, Constants.STATUS_ERROR, e.message))
        }

    }

    private fun syncSurgeryDepartments(hospital: Hospital, insertedBuildings: List<Building>, unit2UpdateAfterSync: MutableList<BaseClass>, unit2DeleteAfterSync: MutableList<BaseClass>, syncResult: MutableList<Triple<String, Int, String?>>) {

        val surgeryDepartments = localRepository.getSurgeryDepartments(hospital.id).blockingFirst()

        try {
            insertedBuildings.forEach { building: Building ->

                val insertedEntities = surgeryDepartments.filter { unit ->
                    unit.buildingId!!.get() == building.rowGuid && unit.status != EnumUtil.EntityStatus.DELETE
                }



                if (insertedEntities.isNotEmpty()) {
                    val callPostData = remoteRepository.postSurgeryDepartments(insertedEntities, hospital.id, building.rowGuid)
                    val response = callPostData.execute()
                    if (!response.isSuccessful) {
                        response.errorBody()?.let {
                            throw Throwable(it.string())
                        }
                    } else {
                        response.body()?.let {
                            if (it.isNotEmpty())
                                unit2UpdateAfterSync.addAll(insertedEntities)
                        }
                    }
                }

                val deletedEntities = surgeryDepartments.filter { unit ->
                    unit.buildingId!!.get() == building.rowGuid && unit.status == EnumUtil.EntityStatus.DELETE
                }

                if (deletedEntities.isNotEmpty()) {
                    var callPostData = remoteRepository.deleteSurgeryDepartments(deletedEntities.map { it.rowGuid }, hospital.id, building.rowGuid)
                    val response = callPostData.execute()
                    if (!response.isSuccessful) {
                        response.errorBody()?.let {
                            throw Throwable(it.string())
                        }
                    } else {
                        response.body()?.let {
                            unit2DeleteAfterSync.addAll(deletedEntities)
                        }
                    }
                }

            }

        } catch (e: Exception) {
            syncResult.add(Triple(SurgeryDepartment::class.java.simpleName, Constants.STATUS_ERROR, e.message))
        }
    }


    private fun syncEvaluations(syncResult: MutableList<Triple<String, Int, String?>>) {


        val evaluations = localRepository.getUnsyncedEvaluations()

        val questions = localRepository.getAllQuestions()

        //init related entities
        var buildings = localRepository.getBuildings().filter { it.status != EnumUtil.EntityStatus.DELETE }

        val units = localRepository.getAllUnitsByStatus(EnumUtil.EntityStatus.SYNCED)

        val insertedEntities = evaluations.filter { unit ->
            unit.status != EnumUtil.EntityStatus.DELETE
        }

        insertedEntities.forEach { evaluation: Evaluation ->

            evaluation.buildingByBuildingId = buildings.find { it.rowGuid == evaluation.buildingId }

            evaluation.questionByQuestionId = questions.find { it.id == evaluation.questionId }

            units.forEach { unit ->

                when (unit) {
                    is SurgeryDepartment -> {
                        if (unit.rowGuid == evaluation.surgeryDepartmentId)
                            evaluation.surgeryDepartmentBySurgeryDepartmentId = unit
                    }
                    is ClinicalUnit -> {
                        if (unit.rowGuid == evaluation.clinicalUnitId)
                            evaluation.clinicalUnitByClinicalUnitId = unit
                    }
                    is NonClinicalUnit -> {
                        if (unit.rowGuid == evaluation.nonClinicalUnitId)
                            evaluation.nonClinicalUnitByNonClinicalUnitId = unit
                    }
                }
            }

        }

        try {

            if (insertedEntities.isNotEmpty()) {
                var callPostData = remoteRepository.postEvaluations(insertedEntities)
                val response = callPostData.execute()
                if (!response.isSuccessful) {
                    response.errorBody()?.let {
                        syncResult.add(Triple(ClinicalUnit::class.java.simpleName, Constants.STATUS_ERROR, it.string()))
                    }
                } else {
                    response.body()?.let {
                        if (it.isNotEmpty()) {
                            insertedEntities.forEach {
                                it.status = EnumUtil.EntityStatus.SYNCED
                                localRepository.updateEntity(it)
                            }
                        }
                    }
                }
            }

            val deletedEntities = evaluations.filter { evaluation ->
                evaluation.status == EnumUtil.EntityStatus.DELETE
            }

            if (deletedEntities.isNotEmpty()) {
                var callPostData = remoteRepository.deleteEvaluation(deletedEntities.map { it.rowGuid })
                val response = callPostData.execute()
                if (!response.isSuccessful) {
                    response.errorBody()?.let {
                        syncResult.add(Triple(ClinicalUnit::class.java.simpleName, Constants.STATUS_ERROR, it.string()))
                    }
                } else {
                    response.body()?.let {
                        deletedEntities.forEach {
                            localRepository.deleteEntity(it)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            syncResult.add(Triple(ClinicalUnit::class.java.simpleName, Constants.STATUS_ERROR, e.toString()))
        }

    }


    override fun login(userName: String, password: String, mImei: String): Single<User> {

        var installationId = PreferencesUtil.getString(Constants.KEY_INSTALLATION_ID, application)

        installationId?.let {
            if (it.isEmpty()) {
                installationId = GeneralUtil.generateGuid()
                PreferencesUtil.putString(Constants.KEY_INSTALLATION_ID, installationId, application)

            }
        }

        return remoteRepository.login(userName, password, installationId, mImei).map {
            updateUserInfo(it)
            it
        }

    }


    override fun logout(mImei: String): Single<Boolean> {
        return remoteRepository.logout(mImei).map {
            it
        }
    }

    override fun getUserInfo(): Single<User> {
        return remoteRepository.getUserInfo().map {
            updateUserInfo(it)
            it
        }
    }

    private fun updateUserInfo(it: User) {
        PreferencesUtil.putBoolean(Constants.USER_ROLE_EVALUATOR, it.authorities?.any { it.authority == Constants.USER_ROLE_EVALUATOR }, application)
        PreferencesUtil.putBoolean(Constants.USER_ROLE_SURVERYOR, it.authorities?.any { it.authority == Constants.USER_ROLE_SURVERYOR }, application)
        PreferencesUtil.putString(Constants.DB_NAME, it.userName, application)

    }


    override fun getDataFromRemote(): Observable<Response<List<Hospital>>>? {

        val list = Arrays.asList(
                remoteRepository.getHospitals(),
                remoteRepository.getQuestionCategories(),
                remoteRepository.getQuestions(),
                remoteRepository.getUnitCategories(),
                remoteRepository.getBuildings(),
                remoteRepository.getAllClinicalUnits4User(),
                remoteRepository.getAllNonClinicalUnits4User(),
                remoteRepository.getAllSurgeryDeps4User(),
                remoteRepository.getAllEvaluations4User(),
                remoteRepository.getAttachments(),
                remoteRepository.getSettings(),
                entityService.getCandidateLocations(getToken()),
                entityService.getEntrances(getToken()),
                entityService.getHospitalComments(getToken())
        )

        return Observable.combineLatest(list) {


            val hospitals = it[0] as Response<List<Hospital>>

            val questionCategories = it[1] as List<QuestionCategory>
            val questions = it[2] as List<Question>
            val unitCategories = it[3] as List<UnitCategory>
            val buildings = it[4] as List<Building>
            val clinicalUnit = it[5] as List<ClinicalUnit>
            val nonclinicalUnit = it[6] as List<NonClinicalUnit>
            val surgericalUnit = it[7] as List<SurgeryDepartment>
            val evaluations = it[8] as List<Evaluation>
            val attachments = it[9] as List<Attachment>

            val settings = it[10] as List<Setting>

            val candidateLocations = it[11] as List<CandidateLocation>

            val entrances = it[12] as List<HospitalEntrance>

            val hospitalComments = it[13] as List<HospitalComments>



            localRepository.insertSettings(settings)


            localRepository.insertHospitals(hospitals.body()!!)
            localRepository.insertQuestionCategories(questionCategories)
            localRepository.insertQuestions(questions)
            localRepository.insertUnitCategories(unitCategories)
            localRepository.insertBuildings(buildings)

            localRepository.crudCandidateLocations(candidateLocations)
            localRepository.crudEntrances(entrances)

            localRepository.crudHospitalComments(hospitalComments)

            localRepository.insertNonClinicalUnit(nonclinicalUnit)
            localRepository.insertClinicalUnit(clinicalUnit)
            localRepository.insertSurgericalUnit(surgericalUnit)

            if (hospitals.body()?.isNotEmpty()!!) {
                localRepository.applyEvaluations(evaluations)


                attachments.forEach {
                    it.status = EnumUtil.EntityStatus.SYNCED
                }
                localRepository.applyAttachments(attachments)

            }


            hospitals


        }
    }


    @SuppressLint("CheckResult")
    override fun getHospitals(): Flowable<Array<Hospital>> {
        return localRepository.getHospitals()
    }

//    override fun getHospitalsWitComments(): Flowable<Array<HospitalWithComment>> {
//        return localRepository.getHospitalsWithComment()
//    }

    override fun getUnitCategories(): LiveData<List<UnitCategoryWithSub>> {
        return localRepository.getUnitCategories()
    }

    override fun updateEntity(baseClass: BaseClass) {
        baseClass.status = EnumUtil.EntityStatus.INSERT
        return localRepository.updateEntity(baseClass)
    }

    override fun setDeleteStatus(baseClass: BaseClass) {
        baseClass.status = EnumUtil.EntityStatus.DELETE
        localRepository.updateEntity(baseClass)
    }

    override fun insertEntity(baseClass: BaseClass) {
        baseClass.status = EnumUtil.EntityStatus.INSERT
        return localRepository.insertEntity(baseClass)
    }

    override fun deleteEntityPermanently(baseClass: BaseClass) {
        localRepository.deleteEntity(baseClass)
    }

    override fun deleteEntity(baseClass: BaseClass) {
        baseClass.status = EnumUtil.EntityStatus.DELETE
        localRepository.updateEntity(baseClass)

    }

    override fun getUnits(hospitalId: Int): Flowable<List<BaseClass>> {
        return localRepository.getUnits(hospitalId)
    }

    override fun getQuestionCategoryWithQuestionAndEvaluations(moduleName: String, building: Building?, unit: Any?): Flowable<List<QuestionCategoryWithQuestion>> {
        return localRepository.getQuestionCategoryWithQuestionAndEvaluations(moduleName, building, unit)
    }

    override fun getQuestion(questionId: Int): Question {
        return localRepository.getQuestion(questionId)
    }

    override fun getBuildings(hospitalId: Int): List<Building> {
        return localRepository.getBuildings(hospitalId)
    }


    override fun getCandiadateLocations(hospitalId: Int): List<CandidateLocation> {
        return localRepository.getCandiadateLocations(hospitalId)
    }

    override fun getHospitalEntrances(hospitalId: Int): List<HospitalEntrance> {
        return localRepository.getHospitalEntrances(hospitalId)
    }

    override fun getLastBuilding(): Building? {
        return localRepository.getLastBuilding()
    }

    override fun getLastCandidateLocation(): CandidateLocation? {
        return localRepository.getLastCandidateLocation()
    }

    override fun getLastHospitalEntrance(): HospitalEntrance? {
        return localRepository.getLastHospitalEntrance()
    }

    override fun getLastEvaluation(): Evaluation? {
        return localRepository.getLastEvaluation()
    }

    override fun findHospitalEntrance(id: Int): HospitalEntrance? {
        return localRepository.findHospitalEntrance(id)
    }

    override fun findCandidateLocation(id: Int): CandidateLocation? {
        return localRepository.findCandidateLocation(id)
    }

    override fun findBuilding(id: Int): Building? {
        return localRepository.findBuilding(id)
    }

    override fun findHospitalById(id: Int): Hospital? {
        return localRepository.findHospitalById(id)
    }

    override fun getEvaluations4SurgeryDepartment(moduleName: String, unitId: String): Flowable<List<Any>> {
        return localRepository.getEvaluations4SurgeryDepartment(moduleName, unitId)
    }

    override fun getEvaluations4NonClinicalUnit(moduleName: String, unitId: String): Flowable<List<Any>> {
        return localRepository.getEvaluations4NonClinicalUnit(moduleName, unitId)
    }

    override fun getEvaluations4ClinicalUnit(moduleName: String, unitId: String): Flowable<List<Any>> {
        return localRepository.getEvaluations4ClinicalUnit(moduleName, unitId)
    }

    override fun getBuildingsFlowable(hospitalId: Int, moduleName: String): Flowable<List<BuildingWithUnits>>? {
        return localRepository.getBuildingsFlowable(hospitalId, moduleName)
    }

    override fun getAttachments(category: String, linkedId: String): Flowable<List<Attachment>> {
        return localRepository.getAttachments(category, linkedId)
    }

    override fun getQuestionsWithEvaluation4Hazard(buildingId: String, moduleName: String): Flowable<List<Any>> {
        return localRepository.getQuestionsWithEvaluation4Hazard(buildingId, moduleName)
    }

    override fun getEvaluations4Building(moduleName: String, buildingId: String): Flowable<List<Any>> {
        return localRepository.getEvaluations4Building(moduleName, buildingId)
    }

    override fun evaluationExists(evaluation: Evaluation): Boolean {
        return localRepository.evaluationExists(evaluation)
    }

    override fun findBuilding(id: String): Building? {
        return localRepository.findBuilding(id)
    }

    override fun deleteAttachments4Feature(rowGuid: String) {
        localRepository.deleteAttachments4Feature(rowGuid)
    }

    override fun getQuestionsWithCategoryAndEvaluations(moduleName: String, building: Building?, unit: Any?): Flowable<List<Question>> {
        return localRepository.getQuestionsWithCategoryAndEvaluations(moduleName, building, unit, PreferencesUtil.getCurrentLanguage(application))
    }

    override fun getAllSettings(): List<Setting> {
        return localRepository.getAllSettings()
    }


    override fun getSetting(key: String): Setting {
        return localRepository.getSetting(key)
    }

    override fun getHospitalCommentByHospital(hospitalId: Int): Single<List<HospitalComments>> {
        return localRepository.getHospitalCommentByHospital(hospitalId)
    }

    fun getToken(): String {
        return PreferencesUtil.getString(Constants.TOKEN, application)

    }
}