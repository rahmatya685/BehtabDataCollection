package ir.zngis.behtabdatacollection.repository.remote.serializer

import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import ir.zngis.behtabdatacollection.baseBinding.BindableDate
import java.lang.reflect.Type
import java.util.*

//class BindableDateSerializer:JsonSerializer<BindableDate> {
//    override fun serialize(src: BindableDate?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
//        var element= JsonPrimitive(Date())
//        src?.let {
//            element = JsonPrimitive(it.value)
//        }
//        return element
//    }
//}