package ir.zngis.behtabdatacollection.repository.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.edsab.pm.dao.BaseDao
import io.reactivex.Flowable
import ir.zngis.behtabdatacollection.repository.dataModel.ClinicalUnit
import ir.zngis.behtabdatacollection.repository.dataModel.NonClinicalUnit
import ir.zngis.behtabdatacollection.repository.dataModel.Pair


@Dao
interface NonClinicalUnitDao : BaseDao<NonClinicalUnit> {


    @Query("SELECT * FROM NON_CLINICAL_UNIT  WHERE  ROW_GUID = :id")
    fun findNonClinicalUnit(id:String): NonClinicalUnit?

    @Query("SELECT * FROM NON_CLINICAL_UNIT WHERE BUILDING_ID   IN (select BUILDING.ROW_GUID from BUILDING where BUILDING.HOSPITAL_ID = :hospitalId)")
    fun getNonClinicalUnit(hospitalId: Int): Flowable<List<NonClinicalUnit>>


    @Query("SELECT * FROM NON_CLINICAL_UNIT WHERE BUILDING_ID = :buildingId ")
    fun getNonClinicalUnitByBuilding(buildingId: String): Flowable<List<NonClinicalUnit>>

    @Query("  select EVALUATION.NON_CLINICAL_UNIT_ID   as first,count(distinct(EVALUATION.QUESTION_ID))  as  second  from EVALUATION inner join ( select *,NON_CLINICAL_UNIT.ROW_GUID as S_ID from NON_CLINICAL_UNIT inner join BUILDING on NON_CLINICAL_UNIT.BUILDING_ID = BUILDING.ROW_GUID where BUILDING.HOSPITAL_ID  = :hospitalId) as T   on T.S_ID  =EVALUATION.NON_CLINICAL_UNIT_ID  AND EVALUATION.QUESTION_ID IN ( select QUESTION.ID from QUESTION inner join QUESTION_CATEGORY on QUESTION_CATEGORY.ID =QUESTION.CATEGORY_ID where QUESTION_CATEGORY.MODULE_NAME  = :moduleName) and EVALUATION.HAS_OBSERVED == :observeStatus group by EVALUATION.NON_CLINICAL_UNIT_ID")
    fun getCountEvaluations(hospitalId: Int, moduleName: String, observeStatus: Int): Flowable<List<Pair>>

    @Query("SELECT * FROM NON_CLINICAL_UNIT")
    fun getNonClinicalUnits(): List<NonClinicalUnit>


    @Query("SELECT * FROM NON_CLINICAL_UNIT where NON_CLINICAL_UNIT.STATUS = :status")
    fun getAllNonClinicalUnit(status: Int): Flowable<List<NonClinicalUnit>>


//    @Query("select  count( distinct EVALUATION.QUESTION_ID )  from EVALUATION where EVALUATION.QUESTION_ID in (   select QUESTION.ID FROM QUESTION join QUESTION_CATEGORY on QUESTION.CATEGORY_ID = QUESTION_CATEGORY.ID  where QUESTION_CATEGORY.MODULE_NAME = :module ) and EVALUATION.NON_CLINICAL_UNIT_ID = :unitId and EVALUATION.HAS_OBSERVED =1")
//    fun getRemainedQuestionCount(module:String,unitId:Int):Int
}