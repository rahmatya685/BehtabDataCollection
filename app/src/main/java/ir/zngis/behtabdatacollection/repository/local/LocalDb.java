package ir.zngis.behtabdatacollection.repository.local;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import ir.zngis.behtabdatacollection.repository.local.dao.*;
import ir.zngis.behtabdatacollection.repository.dataModel.*;


@Database(entities = {Attachment.class, Building.class, CandidateLocation.class,
        ClinicalUnit.class, Evaluation.class, Hospital.class, HospitalEntrance.class,
        NonClinicalUnit.class, Question.class,QuestionCategory.class, SurgeryDepartment.class, UnitCategory.class
        , UnitSubCategory.class,Setting.class,HospitalComments.class}, version = LocalDb.EXISTING_VERSION)
@TypeConverters(Converter.class)
public abstract class LocalDb extends RoomDatabase {

    public static String DB_NAME = "Db_";

    public static final int EXISTING_VERSION = 3;

    public abstract AttachmentDao attachmentDao();

    public abstract BuildingDao buildingDao();

    public abstract CandidateLocationDao candidateLocationDao();

    public abstract ClinicalUnitDao clinicalUnitDao();

    public abstract EvaluationDao evaluationDao();

    public abstract HospitalDao hospitalDao();

    public abstract HospitalEntranceDao hospitalEntranceDao();

    public abstract NonClinicalUnitDao nonClinicalUnitDao();

    public abstract QuestionDao questionDao();

    public abstract SurgeryDepartmentDao surgeryDepartmentDao();

    public abstract UnitCategoryDao unitCategoryDao();

    public abstract UnitSubCategoryDao unitSubCategoryDao();

    public abstract QuestionCategoryDao questionCategoryDao();

    public abstract SettingDao settingDao();

    public abstract HospitalCommentsDao hospitalCommentsDao();
}
