package ir.zngis.behtabdatacollection.repository.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.edsab.pm.dao.BaseDao
import ir.zngis.behtabdatacollection.repository.dataModel.Setting

@Dao
interface SettingDao:BaseDao<Setting> {


    @Query("select * from SETTING")
    fun getAllSettings():List<Setting>

    @Query("select * from SETTING where ROW_GUID = :key")
    fun getSettingByKey(key:String):Setting
}