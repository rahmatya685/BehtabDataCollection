package ir.zngis.behtabdatacollection.repository.remote.deserializer

 import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
 import ir.zngis.geojson.Polygon
 import org.json.JSONObject
import java.lang.reflect.Type

class PolygonDeserializer : JsonDeserializer<Polygon> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Polygon {
        var retVal = Polygon()

        json?.let {
            retVal = Polygon(JSONObject(json.toString()))
        }

        return retVal
    }
}