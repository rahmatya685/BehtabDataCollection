package ir.zngis.behtabdatacollection.repository.remote

import android.app.Application
import io.reactivex.Observable
import io.reactivex.Single
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.repository.dataModel.*
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.PreferencesUtil
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.Pair


@Singleton
class EntityApiImpl @Inject constructor(private val entityService: EntityService, val application: Application) : EntityApi {


    fun getToken(): String {
        val token = PreferencesUtil.getString(Constants.TOKEN, application)

        return token;

    }


    override fun getAllSurgeryDeps4User(): Observable<List<SurgeryDepartment>> {

        return entityService.getAllSurgeryDeps4User(getToken())
    }

    override fun getAllNonClinicalUnits4User(): Observable<List<NonClinicalUnit>> {
        return entityService.getAllNonClinicalUnits4User(getToken())
    }

    override fun getAllClinicalUnits4User(): Observable<List<ClinicalUnit>> {
        return entityService.getAllClinicalUnits4User(getToken())
    }

    override fun getAllEvaluations4User(): Observable<List<Evaluation>> {
        return entityService.getAllEvaluations4User(getToken())
    }


    override fun getBuildings(): Observable<List<Building>> {
        return entityService.getBuildings(getToken())
    }

    override fun postBuildings2(buildings: List<Building>, hospitalId: Int): Observable<List<Building>> {
        return entityService.postBuildings2(getToken(), buildings, hospitalId)
    }

    override fun postBuildings(buildings: List<Building>, hospitalId: Int): Call<List<Building>> {
        return entityService.postBuildings(getToken(), buildings, hospitalId)
    }

    override fun deleteBuilding(buildingIds: List<String>, hospitalId: Int): Call<Boolean> {
        return entityService.deleteBuilding(getToken(), buildingIds, hospitalId)
    }

    override fun postSurgeryDepartments(surgeryDepartmensts: List<SurgeryDepartment>, hospitalId: Int, buildingId: String): Call<List<SurgeryDepartment>> {
        return entityService.postSurgerDepartments(getToken(), surgeryDepartmensts, hospitalId, buildingId)
    }

    override fun deleteSurgeryDepartments(ids: List<String>, hospitalId: Int, buildingId: String): Call<Boolean> {
        return entityService.deleteSurgerDepartments(getToken(), ids, hospitalId, buildingId)
    }

    override fun postClinicalUnit(entities: List<ClinicalUnit>, hospitalId: Int, buildingId: String): Call<List<ClinicalUnit>> {
        return entityService.postClinicalUnit(getToken(), entities, hospitalId, buildingId)
    }

    override fun deleteClinicalUnit(ids: List<String>, hospitalId: Int, buildingId: String): Call<Boolean> {
        return entityService.deleteClinicalUnit(getToken(), ids, hospitalId, buildingId)
    }

    override fun postNonClinicalUnit(entities: List<NonClinicalUnit>, hospitalId: Int, buildingId: String): Call<List<NonClinicalUnit>> {
        return entityService.postNonClinicalUnit(getToken(), entities, hospitalId, buildingId)
    }

    override fun deleteNonClinicalUnit(ids: List<String>, hospitalId: Int, buildingId: String): Call<Boolean> {
        return entityService.deleteNonClinicalUnit(getToken(), ids, hospitalId, buildingId)
    }


    override fun postEvaluations(evaluations: List<Evaluation>): Call<List<Evaluation>> {
        return entityService.postEvaluations(getToken(), evaluations)
    }

    override fun deleteEvaluation(ids: List<String>): Call<ResponseBody> {
        return entityService.deleteEvaluation(getToken(), ids)
    }

    override fun postHospitals(hospitals: Array<Hospital>): Observable<Response<List<Hospital>>> {
        return entityService.postHospitals(getToken(), hospitals)
    }


    override fun getUnitCategories(): Observable<List<UnitCategory>> {
        return entityService.getUintCategories(getToken())
    }

    override fun getQuestionCategories(): Observable<List<QuestionCategory>> {
        return entityService.getQuestionCategories(getToken())
    }


    override fun getQuestions(): Observable<List<Question>> {
        return entityService.getQuestions(getToken())
    }

    override fun getHospitals(): Observable<Response<List<Hospital>>> {
        return entityService.getHospitals(getToken())
    }


    override fun logout(mImei: String): Single<Boolean> {
        return Single.create { subscriber ->
            val callResponse = entityService.logout(getToken(), mImei)
            try {
                val response = callResponse.execute()

                if (response.isSuccessful) {
                    subscriber.onSuccess(true)
                } else {
                    when (response.code()) {
                        404 -> {
                            subscriber.onError(Throwable(application.getString(R.string.msg_error_connecting_server)))
                        }
                        else -> subscriber.onError(Throwable(response.errorBody().toString()))
                    }


                }
            } catch (e: java.lang.Exception) {
                subscriber.onError(e)
            }

        }
    }


    override fun login(userName: String, password: String, installationId: String, mImei: String): Single<User> {
        return Single.create { subscriber ->
            val callResponse = entityService.login(userName, password, installationId, mImei)
            val response = callResponse.execute()

            if (response.isSuccessful) {

                val retVal = response.body()?.string()
                retVal?.let {
                    when (it) {
                        Constants.ERROR_USER_NOT_FOUND -> {
                            subscriber.onError(Throwable(application.getString(R.string.msg_error_invalid_user_name_password)))
                        }
                        Constants.ERROR_MORE_THAN_ONE_USER_LOGIN_IN_NOT_ALLOWED -> {
                            subscriber.onError(Throwable(application.getString(R.string.msg_error_login_is_not_allowed_in_more_than_one_device)))
                        }
                        else -> {

                            val token = it

                            val call = entityService.getUserInfo(it)
                            val response = call.execute()
                            response?.let { response ->
                                if (response.isSuccessful) {

                                    response.body()?.let { user ->

                                        user.authorities?.let {

                                            val authority = it.find { it.authority!!.contentEquals(Constants.USER_ROLE_EVALUATOR) || it.authority!!.contentEquals(Constants.USER_ROLE_SURVERYOR) }

                                            if (authority == null) {

                                                if (entityService.logout(token, mImei).execute().isSuccessful) {
                                                    subscriber.onError(Throwable(application.getString(R.string.msg_error_user_authenrtication_failed)))
                                                } else {
                                                    subscriber.onError(Throwable(application.getString(R.string.msg_error_loging_in_system)))
                                                }
                                            } else {
                                                PreferencesUtil.putString(Constants.TOKEN, token, application)
                                                PreferencesUtil.putInteger(Constants.USER_IDENTIFIER, user.id, application)
                                                subscriber.onSuccess(user)
                                            }
                                        }
                                    }
                                } else {
                                    //subscriber.onError(response.errorBody())
                                }
                            }


                        }
                    }
                }
            } else {
                when (response.code()) {
                    404 -> {
                        subscriber.onError(Throwable(application.getString(R.string.msg_error_connecting_server)))
                    }
                    else -> subscriber.onError(Throwable(response.errorBody().toString()))
                }
            }
        }
    }

    override fun getUserInfo(): Single<User> {
        return Single.create { subscriber ->
            val callResponse = entityService.getUserInfo(getToken())

            try {
                val response = callResponse.execute()
                if (response.isSuccessful) {
                    val retVal = response.body()
                    retVal?.let {
                        subscriber.onSuccess(it)
                    }
                } else {
                    when (response.code()) {
                        404 -> {
                            subscriber.onError(Throwable(application.getString(R.string.msg_error_connecting_server)))
                        }
                        401 -> {
                            //token is cleared in db
                            subscriber.onError(Throwable(Constants.MSG_RELOGIN))
                        }
                        else -> subscriber.onError(Throwable(response.errorBody().toString()))
                    }


                }
            } catch (e: Exception) {
                e.message?.let { errorMsg ->
                    if (errorMsg.startsWith("Failed to connect to /", true)) {
                        subscriber.onError(Throwable(Constants.ERROR_SERVER_IS_NOT_AVAILABLE))
                    }
                }

            }

        }
    }

    override fun uploadAttachment(file: MultipartBody.Part, category: RequestBody, linkedId: RequestBody, rowGUID: RequestBody, comment: RequestBody, extension: RequestBody): Single<Attachment> {
        return entityService.uploadAttachment(getToken(), file, category, linkedId, rowGUID, comment, extension)
    }

    override fun deleteAttachments(guids: List<String>): Observable<List<Pair<String, Boolean>>> {
        return entityService.deleteAttachments(getToken(), guids)
    }

    override fun getAttachments(): Observable<List<Attachment>> {
        return entityService.getAttachments(getToken())
    }

    override fun downloadAttachment(fileName: String): Single<ResponseBody> {
        return entityService.downloadFile(getToken(), fileName)
    }

    override fun getSettings(): Observable<List<Setting>> {
        return entityService.getSettings(getToken())
    }

}