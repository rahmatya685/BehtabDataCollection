package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.ColumnInfo
import androidx.room.Entity


@Entity(tableName = "SETTING")
class Setting : BaseClass() {

    @ColumnInfo(name = "ROW_GUID")
    var rowGuid: String? = null
    @ColumnInfo(name = "NAME")
    var name: String? = null
    @ColumnInfo(name = "VALUE")
    var value: String? = null
}