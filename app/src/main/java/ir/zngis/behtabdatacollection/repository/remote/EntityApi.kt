package ir.zngis.behtabdatacollection.repository.remote

import io.reactivex.Observable
import io.reactivex.Single
import ir.zngis.behtabdatacollection.repository.dataModel.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import kotlin.Pair

interface EntityApi {
    fun login(userName: String, password: String, installationId: String, mImei: String): Single<User>
    fun getUserInfo(): Single<User>
    fun logout(mImei: String): Single<Boolean>
    fun getHospitals(): Observable<Response<List<Hospital>>>
    fun getQuestions(): Observable<List<Question>>
    fun getQuestionCategories(): Observable<List<QuestionCategory>>
    fun getUnitCategories(): Observable<List<UnitCategory>>
    fun getBuildings(): Observable<List<Building>>


    fun getAllSurgeryDeps4User(): Observable<List<SurgeryDepartment>>
    fun getAllNonClinicalUnits4User(): Observable<List<NonClinicalUnit>>
    fun getAllClinicalUnits4User(): Observable<List<ClinicalUnit>>
    fun getAllEvaluations4User(): Observable<List<Evaluation>>


    fun postHospitals(hospitals: Array<Hospital>): Observable<Response<List<Hospital>>>
    fun postBuildings2(buildings: List<Building>, hospitalId: Int): Observable<List<Building>>

    fun postBuildings(buildings: List<Building>, hospitalId: Int): Call<List<Building>>


    fun deleteBuilding(ids: List<String>, hospitalId: Int): Call<Boolean>

    fun postSurgeryDepartments(
            buildings: List<SurgeryDepartment>,
            hospitalId: Int,
            buildingId: String
    ): Call<List<SurgeryDepartment>>

    fun deleteSurgeryDepartments(
            ids: List<String>,
            hospitalId: Int,
            buildingId: String
    ): Call<Boolean>


    fun postClinicalUnit(
            entities: List<ClinicalUnit>,
            hospitalId: Int,
            buildingId: String
    ): Call<List<ClinicalUnit>>

    fun deleteClinicalUnit(
            ids: List<String>,
            hospitalId: Int,
            buildingId: String
    ): Call<Boolean>


    fun postNonClinicalUnit(
            entities: List<NonClinicalUnit>,
            hospitalId: Int,
            buildingId: String
    ): Call<List<NonClinicalUnit>>

    fun deleteNonClinicalUnit(
            ids: List<String>,
            hospitalId: Int,
            buildingId: String
    ): Call<Boolean>


    fun postEvaluations(evaluations: List<Evaluation>): Call<List<Evaluation>>

    fun deleteEvaluation(ids: List<String>): Call<ResponseBody>


    fun uploadAttachment(
            file: MultipartBody.Part,
            category: RequestBody,
            linkedId: RequestBody,
            rowGUID: RequestBody,
            comment: RequestBody,
            extension:RequestBody
    ): Single<Attachment>

    fun deleteAttachments(guids:List<String>):Observable<List<Pair<String,Boolean>>>

    fun getAttachments( ):Observable<List<Attachment>>

    fun downloadAttachment(fileName:String):Single<ResponseBody>



    fun getSettings():Observable<List<Setting>>


}