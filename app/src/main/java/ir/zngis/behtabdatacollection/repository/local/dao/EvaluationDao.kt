package ir.zngis.behtabdatacollection.repository.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.edsab.pm.dao.BaseDao
import io.reactivex.Flowable
import ir.zngis.behtabdatacollection.repository.dataModel.Evaluation
import ir.zngis.behtabdatacollection.repository.dataModel.EvaluationWithQuestion

@Dao
interface EvaluationDao : BaseDao<Evaluation> {


    @Query("select EVALUATION.*,QUESTION.BRIEF_DESCRIPTION,QUESTION.BRIEF_DESCRIPTION_ENG ,QUESTION.CATEGORY_ID,QUESTION.ID AS Q_ID  ,QUESTION.HELP_LINK ,QUESTION.QUESTION_TITLE,QUESTION.QUESTION_TITLE_ENG ,QUESTION.WEIGHT   from EVALUATION INNER  join QUESTION on QUESTION.ID= EVALUATION.QUESTION_ID where  EVALUATION.CLINICAL_UNIT_ID  = :unitId  AND   QUESTION.CATEGORY_ID  IN (select QUESTION_CATEGORY.ID from QUESTION_CATEGORY where QUESTION_CATEGORY.MODULE_NAME = :moduleName) order by QUESTION.QUESTION_TITLE_ENG")
    fun getEvaluations4CinicalUnits(moduleName: String, unitId: String): Flowable<List<EvaluationWithQuestion>>

    @Query("select EVALUATION.*,QUESTION.BRIEF_DESCRIPTION,QUESTION.BRIEF_DESCRIPTION_ENG ,QUESTION.CATEGORY_ID  ,QUESTION.ID AS Q_ID ,QUESTION.HELP_LINK ,QUESTION.QUESTION_TITLE,QUESTION.QUESTION_TITLE_ENG ,QUESTION.WEIGHT   from EVALUATION INNER  join QUESTION on QUESTION.ID= EVALUATION.QUESTION_ID where  EVALUATION.NON_CLINICAL_UNIT_ID  = :unitId  AND   QUESTION.CATEGORY_ID  IN (select QUESTION_CATEGORY.ID from QUESTION_CATEGORY where QUESTION_CATEGORY.MODULE_NAME = :moduleName) order by QUESTION.QUESTION_TITLE_ENG")
    fun getEvaluations4NonCinicalUnits(moduleName: String, unitId: String): Flowable<List<EvaluationWithQuestion>>

    @Query("select EVALUATION.*,QUESTION.BRIEF_DESCRIPTION,QUESTION.BRIEF_DESCRIPTION_ENG ,QUESTION.CATEGORY_ID ,QUESTION.ID AS Q_ID  ,QUESTION.HELP_LINK ,QUESTION.QUESTION_TITLE,QUESTION.QUESTION_TITLE_ENG ,QUESTION.WEIGHT   from EVALUATION INNER  join QUESTION on QUESTION.ID= EVALUATION.QUESTION_ID where  EVALUATION.SURGERY_DEPARTMENT_ID  = :unitId  AND   QUESTION.CATEGORY_ID  IN (select QUESTION_CATEGORY.ID from QUESTION_CATEGORY where QUESTION_CATEGORY.MODULE_NAME = :moduleName) order by QUESTION.QUESTION_TITLE_ENG")
    fun getEvaluations4SurgericalDeps(moduleName: String, unitId: String): Flowable<List<EvaluationWithQuestion>>

    @Query("select EVALUATION.*,QUESTION.BRIEF_DESCRIPTION,QUESTION.BRIEF_DESCRIPTION_ENG ,QUESTION.CATEGORY_ID,QUESTION.ID AS Q_ID  ,QUESTION.HELP_LINK ,QUESTION.QUESTION_TITLE ,QUESTION.QUESTION_TITLE_ENG,QUESTION.WEIGHT   from EVALUATION INNER  join QUESTION on QUESTION.ID= EVALUATION.QUESTION_ID where  EVALUATION.BUILDING_ID  = :buildingId  AND   QUESTION.CATEGORY_ID  IN (select QUESTION_CATEGORY.ID from QUESTION_CATEGORY where QUESTION_CATEGORY.MODULE_NAME = :moduleName) order by QUESTION.QUESTION_TITLE_ENG")
    fun getEvaluations4Building(moduleName: String, buildingId: String): Flowable<List<EvaluationWithQuestion>>

    @Query("select * from EVALUATION where BUILDING_ID = :buildingId")
    fun getEvaluations4Hazard(buildingId:String):Flowable<List<Evaluation>>

    @Query("select * from EVALUATION order by ID desc limit 1")
    fun getLastEvaluation(): Evaluation?

    @Query("select * from EVALUATION  WHERE EVALUATION.STATUS != 3")
    fun getUnsyncedEvaluations():List<Evaluation>

    @Query("select COUNT(*) from EVALUATION  WHERE EVALUATION.ID != :id")
    fun evaluationExists(id: Int): Int

    @Query("select * from EVALUATION")
    fun getAllEvaluations(): List<Evaluation>
}