package ir.zngis.behtabdatacollection.repository.local


import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.room.migration.Migration
import java.lang.Exception

class MigrationFromV3_ToV4 : Migration(3, 5) {

    override fun migrate(database: SupportSQLiteDatabase) {
        try {


            database.execSQL("CREATE TABLE `sqlitebrowser_rename_column_new_table` (\n" +
                    "\t`LINK_ID`\tTEXT NOT NULL,\n" +
                    "\t`NAME`\tTEXT NOT NULL,\n" +
                    "\t`EXTENSION`\tTEXT NOT NULL,\n" +
                    "\t`PATH`\tTEXT,\n" +
                    "\t`CATEGORY`\tTEXT NOT NULL,\n" +
                    "\t`FILE_DOWNLOAD_URI`\tTEXT NOT NULL,\n" +
                    "\t`COMMENT`\tTEXT  ,\n" +
                    "\t`ROW_GUID`\tTEXT NOT NULL,\n" +
                    "\t`ID`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                    "\t`STATUS`\tINTEGER,\n" +
                    "\t`MODIFY_DATE`\tTEXT\n" +
                    ");")

            database.execSQL("INSERT INTO sqlitebrowser_rename_column_new_table SELECT `LINK_ID`,`NAME`,`EXTENSION`,`PATH`,`CATEGORY`,`FILE_DOWNLOAD_URI`,`COMMENT`,`ROW_GUID`,`ID`,`STATUS`,`MODIFY_DATE` FROM `ATTACHMENT`;")

            database.execSQL("DROP TABLE `ATTACHMENT`;")

            database.execSQL("ALTER TABLE `sqlitebrowser_rename_column_new_table` RENAME TO `ATTACHMENT`;")

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}