package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.ColumnInfo
import androidx.room.Entity
import ir.zngis.behtabdatacollection.baseBinding.BindableString
import ir.zngis.behtabdatacollection.util.GeneralUtil
import ir.zngis.geojson.Point

@Entity(tableName = "HOSPITAL_ENTRANCE")
class HospitalEntrance(
        @ColumnInfo(name = "NAME")
        var name: BindableString = BindableString(""),
        @ColumnInfo(name = "HOSPITAL_ID", typeAffinity = ColumnInfo.INTEGER)
        var hospitalId: Int = -1,
        @ColumnInfo(name = "TYPE", typeAffinity = ColumnInfo.INTEGER)
        var type: Int? = null,
        @ColumnInfo(name = "ACCESS_POINT", typeAffinity = ColumnInfo.TEXT)
        var accessPoint: Point? = null,
        @ColumnInfo(name = "ROW_GUID", typeAffinity = ColumnInfo.TEXT)
        var rowGuid: String = GeneralUtil.generateGuid()
) : BaseClass(), Cloneable {


    public override fun clone(): HospitalEntrance {
        var cloned = super.clone() as HospitalEntrance

        cloned.name = this.name.clone() as BindableString

        cloned.accessPoint = this.accessPoint

        return cloned

    }
}