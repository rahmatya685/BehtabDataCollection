package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import android.os.Parcelable
import ir.zngis.behtabdatacollection.baseBinding.BindableDouble
import ir.zngis.behtabdatacollection.baseBinding.BindableInt
import ir.zngis.behtabdatacollection.baseBinding.BindableString
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.geojson.Point
import ir.zngis.geojson.Polygon
import kotlinx.android.parcel.Parcelize


@Parcelize
@Entity(tableName = "HOSPITAL")
class Hospital(

        @ColumnInfo(name = "NAME")
        var name: BindableString = BindableString(""),
        @ColumnInfo(name = "ADDRESS")
        var address: BindableString? = null,
        @ColumnInfo(name = "BOUNDARY")
        var boundary: Polygon? = null,
        @ColumnInfo(name = "PHONE")
        var phone: BindableString? = null,
        @ColumnInfo(name = "FAX")
        var fax: BindableString? = null,
        @ColumnInfo(name = "WEB_SITE")
        var webSite: BindableString? = null,
        @ColumnInfo(name = "NUM_TOTAL_APPROVED_BEDS", typeAffinity = ColumnInfo.INTEGER)
        var numTotalApprovedBeds: BindableInt? = null,
        @ColumnInfo(name = "NUM_TOTAL_ACTIVE_BEDS", typeAffinity = ColumnInfo.INTEGER)
        var numTotalActiveBeds: BindableInt? = null,
        @ColumnInfo(name = "BED_OCCUPANCY_RATE_NORM", typeAffinity = ColumnInfo.REAL)
        var bedOccupancyNormalRate: BindableDouble? = null,
        @ColumnInfo(name = "TOTAL_STAFF_COUNT", typeAffinity = ColumnInfo.INTEGER)
        var totalStaffCount: BindableInt? = null,
        @ColumnInfo(name = "CLINICAL_STAFF_COUNT", typeAffinity = ColumnInfo.INTEGER)
        var clinicalStaffCount: BindableInt? = null,
        @ColumnInfo(name = "NON_CLINICAL_STAFF_COUNT", typeAffinity = ColumnInfo.INTEGER)
        var nonClinicalStaffCount: BindableInt? = null,
        @ColumnInfo(name = "SENIOR_MANAGERS_NAME")
        var seniorManagersName: BindableString? = null,
        @ColumnInfo(name = "CRISIS_MANAGERS_NAMES_AND_PHONES")
        var crisisManagersNamesAndPhones: BindableString? = null,
        @ColumnInfo(name = "ORGANIZATIONAL_AFFILIATION", typeAffinity = ColumnInfo.INTEGER)
        var organizationalAffiliation: BindableInt? = null,
        @ColumnInfo(name = "SERVICE_TYPE", typeAffinity = ColumnInfo.INTEGER)
        var serviceType: BindableInt? = null,
        @ColumnInfo(name = "ROLE_IN_HEALTHCARE_NETWORK", typeAffinity = ColumnInfo.INTEGER)
        var roleInHealthcareNetwork: BindableInt? = null,
        @ColumnInfo(name = "ROLE_IN_CRISIS_AND_DISASTERS", typeAffinity = ColumnInfo.INTEGER)
        var roleInCrisisAndDisasters: BindableInt? = null,
        @ColumnInfo(name = "STRUCTURE_TYPE", typeAffinity = ColumnInfo.TEXT)
        var structureType: BindableString? = null,
        @ColumnInfo(name = "SERVED_POPULATION", typeAffinity = ColumnInfo.INTEGER)
        var servedPopulation: BindableInt? = null,
        @ColumnInfo(name = "INFLUENCE_AREA", typeAffinity = ColumnInfo.TEXT)
        var influenceArea: Polygon? = null,
        @ColumnInfo(name = "LAST_EVALUATION_RATING", typeAffinity = ColumnInfo.REAL)
        var lastEvaluationRating: BindableDouble? = null,
//        @ColumnInfo(name = "OBSERVER_L2", typeAffinity = ColumnInfo.INTEGER)
        @Ignore
        var observer_l2: BindableInt? = null,
//        @ColumnInfo(name = "OBSERVER_L2_COMMENT")
        @Ignore
        var observerL2Comment: BindableString? = null,
        @ColumnInfo(name = "evaluator_user_id", typeAffinity = ColumnInfo.INTEGER)
        var evaluatorUserId: Int? = null,
        @ColumnInfo(name = "surveyor_user_id", typeAffinity = ColumnInfo.INTEGER)
        var surveyorUserId: Int? = null,
        @ColumnInfo(name = "CENTER_POINT", typeAffinity = ColumnInfo.TEXT)
        var centerPoint: Point? = null,

        @ColumnInfo(name = "TYPE", typeAffinity = ColumnInfo.INTEGER)
        var healthLocationType: EnumUtil.HealthLocationType? =null


) : BaseClass(), Parcelable, Cloneable {


    public override fun clone(): Hospital {
        var cloned = super.clone() as Hospital
        cloned.address = address?.clone() as BindableString
        cloned.bedOccupancyNormalRate = bedOccupancyNormalRate?.clone() as BindableDouble
        cloned.clinicalStaffCount = clinicalStaffCount?.clone() as BindableInt
        cloned.crisisManagersNamesAndPhones = crisisManagersNamesAndPhones?.clone() as BindableString
        cloned.fax = fax?.clone() as BindableString
        cloned.lastEvaluationRating = lastEvaluationRating?.clone() as BindableDouble
        cloned.name = name.clone() as BindableString
        cloned.nonClinicalStaffCount = nonClinicalStaffCount?.clone() as BindableInt
        cloned.numTotalActiveBeds = numTotalActiveBeds?.clone() as BindableInt
        cloned.numTotalApprovedBeds = numTotalApprovedBeds?.clone() as BindableInt
//        cloned.observerL2Comment = observerL2Comment?.clone() as BindableString
//        cloned.observer_l2 = observer_l2?.clone() as BindableInt
        cloned.organizationalAffiliation = organizationalAffiliation?.clone() as BindableInt
        cloned.phone = phone?.clone() as BindableString
        cloned.roleInCrisisAndDisasters = roleInCrisisAndDisasters?.clone() as BindableInt
        cloned.roleInHealthcareNetwork = roleInHealthcareNetwork?.clone() as BindableInt
        cloned.seniorManagersName = seniorManagersName?.clone() as BindableString
        cloned.servedPopulation = servedPopulation?.clone() as BindableInt
        cloned.serviceType = serviceType?.clone() as BindableInt
        cloned.structureType = structureType?.clone() as BindableString
        cloned.totalStaffCount = totalStaffCount?.clone() as BindableInt
        cloned.webSite = webSite?.clone() as BindableString

        return cloned
    }
}