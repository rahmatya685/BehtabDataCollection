package ir.zngis.behtabdatacollection.repository.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.edsab.pm.dao.BaseDao
import ir.zngis.behtabdatacollection.repository.dataModel.HospitalEntrance

@Dao
interface HospitalEntranceDao : BaseDao<HospitalEntrance> {

    @Query("SELECT * FROM HOSPITAL_ENTRANCE  WHERE HOSPITAL_ID  = :hospitalId")
    fun getHospitalEntrances(hospitalId: Int): List<HospitalEntrance>


    @Query("select * from HOSPITAL_ENTRANCE where STATUS != 1 order by ID desc limit 1 ")
    fun getLastHospitalEntrance(): HospitalEntrance?

    @Query("select * from HOSPITAL_ENTRANCE WHERE ID = :id")
    fun findHospitalEntrance(id: Int): HospitalEntrance?

    @Query("SELECT * FROM HOSPITAL_ENTRANCE")
    fun getAllEntrances(): List<HospitalEntrance>

}