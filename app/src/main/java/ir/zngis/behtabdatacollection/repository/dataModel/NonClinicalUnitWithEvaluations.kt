package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Relation

@Entity
class NonClinicalUnitWithEvaluations(
        @Embedded var nonClinicalUnit: NonClinicalUnit = NonClinicalUnit(),
        @Relation(
                parentColumn = "ID",
                entityColumn = "NON_CLINICAL_UNIT_ID"
        ) var evaluations: MutableList<Evaluation> = mutableListOf()
) {

}