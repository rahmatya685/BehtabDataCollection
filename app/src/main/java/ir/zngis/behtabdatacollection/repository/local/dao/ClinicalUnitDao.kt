package ir.zngis.behtabdatacollection.repository.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.edsab.pm.dao.BaseDao
import io.reactivex.Flowable
import ir.zngis.behtabdatacollection.repository.dataModel.ClinicalUnit
import ir.zngis.behtabdatacollection.repository.dataModel.Pair

@Dao
interface ClinicalUnitDao : BaseDao<ClinicalUnit> {

    @Query("SELECT * FROM CLINICAL_UNIT  WHERE  ROW_GUID = :id")
    fun findClinicalUnit(id:String):ClinicalUnit?

    @Query("SELECT * FROM CLINICAL_UNIT WHERE BUILDING_ID  IN (select BUILDING.ROW_GUID from BUILDING where BUILDING.HOSPITAL_ID = :hospitalId)")
    fun getClinicalUnit(hospitalId: Int): Flowable<List<ClinicalUnit>>


    @Query("SELECT * FROM CLINICAL_UNIT WHERE BUILDING_ID  =  :buildingId")
    fun getClinicalUnitByBuilding(buildingId: String): Flowable<List<ClinicalUnit>>

    @Query("select EVALUATION.CLINICAL_UNIT_ID  as first,   count(distinct(EVALUATION.QUESTION_ID))  as second  from EVALUATION inner join ( select *,CLINICAL_UNIT.ROW_GUID as S_ID from CLINICAL_UNIT inner join BUILDING on CLINICAL_UNIT.BUILDING_ID = BUILDING.ROW_GUID where BUILDING.HOSPITAL_ID  = :hospitalId) as T   on T.S_ID  =EVALUATION.CLINICAL_UNIT_ID  AND EVALUATION.QUESTION_ID IN ( select QUESTION.ID from QUESTION inner join QUESTION_CATEGORY on QUESTION_CATEGORY.ID =QUESTION.CATEGORY_ID where QUESTION_CATEGORY.MODULE_NAME  = :moduleName) and EVALUATION.HAS_OBSERVED == :observeStatus  group by EVALUATION.CLINICAL_UNIT_ID")
    fun getCountEvaluations(hospitalId: Int, moduleName: String,observeStatus:Int): Flowable<List<Pair>>

    @Query("SELECT * FROM CLINICAL_UNIT")
    fun getClinicalUnits(): List<ClinicalUnit>

    @Query("SELECT * FROM CLINICAL_UNIT where CLINICAL_UNIT.STATUS = :status")
    fun getAllClinicalUnit(status: Int):  Flowable<List<ClinicalUnit>>


//    @Query("select  count( distinct EVALUATION.QUESTION_ID )  from EVALUATION where EVALUATION.QUESTION_ID in (   select QUESTION.ID FROM QUESTION join QUESTION_CATEGORY on QUESTION.CATEGORY_ID = QUESTION_CATEGORY.ID  where QUESTION_CATEGORY.MODULE_NAME = :module ) and EVALUATION.CLINICAL_UNIT_ID = :unitId and EVALUATION.HAS_OBSERVED =1")
//    fun getRemainedQuestionCount(module:String,unitId:Int):Int
}