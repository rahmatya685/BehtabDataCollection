package ir.zngis.behtabdatacollection.repository.local

import androidx.lifecycle.LiveData
import io.reactivex.Flowable
import io.reactivex.Single
import ir.zngis.behtabdatacollection.repository.dataModel.*
import ir.zngis.behtabdatacollection.util.EnumUtil

interface LocalRepository {

//    fun getHospitalsWithComment(): Flowable<Array<HospitalWithComment>>
    fun getHospitals(): Flowable<Array<Hospital>>
    fun getCommentByHospital(hospitalId:Int): Single<List<HospitalComments>>
    fun getHospitalComments(): Single<List<HospitalComments>>



    fun getUnitCategories(): LiveData<List<UnitCategoryWithSub>>
    fun getUnitCategories2(): Flowable<List<UnitCategoryWithSub>>

    fun updateEntity(baseClass: BaseClass)
    fun insertEntity(baseClass: BaseClass)
    fun deleteEntity(baseClass: BaseClass)

    fun getUnits(hospitalId: Int): Flowable<List<BaseClass>>
    fun getAllUnitsByStatus(status: EnumUtil.EntityStatus): List<BaseClass>

    fun getQuestionCategoryWithQuestionAndEvaluations(moduleName: String, building: Building?, unit: Any?): Flowable<List<QuestionCategoryWithQuestion>>
    fun getQuestion(questionId: Int): Question
    fun getAllQuestions():List<Question>

    fun getBuildings(hospitalId: Int): List<Building>
    fun getBuildings( ): List<Building>

    fun getCandiadateLocations(hospitalId: Int): List<CandidateLocation>
    fun getHospitalEntrances(hospitalId: Int): List<HospitalEntrance>

    fun getLastBuilding(): Building?
    fun getLastCandidateLocation(): CandidateLocation?
    fun getLastHospitalEntrance(): HospitalEntrance?
    fun getLastEvaluation(): Evaluation?

    fun findHospitalEntrance(id: Int): HospitalEntrance?
    fun findCandidateLocation(id: Int): CandidateLocation?
    fun findBuilding(id: Int): Building?

    fun getEvaluations4SurgeryDepartment(moduleName: String, unitId: String): Flowable<List<Any>>
    fun getEvaluations4NonClinicalUnit(moduleName: String, unitId: String): Flowable<List<Any>>
    fun getEvaluations4ClinicalUnit(moduleName: String, unitId: String): Flowable<List<Any>>

    fun getUnsyncedEvaluations(): List<Evaluation>

    fun getBuildingsFlowable(hospitalId: Int, moduleName: String): Flowable<List<BuildingWithUnits>>?
    fun getAttachments(category: String, linkedId: String): Flowable<List<Attachment>>
    fun getAllAttachments():List<Attachment>

    fun getQuestionsWithEvaluation4Hazard(buildingId: String, moduleName: String): Flowable<List<Any>>
    fun getEvaluations4Building(moduleName: String, buildingId: String): Flowable<List<Any>>
    fun insertHospitals(entities: List<Hospital>)
    fun insertQuestions(entities: List<Question>)
    fun insertQuestionCategories(entities: List<QuestionCategory>)
    fun insertUnitCategories(unitCategories: List<UnitCategory>)
    fun applyEvaluations(evaluations: List<Evaluation>)

    fun getSurgeryDepartments(hospitalId: Int): Flowable<List<SurgeryDepartment>>
    fun getNonClinicalUnit(hospitalId: Int): Flowable<List<NonClinicalUnit>>
    fun getClinicalUnit(hospitalId: Int): Flowable<List<ClinicalUnit>>
    fun insertBuildings(buildings: List<Building>)
    fun insertNonClinicalUnit(nonclinicalUnit: List<NonClinicalUnit>)
    fun insertClinicalUnit(clinicalUnit: List<ClinicalUnit>)
    fun insertSurgericalUnit(surgericalUnit: List<SurgeryDepartment>)
    fun evaluationExists(evaluation: Evaluation): Boolean

    fun clearAllTables()
    fun findHospitalById(id: Int): Hospital?
    fun applyAttachments(attachments: List<Attachment>)
    fun findBuilding(id: String): Building?
    fun deleteAttachments4Feature(rowGuid: String)
    fun getQuestionsWithCategoryAndEvaluations(moduleName: String, building: Building?, unit: Any?, currentLanguage: String): Flowable<List<Question>>
    fun getAllSettings(): List<Setting>
    fun insertSettings(settings: List<Setting>)

    fun getSetting(key:String):Setting
    fun crudCandidateLocations(candidateLocations: List<CandidateLocation>)
    fun crudEntrances(entrances: List<HospitalEntrance>)
    fun crudHospitalComments(comments: List<HospitalComments>)

    fun getHospitalCommentByHospital(hospitalId: Int): Single<List<HospitalComments>>
}