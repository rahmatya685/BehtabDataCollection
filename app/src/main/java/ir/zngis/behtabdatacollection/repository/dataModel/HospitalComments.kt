package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import ir.zngis.behtabdatacollection.baseBinding.BindableString
import ir.zngis.behtabdatacollection.util.GeneralUtil
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "HospitalComments",
        foreignKeys = [ForeignKey(entity = Hospital::class,parentColumns = arrayOf("ID"),childColumns = arrayOf("HOSPITAL_ID"))])
class HospitalComments : BaseClass(), Cloneable {

    @ColumnInfo(name = "SUGGESTIONS",typeAffinity = ColumnInfo.TEXT)
    var suggestions: BindableString = BindableString("")

    @ColumnInfo(name = "EMERGENCY_ACTIONS",typeAffinity = ColumnInfo.TEXT)
    var emergencyActions: BindableString = BindableString("")

    @ColumnInfo(name = "HOSPITAL_ID")
    var hospitalId: Int? = null

    @ColumnInfo(name = "ROW_GUID", typeAffinity = ColumnInfo.TEXT)
    var rowGuid: String = GeneralUtil.generateGuid()

    public override fun clone(): HospitalComments {

        val cloned = super.clone() as HospitalComments

        cloned.hospitalId = this.hospitalId

        cloned.suggestions = this.suggestions

        cloned.emergencyActions = this.emergencyActions

        return cloned
    }

}