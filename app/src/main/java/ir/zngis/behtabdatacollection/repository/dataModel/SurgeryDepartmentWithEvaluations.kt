package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Relation

@Entity
class SurgeryDepartmentWithEvaluations (
        @Embedded var surgeryDepartment: SurgeryDepartment       = SurgeryDepartment(),
        @Relation(
                parentColumn = "ID",
                entityColumn = "SURGERY_DEPARTMENT_ID"
        ) var evaluations: MutableList<Evaluation> = mutableListOf()
){
}