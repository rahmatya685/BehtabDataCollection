package ir.zngis.behtabdatacollection.repository.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.edsab.pm.dao.BaseDao
import io.reactivex.Flowable
import ir.zngis.behtabdatacollection.repository.dataModel.Question
import ir.zngis.behtabdatacollection.repository.dataModel.QuestionCategoryWithQuestion
import ir.zngis.behtabdatacollection.repository.dataModel.QuestionWithEvaluation

@Dao
interface QuestionDao : BaseDao<Question> {

    @Query("select count(*) from QUESTION inner join QUESTION_CATEGORY on QUESTION.CATEGORY_ID = QUESTION_CATEGORY.ID\n" +
            " where QUESTION_CATEGORY.MODULE_NAME =:moduleName AND QUESTION.STATUS !=1 and QUESTION.HEALTH_HOUSE =1")
    fun getQuestionCount4HealthHospital(moduleName: String): Flowable<Int>

    @Query("select count(*) from QUESTION inner join QUESTION_CATEGORY on QUESTION.CATEGORY_ID = QUESTION_CATEGORY.ID\n" +
            " where QUESTION_CATEGORY.MODULE_NAME =:moduleName AND QUESTION.STATUS !=1 and QUESTION.HOSPITAL =1")
    fun getQuestionCount4Hospital(moduleName: String): Flowable<Int>


    @Query("select * from QUESTION where ID = :questionId AND QUESTION.STATUS !=1")
    fun getQuestion(questionId: Int): Question

    @Query("select * from QUESTION where QUESTION.STATUS !=1")
    fun getQuestions(): List<Question>

    @Query("select * from QUESTION ")
    fun getQuestionsWithDeletedItems(): List<Question>


    @Query("select * from QUESTION  where QUESTION.STATUS !=1 ")
    fun getQuestionWithEvaluation(): Flowable<List<QuestionWithEvaluation>>


}