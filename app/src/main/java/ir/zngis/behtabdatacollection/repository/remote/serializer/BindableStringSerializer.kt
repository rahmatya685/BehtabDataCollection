package ir.zngis.behtabdatacollection.repository.remote.serializer

import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import ir.zngis.behtabdatacollection.baseBinding.BindableString
import java.lang.reflect.Type

class BindableStringSerializer :JsonSerializer<BindableString> {
    override fun serialize(src: BindableString?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        var element= JsonPrimitive("")
        src?.let {
            element = JsonPrimitive(it.get())
        }
        return element
    }
}