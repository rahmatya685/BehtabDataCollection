package ir.zngis.behtabdatacollection.repository.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.edsab.pm.dao.BaseDao
import io.reactivex.Flowable
import ir.zngis.behtabdatacollection.repository.dataModel.Attachment

@Dao
interface AttachmentDao : BaseDao<Attachment> {

    @Query("select * from ATTACHMENT where CATEGORY = :category and LINK_ID  = :linkedId")
    fun getAttachments(category: String, linkedId: String): Flowable<List<Attachment>>


    @Query("select * from ATTACHMENT ")
    fun getAllAttachments():List<Attachment>


    @Query("delete from ATTACHMENT where LINK_ID = :rowGuid")
    fun deleteAttachments4Feature(rowGuid: String)
}