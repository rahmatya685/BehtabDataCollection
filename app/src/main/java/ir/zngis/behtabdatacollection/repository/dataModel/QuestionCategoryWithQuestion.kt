package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.Relation
import ir.zngis.behtabdatacollection.util.EnumUtil


@Entity
class QuestionCategoryWithQuestion(

        @Embedded var questionCategory: QuestionCategory = QuestionCategory(),
        @Relation(
                parentColumn = "ID",
                entityColumn = "CATEGORY_ID"
        ) var allQuestions: MutableList<Question> = mutableListOf()


) {
    @Ignore
    var questions: MutableList<Question> = mutableListOf()
        get() = allQuestions.filter { it.status != EnumUtil.EntityStatus.DELETE }.toMutableList()
}