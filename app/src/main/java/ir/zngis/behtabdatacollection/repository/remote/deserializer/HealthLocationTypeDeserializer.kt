package ir.zngis.behtabdatacollection.repository.remote.deserializer

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import ir.zngis.behtabdatacollection.util.EnumUtil
import java.lang.reflect.Type

class HealthLocationTypeDeserializer:JsonDeserializer<EnumUtil.HealthLocationType> {

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): EnumUtil.HealthLocationType {

        var retVal = EnumUtil.HealthLocationType.HOSPITAL

        json?.let {
            retVal = EnumUtil.HealthLocationType.toHealthLocationType(json.asInt)!!
        }

        return retVal
    }
}