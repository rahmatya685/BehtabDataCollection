package ir.zngis.behtabdatacollection.repository.local

import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.room.migration.Migration

class MigrationFromV2_ToV3 : Migration(2, 3) {
    override fun migrate(database: SupportSQLiteDatabase) {

        try {
            database.execSQL("alter table HOSPITAL add column TYPE numeric; ")
            database.execSQL("update HOSPITAL set TYPE = 1;")

            database.execSQL("alter table QUESTION add column HOSPITAL numeric;")
            database.execSQL("update  QUESTION set HOSPITAL = 1;")

            database.execSQL("alter table QUESTION add column HEALTH_HOUSE numeric;")
            database.execSQL("update QUESTION set HEALTH_HOUSE =0;")
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }
}