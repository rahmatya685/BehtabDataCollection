package ir.zngis.behtabdatacollection.repository.local

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.LiveData
import android.util.Log
import io.reactivex.*
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function3
import io.reactivex.functions.Function9
import ir.zngis.behtabdatacollection.repository.dataModel.*
import ir.zngis.behtabdatacollection.repository.local.dao.*
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.util.GeneralUtil
import ir.zngis.behtabdatacollection.util.PreferencesUtil
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class LocalRepositoryImpl @Inject constructor(
        var application: Application,
        var localDb: LocalDb,
        var attachmentDao: AttachmentDao,
        var buildingDao: BuildingDao,
        var candidateLocationDao: CandidateLocationDao,
        var clinicalUnitDao: ClinicalUnitDao,
        var evaluationDao: EvaluationDao,
        var hospitalDao: HospitalDao,
        var hospitalEntranceDao: HospitalEntranceDao,
        var nonClinicalUnitDao: NonClinicalUnitDao,
        var questionDao: QuestionDao,
        var surgeryDepartmentDao: SurgeryDepartmentDao,
        var unitCategoryDao: UnitCategoryDao,
        var unitSubCategoryDao: UnitSubCategoryDao,
        var questionCategoryDao: QuestionCategoryDao,
        var settingDao: SettingDao,
        var hospitalCommentsDao: HospitalCommentsDao


) : LocalRepository {


    override fun clearAllTables() {
        localDb.clearAllTables()
    }

    override fun getHospitalComments(): Single<List<HospitalComments>> {
        return hospitalCommentsDao.getAllComments()
    }

    override fun getCommentByHospital(hospitalId: Int): Single<List<HospitalComments>> {
        return hospitalCommentsDao.getCommentByHospital(hospitalId)
    }

    override fun insertUnitCategories(entities: List<UnitCategory>) {
        val updateList = mutableListOf<UnitCategory>()
        val insertList = mutableListOf<UnitCategory>()

        val existingEntities = unitCategoryDao.getUnitCategories2().blockingFirst()

        entities.forEach { receivedItem: UnitCategory ->

            if (receivedItem.unitSubCategories.isNotEmpty()) {
                val exitingEntity = existingEntities.find { it.unitCategory.id == receivedItem.id }

                if (exitingEntity == null) {
                    insertList.add(receivedItem)
                } else {
                    updateList.add(receivedItem)
                }

                receivedItem.unitSubCategories.forEach {
                    it.categoryId = receivedItem.id
                }
            }
        }


        if (updateList.isNotEmpty())
            unitCategoryDao.update(*updateList.toTypedArray())
        if (insertList.isNotEmpty())
            unitCategoryDao.insert(*insertList.toTypedArray())

        insertUnitSubCategories(entities, existingEntities)
    }


    private fun insertUnitSubCategories(entities: List<UnitCategory>, existingEntities: List<UnitCategoryWithSub>?) {
        val receivedUnitSubCategories = entities.flatMap { it.unitSubCategories }
        val existingUnitSubCategories = existingEntities?.flatMap { it.unitSubCategories }


        val updateListSub = mutableListOf<UnitSubCategory>()
        val insertListSub = mutableListOf<UnitSubCategory>()

        receivedUnitSubCategories.forEach { receivedItem: UnitSubCategory ->

            val exitingEntity = existingUnitSubCategories?.find { it.id == receivedItem.id }

            if (exitingEntity == null) {
                insertListSub.add(receivedItem)
            } else {
                updateListSub.add(receivedItem)

            }
        }

        if (updateListSub.isNotEmpty())
            unitSubCategoryDao.update(*updateListSub.toTypedArray())
        if (insertListSub.isNotEmpty())
            unitSubCategoryDao.insert(*insertListSub.toTypedArray())
    }


    @SuppressLint("CheckResult")
    override fun insertQuestionCategories(entities: List<QuestionCategory>) {
        val updateList = mutableListOf<QuestionCategory>()
        val insertList = mutableListOf<QuestionCategory>()

        val existingEntities = questionCategoryDao.getCategories().blockingFirst()

        entities.forEach { receivedItem: QuestionCategory ->
            if (existingEntities.count { it.id == receivedItem.id } == 0) {
                insertList.add(receivedItem)
            } else {
                updateList.add(receivedItem)
            }
        }


        if (updateList.isNotEmpty())
            questionCategoryDao.update(*updateList.toTypedArray())
        if (insertList.isNotEmpty())
            questionCategoryDao.insert(*insertList.toTypedArray())


    }

    override fun insertSettings(settings: List<Setting>) {

        val existingEntities = settingDao.getAllSettings()

        settingDao.delete(*existingEntities.toTypedArray())

        settingDao.insert(*settings.toTypedArray())


    }


    override fun insertQuestions(entities: List<Question>) {

        val updateList = mutableListOf<Question>()
        val insertList = mutableListOf<Question>()

        val existingEntities = questionDao.getQuestionsWithDeletedItems()

        entities.forEach { receivedItem: Question ->
            if (existingEntities.count { it.id == receivedItem.id } == 0) {
                insertList.add(receivedItem)
            } else {
                updateList.add(receivedItem)
            }
        }

        if (updateList.isNotEmpty())
            questionDao.update(*updateList.toTypedArray())
        if (insertList.isNotEmpty())
            questionDao.insert(*insertList.toTypedArray())
    }


    override fun insertSurgericalUnit(entites: List<SurgeryDepartment>) {

        val updateList = mutableListOf<SurgeryDepartment>()
        val insertList = mutableListOf<SurgeryDepartment>()

        val existingSurgeryDepartments = surgeryDepartmentDao.getSurgeryDepartments()

        entites.forEach { receivedSurgeryDepartment: SurgeryDepartment ->
            if (receivedSurgeryDepartment.status != EnumUtil.EntityStatus.DELETE) {

                var existingEntity = existingSurgeryDepartments.find { it.rowGuid == receivedSurgeryDepartment.rowGuid }
                if (existingEntity == null) {
                    insertList.add(receivedSurgeryDepartment)
                } else {
                    val oldId = existingEntity.id
                    val category = existingEntity.category
                    existingEntity = receivedSurgeryDepartment.clone()
                    existingEntity.id = oldId
                    existingEntity.category = category
                    updateList.add(existingEntity)
                }
            }
        }

        if (updateList.isNotEmpty())
            surgeryDepartmentDao.update(*updateList.toTypedArray())
        if (insertList.isNotEmpty())
            surgeryDepartmentDao.insert(*insertList.toTypedArray())
        entites.filter { it.status == EnumUtil.EntityStatus.DELETE }.forEach { receivedEntity ->
            var existingBuilding = existingSurgeryDepartments.find { it.rowGuid == receivedEntity.rowGuid }
            existingBuilding?.let {
                surgeryDepartmentDao.delete(it)
            }
        }
    }

    override fun insertNonClinicalUnit(entites: List<NonClinicalUnit>) {
        val updateList = mutableListOf<NonClinicalUnit>()
        val insertList = mutableListOf<NonClinicalUnit>()

        val existingNonClinicalUnits = nonClinicalUnitDao.getNonClinicalUnits()

        entites.forEach { receivedNonClinicalUnit: NonClinicalUnit ->
            if (receivedNonClinicalUnit.status != EnumUtil.EntityStatus.DELETE) {

                var existingEntity = existingNonClinicalUnits.find { it.rowGuid == receivedNonClinicalUnit.rowGuid }
                if (existingEntity == null) {
                    insertList.add(receivedNonClinicalUnit)
                } else {

                    val oldId = existingEntity?.id
                    val category = existingEntity.category
                    existingEntity = receivedNonClinicalUnit.clone()
                    existingEntity.id = oldId
                    existingEntity.category = category
                    updateList.add(existingEntity)
                }

            }
        }

        if (updateList.isNotEmpty())
            nonClinicalUnitDao.update(*updateList.toTypedArray())
        if (insertList.isNotEmpty())
            nonClinicalUnitDao.insert(*insertList.toTypedArray())

        entites.filter { it.status == EnumUtil.EntityStatus.DELETE }.forEach { receivedEntity ->
            var existingBuilding = existingNonClinicalUnits.find { it.rowGuid == receivedEntity.rowGuid }
            existingBuilding?.let {
                nonClinicalUnitDao.delete(it)
            }

        }
    }

    override fun insertClinicalUnit(entites: List<ClinicalUnit>) {
        val updateList = mutableListOf<ClinicalUnit>()
        val insertList = mutableListOf<ClinicalUnit>()

        val existingClinicalUnits = clinicalUnitDao.getClinicalUnits()

        entites.forEach { receivedClinicalUnit: ClinicalUnit ->
            if (receivedClinicalUnit.status != EnumUtil.EntityStatus.DELETE) {

                var existingEntity = existingClinicalUnits.find { it.rowGuid == receivedClinicalUnit.rowGuid }
                if (existingEntity == null) {
                    insertList.add(receivedClinicalUnit)
                } else {

                    val oldId = existingEntity?.id
                    val category = existingEntity.category
                    existingEntity = receivedClinicalUnit.clone()
                    existingEntity.id = oldId
                    existingEntity.category = category
                    updateList.add(existingEntity)
                }
            }
        }


        if (updateList.isNotEmpty())
            clinicalUnitDao.update(*updateList.toTypedArray())
        if (insertList.isNotEmpty())
            clinicalUnitDao.insert(*insertList.toTypedArray())

        entites.filter { it.status == EnumUtil.EntityStatus.DELETE }.forEach { receivedEntity ->
            var existingBuilding = existingClinicalUnits.find { it.rowGuid == receivedEntity.rowGuid }
            existingBuilding?.let {
                clinicalUnitDao.delete(it)
            }
        }
    }

    override fun insertBuildings(entites: List<Building>) {
        val updateList = mutableListOf<Building>()
        val insertList = mutableListOf<Building>()


        val existingBuildings = buildingDao.getBuildings()

        entites.forEach { receivedBuilding: Building ->
            if (receivedBuilding.status != EnumUtil.EntityStatus.DELETE) {

                var existingBuilding = existingBuildings.find { it.rowGuid == receivedBuilding.rowGuid }

                if (existingBuilding == null) {
                    insertList.add(receivedBuilding)
                } else {
                    val oldId = existingBuilding.id
                    existingBuilding = receivedBuilding.clone()
                    existingBuilding.id = oldId
                    updateList.add(existingBuilding)
                }
            }
        }

        if (updateList.isNotEmpty()) {
            buildingDao.update(*updateList.toTypedArray())
        }

        if (insertList.isNotEmpty())
            buildingDao.insert(*insertList.toTypedArray())

        entites.filter { it.status == EnumUtil.EntityStatus.DELETE }.forEach { receivedBuilding ->
            val existingBuilding = existingBuildings.find { it.rowGuid == receivedBuilding.rowGuid }
            existingBuilding?.let {
                buildingDao.delete(it)
            }

        }
    }

    override fun crudCandidateLocations(entites: List<CandidateLocation>) {
        val updateList = mutableListOf<CandidateLocation>()
        val insertList = mutableListOf<CandidateLocation>()


        val existingBuildings = candidateLocationDao.getCandiadateLocations()

        entites.forEach { receivedBuilding: CandidateLocation ->
            if (receivedBuilding.status != EnumUtil.EntityStatus.DELETE) {

                var existingBuilding = existingBuildings.find { it.rowGuid == receivedBuilding.rowGuid }

                if (existingBuilding == null) {
                    insertList.add(receivedBuilding)
                } else {
                    val oldId = existingBuilding.id
                    existingBuilding = receivedBuilding.clone()
                    existingBuilding.id = oldId
                    updateList.add(existingBuilding)
                }
            }
        }

        if (updateList.isNotEmpty()) {
            candidateLocationDao.update(*updateList.toTypedArray())
        }

        if (insertList.isNotEmpty())
            candidateLocationDao.insert(*insertList.toTypedArray())

        entites.filter { it.status == EnumUtil.EntityStatus.DELETE }.forEach { receivedBuilding ->
            var existingBuilding = existingBuildings.find { it.rowGuid == receivedBuilding.rowGuid }
            existingBuilding?.let {
                candidateLocationDao.delete(it)
            }

        }
    }

    override fun crudEntrances(entites: List<HospitalEntrance>) {
        val updateList = mutableListOf<HospitalEntrance>()
        val insertList = mutableListOf<HospitalEntrance>()


        val existingBuildings = hospitalEntranceDao.getAllEntrances()

        entites.forEach { receivedBuilding: HospitalEntrance ->
            if (receivedBuilding.status != EnumUtil.EntityStatus.DELETE) {

                var existingBuilding = existingBuildings.find { it.rowGuid == receivedBuilding.rowGuid }

                if (existingBuilding == null) {
                    insertList.add(receivedBuilding)
                } else {
                    val oldId = existingBuilding.id
                    existingBuilding = receivedBuilding.clone()
                    existingBuilding.id = oldId
                    updateList.add(existingBuilding)
                }
            }
        }

        if (updateList.isNotEmpty()) {
            hospitalEntranceDao.update(*updateList.toTypedArray())
        }

        if (insertList.isNotEmpty())
            hospitalEntranceDao.insert(*insertList.toTypedArray())

        entites.filter { it.status == EnumUtil.EntityStatus.DELETE }.forEach { receivedBuilding ->
            var existingBuilding = existingBuildings.find { it.rowGuid == receivedBuilding.rowGuid }
            existingBuilding?.let {
                hospitalEntranceDao.delete(it)
            }

        }
    }

    override fun crudHospitalComments(entites: List<HospitalComments>) {

        val updateList = mutableListOf<HospitalComments>()
        val insertList = mutableListOf<HospitalComments>()


        val existingEntities = hospitalCommentsDao.getAllComments().blockingGet()

        entites.forEach { receivedEntity: HospitalComments ->
            if (receivedEntity.status != EnumUtil.EntityStatus.DELETE) {

                var existingEntity = existingEntities.find { it.rowGuid == receivedEntity.rowGuid }

                if (existingEntity == null) {
                    insertList.add(receivedEntity)
                } else {
                    val oldId = existingEntity.id
                    existingEntity = receivedEntity.clone()
                    existingEntity.id = oldId
                    updateList.add(existingEntity)
                }
            }
        }

        if (updateList.isNotEmpty()) {
            hospitalCommentsDao.update(*updateList.toTypedArray())
        }

        if (insertList.isNotEmpty())
            hospitalCommentsDao.insert(*insertList.toTypedArray())

        entites.filter { it.status == EnumUtil.EntityStatus.DELETE }.forEach { receivedBuilding ->
            var existingBuilding = existingEntities.find { it.rowGuid == receivedBuilding.rowGuid }
            existingBuilding?.let {
                hospitalCommentsDao.delete(it)
            }

        }
    }

    override fun applyEvaluations(entites: List<Evaluation>) {
        val updateList = mutableListOf<Evaluation>()
        val insertList = mutableListOf<Evaluation>()

        val existingEvaluations = evaluationDao.getAllEvaluations()

        entites.forEach { receivedItem: Evaluation ->

            var existingEntity = existingEvaluations.find { it.rowGuid == receivedItem.rowGuid }
            if (existingEntity == null) {
                insertList.add(receivedItem)
            } else {
                val oldId = existingEntity.id
                existingEntity = receivedItem.clone()
                existingEntity.id = oldId
                updateList.add(existingEntity)
            }
        }

        if (updateList.isNotEmpty())
            evaluationDao.update(*updateList.toTypedArray())
        if (insertList.isNotEmpty()) {
            insertList.forEach {

                try {
                    Log.e("evaluationDao", it.toString())

                    evaluationDao.insert(it)

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
//            evaluationDao.insert(*insertList.toTypedArray())

        entites.filter { it.status == EnumUtil.EntityStatus.DELETE }.forEach { receivedBuilding ->
            var existingBuilding = existingEvaluations.find { it.rowGuid == receivedBuilding.rowGuid }
            existingBuilding?.let {
                evaluationDao.delete(it)
            }
        }
    }

    override fun applyAttachments(entites: List<Attachment>) {
        val updateList = mutableListOf<Attachment>()
        val insertList = mutableListOf<Attachment>()

        entites.forEach {
            if (it.path == null)
                it.path = ""
        }

        val existingAttachment = attachmentDao.getAllAttachments()

        entites.forEach { receivedItem: Attachment ->

            var existingEntity = existingAttachment.find { it.rowGuid == receivedItem.rowGuid }
            if (existingEntity == null) {
                insertList.add(receivedItem)
            } else {
                val oldId = existingEntity?.id
                existingEntity = receivedItem.clone()
                existingEntity.id = oldId
                updateList.add(existingEntity)
            }

        }



        if (updateList.isNotEmpty())
            attachmentDao.update(*updateList.toTypedArray())
        if (insertList.isNotEmpty())
            attachmentDao.insert(*insertList.toTypedArray())

        entites.filter { it.status == EnumUtil.EntityStatus.DELETE }.forEach { receivedItem ->
            val existingBuilding = existingAttachment.find { it.rowGuid == receivedItem.rowGuid }
            existingBuilding?.let {
                attachmentDao.delete(it)
            }

        }
    }

    override fun insertHospitals(entites: List<Hospital>) {

        val updateList = mutableListOf<Hospital>()
        val insertList = mutableListOf<Hospital>()

        val existingHospitals = hospitalDao.getHospitalsWitDeletedItems()

        entites.forEach { receivedHospital: Hospital ->
            if (existingHospitals.count { it.id == receivedHospital.id } == 0) {
                insertList.add(receivedHospital)
            } else {
                updateList.add(receivedHospital)
            }
        }

        if (updateList.isNotEmpty())
            hospitalDao.update(*updateList.toTypedArray())
        if (insertList.isNotEmpty())
            hospitalDao.insert(*insertList.toTypedArray())

    }

    override fun insertEntity(baseClass: BaseClass) {
        when (baseClass) {
            is SurgeryDepartment -> {
                surgeryDepartmentDao.insert(baseClass)
            }
            is ClinicalUnit -> {
                clinicalUnitDao.insert(baseClass)
            }
            is NonClinicalUnit -> {
                nonClinicalUnitDao.insert(baseClass)
            }
            is Building -> {
                buildingDao.insert(baseClass)
            }
            is CandidateLocation -> {
                candidateLocationDao.insert(baseClass)
            }
            is HospitalEntrance -> {
                hospitalEntranceDao.insert(baseClass)
            }
            is Attachment -> {
                attachmentDao.insert(baseClass)
            }
            is Evaluation -> {
                evaluationDao.insert(baseClass)
            }
            is Setting -> {
                settingDao.insert(baseClass)
            }
            is HospitalComments -> {
                hospitalCommentsDao.insert(baseClass)
            }

        }
    }

    override fun deleteEntity(baseClass: BaseClass) {
        when (baseClass) {
            is SurgeryDepartment -> {
                surgeryDepartmentDao.delete(baseClass)
            }
            is ClinicalUnit -> {
                clinicalUnitDao.delete(baseClass)
            }
            is NonClinicalUnit -> {
                nonClinicalUnitDao.delete(baseClass)
            }
            is Building -> {
                buildingDao.delete(baseClass)

            }
            is CandidateLocation -> {
                candidateLocationDao.delete(baseClass)
            }
            is HospitalEntrance -> {
                hospitalEntranceDao.delete(baseClass)
            }
            is Attachment -> {
                attachmentDao.delete(baseClass)
            }
            is Evaluation -> {
                evaluationDao.delete(baseClass)
            }

        }
    }

    override fun updateEntity(baseClass: BaseClass) {

        when (baseClass) {
            is SurgeryDepartment -> surgeryDepartmentDao.update(baseClass)
            is ClinicalUnit -> clinicalUnitDao.update(baseClass)
            is NonClinicalUnit -> nonClinicalUnitDao.update(baseClass)
            is Building -> buildingDao.update(baseClass)
            is CandidateLocation -> candidateLocationDao.update(baseClass)
            is HospitalEntrance -> hospitalEntranceDao.update(baseClass)
            is Attachment -> attachmentDao.update(baseClass)
            is Evaluation -> evaluationDao.update(baseClass)
            is Hospital -> hospitalDao.update(baseClass)
            is Setting -> settingDao.update(baseClass)
            is HospitalComments -> hospitalCommentsDao.update(baseClass)
        }
    }


    override fun getUnitCategories2(): Flowable<List<UnitCategoryWithSub>> {
        return unitCategoryDao.getUnitCategories2()
    }

    override fun getUnitCategories(): LiveData<List<UnitCategoryWithSub>> {
        return unitCategoryDao.getUnitCategories()
    }


    override fun getHospitals(): Flowable<Array<Hospital>> {
        return hospitalDao.getHospitals()
    }

    override fun getHospitalCommentByHospital(hospitalId: Int): Single<List<HospitalComments>> {
        return hospitalCommentsDao.getCommentByHospital(hospitalId)
    }

//    override fun getHospitalsWithComment(): Flowable<Array<HospitalWithComment>> {
//         return hospitalDao.getHospitalsWithComment()
//    }


    override fun getBuildings(): List<Building> {
        return buildingDao.getBuildings()
    }

    override fun getAllUnitsByStatus(status: EnumUtil.EntityStatus): List<BaseClass> {
        val f = Flowable.combineLatest(
                surgeryDepartmentDao.getAllSyncedSurgeryDepartments(EnumUtil.EntityStatus.toInteger(status)),
                nonClinicalUnitDao.getAllNonClinicalUnit(EnumUtil.EntityStatus.toInteger(status)),
                clinicalUnitDao.getAllClinicalUnit(EnumUtil.EntityStatus.toInteger(status)),
                Function3<List<SurgeryDepartment>, List<NonClinicalUnit>, List<ClinicalUnit>, List<BaseClass>> { t1: List<SurgeryDepartment>, t2: List<NonClinicalUnit>, t3: List<ClinicalUnit> ->
                    val list = mutableListOf<BaseClass>()

                    list.addAll(t1)
                    list.addAll(t2)
                    list.addAll(t3)

                    list
                }
        )


        return f.blockingFirst()

    }

    override fun getUnits(hospitalId: Int): Flowable<List<BaseClass>> {

        val f = Flowable.combineLatest(
                buildingDao.getBuildingsFlowables(hospitalId),
                unitSubCategoryDao.getUnitSubCategories(),
                BiFunction<List<BuildingWithUnits>, List<UnitSubCategory>, List<BaseClass>> { buildings: List<BuildingWithUnits>, t4: List<UnitSubCategory> ->

                    val data = mutableListOf<BaseClass>()

                    buildings.forEach {

                        if (it.building.status != EnumUtil.EntityStatus.DELETE) {


                            data.add(it.building)

                            it.clinicalUnits.forEach {
                                val id = it.name.value
                                val subCat = t4.find { it.id == id }

                                when (PreferencesUtil.getCurrentLanguage(application)) {
                                    Constants.ENGLISH -> it.nameTemp = subCat?.titleEn
                                    Constants.PERSIAN -> it.nameTemp = subCat?.titleFa
                                }
                                data.add(it)
                            }
                            it.nonClinicalUnits.forEach {
                                val id = it.name.value
                                val subCat = t4.find { it.id == id }

                                when (PreferencesUtil.getCurrentLanguage(application)) {
                                    Constants.ENGLISH -> it.nameTemp = subCat?.titleEn
                                    Constants.PERSIAN -> it.nameTemp = subCat?.titleFa
                                }

                                data.add(it)
                            }
                            it.surgeryDepartments.forEach {
                                val id = it.name.value
                                val subCat = t4.find { it.id == id }

                                when (PreferencesUtil.getCurrentLanguage(application)) {
                                    Constants.ENGLISH -> it.nameTemp = subCat?.titleEn
                                    Constants.PERSIAN -> it.nameTemp = subCat?.titleFa
                                }

                                data.add(it)
                            }
                        }
                    }

                    data
                }
        )


        return f
    }


    override fun getUnsyncedEvaluations(): List<Evaluation> {
        return evaluationDao.getAllEvaluations()
    }

    override fun getEvaluations4Building(moduleName: String, buildingId: String): Flowable<List<Any>> {

        val evaluations = evaluationDao.getEvaluations4Building(moduleName, buildingId)

        var locationType = EnumUtil.HealthLocationType.HOSPITAL

        buildingDao.findBuilding(buildingId)?.let {
            hospitalDao.findHospitalById(it.hospitalId)?.healthLocationType?.let {
                locationType = it
            }
        }



        return combineEvaluations(evaluations, moduleName, locationType)

    }


    override fun getEvaluations4SurgeryDepartment(moduleName: String, unitId: String): Flowable<List<Any>> {

        val evaluations = evaluationDao.getEvaluations4SurgericalDeps(moduleName, unitId)

        var locationType = EnumUtil.HealthLocationType.HOSPITAL
        surgeryDepartmentDao.findSurgeryDepartment(unitId)?.let {

            buildingDao.findBuilding(it.buildingId!!.get())?.let {
                hospitalDao.findHospitalById(it.hospitalId)?.healthLocationType?.let {
                    locationType = it
                }
            }

        }

        return combineEvaluations(evaluations, moduleName, locationType)

    }

    override fun getEvaluations4NonClinicalUnit(moduleName: String, unitId: String): Flowable<List<Any>> {

        val evaluations = evaluationDao.getEvaluations4NonCinicalUnits(moduleName, unitId)

        var locationType = EnumUtil.HealthLocationType.HOSPITAL
        nonClinicalUnitDao.findNonClinicalUnit(unitId)?.let {

            buildingDao.findBuilding(it.buildingId!!.get())?.let {
                hospitalDao.findHospitalById(it.hospitalId)?.healthLocationType?.let {
                    locationType = it
                }
            }

        }

        return combineEvaluations(evaluations, moduleName, locationType)

    }

    override fun getEvaluations4ClinicalUnit(moduleName: String, unitId: String): Flowable<List<Any>> {

        val evaluations = evaluationDao.getEvaluations4CinicalUnits(moduleName, unitId)

        var locationType = EnumUtil.HealthLocationType.HOSPITAL

        clinicalUnitDao.findClinicalUnit(unitId)?.let {

            buildingDao.findBuilding(it.buildingId!!.get())?.let {
                hospitalDao.findHospitalById(it.hospitalId)?.healthLocationType?.let {
                    locationType = it
                }
            }
        }

        return combineEvaluations(evaluations, moduleName, locationType)
    }

    private fun combineEvaluations(evaluations: Flowable<List<EvaluationWithQuestion>>, moduleName: String, locationType: EnumUtil.HealthLocationType): Flowable<List<Any>> {

        return Flowable.combineLatest(
                evaluations,
                questionCategoryDao.getCategories(),
                if (locationType == EnumUtil.HealthLocationType.HOSPITAL) questionDao.getQuestionCount4Hospital(moduleName) else questionDao.getQuestionCount4HealthHospital(moduleName),
                Function3<List<EvaluationWithQuestion>, List<QuestionCategory>, Int, List<Any>> { evaluations: List<EvaluationWithQuestion>, categories: List<QuestionCategory>, Int ->
                    val list = mutableListOf<Any>()

                    var counter = 0

                    categories.forEach { cat ->

                        val catEvaluations = evaluations.filter { it.categoryId == cat.id }

                        if (catEvaluations.isNotEmpty()) {

                            list.add(cat)

                            catEvaluations.forEach {
                                counter++
                                it.rowNum = counter
                            }

                            list.addAll(catEvaluations)

                        }
                    }

                    list
                }
        )
    }


    override fun getBuildingsFlowable(hospitalId: Int, moduleName: String): Flowable<List<BuildingWithUnits>>? {

        var locationType = EnumUtil.HealthLocationType.HOSPITAL

        hospitalDao.findHospitalById(hospitalId)?.healthLocationType?.let {
            locationType = it
        }


        return Flowable.combineLatest(
                if (locationType == EnumUtil.HealthLocationType.HOSPITAL) questionDao.getQuestionCount4Hospital(moduleName) else questionDao.getQuestionCount4HealthHospital(moduleName),
                buildingDao.getBuildingsFlowables(hospitalId),
                unitSubCategoryDao.getUnitSubCategories(),
                surgeryDepartmentDao.getCountEvaluations(hospitalId, moduleName, 0),
                clinicalUnitDao.getCountEvaluations(hospitalId, moduleName, 0),
                nonClinicalUnitDao.getCountEvaluations(hospitalId, moduleName, 0),
                surgeryDepartmentDao.getCountEvaluations(hospitalId, moduleName, 1),
                clinicalUnitDao.getCountEvaluations(hospitalId, moduleName, 1),
                nonClinicalUnitDao.getCountEvaluations(hospitalId, moduleName, 1),
                Function9<Int, List<BuildingWithUnits>, List<UnitSubCategory>, List<Pair>, List<Pair>, List<Pair>, List<Pair>, List<Pair>, List<Pair>, List<BuildingWithUnits>> { countQuestions: Int, t1: List<BuildingWithUnits>, t2: List<UnitSubCategory>, countUnobservedEvalSurg: List<Pair>, countUnobservedEvalClinincal: List<Pair>,
                                                                                                                                                                                  countUnobservedEvalNonclinincal: List<Pair>, countObservedEvalSurg: List<Pair>, countObservedEvalClinincal: List<Pair>, countObservedEvalNonclinincal: List<Pair> ->


                    t1.forEach { buildingWithUnits ->
                        val observedEvaluationCount = buildingDao.getEvaluationCount(moduleName, buildingWithUnits.building.rowGuid, 1)
                        val unobservableEvaluationCount = buildingDao.getEvaluationCount(moduleName, buildingWithUnits.building.rowGuid, 0)
                        buildingWithUnits.building.countVisited = observedEvaluationCount
                        buildingWithUnits.building.countUnVisited = unobservableEvaluationCount
                        buildingWithUnits.building.countRemainedQuestions = countQuestions - (observedEvaluationCount + unobservableEvaluationCount)
                    }

                    if (moduleName == Constants.MODULE_NON_STRUCTURAL) {

                        t1.forEach { buildingWithUnits ->

                            buildingWithUnits.surgeryDepartments.forEach { unit ->

                                unit.countUnVisited = 0
                                unit.countVisited = 0
                                unit.countRemainedQuestions = 0

                                val id = unit.name.value
                                val subCat = t2.find { it.id == id }

                                when (PreferencesUtil.getCurrentLanguage(application)) {
                                    Constants.ENGLISH -> unit.nameTemp = subCat?.titleEn
                                    Constants.PERSIAN -> unit.nameTemp = subCat?.titleFa
                                }

                                unit.countRemainedQuestions = countQuestions

                                countObservedEvalSurg.find { it.first == unit.rowGuid }?.let { f ->
                                    unit.countVisited = f.second
                                    unit.countRemainedQuestions -= f.second
                                }

                                countUnobservedEvalSurg.find { it.first == unit.rowGuid }?.let { f ->
                                    unit.countUnVisited = f.second
                                    unit.countRemainedQuestions -= f.second
                                }

                                unit.countUnVisited?.let {
                                    buildingWithUnits.building.countUnVisited += it
                                }
                                unit.countVisited?.let {
                                    buildingWithUnits.building.countVisited += it
                                }

                            }

                            buildingWithUnits.clinicalUnits.forEach { unit ->
                                val catId = unit.name.value
                                val subCat = t2.find { it.id == catId }

                                unit.countUnVisited = 0
                                unit.countVisited = 0
                                unit.countRemainedQuestions = 0

                                when (PreferencesUtil.getCurrentLanguage(application)) {
                                    Constants.ENGLISH -> unit.nameTemp = subCat?.titleEn
                                    Constants.PERSIAN -> unit.nameTemp = subCat?.titleFa
                                }

                                unit.countRemainedQuestions = countQuestions

                                countObservedEvalClinincal.find { it.first == unit.rowGuid }?.let { f ->
                                    unit.countVisited = f.second
                                    unit.countRemainedQuestions -= f.second
                                }

                                countUnobservedEvalClinincal.find { it.first == unit.rowGuid }?.let { f ->
                                    unit.countUnVisited = f.second
                                    unit.countRemainedQuestions -= f.second
                                }

                                unit.countUnVisited?.let {
                                    buildingWithUnits.building.countUnVisited += it
                                }
                                unit.countVisited?.let {
                                    buildingWithUnits.building.countVisited += it
                                }
                            }
                            buildingWithUnits.nonClinicalUnits.forEach { unit ->
                                val catId = unit.name.value
                                val subCat = t2.find { it.id == catId }

                                unit.countUnVisited = 0
                                unit.countVisited = 0
                                unit.countRemainedQuestions = 0

                                when (PreferencesUtil.getCurrentLanguage(application)) {
                                    Constants.ENGLISH -> unit.nameTemp = subCat?.titleEn
                                    Constants.PERSIAN -> unit.nameTemp = subCat?.titleFa
                                }

                                unit.countRemainedQuestions = countQuestions

                                countObservedEvalNonclinincal.find { it.first == unit.rowGuid }?.let { f ->
                                    unit.countVisited = f.second
                                    unit.countRemainedQuestions -= f.second
                                }
                                countUnobservedEvalNonclinincal.find { it.first == unit.rowGuid }?.let { f ->
                                    unit.countUnVisited = f.second
                                    unit.countRemainedQuestions -= f.second
                                }

                                unit.countUnVisited?.let {
                                    buildingWithUnits.building.countUnVisited += it
                                }
                                unit.countVisited?.let {
                                    buildingWithUnits.building.countVisited += it
                                }
                            }
                        }
                    }
                    t1
                }
        )
    }

    override fun getQuestionsWithEvaluation4Hazard(buildingId: String, moduleName: String): Flowable<List<Any>> {

        var locationType = getLocationType(buildingId)

        return Flowable.combineLatest(
                questionCategoryDao.getCategoriesWithQuestion(moduleName),
                evaluationDao.getEvaluations4Hazard(buildingId),
                BiFunction<List<QuestionCategoryWithQuestion>, List<Evaluation>, List<Any>>
                { questions: List<QuestionCategoryWithQuestion>, evaluations: List<Evaluation> ->
                    val retVal = mutableListOf<Any>()

                    var counter = 0

                    val filteredQuestions: MutableList<QuestionCategoryWithQuestion> = mutableListOf()

                    questions.forEach {
                        val category = QuestionCategoryWithQuestion(it.questionCategory, it.allQuestions.filter { if (locationType == EnumUtil.HealthLocationType.HOSPITAL) it.used4Hospital else it.used4HealthHouse }.toMutableList())
                        filteredQuestions.add(category)
                    }

                    filteredQuestions.forEach { questionsWithEvals ->
                        retVal.add(questionsWithEvals.questionCategory)
                        questionsWithEvals.questions.sortByDescending { it.questionTitleEn }
                        questionsWithEvals.questions.forEach { q ->

                            val questionWithEvaluation = EvaluationWithQuestion()

                            val evaluation = evaluations.find { it.questionId == q.id }


                            questionWithEvaluation.categoryId = q.categoryId
                            questionWithEvaluation.helpLink = q.helpLink
                            questionWithEvaluation.weight = q.weight

                            questionWithEvaluation.briefDescriptionEn = q.briefDescriptionEn
                            questionWithEvaluation.questionTitleEn = q.questionTitleEn
                            questionWithEvaluation.briefDescriptionFa = q.briefDescriptionFa
                            questionWithEvaluation.questionTitleFa = q.questionTitleFa


                            questionWithEvaluation.questionId = q.id

                            evaluation?.let { eval ->
                                questionWithEvaluation.evaluation = eval
                            }

                            counter++

                            questionWithEvaluation.rowNum = counter

                            retVal.add(questionWithEvaluation)
                        }
                    }

                    retVal.toList()
                }
        )
    }

    private fun getLocationType(buildingId: String): EnumUtil.HealthLocationType {
        var locationType = EnumUtil.HealthLocationType.HOSPITAL

        buildingDao.findBuilding(buildingId)?.let {
            val hospital = hospitalDao.findHospitalById(it.hospitalId)
            hospital?.let {
                locationType = it.healthLocationType!!
            }
        }

        return locationType
    }


    override fun getQuestionsWithCategoryAndEvaluations(moduleName: String, building: Building?, unit: Any?, currentLanguage: String): Flowable<List<Question>> {
        var flowableEvaluation: Flowable<List<EvaluationWithQuestion>>? = null
        var locationType = EnumUtil.HealthLocationType.HOSPITAL

        building?.let {
            locationType = getLocationType(building.rowGuid)

            flowableEvaluation = evaluationDao.getEvaluations4Building(moduleName, building.rowGuid)
        }

        unit?.let {

            var buildingId = ""


            when (unit) {
                is SurgeryDepartment -> {
                    flowableEvaluation = evaluationDao.getEvaluations4SurgericalDeps(moduleName, unit.rowGuid)
                    buildingId = unit.buildingId!!.get()
                }
                is NonClinicalUnit -> {
                    flowableEvaluation = evaluationDao.getEvaluations4NonCinicalUnits(moduleName, unit.rowGuid)
                    buildingId = unit.buildingId!!.get()
                }
                is ClinicalUnit -> {
                    flowableEvaluation = evaluationDao.getEvaluations4CinicalUnits(moduleName, unit.rowGuid)
                    buildingId = unit.buildingId!!.get()
                }
            }

            locationType = getLocationType(buildingId)
        }


        return Flowable.combineLatest(
                questionCategoryDao.getCategoriesWithQuestion(moduleName),
                flowableEvaluation,
                BiFunction<List<QuestionCategoryWithQuestion>, List<EvaluationWithQuestion>, List<Question>> { questions: List<QuestionCategoryWithQuestion>, evaluations: List<EvaluationWithQuestion> ->

                    val filteredQuestions: MutableList<QuestionCategoryWithQuestion> = mutableListOf()

                    questions.forEach {
                        val category = QuestionCategoryWithQuestion(it.questionCategory, it.allQuestions.filter { if (locationType == EnumUtil.HealthLocationType.HOSPITAL) it.used4Hospital else it.used4HealthHouse }.toMutableList())
                        filteredQuestions.add(category)
                    }


                    var questions = mutableListOf<Question>()

                    var moduleNum = GeneralUtil.getModuleNumber(moduleName)
                    var counterCat = 0


                    filteredQuestions.forEach { questionCategory ->

                        var counterQuestion = 0


                        when (currentLanguage) {
                            Constants.ENGLISH -> {
                                questionCategory.questionCategory.number = "$moduleNum-${counterCat + 1}"
                            }
                            Constants.PERSIAN -> {
                                questionCategory.questionCategory.number = "${counterCat + 1}-$moduleNum"
                            }
                        }

                        questionCategory.questions.forEach { question ->


                            when (currentLanguage) {
                                Constants.ENGLISH -> {
                                    question.number = "$moduleNum-${counterCat + 1}-${counterQuestion + 1}"
                                }
                                Constants.PERSIAN -> {
                                    question.number = "${counterQuestion + 1}-${counterCat + 1}-$moduleNum-"
                                }
                            }

                            question.questionCategory = questionCategory.questionCategory

                            val filteredEvaluations = evaluations.map { it.evaluation }.filter { it?.questionId == question.id }
                            filteredEvaluations.sortedBy { it?.id }
                            question.evaluations = mutableListOf()

                            filteredEvaluations.forEach {
                                it?.let {
                                    question.evaluations.add(it)
                                }
                            }
                            questions.add(question)

                            counterQuestion++
                        }
                        counterCat++
                    }


                    questions
                }
        )
    }

    override fun getQuestionCategoryWithQuestionAndEvaluations(moduleName: String, building: Building?, unit: Any?): Flowable<List<QuestionCategoryWithQuestion>> {

        var flowableEvaluation: Flowable<List<EvaluationWithQuestion>>? = null
        var locationType = EnumUtil.HealthLocationType.HOSPITAL

        building?.let {
            locationType = getLocationType(building.rowGuid)

            flowableEvaluation = evaluationDao.getEvaluations4Building(moduleName, building.rowGuid)
        }

        unit?.let {
            var buildingId = ""

            when (unit) {
                is SurgeryDepartment -> {
                    flowableEvaluation = evaluationDao.getEvaluations4SurgericalDeps(moduleName, unit.rowGuid)
                    buildingId = unit.buildingId!!.get()
                }
                is NonClinicalUnit -> {
                    flowableEvaluation = evaluationDao.getEvaluations4NonCinicalUnits(moduleName, unit.rowGuid)
                    buildingId = unit.buildingId!!.get()
                }
                is ClinicalUnit -> {
                    flowableEvaluation = evaluationDao.getEvaluations4CinicalUnits(moduleName, unit.rowGuid)
                    buildingId = unit.buildingId!!.get()
                }
            }
            locationType = getLocationType(buildingId)
        }

        return Flowable.combineLatest(
                questionCategoryDao.getCategoriesWithQuestion(moduleName),
                flowableEvaluation,
                BiFunction<List<QuestionCategoryWithQuestion>, List<EvaluationWithQuestion>, List<QuestionCategoryWithQuestion>> { questions: List<QuestionCategoryWithQuestion>, evaluations: List<EvaluationWithQuestion> ->

                    val filteredQuestions: MutableList<QuestionCategoryWithQuestion> = mutableListOf()

                    questions.forEach {
                        val category = QuestionCategoryWithQuestion(it.questionCategory, it.allQuestions.filter { if (locationType == EnumUtil.HealthLocationType.HOSPITAL) it.used4Hospital else it.used4HealthHouse }.toMutableList())
                        filteredQuestions.add(category)
                    }

                    evaluations.forEach { evaluation ->

                        filteredQuestions.forEach { questionCategory ->

                            val question = questionCategory.questions.find { it.id == evaluation.questionId }
                            question?.let {
                                evaluation.evaluation?.let { it1 -> it.evaluations.add(it1) }
                            }
                        }

                    }
                    filteredQuestions.forEach { cat ->

                        cat.questions.forEach {
                            if (it.evaluations.count { it.hasObserved?.get() == true } > 0)
                                cat.questionCategory.countVisited++

                            if (it.evaluations.count { it.hasObserved?.get() == false } > 0)
                                cat.questionCategory.countUnVisited++
                        }

                        cat.questionCategory.countRemainedQuestions = cat.questions.count { it.evaluations.isEmpty() }
                    }
                    filteredQuestions
                }
        )
    }


    override fun getBuildings(hospitalId: Int): List<Building> {
        return buildingDao.getBuildings(hospitalId)
    }


    override fun getLastBuilding(): Building? {
        return buildingDao.getLastBuilding()
    }

    override fun findBuilding(id: Int): Building? {
        return buildingDao.findBuilding(id)
    }

    override fun findHospitalById(id: Int): Hospital? {
        return hospitalDao.findHospitalById(id)
    }

    override fun getCandiadateLocations(hospitalId: Int): List<CandidateLocation> {
        return candidateLocationDao.getCandiadateLocations(hospitalId)
    }

    override fun findCandidateLocation(id: Int): CandidateLocation? {
        return candidateLocationDao.findCandidateLocation(id)
    }


    override fun getLastCandidateLocation(): CandidateLocation? {
        return candidateLocationDao.getLastCandidateLocation()
    }

    override fun getHospitalEntrances(hospitalId: Int): List<HospitalEntrance> {
        return hospitalEntranceDao.getHospitalEntrances(hospitalId)
    }

    override fun getLastHospitalEntrance(): HospitalEntrance? {
        return hospitalEntranceDao.getLastHospitalEntrance()
    }

    override fun getLastEvaluation(): Evaluation? {
        return evaluationDao.getLastEvaluation()
    }

    override fun findHospitalEntrance(id: Int): HospitalEntrance? {
        return hospitalEntranceDao.findHospitalEntrance(id)
    }

    override fun getQuestion(questionId: Int): Question {
        return questionDao.getQuestion(questionId)
    }

    override fun getAllQuestions(): List<Question> {
        return questionDao.getQuestions()
    }

    override fun getAttachments(category: String, linkedId: String): Flowable<List<Attachment>> {

        return attachmentDao.getAttachments(category, linkedId)
    }

    override fun getAllAttachments(): List<Attachment> {
        return attachmentDao.getAllAttachments()
    }

    override fun getClinicalUnit(hospitalId: Int): Flowable<List<ClinicalUnit>> {
        return clinicalUnitDao.getClinicalUnit(hospitalId)
    }

    override fun getNonClinicalUnit(hospitalId: Int): Flowable<List<NonClinicalUnit>> {
        return nonClinicalUnitDao.getNonClinicalUnit(hospitalId)
    }

    override fun getSurgeryDepartments(hospitalId: Int): Flowable<List<SurgeryDepartment>> {
        return surgeryDepartmentDao.getSurgeryDepartments(hospitalId)
    }

    override fun evaluationExists(evaluation: Evaluation): Boolean {
        return evaluationDao.evaluationExists(evaluation.id) > 0
    }

    override fun findBuilding(id: String): Building? {
        return buildingDao.findBuilding(id)
    }

    override fun deleteAttachments4Feature(rowGuid: String) {
        attachmentDao.deleteAttachments4Feature(rowGuid)
    }


    override fun getAllSettings(): List<Setting> {
        return settingDao.getAllSettings()
    }


    override fun getSetting(key: String): Setting {
        return settingDao.getSettingByKey(key)
    }


}

