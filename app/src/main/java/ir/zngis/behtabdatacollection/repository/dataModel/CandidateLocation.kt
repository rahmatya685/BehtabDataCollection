package ir.zngis.behtabdatacollection.repository.dataModel

import androidx.room.ColumnInfo
import androidx.room.Entity
import ir.zngis.behtabdatacollection.baseBinding.BindableBoolean
import ir.zngis.behtabdatacollection.baseBinding.BindableDouble
import ir.zngis.behtabdatacollection.baseBinding.BindableString
import ir.zngis.behtabdatacollection.util.GeneralUtil
import ir.zngis.geojson.Polygon


@Entity(tableName = "CANDIDATE_LOCATION")
class CandidateLocation(

        @ColumnInfo(name = "NAME")
        var name: BindableString = BindableString(""),
        @ColumnInfo(name = "COMMENT")
        var comment: BindableString = BindableString(""),
        @ColumnInfo(name = "GEOM", typeAffinity = ColumnInfo.TEXT)
        var geom: Polygon? = null,
        @ColumnInfo(name = "LAND_USE", typeAffinity = ColumnInfo.TEXT)
        var landUse: BindableString = BindableString(""),
        @ColumnInfo(name = "HAS_WATER", typeAffinity = ColumnInfo.INTEGER)
        var hasWater: BindableBoolean? = BindableBoolean(false),
        @ColumnInfo(name = "HAS_ELECTRICITY", typeAffinity = ColumnInfo.INTEGER)
        var hasElectricity: BindableBoolean? = BindableBoolean(false),
        @ColumnInfo(name = "HAS_TELEPHONE", typeAffinity = ColumnInfo.INTEGER)
        var hasTelephone: BindableBoolean? = BindableBoolean(false),
        @ColumnInfo(name = "HAS_LANDFILL_MANAGEMENT", typeAffinity = ColumnInfo.INTEGER)
        var hasLandfillManagement: BindableBoolean? = BindableBoolean(false),
        @ColumnInfo(name = "HAS_HEATING", typeAffinity = ColumnInfo.INTEGER)
        var hasHeating: BindableBoolean? = BindableBoolean(false),
        @ColumnInfo(name = "HAS_COOLING", typeAffinity = ColumnInfo.INTEGER)
        var hasCooling: BindableBoolean? = BindableBoolean(false),
        @ColumnInfo(name = "HAS_VENTILATION", typeAffinity = ColumnInfo.INTEGER)
        var hasVentilation: BindableBoolean? = BindableBoolean(false),
        @ColumnInfo(name = "AREA", typeAffinity = ColumnInfo.REAL)
        var area :BindableDouble? = BindableDouble(0.0),
        @ColumnInfo(name = "HOSPITAL_ID", typeAffinity = ColumnInfo.INTEGER)
        var hospitalId: Int = -1,
        @ColumnInfo(name = "ROW_GUID", typeAffinity = ColumnInfo.TEXT)
        var rowGuid: String = GeneralUtil.generateGuid()

) : BaseClass(), Cloneable {

    public override fun clone(): CandidateLocation {
        val cloned = super.clone() as CandidateLocation
        cloned.comment = this.comment.clone() as BindableString
        cloned.hasCooling = this.hasCooling?.clone() as BindableBoolean
        cloned.hasHeating = this.hasHeating?.clone() as BindableBoolean
        cloned.hasElectricity = this.hasElectricity?.clone() as BindableBoolean
        cloned.hasLandfillManagement = this.hasLandfillManagement?.clone() as BindableBoolean
        cloned.hasTelephone = this.hasTelephone?.clone() as BindableBoolean
        cloned.hasVentilation = this.hasVentilation?.clone() as BindableBoolean
        cloned.hasWater = this.hasWater?.clone() as BindableBoolean
        cloned.name = this.name.clone() as BindableString

        return cloned
    }
}