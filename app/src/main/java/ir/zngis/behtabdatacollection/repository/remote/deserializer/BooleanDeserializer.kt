package ir.zngis.behtabdatacollection.repository.remote.deserializer

import android.text.BoringLayout
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class BooleanDeserializer : JsonDeserializer<Boolean> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Boolean {
        var retVal = true

        json?.let {
            retVal = json.asInt == 1
        }

        return retVal
    }
}