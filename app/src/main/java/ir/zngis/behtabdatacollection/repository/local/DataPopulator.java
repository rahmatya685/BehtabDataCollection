package ir.zngis.behtabdatacollection.repository.local;

import android.app.Application;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.room.RoomDatabase;

import io.reactivex.annotations.NonNull;

public class DataPopulator extends RoomDatabase.Callback {

    private Application application;

    public DataPopulator(Application application) {
        this.application = application;
    }

    @Override
    public void onOpen(@NonNull SupportSQLiteDatabase db) {
        super.onOpen(db);
//        Cursor cursor = db.query("select count(*) from HOSPITAL");
//        cursor.moveToFirst();
//        int count = cursor.getInt(0);
//        if (count == 0) {
//            List<String> hostpitalNames = new ArrayList<>();
//            hostpitalNames.add("امیر اعلم" + "\n Amir alam");
//            hostpitalNames.add("سینا" + "\n Sina");
//            hostpitalNames.add("شهدای تجریش" + "\n Shohadaye Tajrish");
//            hostpitalNames.add("شهید مطهری" + "\n Motahary");
//            hostpitalNames.add("دی" + "\n Dey");
//            hostpitalNames.add("قلب تهران" + "\n Tehran Heart Center");
//            hostpitalNames.add("فجر" + "\n Fajr");
//            hostpitalNames.add("بهمن" + "\n Bahman");
//            hostpitalNames.add("جم" + "\n Jam");
//
//
//            try {
//                db.beginTransaction();
//                for (int i = 0; i < hostpitalNames.size(); i++) {
//                    String sql = "insert into HOSPITAL('NAME') values ( '" + hostpitalNames.get(i) + "')";
//                    db.execSQL(sql);
//                }
//                db.setTransactionSuccessful();
//            } catch (Exception e) {
//                db.endTransaction();
//            }
//
//            List<Triple<String, String, List<String>>> categories = new ArrayList<>();
//
//            List<String> catDep = new ArrayList<>();
//            catDep.add("عمومی");
//            catDep.add("کودکان");
//            catDep.add("قلب");
//            catDep.add("ریه");
//            catDep.add("مغز و اعصاب");
//            catDep.add("غدد");
//            catDep.add("خون");
//            catDep.add("گوارش");
//            catDep.add("طب فیزیکی و توانبخشی");
//            catDep.add("روانپزشکی");
//            catDep.add("پوست");
//            catDep.add("سوختگی");
//            catDep.add("نفرولوژی");
//            catDep.add("عفونی");
//            catDep.add("نوزادان");
//
//            categories.add(new Triple<>(ClinicalUnit.class.getSimpleName(), "بخش های داخلی" + "Intenal Medicine ", catDep));
//
//            catDep = new ArrayList<>();
//            catDep.add("جراحی عمومی");
//            catDep.add("جراحی زنان");
//            catDep.add("جراحی زنان");
//            catDep.add("زایمان و لیبر");
//            catDep.add("ارتوپدی");
//            catDep.add("اورولوژی");
//            catDep.add("گوش و حلق و بینی");
//            catDep.add("چشم");
//            catDep.add("جراحی مغز و اعصاب");
//            catDep.add("جراحی ترمیمی و سوختگی");
//            catDep.add(" جراحی قلب");
//            catDep.add("جراحی عروق");
//
//
//            categories.add(new Triple<>(ClinicalUnit.class.getSimpleName(), "بخش های جراحی", catDep));
//
//            catDep = new ArrayList<>();
//            catDep.add("مراقبت ویژه عمومی");
//            catDep.add("مراقبت حد واسط عمومی٭");
//            catDep.add("ICU جراحی قلب و عروق");
//            catDep.add("ICU کودکان");
//            catDep.add("ICU سوختگی");
//            catDep.add("CCU");
//            catDep.add("ICU نوزادان");
//            catDep.add("پیوند");
//
//            categories.add(new Triple<>(ClinicalUnit.class.getSimpleName(), "بخش مراقبتهای ویژه (ICU)", catDep));
//
//            catDep = new ArrayList<>();
//            catDep.add("سرویسهای تشخیصی (آزمایشگاه، رادیولوژی و غیره)");
//            catDep.add("بانک خون");
//            catDep.add("داروخانه");
//            catDep.add("مهندسی پزشکی");
//            catDep.add("مهندسی ساختمان");
//            catDep.add("مهندسی سیستمهای حیاتی و تأسیسات");
//            catDep.add("آلودگی زدایی");
//            catDep.add("حراست");
//            catDep.add("اداری-مالی");
//
//            categories.add(new Triple<>(NonClinicalUnit.class.getSimpleName(), "سرویسهای پشتیبانی بالینی و غیر بالینی", catDep));
//
//            catDep = new ArrayList<>();
//            catDep.add("مدیریت عملیات بحران و حوادث (فرماندهی، کنترل و هماهنگی)");
//            catDep.add("عوامل پشتیبانی");
//            catDep.add("افسران ارتباطات و اطلاعات");
//            catDep.add("اداری (منابع انسانی، مالی)");
//            catDep.add("مسئولین روابط عمومی و رسانه");
//            catDep.add("کارکنان آمبولانس");
//
//            categories.add(new Triple<>(NonClinicalUnit.class.getSimpleName(), "عملیات بحران و بلایا", catDep));
//
//
//            catDep = new ArrayList<>();
//            catDep.add("Septic surgery جراحی سپتیک");
//            catDep.add("جراحی آسپتیک Aseptic surgegy");
//            catDep.add("جراحی کودکانPediatrics surgery");
//            catDep.add("جراحی زنان و زایمانObstetrics and gynecology surgery");
//            catDep.add("جراحی اورژانس Emergency surgery");
//            categories.add(new Triple<>(SurgeryDepartment.class.getSimpleName(), "operating threaters اتاق های عمل جراحی", catDep));
//
//
//            try {
//
//                int counter = 0;
//
//                for (int i = 0; i < categories.size(); i++) {
//                    Triple<String, String, List<String>> category = categories.get(i);
//                    ContentValues contentValues = new ContentValues();
//                    contentValues.put("ID", i);
//                    contentValues.put("TITLE", category.getSecond());
//                    contentValues.put("TYPE", category.getFirst());
//
//                    db.insert("UNIT_CATEGORY", SQLiteDatabase.CONFLICT_REPLACE, contentValues);
//
//                    for (int i1 = 0; i1 < category.getThird().size(); i1++) {
//
//                        String subCat = category.getThird().get(i1);
//                        contentValues = new ContentValues();
//                        contentValues.put("TITLE", subCat);
//                        contentValues.put("CATEGORY_ID", i);
//
//                        db.insert("UNIT_SUB_CATEGORY", SQLiteDatabase.CONFLICT_REPLACE, contentValues);
//
//                    }
//
//                }
//                db.setTransactionSuccessful();
//            } catch (Exception e) {
//                db.endTransaction();
//            }
//
//
//            QuestionCategory questionCategory = new QuestionCategory();
//            questionCategory.add
//
//            Question  question=new Question();
//            question.setQuestionTitleFa(" تحمل آسیب در عمر مفید سازه");
//            question.setQuestionTitleEn("Prior major structural damage or failure of the hospital building(s)");
//            question.setBriefDescriptionFa("Safety ratings: Low = Major damage and no repairs; Average = Moderate damage and building only partially repaired; High = Minor or no damage, or building fully repaired.");
//
//            List<kotlin.Triple<String,String,String>> modQuestions = new ArrayList<>();
//            modQuestions.add(new kotlin.Triple<>( ,
//                    "Prior major structural damage or failure of the hospital building(s)",
//                    ""));
//            modQuestions.add(new kotlin.Triple<>("",
//                    "طراحی یا مقاوم سازی سازه در تطبیق با آیین نامه های  به روز",
//                    ""));
//            modQuestions.add(new kotlin.Triple<>("",
//                    "",
//                    ""));
//
//            modQuestions.add("   ");
//            modQuestions.add(" تغییر شرایط سازه ای موجود نسبت به فرضیات طراحی");
//
//            questions.add(new Triple<>(Constants.MODULE_STRUCTURAL, "3-1 آسیب یا خرابی بزرگ قبلی سازه های بیمارستان", modQuestions));
//
//            modQuestions = new ArrayList<>();
//            modQuestions.add(" سیستم باربر جانبی");
//            modQuestions.add(" میزان آسیب سازه در شرایط موجود");
//            modQuestions.add(" کیفیت مصالح به کار رفته در ساخت");
//            modQuestions.add(" اندرکنش اجزای غیر سازه ای با سازه");
//            modQuestions.add(" امکان برخورد سازه های مجاور در حرکت دینامیکی");
//            modQuestions.add(" کنترل پلان جانمایی ساختمانها از دیدگاه مهندسی باد و آتش");
//            modQuestions.add(" وجود مسیرهای بالقوه جایگزین برای باربری جانبی");
//            modQuestions.add(" کیفیت اتصالات سازه");
//            modQuestions.add(" نسبت مقاومت ستون به مقاومت تیر");
//            modQuestions.add("آسیب پذیری فونداسیونها");
//
//            questions.add(new Triple<>(Constants.MODULE_STRUCTURAL, "3-2 یکپارچگی ساختمان", modQuestions));
//
//            modQuestions = new ArrayList<>();
//            modQuestions.add("زلزله");
//            modQuestions.add("آتشفشان");
//            modQuestions.add("حرکت توده خشک - رانش زمین به دنبال زلزله");
//            modQuestions.add("سونامی");
//            modQuestions.add("آبگونگی خاک");
//            modQuestions.add("خاک های رسی ");
//            modQuestions.add("شیب های ناپایدار");
//            modQuestions.add("سایر (سقوط سنگ، فرونشست یا نشست خاک، جریان شن)");
//
//
//            questions.add(new Triple<>(Constants.MODULE_HAZARD, "1. مخاطرات زمین شناختی ", modQuestions));
//
//            modQuestions = new ArrayList<>();
//            modQuestions.add("طوفان (تیفون، سیکلون، گردباد)");
//            modQuestions.add("چرخند (تورنادو) ");
//            modQuestions.add("طوفان محلی");
//            modQuestions.add("گردباد");
//            modQuestions.add("طوفان شن");
//            modQuestions.add("باران های سیل آسا");
//            modQuestions.add("سیل برق آسا");
//            modQuestions.add("سیل رودخانه یا یا امواج بلند ناشی از طوفان، سیلهای ساحلی");
//            modQuestions.add("رانش زمین بدنبال بارش شدید و سیل");
//            modQuestions.add("شرایط جوی شدید (گرما یا سرمای شدید)");
//            modQuestions.add("گرد و خاک");
//            modQuestions.add("آتش سوزی طبیعی (جنگل، زمینهای زراعی یا مناطق پرجمعیت)");
//            modQuestions.add("قحطی");
//            modQuestions.add("موارد دیگر مانند افزایش سطح آب دریا");
//
//            questions.add(new Triple<>(Constants.MODULE_HAZARD, "2. مخاطرات آب و هوایی", modQuestions));
//
//            modQuestions = new ArrayList<>();
//            modQuestions.add("تهدید امنیتی برای بیمارستان یا کارکنان آن");
//            modQuestions.add("ناآرامی های مردمی (مانند تظاهرات)");
//            modQuestions.add("حمله مسلحانه");
//            modQuestions.add("رویدادهای با تجمع انبوه");
//            modQuestions.add("جابجایی جمعیت");
//            modQuestions.add("گروگانگیری");
//            modQuestions.add("تهدیدات سایبر (در صورت ارتباط بیمارستان به شبکه اینترنت)");
//
//            questions.add(new Triple<>(Constants.MODULE_HAZARD, "3. پدیده های اجتماعی", modQuestions));
//
//
//            for (int i = 0; i < questions.size(); i++) {
//                Triple<String, String, List<String>> triple = questions.get(i);
//
//                ContentValues contentValues = new ContentValues();
//                contentValues.put("ID", i);
//                contentValues.put("MODULE_NAME", triple.getFirst());
//                contentValues.put("CATEGORY_TITLE", triple.getSecond());
//
//                db.insert("QUESTION_CATEGORY", SQLiteDatabase.CONFLICT_REPLACE, contentValues);
//
//                for (int i1 = 0; i1 < triple.getThird().size(); i1++) {
//                    String title = triple.getThird().get(i1);
//                    contentValues = new ContentValues();
//
//                    contentValues.put("CATEGORY_ID", i);
//                    contentValues.put("QUESTION_TITLE", title);
//                    contentValues.put("CODE", String.format("F_%d_%d", i, i1));
//                    contentValues.put("HELP_LINK", "https://www.apple-nic.com/applestore/iphone-shop/iphone-xr/apple-iphone-xr-64gb.html");
//                    contentValues.put("BRIEF_DESCRIPTION", "شرکت اپل (به انگلیسی: Apple Inc.) یک شرکت چند ملیتی آمریکایی است که در زمینهٔ طراحی و ساخت لوازم الکترونیکی مصرفی و نرم\u200Cافزار کامپیوتر فعالیت می\u200Cکند. این شرکت ابتدا با نام شرکت کامپیوتری اپل (.Apple Computer Inc) در شهر کوپرتینو در ایالت کالیفورنیا، واقع در دره سیلیکون کشور آمریکا تأسیس شد.\n" +
//                            "\n" +
//                            "این شرکت در دههٔ هفتاد میلادی با معرفی ریزرایانه\u200Cهای اپل I، اپل II، اپل III و پس از آن مکینتاش به بازار به آغاز و گسترش نوعی رایانه شخصی کمک فراوانی نمود.\n" +
//                            "\n" +
//                            "اپل معمولاً به تولید سخت\u200Cافزارهای نوین و دارای طراحی صحیح معروف است. از محصولات سخت\u200Cافزاری این شرکت می\u200Cتوان آیپد، آی\u200Cمک، مک بوک و آی پاد و آیفون و اپل واچ و اپل تی وی را نام برد.\n" +
//                            "\n" +
//                            "همچنین این شرکت در زمینهٔ تولید نرم\u200Cافزار هم فعالیت دارد. نرم\u200Cافزارهای آی\u200Cتونز، آی\u200Cلایف، آی ورک و سیستم\u200Cعامل مورد استفاده در رایانه\u200Cهای این شرکت مک اواس ده نیز نمونه\u200Cهایی از فعالیت نرم\u200Cافزاری این شرکت هستند.\n" +
//                            "\n" +
//                            "مدیریت اجرایی این شرکت برعهده استیو جابز بوده\u200Cاست. وی یک ماه قبل از مرگش از ریاست این شرکت کناره\u200Cگیری کرد و به جای آن تیم کوک ریاست این شرکت را بر عهده گرفته\u200Cاست. استیو جابز همراه با استیو وزنیاک و رونالد وین[۳] این شرکت را بنیانگذاری کردند. رونالد وین در کمتر از دو هفته پس از تأسیس شرکت سهام خود را به جابز و وزنیاک واگذار کرد.[۴][۵]\n" +
//                            "\n" +
//                            "شرکت اپل در سال ۲۰۱۷ برای دهمین سال متوالی، عنوان تحسین شده\u200Cترین شرکت فناوری اطلاعات در جهان را به خود اختصاص داد.[۶]\n" +
//                            "\n" +
//                            "این شرکت از سوی مجله فوربز یکی از مشهورترین مجله\u200Cهای اقتصادی آمریکا و جهان برای هشتمین سال پیاپی (از سال ۲۰۱۱ تا سال ۲۰۱۸) به عنوان با ارزش\u200Cترین برند جهان انتخاب شد.\n" +
//                            "\n" +
//                            "در ماه اوت سال ۲۰۱۸ ارزش سهام شرکت اپل در بازار از مرز یک تریلیون دلار گذشت. این اولین باری است که مجموع ارزش سهام یک شرکت به چنین سطحی می\u200Cرسد و به این ترتیب اپل به نخستین شرکت در جهان با ارزشی بیش از یک تریلیون دلار تبدیل شد.\n" +
//                            "محتویات\n" +
//                            "\n" +
//                            "    ۱ تاریخچه\n" +
//                            "        ۱.۱ بنیانگذاری اپل (۱۹۷۶ تا ۱۹۸۰)\n" +
//                            "        ۱.۲ لیزا و مکینتاش (۱۹۸۱ تا ۱۹۸۹)\n" +
//                            "        ۱.۳ دوران طلایی (۱۹۸۹ تا ۱۹۹۱)\n" +
//                            "        ۱.۴ پاوربوک (۱۹۹۰ تا ۱۹۹۳) و نزول\n" +
//                            "        ۱.۵ همکاری بزرگ (۱۹۹۴ تا ۱۹۹۷)\n" +
//                            "        ۱.۶ شروع دوباره (۱۹۹۸ تا ۲۰۰۵)\n" +
//                            "        ۱.۷ عصر اینتل (۲۰۰۶ تا کنون)\n" +
//                            "        ۱.۸ حرکت به سمت آرم\n" +
//                            "        ۱.۹ هک شدن برخی از سیستم\u200Cها\n" +
//                            "    ۲ نماد اپل\n" +
//                            "    ۳ پیشینه نماد اپل\n" +
//                            "    ۴ خدمات اپل در ایران\n" +
//                            "    ۵ درآمد و سود اپل در سه ماهٔ آخر سال ۲۰۱۴\n" +
//                            "    ۶ نقض حقوق کارگران\n" +
//                            "    ۷ برخی خدمات و محصولات شرکت اپل\n" +
//                            "    ۸ جستارهای وابسته\n" +
//                            "    ۹ پیوند به بیرون\n" +
//                            "    ۱۰ منابع\n" +
//                            "    ۱۱ منابع برای مطالعهٔ بیشتر\n" +
//                            "\n" +
//                            "تاریخچه\n" +
//                            "استیو جابز مؤسس شرکت اپل\n" +
//                            "مرکز شرکت اپل در «اینفینیت لوپ» شهر کوپرتینو در ایالت کالیفرنیای آمریکا\n" +
//                            "بنیانگذاری اپل (۱۹۷۶ تا ۱۹۸۰)\n" +
//                            "\n" +
//                            "کمپانی اپل در سال ۱۹۷۱ با دوستی استیو وزنیاک ۲۱ ساله مهندس کامپیوتر و استیو جابز ۱۶ ساله متولد شد به\u200Cطوری\u200Cکه با گذشت شش سال از آشنایی این دو نفر در سال ۱۹۷۷ این کمپانی با معرفی کامپیوتر شخصی Apple I که در گاراژ خانه جابز به صورت دستی ساخته شده بود رسماً با نام تجاری Apple Computer Inc به بازار تکنولوژی وارد شد و توانست ظرف مدت کوتاهی با فروش تعدادی از این مدل کامپیوتر شخصی، اعتباری برای خود دست و پا کند. بلافاصله در سال ۱۹۷۷ اپل نوع دیگری از کامپیوتر شخصی یعنی Apple II را وارد بازار کرد که به دلیل مجهز بودنش به فلاپی درایو ۵٫۲۵ اینچی از رقبای دیگر خود در آن دوره نظیر Commodore که از نوار مغناطیسی برای ذخیره\u200Cسازی اطلاعات استفاده می\u200Cکرد، پیشی گرفت. در سال ۱۹۸۰ اپل سعی کرد با معرفی مدل Apple III که به نوعی بهینه شده سری Apple II به\u200Cشمار می\u200Cآمد با بزرگ\u200Cترین رقیب آن دوره یعنی آی\u200Cبی\u200Cام که با تجهیز کامپیوترهای خود به سیستم\u200Cعامل DOS توانسته بود سهم زیادی از بازار فروش را به خود اختصاص دهد، وارد رقابت جدی شود اما به دلیل ایراداتی که در طراحی این مدل وجود داشت مجبور شد تا صدها دستگاه از Apple III فروخته شده را به کمپانی برگردانده و از همین\u200Cجا با تعیین و تشخیص راه\u200Cحل\u200Cهای ممکن برای رفع مشکل مدل Apple III ایده ساخت مدلی جدید خلق شد.\n" +
//                            "لیزا و مکینتاش (۱۹۸۱ تا ۱۹۸۹)\n" +
//                            "\n" +
//                            "در سال ۱۹۸۳ اپل با ارائه مدل «لیزا» (Lisa) که اولین کامپیوتر مجهز به ماوس و سیستم\u200Cعامل دارای واسط گرافیکی، آیکون\u200Cها و پنجره\u200Cها بود توانست انقلابی بزرگ در زمینه کامپیوترهای شخصی به وجود آورد. بعد از آن در سال ۱۹۸۴ سیستم\u200Cعامل مکینتاش توسط اپل در حالی روانه بازار شد که تبلیغات بسیار گسترده\u200Cای برایش انجام گرفته بود. ظهور مکینتاش و ارائه نسخه\u200Cهای مختلف برنامه\u200Cهای نشر رومیزی توسط کمپانی ادوبی (Adobe) نظیر PageMaker و بعدها برنامه ساخت انیمیشن موجب شد تا مکینتاش در بین انتشارات و سازندگان فیلم و موسیقی طرفداران بسیاری پیدا کند. بسیاری از نرم\u200Cافزارهای مشهور کنونی مانند مایکروسافت ورد و ادوبی فتوشاپ اولین بار برای کامپیوتر مکینتاش نوشته شدند.\n" +
//                            "\n" +
//                            "اواسط دهه ۱۹۸۰ اپل دارای دو دپارتمان بود: یکی روی سری\u200Cهای Apple II که خاص کاربران خانگی بود و دیگری روی سیستم\u200Cهای مکینتاش که مخصوص کاربران حرفه\u200Cای طراحی شده بود و به نوعی روی این دو دپارتمان تحقیقات بسیاری را انجام داد اما با تمام این تلاش\u200Cها و تحقیقات مکینتاش نتوانست به سهم بازاری مشابه دهه ۷۰ کامپیوترهای Apple II دست پیدا کند، اما همچنان به رقابت خود با کامپیوترهای IBM ادامه داد. (مکینتاش (سیستم Unix) با مک او اس فرق دارد).\n" +
//                            "دوران طلایی (۱۹۸۹ تا ۱۹۹۱)\n" +
//                            "\n" +
//                            "ارائه مدل جدیدی از کامپیوترهای قابل حمل توسط اپل در سال ۱۹۸۹ به نام Macintosh Portable کلید خورد و بعد از آن نوت بوک PowerBook وارد بازار شد. نوت بوکی که با همکاری کمپانی سونی و به\u200Cکارگیری باتری\u200Cهای دو ساعته، صفحه مانیتور ۹ اینچی و هارد درایو ۲۰ مگابایتی به فرمی کاملاً ارگونومیک طراحی شده بود توانست در سال ۱۹۹۱ سود زیادی را نصیب این کمپانی کند. این دوران به قدری در دوران کاری اپل سودآور و درخشان به\u200Cشمار می\u200Cآید که از آن به عنوان «دوران طلایی» یاد می\u200Cکنند.\n" +
//                            "پاوربوک (۱۹۹۰ تا ۱۹۹۳) و نزول\n" +
//                            "\n" +
//                            "با یادگیری چند درس دردناک بعد از معرفی کامپیوتر حجیم مکینتاش پرتابل در ۱۹۸۹، اپل به طراحی صنعتی روی آورد و استراتژی خود را بر پایه ارائه سه مدل کامپیوتر قابل حمل بنا کرد. یکی از این مدل\u200Cها توسط Sony (که دارای مهارت در طراحی قطعات الکترونیکی کوچک، بادوام و کارا بود) ارائه شد. PowerBook ۱۰۰ در سال ۱۹۹۱ ارائه شد و استاندارد شکل و ترکیب ارگونومیک کامپیوترهای لپ\u200Cتاپ را بنا کرد. این محصول شهرت اپل را به عنوان یک تولیدکنندهٔ با کیفیت هم در زمینه کامپیوترهای رومیزی و هم کامپیوترهای قابل حمل تثبیت کرد. موفقیت PowerBook و چند محصول دیگر اپل در این مدت باعث افزایش سود مالی شرکت شد. مجله MacAddict سالهای ۸۹ تا ۹۱ را دوره اول طلایی مکینتاش نامگذاری کرد.\n" +
//                            "\n" +
//                            "ولی دوره طلایی ادامه پیدا نکرد. مایکروسافت ویندوز یک رابط جدید را به نمایش گذاشت که به عقیده بسیاری از مردم از نظر کارایی به راحتی مکینتاش بود. اپل، مایکروسافت را متهم به کپی\u200Cبرداری از روی مکینتاش کرد ولی ویندوز به پیشرفت خود ادامه داد. در طول دهه ۹۰، اپل خط تولید خود را گسترش داد. اپل تنوع زیادی از محصولات را ارائه می\u200Cداد. هزینهٔ تولید این گوناگونی محصولات از یک طرف و گسترش محبوبیت مایکروسافت ویندوز که با تبلیغات زیادی همراه بود از طرف دیگر منجر شد اپل تا مرز ورشکستگی پیش رود.\n" +
//                            "همکاری بزرگ (۱۹۹۴ تا ۱۹۹۷)\n" +
//                            "\n" +
//                            "در اوایل دهه ۹۰ اپل به این نتیجه رسید که برای باقی\u200Cماندن در دنیای کامپیوتر باید مکینتاش را بازنویسی کند. احتیاجات کاربران و برنامه\u200Cهای کامپیوتری با سخت\u200Cافزار و سیستم\u200Cعامل\u200Cهای فعلی قابل پاسخگویی نبودند. در سال ۱۹۹۴ اپل طرفداران خود را با اتحاد با رقیب قدیمی خود یعنی IBM و موتورولا غافلگیر کرد. این یک پیشنهاد برای تولید یک کامپیوتر جدید بود که از سخت\u200Cافزار IBM و موتورولا و نرم\u200Cافزار اپل استفاده می\u200Cکرد. اپل با اتحاد AIM (Apple IBM Motorola) امید داشت که از مایکروسافت تنها رقیبش جلو بیفتد. بعد از آن اپل خط تولید Power Macintosh را راه\u200Cاندازی کرد که از پردازنده PowerPC IBM به جای سری ۶۸k موتورولا استفاده می\u200Cکرد. سیستم\u200Cعامل اپل برای پردازنده جدید بازنویسی شد.\n" +
//                            "\n" +
//                            "در اواسط دهه ۹۰ اپل بر روی بهبود قابلیت\u200Cهای سیستم\u200Cعامل مکینتاش کار می\u200Cکرد، بعد از اولین تلاش برای تغییر کدهای آن اپل به این نتیجه رسید که بهتر است نوشتن یک سیستم\u200Cعامل جدید را آغاز کند و بعد آن را مطابق رابط کاربری مکینتاش اصلاح کند. بعد از شکست پروژه مشترک با IBM برای نوشتن سیستم\u200Cعامل جدید، اپل سیستم\u200Cعامل NEXTstep را خرید که محصول شرکت استیو جابز بود. این باعث بازگشت جابز به اپل شد. در ۹ ژوئیه ۱۹۹۷ هیئت مدیره اپل جابز را بعد از ۱۲ سال ضرر مالی و ارزش سهام پایین به سمت مدیرعامل منصوب کرد. جابز کار خود را به عنوان مدیرعامل موقت شروع کرد و دست به اصلاحات گسترده\u200Cای در ساختار اپل زد.\n" +
//                            "شروع دوباره (۱۹۹۸ تا ۲۰۰۵)\n" +
//                            "خانواده آی\u200Cپاد\n" +
//                            "\n" +
//                            "در ۱۹۹۸ یک سال بعد از برگشت جابز به شرکت، اپل یک مکینتاش کامل را به نام iMac به بازار معرفی کرد، یک طراحی جدید که بیشتر استانداردهای اپل را مانند SCSI و ADB حذف کرده و به جای آن دو درگاه USB قرارداده بود. در حالی که از نظر فنی چندان قابل توجه نبود دارای یک خاصیت ابتکاری جدید بود یعنی پوسته پلاستیکی شفاف که در رنگ\u200Cهای آبی و سفید ارائه می\u200Cشد. ۸۰۰٬۰۰۰ سیستم iMac در سال ۹۸ فروخته شد. بعد از سال ۹۳ این اولین سالی بود که اپل به سود می\u200Cرسید. در سال ۲۰۰۱ اپل سیستم\u200Cعامل Mac OS X را ارائه کرد که بر پایه نکست\u200Cستپ و بی\u200Cاس\u200Cدی بنا شده بود. OS X بین مشتریان و حرفه\u200Cای\u200Cها به ثبات و قابلیت اعتماد بالا همراه با ضریب امنیتی یونیکس و با یک رابط کاربری شناخته شد که استفاده آسان از آن را تضمین می\u200Cکرد. در اواخر ۲۰۰۱ اپل اولین iPod که دستگاه پخش دیجیتال موسیقی قابل حمل بود را ارائه کرد. محصولی که با فروش بیش از ۴۲ میلیون به طرز شگفت\u200Cآوری موفقیت\u200Cآمیز بود. iPod با iTunes (که برنامه\u200Cای برای فروش موسیقی به قیمت هر آهنگ ۹۹ سنت است) همراه شد. تا فوریه ۲۰۰۶ بیش یک میلیارد آهنگ قابل بارگیری از اینترنت برای iPod وجود دارد. در مارس ۲۰۰۵ اپل پشتیبانی خود را از تکنولوژی جدید Blu-Ray سونی اعلام کرد و به Blu-Ray Association پیوست. جابز در ژوین ۲۰۰۵ در یک سخنرانی از فروش کامپیوترهای جدید مکینتاش با پردازنده اینتل در ژانویه ۲۰۰۶ خبر داد. وی همچنین گفت که انتقال مکینتاش از PowerPC به Intel تا سال ۲۰۰۷ طول خواهد کشید.\n" +
//                            "عصر اینتل (۲۰۰۶ تا کنون)\n" +
//                            "مک\u200Cبوک پرو اولین لپ\u200Cتاپ معرفی شده توسط اپل مجهز به پردازنده اینتل معرفی شده در سال ۲۰۰۶\n" +
//                            "\n" +
//                            "اپل در دهم ژانویه ۲۰۰۶ اولین رایانه\u200Cهایش را با پردازنده اینتل ارائه کرد. لپ\u200Cتاپ جدید MacBook Pro جایگزین سری قدیمی PowerBook شده\u200Cاست که حدود ۴ برابر سریعتر است[نیازمند منبع] و آی\u200Cمک جدید که ۲تا ۳ برابر سریعتر شده\u200Cاست. هر دو از پردازنده\u200Cهای اینتل با تکنولوژی Core Duo استفاده می\u200Cکنند. مشارکت اپل و اینتل یک اصطلاح جدید را میان کاربران کامپیوتر به وجود آورده\u200Cاست، مکتل “Mactel” در جواب عبارت “Wintel” که به کامپیوترهایی که با پردازنده Intel و سیستم\u200Cعامل Windows کار می\u200Cکردند اطلاق می\u200Cشد ساخته شده\u200Cاست. البته این عبارت هیچگاه توسط یک مقام رسمی دو شرکت استفاده نشده\u200Cاست و بیشتر در میان طرفداران رواج پیدا کرده\u200Cاست[نیازمند منبع].\n" +
//                            "حرکت به سمت آرم\n" +
//                            "\n" +
//                            "سیاست\u200Cهای کلی شرکت (که نشئت گرفته از نوع تفکر استیو جابز می\u200Cباشد) بر آن است تا محصولاتی یکپارچه بدون وابستگی به دیگر شرکت\u200Cها تولید شود. این وابستگی در ارائهٔ پردازنده\u200Cهای سری برادول اینتل و در نتیجه عقب افتادن ارائهٔ مک بوک\u200Cها، خود را نشان داده\u200Cاست. از همین رو و نیز به دلیل موفقیت پردازنده\u200Cهای مبتنی بر معماری آرم سری Ax مورد استفاده در گجت\u200Cها شرکت، اپل را به استفاده از پردازنده\u200Cهای آرم در کامپیوترهای شخصی نموده\u200Cاست. بر طبق اطلاعات لو رفته از مدیران ارشد اپل، این نوع کامپیوترها تا سال ۲۰۱۶ وارد بازار خواهند شد.\n" +
//                            "هک شدن برخی از سیستم\u200Cها\n" +
//                            "\n" +
//                            "شرکت آمریکایی اَپِل روز سه\u200Cشنبه، اول اسفند، ۱۳۹۱ اعلام کرد که «تعداد کمی» از رایانه\u200Cهای کارکنان این شرکت هدف حمله هکری قرار گرفته اما «هیچ اطلاعاتی به بیرون درز نکرده\u200Cاست.»\n" +
//                            "\n" +
//                            "به گزارش شبکه خبری سی ان ان، شرکت اپل طی بیانیه\u200Cای گفت که این رخنه از طریق یک سایت متعلق به «برنامه\u200Cنویسان خارج از این مجموعه انجام شده\u200Cاست.»\n" +
//                            "به ظاهر هکرها با استفاده از نقطه ضعفی در یک افزونه مرورگر جاوا، موفق به نصب بدافزار خود بر روی دستگاه\u200Cهای مَک چند نفر از کارمندان اپل شده\u200Cاند.\n" +
//                            "\n" +
//                            "در ادامه بیانیه اَپل آمده\u200Cاست: «ما در اَپل تعداد اندکی از سیستم\u200Cهای آلوده را شناسایی و آن\u200Cها را از شبکه حذف کردیم. هیچ مدرکی دال بر خروج داده\u200Cهای اطلاعاتی از اَپل وجود ندارد.» بیانیه رسمی شرکت اَپل زمان وقوع این حمله هکری را مشخص نکرده\u200Cاست.[۷] ");
//
//                    db.insert("QUESTION", SQLiteDatabase.CONFLICT_REPLACE, contentValues);
//
//                }
//
//            }
//            db.setTransactionSuccessful();
//
//
//            db.endTransaction();
//        }

    }

    @Override
    public void onCreate(@androidx.annotation.NonNull SupportSQLiteDatabase db) {
        super.onCreate(db);

    }
}
