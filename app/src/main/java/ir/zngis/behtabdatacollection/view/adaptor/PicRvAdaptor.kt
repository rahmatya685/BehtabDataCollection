package ir.zngis.behtabdatacollection.view.adaptor

import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.databinding.ItemRvPicBinding
import ir.zngis.behtabdatacollection.repository.dataModel.Attachment
import ir.zngis.behtabdatacollection.util.DialogUtil
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.util.GeneralUtil
import ir.zngis.behtabdatacollection.util.ToastUtil
import java.util.ArrayList

class PicRvAdaptor(private val fileInfos: List<Attachment>,
                   private val listener: androidx.fragment.app.Fragment,
                   private var isInSelectionMode: Boolean = false) : androidx.recyclerview.widget.RecyclerView.Adapter<PicRvAdaptor.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: ItemRvPicBinding = DataBindingUtil.inflate<ItemRvPicBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_rv_pic,
                parent,
                false
        )
        val itemView = binding.getRoot()

        val height = parent.measuredHeight / 4

        val params = itemView.getLayoutParams() as androidx.recyclerview.widget.GridLayoutManager.LayoutParams

       // params.height = height
        itemView.setLayoutParams(params)

        return ViewHolder(
                binding, listener as Listener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(fileInfos[position], position)
    }

    override fun getItemCount(): Int {
        return fileInfos.size
    }


    inner class ViewHolder(var binding: ItemRvPicBinding, var listener: Listener) : androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.getRoot()) {

        fun bind(dataItem: Attachment, position: Int) {
            val imageExtensions = ArrayList<String>()
            imageExtensions.add("BMP")

            imageExtensions.add(".GIF")
            imageExtensions.add(".JPEG")
            imageExtensions.add(".PNG")
            imageExtensions.add(".JPG")

            if (imageExtensions.count { it.contentEquals(".${dataItem.extension.toUpperCase()}") } > 0) {
                val picDirec = GeneralUtil.getPicsDirectory(binding.itemCheckbox.getContext())?.getAbsolutePath()
                //dataItem.path = Uri.fromFile(File(String.format("%s//%s", picDirec, dataItem.name)))
            } else {
                dataItem.path = GeneralUtil.getFileExtensionImagePath(itemView.context, dataItem.extension.replace(".", "")).path
            }

            binding.data = dataItem

            binding.executePendingBindings()
            binding.root.setOnLongClickListener { view ->
                if (!isInSelectionMode) {
                    isInSelectionMode = true
                    listener.showContextualMenu()
                    dataItem.isSelected = true
                    notifyItemChanged(position)
                    listener.addItem2DeleteQueue(dataItem, position)
                }
                true
            }
            binding.getRoot().setOnClickListener { view ->
                if (isInSelectionMode) {
                    if (dataItem.isSelected) {
                        dataItem.isSelected = false
                        listener.removeItemFromDeleteQueue(dataItem, position)
                    } else {
                        dataItem.isSelected = true
                        listener.addItem2DeleteQueue(dataItem, position)
                    }
                    notifyItemChanged(position)
                } else {
                    if (imageExtensions.contains(".${dataItem.extension.toUpperCase()}")) {
                        listener.showPicViewer(dataItem.fileName, String.format("%s \n %s ", dataItem.comments, dataItem.fileName),dataItem.fileDownloadUri)
                    } else {
                        listener.openFileIntent(dataItem.fileName)
                    }
                }
            }
            binding.btnEditComment.setOnClickListener { view ->

                DialogUtil.showGetFileNameDialog(view.context, View.OnClickListener {

                    val etName = it.getTag(R.integer.tag_id_get_name_dialog_edittext) as EditText
                    val dialog = it.getTag(R.integer.tag_id_get_name_dialog_layout) as AlertDialog

                    if (etName.text.isEmpty()) {
                        ToastUtil.ShowToast(view.context, view.context.getString(R.string.msg_invalid_inputs), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                        return@OnClickListener
                    }

                    dataItem.comments = etName.text.toString()
                    listener.onAttachmentCommnetChanged(dataItem)
                    notifyItemChanged(position, dataItem)
                     dialog.dismiss()
                }, view.context.getString(R.string.label_edt_comments))

            }

        }
    }

    fun setItemsUnselected() {
        for (fileInfo in fileInfos) {
            fileInfo.isSelected = false
        }
        isInSelectionMode = false
        notifyDataSetChanged()
    }

    interface Listener {

        fun onAttachmentCommnetChanged(attachment: Attachment)

        fun showContextualMenu(): Boolean

        fun addItem2DeleteQueue(fileInfo: Attachment, position: Int)

        fun removeItemFromDeleteQueue(
                fileInfo: Attachment,
                position: Int
        )

        fun showPicViewer(imgPath: String, imgTitle: String, fileDownloadUri: String)

        fun openFileIntent(path: String)
    }
}