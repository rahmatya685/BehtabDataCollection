package ir.zngis.behtabdatacollection.view.adaptor

import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import com.edsab.gedat.edsgeneral.util.inflate
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.repository.dataModel.QuestionCategoryWithQuestion
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.util.PreferencesUtil
import ir.zngis.behtabdatacollection.view.callback.GeneralRvCallback

class SearchQuestionsExListAdaptor(var questions: List<QuestionCategoryWithQuestion>, var listener: GeneralRvCallback) : BaseExpandableListAdapter() {




    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {

        val questionCategory = questions[groupPosition].questionCategory

        val view = parent?.inflate(R.layout.item_question_cat)

        val context = parent!!.context

        view?.let { v ->

            val tv_question_cat_title = v.findViewById<TextView>(R.id.tv_question_cat_title)

            tv_question_cat_title.text = "${questionCategory.number}-" + if (PreferencesUtil.getCurrentLanguage(context) == Constants.ENGLISH) {
                questionCategory.categoryTitleEn

            } else {
                questionCategory.categoryTitleFa
            }

            val tv_count_remained_questions = v.findViewById<TextView>(R.id.tv_count_remained_questions)
            tv_count_remained_questions.text = questionCategory.countRemainedQuestions.toString()

            val tv_count_observed = v.findViewById<TextView>(R.id.tv_count_observed)
            tv_count_observed.text = questionCategory.countVisited.toString()

            val tv_count_unobservable = v.findViewById<TextView>(R.id.tv_count_unobservable)
            tv_count_unobservable.text = questionCategory.countUnVisited.toString()

        }

        return view!!

    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
        val view = parent?.inflate(R.layout.item_question_search_rv)

        val context = parent!!.context

        var question = questions[groupPosition].questions[childPosition]
        view?.let { v ->
            var tv_question_title = v.findViewById<TextView>(R.id.tv_question_title)
            val tv_question_count = v.findViewById<TextView>(R.id.tv_question_count)

            tv_question_title.text = "${question.number}-${
            if (PreferencesUtil.getCurrentLanguage(context) == Constants.ENGLISH) {
                question.questionTitleEn
            } else {
                question.questionTitleFa
            }
            }"

            tv_question_count.text = "${
            if (PreferencesUtil.getCurrentLanguage(context) == Constants.ENGLISH) {
                "Evaluation Count: "
            } else {
                "تعداد پاسخ : "
            }}${question.evaluations.count { it.status != EnumUtil.EntityStatus.DELETE }}"


            if (question.evaluations.count { it.status != EnumUtil.EntityStatus.DELETE } > 0)
                v.setBackgroundColor(ContextCompat.getColor(v.context, R.color.green))
            else
                v.setBackgroundColor(ContextCompat.getColor(v.context, R.color.icons))



            v.setOnClickListener { listener.onItemClicked(question) }

            //PreferencesUtil.putString(Constants.SEARCH_POSITION, position.toString(), viewItem.context)

        }



        return view!!
    }

    override fun getGroup(groupPosition: Int): Any = questions[groupPosition]

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean = true

    override fun hasStableIds(): Boolean = false


    override fun getChildrenCount(groupPosition: Int): Int = questions[groupPosition].questions.size

    override fun getChild(groupPosition: Int, childPosition: Int): Any = questions[groupPosition].questions[childPosition]

    override fun getGroupId(groupPosition: Int): Long = groupPosition.toLong()


    override fun getChildId(groupPosition: Int, childPosition: Int): Long = childPosition.toLong()

    override fun getGroupCount(): Int = questions.size

    fun replaceAll(filteredItems: List<QuestionCategoryWithQuestion>) {
        questions = filteredItems
        notifyDataSetChanged()
    }

    fun add(it: List<QuestionCategoryWithQuestion>) {
         questions = it
    }


}