package ir.zngis.behtabdatacollection.view.ui


import android.location.Location
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.tasks.OnSuccessListener

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Map2Fragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class Map2Fragment : androidx.fragment.app.Fragment() ,Injectable, OnMapReadyCallback, OnSuccessListener<Location>{
    override fun onMapReady(p0: GoogleMap?) {

    }

    override fun onSuccess(p0: Location?) {

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_map2, container, false)
    }


    companion object {

        @JvmStatic
        fun newInstance( ) =
                Map2Fragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }
}
