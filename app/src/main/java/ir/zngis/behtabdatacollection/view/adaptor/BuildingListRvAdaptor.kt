package ir.zngis.behtabdatacollection.view.adaptor

import android.annotation.SuppressLint
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.databinding.ItemBuildingBinding
import ir.zngis.behtabdatacollection.databinding.ItemBuildingWithUnitsBinding
import ir.zngis.behtabdatacollection.repository.dataModel.BaseClass
import ir.zngis.behtabdatacollection.repository.dataModel.BuildingWithUnits
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.view.callback.BuildingListRvCallback
import ir.zngis.behtabdatacollection.view.callback.GeneralRvCallback

class BuildingListRvAdaptor(val buildingWithEvaluations: List<BuildingWithUnits>, val callBack: BuildingListRvCallback, val module: String) : androidx.recyclerview.widget.RecyclerView.Adapter<BuildingListRvAdaptor.ViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, viewType: Int): ViewHolder {

        when (viewType) {
            BUILDING_ONLY -> {
                val binding: ItemBuildingBinding = DataBindingUtil.inflate(LayoutInflater.from(p0.context), R.layout.item_building, p0, false)
                return ViewHolderSingleHospital(binding, callBack)
            }
            BUILDING_WITH_UNITS -> {
                val binding: ItemBuildingWithUnitsBinding = DataBindingUtil.inflate(LayoutInflater.from(p0.context), R.layout.item_building_with_units, p0, false)
                return ViewHolderHospitalWithUnits(binding, callBack)
            }
            else -> {
                return super.createViewHolder(p0, viewType)
            }
        }

    }

    override fun getItemCount() = buildingWithEvaluations.size

    override fun onBindViewHolder(viewHolder: ViewHolder, p1: Int) {
        when (viewHolder) {
            is ViewHolderSingleHospital -> viewHolder.bind(buildingWithEvaluations[p1])
            is ViewHolderHospitalWithUnits -> viewHolder.bind(buildingWithEvaluations[p1])

        }
    }

    override fun getItemViewType(position: Int): Int {
        when (module) {
            Constants.MODULE_HAZARD -> {
                return BUILDING_ONLY
            }
            Constants.MODULE_STRUCTURAL -> {
                return BUILDING_ONLY
            }
            Constants.MODULE_NON_STRUCTURAL -> {
                return BUILDING_WITH_UNITS
            }
            Constants.MODULE_INSTITUTIONAL->{
                return BUILDING_ONLY
            }
            else -> {
                return super.getItemViewType(position)

            }
        }
    }

    open class ViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view)

    class ViewHolderSingleHospital(val binding: ItemBuildingBinding, val callBack: BuildingListRvCallback) : ViewHolder(binding.root) {

        fun bind(dataItem: BuildingWithUnits) {

            binding.dataItem = dataItem.building

            binding.root.setOnClickListener {
                callBack.onBuildingItemClicked(dataItem.building)
            }

            binding.root.setOnLongClickListener {
                callBack.onBuildingItemLongClicked(dataItem.building)
                true
            }
        }
    }

    class ViewHolderHospitalWithUnits(val binding: ItemBuildingWithUnitsBinding, val callBack: BuildingListRvCallback) : ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(dataItem: BuildingWithUnits) {

            binding.dataItem = dataItem


            binding.dataItem?.let {
                binding.tvUnitsLabel.text = "${binding.tvUnitsLabel.text} (${
                it.nonClinicalUnits.filter { it.status != EnumUtil.EntityStatus.DELETE }.size +
                        it.clinicalUnits.filter { it.status != EnumUtil.EntityStatus.DELETE }.size +
                        it.surgeryDepartments.filter { it.status != EnumUtil.EntityStatus.DELETE }.size})"
            }


            val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(binding.root.context)
            layoutManager.isSmoothScrollbarEnabled = true
            layoutManager.orientation = androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
            binding.rvUnits.layoutManager = layoutManager

            val listUnits = mutableListOf<BaseClass>()

            listUnits.addAll(dataItem.clinicalUnits)
            listUnits.addAll(dataItem.nonClinicalUnits)
            listUnits.addAll(dataItem.surgeryDepartments)


            binding.rvUnits.adapter = HospitalDepartmentSimpleAdaptor(listUnits.filter { it.status != EnumUtil.EntityStatus.DELETE }, object : GeneralRvCallback {
                override fun onItemLongClicked(unit: Any) {
                    callBack.onUnitItemLongClicked(dataItem.building, unit)
                }

                override fun onItemClicked(unit: Any) {
                    callBack.onUnitItemClicked(dataItem.building, unit)
                }

            })

        }
    }

    companion object {
        const val BUILDING_ONLY = 10
        const val BUILDING_WITH_UNITS = 45
    }
}