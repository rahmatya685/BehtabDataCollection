package ir.zngis.behtabdatacollection.view.adaptor

import android.annotation.SuppressLint
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.databinding.ItemQuestionBinding
import ir.zngis.behtabdatacollection.databinding.ItemQuestionCatBinding
import ir.zngis.behtabdatacollection.repository.dataModel.EvaluationWithQuestion
import ir.zngis.behtabdatacollection.repository.dataModel.QuestionCategory
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.PreferencesUtil
import ir.zngis.behtabdatacollection.view.callback.GeneralRvCallback
import ir.zngis.behtabdatacollection.view.ui.MainActivity

class QuestionRvAdaptor(private val questionsWithCates: List<Any>, val callback: GeneralRvCallback) : androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder>() {


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): BaseViewHolder {

        var viewHolder: BaseViewHolder? = null
        val inflator = LayoutInflater.from(viewGroup.context)
        when (viewType) {
            VIEW_QUESTION -> {
                val binding: ItemQuestionBinding = DataBindingUtil.inflate(inflator, R.layout.item_question, viewGroup, false)
                viewHolder = QuestionViewHolder(binding, callback)
            }
            VIEW_SUB_QUESTION -> {
                val binding: ItemQuestionCatBinding = DataBindingUtil.inflate(inflator, R.layout.item_question_cat, viewGroup, false)
                viewHolder = QuestionCategoryViewHolder(binding)
            }
        }
        return viewHolder!!
    }

    override fun getItemCount() = questionsWithCates.size

    override fun onBindViewHolder(baseViewHolder: BaseViewHolder, positiob: Int) {
        when (baseViewHolder) {
            is QuestionViewHolder -> {

                val currentQuestion = questionsWithCates[positiob] as EvaluationWithQuestion

                baseViewHolder.bind(currentQuestion   )
            }
            is QuestionCategoryViewHolder -> {
                baseViewHolder.bind(questionsWithCates[positiob] as QuestionCategory)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (questionsWithCates[position]) {
            is EvaluationWithQuestion -> {
                VIEW_QUESTION
            }
            is QuestionCategory -> {
                VIEW_SUB_QUESTION
            }
            else -> super.getItemViewType(position)
        }
    }


    class QuestionViewHolder(val binding: ItemQuestionBinding, val callback: GeneralRvCallback) : BaseViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(dataItem: EvaluationWithQuestion  ) {


            binding.tvQuestionTitle.text = "${dataItem.rowNum } - ${
            if (PreferencesUtil.getCurrentLanguage(binding.root.context) == Constants.ENGLISH) {
                dataItem.questionTitleEn
            } else {
                dataItem.questionTitleFa
            }
            }"


            if (MainActivity.IS_RTL) {
                binding.tvEvaluation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_help_circular_filled_button, 0)
            } else {
                binding.tvEvaluation.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_help_circular_filled_button, 0, 0, 0)
            }
            binding.tvEvaluation.text = binding.root.context.getString(R.string.label_unevaluated)

            dataItem.evaluation?.let { evaluation ->


                if (evaluation.hasObserved!!.get()) {
                    when (evaluation.riskLevel) {
                        Constants.NON_RISK_LEVEL -> {
                            if (MainActivity.IS_RTL) {
                                binding.tvEvaluation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.success, 0)
                            } else {
                                binding.tvEvaluation.setCompoundDrawablesWithIntrinsicBounds(R.drawable.success, 0, 0, 0)
                            }
                            binding.tvEvaluation.text = binding.root.context.getText(R.string.label_non_occurrence)
                        }
                        Constants.HIGH_RISK_LEVEL -> {
                            if (MainActivity.IS_RTL) {
                                binding.tvEvaluation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_high_risk, 0)
                            } else {
                                binding.tvEvaluation.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_high_risk, 0, 0, 0)
                            }
                            binding.tvEvaluation.text = "${binding.root.context.getText(R.string.label_hazard_level)} ${binding.root.context.getText(R.string.label_high_occurrence)}"

                        }
                        Constants.MEDIUM_RISK_LEVEL -> {
                            if (MainActivity.IS_RTL) {
                                binding.tvEvaluation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_medioum_risk, 0)
                            } else {
                                binding.tvEvaluation.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_medioum_risk, 0, 0, 0)
                            }
                            binding.tvEvaluation.text = "${binding.root.context.getText(R.string.label_hazard_level)} ${binding.root.context.getText(R.string.label_medium_occurrence)}"

                        }
                        Constants.LOW_RISK_LEVEL -> {
                            if (MainActivity.IS_RTL) {
                                binding.tvEvaluation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_low_risk, 0)
                            } else {
                                binding.tvEvaluation.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_low_risk, 0, 0, 0)
                            }
                            binding.tvEvaluation.text = "${binding.root.context.getText(R.string.label_hazard_level)} ${binding.root.context.getText(R.string.label_low_occurrence)}"
                        }
                    }

                } else {
                    binding.tvEvaluation.text = binding.root.context.getString(R.string.label_unable_to_record)
                    if (MainActivity.IS_RTL) {
                        binding.tvEvaluation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_visibility_off_black_24dp, 0)
                    } else {
                        binding.tvEvaluation.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_visibility_off_black_24dp, 0, 0, 0)
                    }
                }
            }




            binding.root.setOnClickListener {
                callback.onItemClicked(dataItem)
            }
            binding.root.setOnLongClickListener {
                callback.onItemLongClicked(dataItem)
                true
            }

        }

    }

    class QuestionCategoryViewHolder(val binding: ItemQuestionCatBinding) : BaseViewHolder(binding.root) {

        fun bind(dataItem: QuestionCategory) {
            binding.questionCat = dataItem
            binding.tvQuestionCatTitle.text = if (PreferencesUtil.getCurrentLanguage(binding.root.context) == Constants.PERSIAN) {
                dataItem.categoryTitleFa
            } else {
                dataItem.categoryTitleEn
            }
        }

    }

    companion object {
        const val VIEW_QUESTION = 100
        const val VIEW_SUB_QUESTION = 12
    }
}