package ir.zngis.behtabdatacollection.view.ui

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.PreferencesUtil
import ir.zngis.behtabdatacollection.view.adaptor.HospitalFrgAdaptor
import ir.zngis.behtabdatacollection.viewModel.SurveyActivityVm
import kotlinx.android.synthetic.main.fragment_hospitals_container.*
import javax.inject.Inject


class HospitalsContainerFragment : androidx.fragment.app.Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hospitals_container, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var viewModel = ViewModelProviders.of(this, viewModelFactory).get(SurveyActivityVm::class.java)

        viewModel.getHospitals()?.observe(this, Observer { it ->
            it?.let {
                vp_hospitals.addOnPageChangeListener(object : androidx.viewpager.widget.ViewPager.OnPageChangeListener {
                    override fun onPageScrollStateChanged(p0: Int) {

                    }

                    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
                    }

                    override fun onPageSelected(curre: Int) {
                        PreferencesUtil.putInteger(Constants.CURRENT_PAGE_HOSPITAL_VIEW_PAGER, vp_hospitals.currentItem, activity)
                        setCurrentPage()
                    }

                })
                vp_hospitals.adapter = HospitalFrgAdaptor(childFragmentManager, it)


                val currentPageIndex = PreferencesUtil.getInteger(Constants.CURRENT_PAGE_HOSPITAL_VIEW_PAGER, activity)
                if (currentPageIndex != -1) {
                    vp_hospitals.currentItem = currentPageIndex
                }

                setCurrentPage()
            }
        })

        btn_next.setOnClickListener(this::onBtnNextClicked)
        btn_previous.setOnClickListener(this::onBtnPreviousClicked)

    }

    override fun onResume() {
        val mainActivity = activity as MainActivity
        mainActivity.supportActionBar?.title = getString(R.string.label_survey)
        mainActivity.supportActionBar?.subtitle=""
        super.onResume()
    }

    fun setCurrentPage() {
        vp_hospitals.adapter?.let {
            tv_current_page.text = "${vp_hospitals.currentItem + 1} / ${it.count} "
        }
    }

    fun onBtnNextClicked(view: View) {
        vp_hospitals.adapter?.let {
            if (vp_hospitals.currentItem < it.count)
                vp_hospitals.currentItem = ++vp_hospitals.currentItem
        }
    }


    fun onBtnPreviousClicked(view: View) {
        vp_hospitals.adapter?.let {
            if (vp_hospitals.currentItem > 0)
                vp_hospitals.currentItem = --vp_hospitals.currentItem

        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
                HospitalsContainerFragment().apply {
                    arguments = Bundle().apply {
                    }
                }
    }


}
