package ir.zngis.behtabdatacollection.view.adaptor

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.databinding.ItemHeaderBuildingBinding
import ir.zngis.behtabdatacollection.databinding.ItemHospitalDepartmentClinicalUnitBinding
import ir.zngis.behtabdatacollection.databinding.ItemHospitalDepartmentNonClinicalUnitBinding
import ir.zngis.behtabdatacollection.databinding.ItemHospitalDepartmentSurgeryUnitBinding
import ir.zngis.behtabdatacollection.repository.dataModel.*
import ir.zngis.behtabdatacollection.view.callback.GeneralRvCallback

class HospitalDepartmentAdaptor(
        var dataItems: List<BaseClass>, val callback: CallBack) : androidx.recyclerview.widget.RecyclerView.Adapter< BaseViewHolder>() {


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int):  BaseViewHolder {
        var viewHolder: BaseViewHolder? = null
        val inflator = LayoutInflater.from(viewGroup.context)
        when (viewType) {
            SURGERY_DEPARTMENT -> {
                val binding: ItemHospitalDepartmentSurgeryUnitBinding = DataBindingUtil.inflate(inflator, R.layout.item_hospital_department_surgery_unit, viewGroup, false)
                viewHolder = SurgeryDepartmentViewHolder(binding, callback)
            }
            CLINICAL_UNIT -> {
                val binding: ItemHospitalDepartmentClinicalUnitBinding = DataBindingUtil.inflate(inflator, R.layout.item_hospital_department_clinical_unit, viewGroup, false)
                viewHolder = ClinicalUnitViewHolder(binding, callback)
            }
            NON_CLINICAL_UNIT -> {
                val binding: ItemHospitalDepartmentNonClinicalUnitBinding = DataBindingUtil.inflate(inflator, R.layout.item_hospital_department_non_clinical_unit, viewGroup, false)
                viewHolder = NonClinicalUnitViewHolder(binding, callback)
            }
            BUILDING ->{
                val binding :ItemHeaderBuildingBinding =DataBindingUtil.inflate(inflator,R.layout.item_header_building,viewGroup,false)
                viewHolder = BuildingViewHolder(binding, callback)

            }
        }
        return viewHolder!!
    }

    override fun getItemCount() = dataItems.size

    override fun onBindViewHolder(viewHolder:  BaseViewHolder, position: Int) {
        when (viewHolder) {
            is SurgeryDepartmentViewHolder -> {
                viewHolder.bindSurgeryDepartment(dataItems[position] as SurgeryDepartment)
            }
            is ClinicalUnitViewHolder -> {
                viewHolder.bindClinicalUnit(dataItems[position] as ClinicalUnit)
            }
            is NonClinicalUnitViewHolder -> {
                viewHolder.bindNonClinicalUnit(dataItems[position] as NonClinicalUnit)
            }
            is BuildingViewHolder->{
                viewHolder.bindSurgeryDepartment(dataItems[position] as Building)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val dep = dataItems[position]
        return when (dep) {
            is SurgeryDepartment -> SURGERY_DEPARTMENT
            is NonClinicalUnit -> NON_CLINICAL_UNIT
            is ClinicalUnit -> CLINICAL_UNIT
            is Building ->BUILDING
            else -> super.getItemViewType(position)
        }
    }


    class BuildingViewHolder(var binding: ItemHeaderBuildingBinding, val callback: CallBack) : BaseViewHolder(binding.root) {
        fun bindSurgeryDepartment(dataItem: Building) {
            binding.btnAdd.setOnClickListener {
                callback.onAddUnitClicked(dataItem)
            }
            binding.building = dataItem

        }
    }
    class SurgeryDepartmentViewHolder(var binding: ItemHospitalDepartmentSurgeryUnitBinding, val callback: CallBack) : BaseViewHolder(binding.root) {
        fun bindSurgeryDepartment(dataItem: SurgeryDepartment) {
            binding.root.setOnClickListener {
                callback.onItemClicked(dataItem)
            }
            binding.root.setOnLongClickListener {
                callback.onItemLongClicked(dataItem)
                true
            }
            binding.surgeryDepartment = dataItem

        }
    }

    class NonClinicalUnitViewHolder(var binding: ItemHospitalDepartmentNonClinicalUnitBinding, val callback: CallBack) : BaseViewHolder(binding.root) {

        fun bindNonClinicalUnit(dataItem: NonClinicalUnit) {
            binding.root.setOnClickListener {
                callback.onItemClicked(dataItem)
            }
            binding.root.setOnLongClickListener {
                callback.onItemLongClicked(dataItem)
                true
            }
            binding.nonClinicalUnit = dataItem
        }

    }

    class ClinicalUnitViewHolder(var binding: ItemHospitalDepartmentClinicalUnitBinding, val callback: CallBack) : BaseViewHolder(binding.root) {

        fun bindClinicalUnit(dataItem: ClinicalUnit) {
            binding.root.setOnClickListener {
                callback.onItemClicked(dataItem)
            }
            binding.root.setOnLongClickListener {
                callback.onItemLongClicked(dataItem)
                true
            }
            binding.clinicalUnit = dataItem
        }

    }

    companion object {
        const val BUILDING =10
        const val SURGERY_DEPARTMENT = 1000
        const val CLINICAL_UNIT = 105400
        const val NON_CLINICAL_UNIT = 10040

    }

    interface CallBack:GeneralRvCallback{
        fun onAddUnitClicked(building: Building)
    }
}