package ir.zngis.behtabdatacollection.view.ui


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import androidx.appcompat.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.databinding.FragmentHospitalDepartmentBinding
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.*
import ir.zngis.behtabdatacollection.util.Constants
import kotlinx.android.synthetic.main.fragment_hospital_department.*
import javax.inject.Inject
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Toast
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.util.PreferencesUtil
import ir.zngis.behtabdatacollection.util.ToastUtil
import ir.zngis.behtabdatacollection.viewModel.HospitalDepVm


class HospitalDepartmentFragment : FullScreenDialogFragment(), Injectable {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: HospitalDepVm

    lateinit var binding: FragmentHospitalDepartmentBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(HospitalDepVm::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_hospital_department, container, false);

        return binding.root
    }


    companion object {
        const val UNIT = "UNIT"
        @JvmStatic
        fun newInstance(unit: BaseClass, building: Building) =
                HospitalDepartmentFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(Constants.BUILDING, building)
                        putParcelable(UNIT, unit)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.navigationIcon = ContextCompat.getDrawable(activity!!, R.drawable.ic_close_black_24dp)

        if (MainActivity.IS_RTL) {
            toolbar.navigationIcon = ContextCompat.getDrawable(activity!!, R.drawable.ic_close_black_24dp)
        }
        toolbar.setNavigationOnClickListener { dismiss() }

        val unit = arguments!![UNIT] as BaseClass

        if (unit.id == 0) {
            toolbar.title = getString(R.string.label_add_department)
        } else {
            toolbar.title = getString(R.string.label_edit_department)
        }

        when (unit) {
            is ClinicalUnit -> {
                layout_clinical_unit.visibility = View.VISIBLE
                binding.clinicalUnit = unit.clone()
            }
            is NonClinicalUnit -> {
                layout_non_clinical_unit.visibility = View.VISIBLE
                binding.nonClinicalUnit = unit.clone()
            }
            is SurgeryDepartment -> {
                layout_surgery_room.visibility = View.VISIBLE
                binding.surgeryDepartment = unit.clone()
            }
        }


        binding.btnSaveChanges.setOnClickListener {

            arguments?.let {

                val building = it[Constants.BUILDING] as Building

                var bindedUnit = BaseClass()

                when (unit) {
                    is ClinicalUnit -> {
                        bindedUnit = binding.clinicalUnit!!
                        bindedUnit.buildingId?.set(building.rowGuid)
                    }
                    is NonClinicalUnit -> {
                        bindedUnit = binding.nonClinicalUnit!!
                        bindedUnit.buildingId?.set(building.rowGuid)
                    }
                    is SurgeryDepartment -> {
                        bindedUnit = binding.surgeryDepartment!!
                        bindedUnit.buildingId?.set(building.rowGuid)
                    }
                }

                ToastUtil.ShowToast(activity!!, getString(R.string.msg_info_information_saved_successfully), EnumUtil.ToastActionState.Info, Toast.LENGTH_SHORT)

                if (unit.id == 0) {
                    viewModel.insertEntity(bindedUnit)
                } else {
                    viewModel.updateEntity(bindedUnit)
                }

                dismiss()
            }


        }


    }


}
