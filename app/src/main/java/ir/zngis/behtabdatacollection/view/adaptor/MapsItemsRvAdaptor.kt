package ir.zngis.behtabdatacollection.view.adaptor

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import com.edsab.gedat.edsgeneral.util.inflate
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.repository.dataModel.Building
import ir.zngis.behtabdatacollection.repository.dataModel.CandidateLocation
import ir.zngis.behtabdatacollection.repository.dataModel.HospitalEntrance
import ir.zngis.behtabdatacollection.view.MapItem
import ir.zngis.behtabdatacollection.view.callback.MapItemsRvAdaptorCallback
import kotlinx.android.synthetic.main.item_map_rv.view.*

class MapsItemsRvAdaptor(private var data: MutableList<MapItem>, var callback: MapItemsRvAdaptorCallback) : androidx.recyclerview.widget.RecyclerView.Adapter<MapsItemsRvAdaptor.ViewHolder>() {


    override fun onCreateViewHolder(parentView: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parentView.inflate(R.layout.item_map_rv), callback)

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(data[position])
    }


    inner class ViewHolder(itemView: View, var generalRvCallback: MapItemsRvAdaptorCallback) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        fun bind(mapItem: MapItem) = with(itemView) {


            val baseClass = mapItem.type
            when (baseClass) {
                is Building -> {
                    tv_item_type.text = context.getString(R.string.label_building)
                    tv_item_name.text = baseClass.name.get()
                }
                is HospitalEntrance -> {
                    tv_item_type.text = context.getString(R.string.label_hospital_entrance)
                    tv_item_name.text = baseClass.name.get()

                }
                is CandidateLocation -> {
                    tv_item_type.text = context.getString(R.string.label_candidate_locations)
                    tv_item_name.text = baseClass.name.get()
                }

            }

            if (mapItem.isSelected) {
                setBackgroundResource(R.drawable.radio_group_bg_selected_state)
            } else {
                setBackgroundResource(R.drawable.radio_group_bg_unselected_state)
            }


            setOnClickListener {

                this@MapsItemsRvAdaptor.data.forEach {
                    it.isSelected = false
                }
                mapItem.isSelected = true
                generalRvCallback.onItemClicked(mapItem)

                notifyDataSetChanged()

            }


            btn_show_menu.setOnClickListener {

                val popupMenu = PopupMenu(it.context, it)

                popupMenu.inflate(R.menu.menu_map_item)


                // Force icons to show
                var menuHelper: Any;
                var argTypes: ArrayList<java.lang.Class<*>>? = null
                try {
                    var fMenuHelper = PopupMenu::class.java.getDeclaredField("mPopup");
                    fMenuHelper.setAccessible(true);
                    menuHelper = fMenuHelper.get(popupMenu);
                    argTypes = arrayListOf(Boolean::class.java)
                    menuHelper::class.java.getDeclaredMethod("setForceShowIcon", *argTypes.toTypedArray()).invoke(menuHelper, true)
                } catch (e: Exception) {
                    // Possible exceptions are NoSuchMethodError and NoSuchFieldError
                    //
                    // In either case, an exception indicates something is wrong with the reflection code, or the
                    // structure of the PopupMenu class or its dependencies has changed.
                    //
                    // These exceptions should never happen since we're shipping the AppCompat library in our own apk,
                    // but in the case that they do, we simply can't force icons to display, so log the error and
                    // show the menu normally.

                    popupMenu.show()
                }

                popupMenu.show()


                popupMenu.setOnMenuItemClickListener { menuItem ->
                    when (menuItem.itemId) {
                        R.id.action_delete -> {
                            generalRvCallback.onDeleteMenuClicked(mapItem)
                        }
                        R.id.action_edit_geom -> {
                            generalRvCallback.onEditGeomMenuClicked(mapItem)
                        }
                        R.id.action_edit_attribute -> {
                            generalRvCallback.onEditAttributeMenuClicked(mapItem)
                        }
                    }

                    true
                }

            }


        }
    }


    fun addItem(mapItem: MapItem) {

        data.add(mapItem)

        notifyDataSetChanged()
    }

    fun removeItem(mapItem: MapItem) {

        var deleteIndex = data.indexOfFirst { it.type == mapItem.type && it.type.id == mapItem.type.id }

        if (deleteIndex != -1)
            data.removeAt(deleteIndex)

        notifyDataSetChanged()
    }
}