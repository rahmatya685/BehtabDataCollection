package ir.zngis.behtabdatacollection.view.ui

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.telecom.Call
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import ir.zngis.behtabdatacollection.R;
import ir.zngis.behtabdatacollection.util.GeneralUtil
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.fragment_selectableitem_list_dialog.*
import kotlinx.android.synthetic.main.fragment_selectableitem_list_dialog_item.view.*

// TODO: Customize parameter argument names
const val ARG_ITEM_COUNT = "item_count"

/**
 *
 * A fragment that shows a list of items as a modal bottom sheet.
 *
 * You can show this modal bottom sheet from your activity like this:
 * <pre>
 *    SelectableListDialogFragment.newInstance(30).show(supportFragmentManager, "dialog")
 * </pre>
 *
 * You activity (or fragment) needs to implement [SelectableListDialogFragment.Listener].
 */
class SelectableListDialogFragment : BottomSheetDialogFragment() {


    private lateinit var mCallback: Callback

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_selectableitem_list_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        list.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
        list.adapter = SelectableItemAdapter(arguments?.getParcelableArrayList(ARG_ITEM_COUNT)!!)

        btnOk.setOnClickListener {
            if (::mCallback.isInitialized) {
                mCallback.onDialogClose()
                dismiss()
            }

        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment

    }

    override fun onDetach() {
        super.onDetach()
    }


    private inner class ViewHolder internal constructor(var viewItem: View)
        : androidx.recyclerview.widget.RecyclerView.ViewHolder(viewItem) {
        fun bind(selectableItem: SelectableItem) = with(viewItem) {
            text.setOnCheckedChangeListener { buttonView, isChecked ->
                selectableItem.checked = isChecked
            }
            text.text = selectableItem.displayValue
            text.isChecked = selectableItem.checked

            text.typeface = GeneralUtil.getTypeFace(context)
        }


    }

    private inner class SelectableItemAdapter internal constructor(private val selectableItems: List<SelectableItem>) : androidx.recyclerview.widget.RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            var view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_selectableitem_list_dialog_item, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(selectableItems[position])
        }

        override fun getItemCount(): Int = selectableItems.size
    }

    companion object {

        fun newInstance(items: ArrayList<SelectableItem>, callback: Callback): SelectableListDialogFragment =
                SelectableListDialogFragment().apply {
                    this.mCallback = callback
                    arguments = Bundle().apply {
                        putParcelableArrayList(ARG_ITEM_COUNT, items)
                    }
                }

    }

    interface Callback {
        fun onDialogClose()
    }

    @Parcelize
    class SelectableItem(var key: String,
                         var displayValue: String) : Parcelable {

        var checked = false
    }
}
