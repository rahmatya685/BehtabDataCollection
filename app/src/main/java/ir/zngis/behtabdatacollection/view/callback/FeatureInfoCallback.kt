package ir.zngis.behtabdatacollection.view.callback

interface FeatureInfoCallback {

    fun featureSave()

    fun featureCanceled()
}