package ir.zngis.behtabdatacollection.view.ui

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import androidx.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.databinding.FragmentHospitalEntranceBinding
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.Hospital
import ir.zngis.behtabdatacollection.repository.dataModel.HospitalEntrance
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.util.ToastUtil
import ir.zngis.behtabdatacollection.view.callback.FeatureInfoCallback
import ir.zngis.behtabdatacollection.viewModel.HospitalEntranceVm
import kotlinx.android.synthetic.main.fragment_hospital_entrance.*
import javax.inject.Inject


class HospitalEntranceFragment : FullScreenDialogFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: HospitalEntranceVm
    lateinit var binding: FragmentHospitalEntranceBinding

    lateinit var mFeatureInfoCallback: FeatureInfoCallback

    private var mIsSaved = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HospitalEntranceVm::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_hospital_entrance, container, false)
        return binding.root
    }


    companion object {
        @JvmStatic
        fun newInstance(hospitalEntrance: HospitalEntrance, hospital: Hospital, featureInfoCallback: FeatureInfoCallback) =
                HospitalEntranceFragment().apply {
                    this.mFeatureInfoCallback = featureInfoCallback
                    arguments = Bundle().apply {
                        putParcelable(Constants.HOSPITAL_ENTRANCE, hospitalEntrance)
                        putParcelable(Constants.HOSPITAL, hospital)

                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        toolbar.navigationIcon = ContextCompat.getDrawable(activity!!, R.drawable.ic_close_black_24dp)
        if (MainActivity.IS_RTL) {
            toolbar.navigationIcon = ContextCompat.getDrawable(activity!!, R.drawable.ic_close_black_24dp)
        }
        toolbar.setNavigationOnClickListener { dismiss() }

        val hospital = arguments!![Constants.HOSPITAL] as Hospital

        toolbar.title = hospital.name.get()


        arguments?.let {
            val hospitalEntrance = it[Constants.HOSPITAL_ENTRANCE] as HospitalEntrance
            toolbar.subtitle = hospitalEntrance.name.get()

            binding.entrance = hospitalEntrance

        }


        binding.btnSaveChanges.setOnClickListener {
            if (binding.entrance?.name?.get().isNullOrEmpty()) {
                ToastUtil.ShowToast(activity!!, getString(R.string.msg_error_invalid_name), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                return@setOnClickListener
            }

            binding.entrance?.let { e ->
                if (e.id == 0) {
                    viewModel.localRepo.insertEntity(e)
                } else {
                    viewModel.localRepo.updateEntity(e)
                }

            }

            mFeatureInfoCallback.featureSave()
            mIsSaved = true

            dismiss()
        }


        binding.btnAttachments.setOnClickListener {
            val attachments = AttachmentsFragment.newInstance(getString(R.string.label_attachments), HospitalEntrance::class.java.simpleName, binding.entrance!!.rowGuid)
            attachments.show(childFragmentManager, AttachmentsFragment::class.java.simpleName)
        }
    }


    override fun onDestroy() {
        if (!mIsSaved) {
            mFeatureInfoCallback.featureCanceled()
        }
        super.onDestroy()
    }
}
