package ir.zngis.behtabdatacollection.view.ui

import android.Manifest
import android.app.Dialog
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import android.telephony.TelephonyManager
import android.view.View
import android.widget.Toast
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.repository.dataModel.User
import ir.zngis.behtabdatacollection.util.*
import ir.zngis.behtabdatacollection.view.callback.ApiObserver
import ir.zngis.behtabdatacollection.viewModel.UserVm

import kotlinx.android.synthetic.main.activity_login.*
import java.util.*
import javax.inject.Inject

class LoginActivity : BaseActivity(), HasAndroidInjector {


    @Inject
    lateinit var supFragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Any>


    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: UserVm

    lateinit var mImei: String

    var progressDialog: Dialog? = null

    companion object {
        const val PERMISSION_READ_PHONE_STATE_CODE = 5436
    }


    override fun onResume() {
        super.onResume()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //   setlang()


        setContentView(R.layout.activity_login)
        supportActionBar?.hide()

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(UserVm::class.java)


        ask4PhoneInfoPermission()

        btnOk.setOnClickListener {

            if (!::mImei.isInitialized) {
                ToastUtil.ShowToast(this, getString(R.string.msg_warning_reading_phone_info_permmision_is_required_for_login), EnumUtil.ToastActionState.Info)
                ask4PhoneInfoPermission()
                return@setOnClickListener
            }

            if (editTextUserName.text.isNullOrEmpty() || editTextUrPass.text.isNullOrEmpty()) {
                ToastUtil.ShowToast(this@LoginActivity, getString(R.string.msg_invalid_inputs), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                return@setOnClickListener
            }

            progressDialog = DialogUtil.showProgressBar(this, getString(R.string.msg_progress_recieving_user_info))
            progressDialog?.show()

            viewModel.login(editTextUserName.text.toString(), editTextUrPass.text.toString(), mImei).observe(this, ApiObserver(object : ApiObserver.ChangeListener<User> {

                override fun onSuccess(user: User) {
                    progressDialog?.let {
                        it.dismiss()
                    }
                    startMainActivity(user.name)
                }

                override fun onError(exception: Throwable) {
                    progressDialog?.let {
                        it.dismiss()
                    }
                    ToastUtil.ShowToast(this@LoginActivity, exception.message, EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                }
            }))
        }
    }

    fun setlang() {
        if (PreferencesUtil.getString(Constants.LANGUAGE, this) == Constants.PERSIAN) {
            val iran = Locale("fa", "IRA")
            updateLocale(iran)
        } else {
            updateLocale(Locale.ENGLISH)
        }
    }


    private fun startMainActivity(name: String?) {
        runOnUiThread {
            val b = Handler().postDelayed({
                val i = Intent(this, MainActivity::class.java)
                i.putExtra(Constants.USER_NAME, name)
                startActivity(i)
                try {
                    overridePendingTransition(R.anim.slide_up, R.anim.slide_down)
                } catch (e: Exception) {
                }
                finish()
                //mProgress.setVisibility(View.GONE);
            }, 100)
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) =
            if (requestCode == PERMISSION_READ_PHONE_STATE_CODE) {
                if (permissions.isEmpty() || (permissions.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED)) {
                    DialogUtil.showInfo(this, getString(R.string.msg_warning_reading_phone_info_permmision_is_required_for_login), getString(R.string.label_try_again), "", View.OnClickListener {
                        val dialog = it.tag as Dialog
                        if (it.id == R.id.btnOk) {
                            ask4PhoneInfoPermission()
                        }
                        dialog.dismiss()
                    })
                } else {
                    ask4PhoneInfoPermission()
                }
            } else {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }

    private fun ask4PhoneInfoPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                var listPermission = mutableListOf<String>()
                listPermission.add(Manifest.permission.READ_PHONE_STATE)
                requestPermissions(listPermission.toTypedArray(), PERMISSION_READ_PHONE_STATE_CODE)
            }
            ToastUtil.ShowToast(this, "لطفا مجوز دسترسی به Phone Info را فعال نمایید", EnumUtil.ToastActionState.Info, Toast.LENGTH_LONG)

        } else {

            val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager?

            val android_id = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID);

            try {
                mImei = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    if (telephonyManager != null && telephonyManager.imei != null)
                        telephonyManager.imei
                    else
                        android_id
                } else {
                    if (telephonyManager != null && telephonyManager.deviceId != null)
                        telephonyManager.deviceId
                    else
                        android_id
                }
            } catch (e: Exception) {
                mImei = android_id;
                e.printStackTrace()
            }
        }
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return supFragmentDispatchingAndroidInjector
    }

}
