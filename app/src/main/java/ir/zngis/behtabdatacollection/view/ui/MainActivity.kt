package ir.zngis.behtabdatacollection.view.ui

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.http.HttpResponseCache
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.StrictMode
import com.google.android.material.navigation.NavigationView
import androidx.core.app.ActivityCompat
import androidx.appcompat.app.AppCompatActivity
import ir.zngis.behtabdatacollection.R
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import androidx.core.text.TextUtilsCompat
import androidx.core.view.ViewCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import android.telephony.TelephonyManager
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.TextView
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import dagger.android.AndroidInjector
import javax.inject.Inject
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ir.zngis.behtabdatacollection.task.GetDataService
import ir.zngis.behtabdatacollection.task.GetDataServiceReceiver
import ir.zngis.behtabdatacollection.task.SyncDataService
import ir.zngis.behtabdatacollection.util.*
import ir.zngis.behtabdatacollection.view.callback.ApiObserver
import ir.zngis.behtabdatacollection.viewModel.UserVm
import ir.zngis.geojson.util.JSONUtils
import org.json.JSONObject
import retrofit2.HttpException
import java.net.ConnectException
import java.security.Permission
import java.util.*
import java.util.jar.Manifest


class MainActivity : BaseActivity(), HasAndroidInjector {

    @Inject
    lateinit var supFragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: UserVm

    lateinit var mToolbar: Toolbar

    lateinit var mNavigationView: NavigationView

    lateinit var mDrawerLayout: androidx.drawerlayout.widget.DrawerLayout

    lateinit var mDrawerToggle: ActionBarDrawerToggle

    var navigationDrawerGravity = Gravity.START

    private lateinit var getDataServiceReceiver: GetDataServiceReceiver

    private lateinit var compositions: CompositeDisposable

    private val PERMISSION_KEY_WRITE_EXTERNAL_STORAGE = 546

    lateinit var mImei: String

    @SuppressLint("CheckResult")

    override fun onCreate(savedInstanceState: Bundle?) {

//        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
//                .detectDiskReads()
//                .detectDiskWrites()
//                .detectNetwork()   // or .detectAll() for all detectable problems
//                .penaltyLog()
//                .build())
//        StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder()
//                .detectLeakedSqlLiteObjects()
//                .detectLeakedClosableObjects()
//                .penaltyLog()
//                .penaltyDeath()
//                .build())

        super.onCreate(savedInstanceState)
//        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        setContentView(R.layout.activity_main)

        compositions = CompositeDisposable()

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(UserVm::class.java)

        IS_RTL = TextUtilsCompat.getLayoutDirectionFromLocale(Locale.getDefault()) == ViewCompat.LAYOUT_DIRECTION_RTL

        mDrawerLayout = findViewById(R.id.drawer_layout)

        mToolbar = findViewById<Toolbar>(R.id.toolbar)

        supportActionBar?.title = getString(R.string.app_name)

        mNavigationView = findViewById(R.id.nav_view)




        mNavigationView.setNavigationItemSelectedListener { menuItem ->
            mDrawerLayout.closeDrawers()
            onNavigationViewItemClicked(menuItem)
            true
        }

        setSupportActionBar(mToolbar)

        val actionBar = supportActionBar
        actionBar?.let { a ->
            a.setDisplayHomeAsUpEnabled(true)
        }


        mDrawerToggle = object : ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close) {


        }

        supportActionBar?.elevation = 10F

        mToolbar.setNavigationOnClickListener { v ->
            if (v.id == -1) {
                if (supportFragmentManager.backStackEntryCount == 0) {
                    if (mDrawerLayout.isDrawerOpen(navigationDrawerGravity)) {
                        mDrawerLayout.openDrawer(navigationDrawerGravity)
                    } else {
                        mDrawerLayout.openDrawer(navigationDrawerGravity)
                    }
                } else {
                    onBackPressed()
                }
            }
        }

        mDrawerToggle.isDrawerIndicatorEnabled = true
        mDrawerToggle.isDrawerSlideAnimationEnabled = true
        mDrawerToggle.setHomeAsUpIndicator(null)
        mDrawerToggle.syncState()


        if (supportFragmentManager.backStackEntryCount > 0) {
            setMyHomeAsUp(true)
        }

        val userName = intent.getStringExtra(Constants.USER_NAME)


        val headerView = mNavigationView.getHeaderView(0)
        headerView?.let { view ->
            val tvUserName = view.findViewById<TextView>(R.id.tv_user_name)
            tvUserName.text = userName
        }


//        getDataServiceReceiver = GetDataServiceReceiver(Handler(), object : GetDataServiceReceiver.Callback {
//            override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
//                when (resultCode) {
//                    Constants.ERROR_CODE -> {
//                        ToastUtil.ShowToast(this@MainActivity, resultData!!.getString(Constants.ERROR), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
//                    }
//                    Constants.DOWNLOAD_COMPLETED_HOSPITALS -> {
//
//                    }
//                    Constants.DOWNLOAD_COMPLETED_QUESTIONS -> {
//
//                    }
//                }
//            }
//
//        })
//
//        val getDataService = Intent(this, GetDataService::class.java)
//        getDataService.putExtra(Constants.GET_DATA_RECIVER_KEY, getDataServiceReceiver)
//        startService(getDataService)


        ask4WritingInternalStorage()


    }

    private fun ask4WritingInternalStorage() {

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION_KEY_WRITE_EXTERNAL_STORAGE)
        }


    }


    private fun onNavigationViewItemClicked(menuItem: MenuItem) {

        when (menuItem.itemId) {
            R.id.nav_setting -> {
                val settingFrg = SettingFragment.newInstance()
                settingFrg.show(supportFragmentManager, SettingFragment::class.java.simpleName)
            }
            R.id.nav_exit -> {
                ask4PhoneInfoPermission()
//                DialogUtil.showInfo(this, getString(R.string.msg_warning_are_you_sure_to_log_out), "", "", View.OnClickListener { v ->
//                    when (v.id) {
//                        R.id.btnOk -> {
//                            ask4PhoneInfoPermission()
//                        }
//                    }
//                    val dialog = v.tag as Dialog
//                    dialog.dismiss()
//                })
            }
        }

    }


    private fun ask4PhoneInfoPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                var listPermission = mutableListOf<String>()
                listPermission.add(android.Manifest.permission.READ_PHONE_STATE)
                requestPermissions(listPermission.toTypedArray(), LoginActivity.PERMISSION_READ_PHONE_STATE_CODE)
            }
            ToastUtil.ShowToast(this, "لطفا مجوز دسترسی به Phone Info را فعال نمایید", EnumUtil.ToastActionState.Info, Toast.LENGTH_LONG)

        } else {

            val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager?

            mImei = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                telephonyManager!!.getImei(0)
            } else {
                telephonyManager!!.getDeviceId()
            }

            logout(mImei)
        }
    }

    private fun logout(mImei: String) {
        viewModel.logOut(mImei).observe(this, ApiObserver<Boolean>(object : ApiObserver.ChangeListener<Boolean> {
            override fun onSuccess(dataWrapper: Boolean) {

                PreferencesUtil.putString(Constants.TOKEN, "", this@MainActivity)
                startLoginActivity()
            }

            override fun onError(exception: Throwable) {
                ToastUtil.ShowToast(this@MainActivity, "", EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
            }

        }))

    }


    private fun startLoginActivity() {
        runOnUiThread {
            val b = Handler().postDelayed({
                val i = Intent(this, LoginActivity::class.java)
                startActivity(i)
                try {
                    //overridePendingTransition(R.anim.s, R.anim.fadeout)
                } catch (e: Exception) {
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    overridePendingTransition(R.anim.slide_up, R.anim.slide_down)
                }
                finish()
                //mProgress.setVisibility(View.GONE);
            }, 100)
        }
    }

    fun getDataFromServer(view: View) {
        var canceled = false
        var progress: Dialog? = null
        compositions.add(viewModel.getRemoteData()!!.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    progress = DialogUtil.showProgressBar(this, getString(R.string.msg_progress_getting_information_form_server), true)
                    progress?.show()
                }.subscribe(
                        { t ->

                            progress?.dismiss()

                            if (!canceled) {
                                if (t.isSuccessful) {
                                    ToastUtil.ShowToast(this, getString(R.string.msg_success_information_upated_successfulluy), EnumUtil.ToastActionState.Success, Toast.LENGTH_SHORT)
                                } else {
                                    DialogUtil.showInfo(this, t.errorBody()!!.string())
                                }
                            }
                        },
                        { t: Throwable ->
                            runOnUiThread {
                                progress?.dismiss()

                                if (t is HttpException) {

                                    t.response().errorBody()?.let {
                                        val error = JSONUtils.optString(JSONObject(it.string()), "message")
                                        DialogUtil.showInfo(this, error)
                                    }

                                } else if (t is ConnectException) {
                                    DialogUtil.showInfo(this, getString(R.string.msg_error_unable_connecting_server))
                                } else {
                                    t.message?.let {

                                        DialogUtil.showInfo(this, it)
                                        Log.e(this.javaClass.simpleName, it)
                                    }
                                }

                            }
                            try {
                                Crashlytics.logException(t);

                            } catch (e: java.lang.Exception) {
                            }
                        }
                ))
        progress?.setOnCancelListener {
            canceled = true
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                if (supportFragmentManager.backStackEntryCount == 0) {
                    mDrawerLayout.openDrawer(navigationDrawerGravity)
                    true
                } else {
                    false
                }
            }
            else -> return super.onOptionsItemSelected(item)
        }

    }


//    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
//        super.onSaveInstanceState(outState, outPersistentState)
//
//        val fragment = supportFragmentManager.findFragmentByTag("MY TAG")
//        if (fragment != null) {
//            supportFragmentManager.putFragment(outState, "KEY", fragment)
//        }
//    }


    fun showFrg(frg: androidx.fragment.app.Fragment, frgName: String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.abc_popup_enter, R.anim.abc_popup_exit)
        transaction.add(R.id.fragment_container, frg, frgName).addToBackStack(frgName).commit()

        if (supportFragmentManager.backStackEntryCount == 0)
            setMyHomeAsUp(true)
    }


    //call this method for animation between hamburged and arrow
    private fun setMyHomeAsUp(isHomeAsUp: Boolean) {
        var anim: ValueAnimator = if (isHomeAsUp) {
            ValueAnimator.ofFloat(0F, 1F)
        } else {
            ValueAnimator.ofFloat(1F, 0F)
        }
        anim.addUpdateListener {
            var slideOffset: Float = it.animatedValue as Float
            mDrawerToggle.onDrawerSlide(mDrawerLayout, slideOffset)
        }
        anim.interpolator = DecelerateInterpolator()
        // You can change this duration to more closely match that of the default animation.
        anim.duration = 400
        anim.start()
    }

    override fun onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(navigationDrawerGravity)) {
            mDrawerLayout.closeDrawers()
        } else {
            super.onBackPressed()
            if (supportFragmentManager.backStackEntryCount == 0) {
                supportActionBar?.title = getString(R.string.app_name)
                supportActionBar?.subtitle = ""
                setMyHomeAsUp(false)
            } else {
                supportFragmentManager.fragments.forEach {
                    if (it !is FullScreenDialogFragment)
                        it.onResume()
                }
            }

        }
    }

    companion object {
        var IS_RTL = false
    }

    fun onBtnSurveyClick(view: View) {
        val frg = HospitalsContainerFragment.newInstance()
        showFrg(frg, HospitalsContainerFragment::class.java.simpleName)
    }

    fun StartSyncService(view: View) {

//        if (GeneralUtil.isServiceRunning(this, SyncDataService::class.java)) {
//            DialogUtil.showInfo(this, getString(R.string.msg_info_data_are_being_sent_to_server_pls_wait))
//            return
//        }س

        ToastUtil.ShowToast(this, getString(R.string.msg_progress_sending_info_server), EnumUtil.ToastActionState.Info)

        val syncDataService = Intent(this, SyncDataService::class.java)
        syncDataService.action = Constants.ACTION_START_SYNC_DATA_SERVICE
        startService(syncDataService)


    }


    override fun onDestroy() {
        super.onDestroy()
        compositions.clear()
    }

    fun relaunch(activity: Activity) {
        val intent = Intent(activity, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        activity.startActivity(intent)
        Runtime.getRuntime().exit(0)
        activity.finish()
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_KEY_WRITE_EXTERNAL_STORAGE) {
            if (permissions.isEmpty() || (permissions.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED)) {
                DialogUtil.showInfo(this, getString(R.string.msg_warning_saving_information_on_hard_disk_requires_permission), getString(R.string.label_try_again), "", View.OnClickListener {
                    val dialog = it.tag as Dialog
                    if (it.id == R.id.btnOk) {
                        ask4WritingInternalStorage()
                    }
                    dialog.dismiss()
                })
            } else {
                GeneralUtil.createDbFolder()
            }
        } else if (requestCode == LoginActivity.PERMISSION_READ_PHONE_STATE_CODE) {
            if (permissions.isEmpty() || (permissions.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED)) {
                DialogUtil.showInfo(this, getString(R.string.msg_warning_reading_phone_info_permmision_is_required_for_login), getString(R.string.label_try_again), "", View.OnClickListener {
                    val dialog = it.tag as Dialog
                    if (it.id == R.id.btnOk) {
                        ask4PhoneInfoPermission()
                    }
                    dialog.dismiss()
                })
            } else {
                ask4PhoneInfoPermission()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }


    }

    override fun androidInjector(): AndroidInjector<Any> {
        return supFragmentDispatchingAndroidInjector
    }

}
