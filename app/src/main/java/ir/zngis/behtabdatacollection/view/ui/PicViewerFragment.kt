package ir.zngis.behtabdatacollection.view.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.util.GeneralUtil
import kotlinx.android.synthetic.main.fragment_pic_viewer.*
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.nio.channels.FileChannel

class PicViewerFragment : FullScreenDialogFragment() {


    lateinit var imgAbsulutePath: String
    var imgTitle: String? = null
    var fileDownloadUri: String  = ""


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_pic_viewer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var file = File(imgAbsulutePath)
        if (file.exists()) {
            showImage()
        } else {
            activity?.let {fragmentActivity ->
                if (fileDownloadUri.isNullOrEmpty())
                    return

                Glide.with(fragmentActivity).downloadOnly().load(fileDownloadUri).listener(object : RequestListener<File> {
                    override fun onLoadFailed(e: GlideException?, model: Any, target: Target<File>, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: File, model: Any, target: Target<File>, dataSource: DataSource, isFirstResource: Boolean): Boolean {

                        val fileName = fileDownloadUri.substring(fileDownloadUri.lastIndexOf("/") + 1, fileDownloadUri.length)

                        val dest = File(GeneralUtil.getPicsDirectory(fragmentActivity).toString() + File.separator + fileName)

                        try {
                            copyFile(resource, dest)
                            showImage()
                        } catch (e: Exception) {

                        }

                        return false
                    }
                }).submit(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)

            }

            title.text = getString(R.string.label_no_file_found)
        }

    }

    private fun showImage() {
        img.orientation = SubsamplingScaleImageView.ORIENTATION_USE_EXIF
        img.setImage(ImageSource.uri(imgAbsulutePath).tilingEnabled())
        title.text = imgTitle
    }

    @Throws(IOException::class)
    fun copyFile(sourceFile: File, destFile: File) {
        if (!destFile.parentFile.exists())
            destFile.parentFile.mkdirs()

        if (!destFile.exists()) {
            destFile.createNewFile()
        }

        var source: FileChannel? = null
        var destination: FileChannel? = null

        try {
            source = FileInputStream(sourceFile).channel
            destination = FileOutputStream(destFile).channel
            destination!!.transferFrom(source, 0, source!!.size())
        } finally {
            source?.close()
            destination?.close()
        }
    }

    companion object {
        fun newInstance(imgAbsulutePath: String, imgTitle: String?, fileDownloadUri: String): PicViewerFragment {
            val fragment = PicViewerFragment()
            fragment.imgAbsulutePath = imgAbsulutePath
            fragment.imgTitle = imgTitle
            fragment.fileDownloadUri = fileDownloadUri
            return fragment
        }
    }
}
