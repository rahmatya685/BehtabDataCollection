package ir.zngis.behtabdatacollection.view.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.Hospital
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject
import androidx.lifecycle.ViewModelProviders
import androidx.core.content.ContextCompat
import ir.zngis.behtabdatacollection.repository.dataModel.HospitalComments
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.Constants.HOSPITAL
import ir.zngis.behtabdatacollection.util.Constants.HOSPITAL_COMMENT
import ir.zngis.behtabdatacollection.util.PreferencesUtil
import ir.zngis.behtabdatacollection.viewModel.SurveyActivityVm
import kotlinx.android.synthetic.main.fragment_modules.*


class ModulesFragment : androidx.fragment.app.Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(SurveyActivityVm::class.java)


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_modules, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val hospital = arguments!![HOSPITAL] as Hospital


        tv_hospital_name.text = hospital.name.get()

        val userIdentifier = PreferencesUtil.getInteger(Constants.USER_IDENTIFIER, activity)

        val isUserEvaluator = hospital.evaluatorUserId == userIdentifier

        val isUserSurveyor = hospital.surveyorUserId == userIdentifier


        card_general_info.setOnClickListener {
            var frg = GeneralModuleFragment.newInstance(hospital)
            (activity as MainActivity).showFrg(frg, GeneralModuleFragment::class.java.simpleName)
        }
        tv_general_info.setOnClickListener { card_general_info.callOnClick() }

        card_hazard_info.setOnClickListener {
            showBuildingListFrg(Constants.MODULE_HAZARD, hospital)
        }
        card_non_structural_info.setOnClickListener {
            showBuildingListFrg(Constants.MODULE_NON_STRUCTURAL, hospital)
        }
        card_structural_info.setOnClickListener {
            showBuildingListFrg(Constants.MODULE_STRUCTURAL, hospital)
        }
        card_institutional_info.setOnClickListener {
            showBuildingListFrg(Constants.MODULE_INSTITUTIONAL, hospital)
        }
        tv_organizational?.setOnClickListener { card_institutional_info.callOnClick() }
        card_map.setOnClickListener {
            val frg = MapFragment.newInstance(hospital)
            (activity as MainActivity).showFrg(frg, MapFragment::class.java.simpleName)
        }


        val disableColor = ContextCompat.getColor(activity!!, R.color.secondary_text)


        card_map.isEnabled = isUserSurveyor
        card_general_info.isEnabled = isUserSurveyor
        tv_general_info.isEnabled = isUserSurveyor


        card_hazard_info.isEnabled = isUserSurveyor || isUserEvaluator
        card_non_structural_info.isEnabled = isUserSurveyor || isUserEvaluator
        card_structural_info.isEnabled = isUserSurveyor || isUserEvaluator
        card_institutional_info.isEnabled = isUserSurveyor || isUserEvaluator

        if (!isUserSurveyor) {
            card_general_info.setCardBackgroundColor(disableColor)
            card_map.setCardBackgroundColor(disableColor)
        }

        if (!(isUserEvaluator || isUserSurveyor)) {
            card_hazard_info.setCardBackgroundColor(disableColor)
            card_non_structural_info.setCardBackgroundColor(disableColor)
            card_institutional_info.setCardBackgroundColor(disableColor)
            card_structural_info.setCardBackgroundColor(disableColor)
            card_institutional_info.setCardBackgroundColor(disableColor)
        }


    }

    private fun showBuildingListFrg(module: String, hospital: Hospital) {
        val frg = BuildingListFragment.newInstance(hospital, module)
        (activity as MainActivity).showFrg(frg, module)
    }


    companion object {

        /**
         * @param hospital Hospital  .
         * @return A new instance of fragment ModulesFragment.
         */
        @JvmStatic
        fun newInstance(hospital: Hospital) =
                ModulesFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(HOSPITAL, hospital)
                    }
                }
    }
}
