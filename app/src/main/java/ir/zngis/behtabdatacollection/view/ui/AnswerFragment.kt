package ir.zngis.behtabdatacollection.view.ui


import android.annotation.SuppressLint
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.databinding.FragmentAnswerBinding
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.*
import ir.zngis.behtabdatacollection.util.*
import ir.zngis.behtabdatacollection.viewModel.AnswerVm
import javax.inject.Inject

class AnswerFragment : androidx.fragment.app.Fragment(), Injectable {


    lateinit var viewModel: AnswerVm

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var binding: FragmentAnswerBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(AnswerVm::class.java)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_answer, container, false)
        return binding.root
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AnswerFragment.
         */
        @JvmStatic
        fun newInstance(hospital: Hospital,building: Building, unit: Any?, moduleName: String, questions: Question) =
                AnswerFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(Constants.HOSPITAL,hospital)
                        putParcelable(Constants.BUILDING, building)
                        unit?.let {
                            putParcelable(Constants.UNIT, it as Parcelable)
                        }
                        putString(Constants.MODULE, moduleName)
                        putParcelable(Constants.QUESTION, questions)
                    }
                }

        @JvmStatic
        fun newInstance2(hospital: Hospital, building: Building, unit: Any?, moduleName: String, evaluation: EvaluationWithQuestion) =
                AnswerFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(Constants.HOSPITAL,hospital)
                        putParcelable(Constants.BUILDING, building)
                        putString(Constants.MODULE, moduleName)
                        putParcelable(Constants.EVALUATION_WITH_QUESTION, evaluation)
                        unit?.let {
                            putParcelable(Constants.UNIT, it as Parcelable)
                        }
                    }
                }
    }

    private fun setFrgTitle() {
        arguments?.let {bundle ->


            val building =bundle[Constants.BUILDING] as Building
            val module = bundle[Constants.MODULE] as String
            val mainActivity = activity as MainActivity
            val hospital =bundle[Constants.HOSPITAL] as Hospital

            mainActivity.mToolbar.title = GeneralUtil.getModuleFaName(activity, module)


            when (module) {
                Constants.MODULE_HAZARD, Constants.MODULE_STRUCTURAL,Constants.MODULE_INSTITUTIONAL-> {
                    mainActivity.supportActionBar?.subtitle = "${hospital.name.get()} || ${building.name.get()}"

                }
                Constants.MODULE_NON_STRUCTURAL -> {

                    arguments?.let { arg ->

                        if (arg.containsKey(Constants.UNIT)) {

                            val unit: Any = arg[Constants.UNIT] as Any

                            when (unit) {
                                is ClinicalUnit -> {
                                    mainActivity.supportActionBar?.subtitle = "${hospital.name.get()} || ${building.name.get()} || ${unit.id}-${unit.nameTemp}"
                                }
                                is NonClinicalUnit -> {
                                    mainActivity.supportActionBar?.subtitle = "${hospital.name.get()} || ${building.name.get()} || ${unit.id}-${unit.nameTemp}"
                                }
                                is SurgeryDepartment -> {
                                    mainActivity.supportActionBar?.subtitle = "${hospital.name.get()} || ${building.name.get()} || ${unit.id}-${unit.nameTemp}"

                                }
                            }

                        }
                    }

                }
                else ->{

                }
            }

        }

    }

    override fun onResume() {
        super.onResume()
        setFrgTitle()
    }


    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val building = arguments!![Constants.BUILDING] as Building
        val module = arguments!![Constants.MODULE] as String

        activity?.let {
            binding.swQuestionTitle.typeface = GeneralUtil.getTypeFace(it)
        }



        setFrgTitle()


        var question: Question? = null
        arguments?.let {
            if (it.containsKey(Constants.QUESTION)) {
                question = it[Constants.QUESTION] as Question
            } else {
                val evaluationWithQuestion = it[Constants.EVALUATION_WITH_QUESTION] as EvaluationWithQuestion

                evaluationWithQuestion.evaluation?.let { evaluation ->
                    question = viewModel.localRepo.getQuestion(evaluation.questionId)

                }
            }
        }

        binding.question = question

        binding.tvQuestionTitle.text = "${
        if (PreferencesUtil.getCurrentLanguage(context) == Constants.ENGLISH) {
            question?.questionTitleEn
        } else {
            question?.questionTitleFa
        }
        }"

        binding.swQuestionTitle.setOnCheckedChangeListener { buttonView, isChecked ->
            binding.evaluation!!.hasObserved?.set(isChecked)
            if (isChecked) {
                binding.layoutRiskLevel.visibility = View.VISIBLE
                binding.swQuestionTitle.text = getString(R.string.lablel_ebable_to_answer_question)

            } else {
                binding.layoutRiskLevel.visibility = View.GONE
                binding.rgRiskLevel.clearCheck()

                binding.swQuestionTitle.text = getString(ir.zngis.behtabdatacollection.R.string.label_unable_to_answer_question)

            }


        }

        binding.rgRiskLevel.setOnCheckedChangeListener { group, checkedId ->
            if (binding.swQuestionTitle.isChecked) {
                binding.evaluation?.riskLevel = when (checkedId) {
                    R.id.rb_high_occurrence -> Constants.HIGH_RISK_LEVEL
                    R.id.rb_low_occurrence -> Constants.LOW_RISK_LEVEL
                    R.id.rb_medium_occurrence -> Constants.MEDIUM_RISK_LEVEL
                    R.id.rb_non_occurrence -> Constants.NON_RISK_LEVEL
                    else -> -1
                }
            }
        }



        if (arguments!!.containsKey(Constants.EVALUATION_WITH_QUESTION)) {

            val evaluationWithQuestion = arguments!![Constants.EVALUATION_WITH_QUESTION] as EvaluationWithQuestion

            evaluationWithQuestion.evaluation?.let { e ->

                binding.evaluation = e

                binding.swQuestionTitle.isChecked = e.hasObserved?.get()!!


                binding.btnSave.tag = "object already persisted"

            }


        } else {
            var evaluation = Evaluation()
            // evaluation.buildingId = building.id
            evaluation.questionId = question!!.id

            binding.evaluation = evaluation

            binding.btnSave.tag = null

        }



        binding.evaluation?.riskLevel.let {
            when (it) {
                Constants.NON_RISK_LEVEL -> binding.rbNonOccurrence.isChecked = true
                Constants.LOW_RISK_LEVEL -> binding.rbLowOccurrence.isChecked = true
                Constants.MEDIUM_RISK_LEVEL -> binding.rbMediumOccurrence.isChecked = true
                Constants.HIGH_RISK_LEVEL -> binding.rbHighOccurrence.isChecked = true
            }
        }



        binding.btnAttachments.setOnClickListener {

            val attachments = AttachmentsFragment.newInstance(getString(R.string.label_attachments), Evaluation::class.java.simpleName, binding.evaluation!!.rowGuid)
            attachments.show(childFragmentManager, AttachmentsFragment::class.java.simpleName)

        }

        binding.btnSave.setOnClickListener {

            when (module) {
                Constants.MODULE_HAZARD, Constants.MODULE_STRUCTURAL,Constants.MODULE_INSTITUTIONAL -> {
                    binding.evaluation?.buildingId = building.rowGuid

                }
                Constants.MODULE_NON_STRUCTURAL -> {
                    arguments?.let { arg ->
                        if (arg.containsKey(Constants.UNIT)) {
                            val unit = arguments!![Constants.UNIT] as Any
                            when (unit) {
                                is ClinicalUnit -> {
                                    binding.evaluation?.clinicalUnitId = unit.rowGuid
                                }
                                is NonClinicalUnit -> {
                                    binding.evaluation?.nonClinicalUnitId = unit.rowGuid
                                }
                                is SurgeryDepartment -> {
                                    binding.evaluation?.surgeryDepartmentId = unit.rowGuid

                                }
                            }
                        }
                    }

                }
            }

            /*
            * if user answers a question more than once, he should provide comment for answers
             */
            if (question != null && question!!.evaluations.count { it.status != EnumUtil.EntityStatus.DELETE } >= 1) {

                binding.evaluation?.comment?.get()?.let {
                    if (it.isNullOrEmpty()) {

                        ToastUtil.ShowToast(activity, getString(R.string.msg_error_pls_provide_commnet_on_this_question), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                        return@setOnClickListener
                    }
                }


            }

            if (binding.swQuestionTitle.isChecked) {
                binding.evaluation?.let {
                    if (it.riskLevel == null) {
                        ToastUtil.ShowToast(activity!!, getString(R.string.msg_level_of_occurance_is_not_defined), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                        return@setOnClickListener
                    }
                }
            } else {
                if (binding.evaluation?.comment?.get().isNullOrEmpty()) {
                    ToastUtil.ShowToast(activity!!, getString(R.string.msg_error_provide_comment_for_undeteriminig_risk_level), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                    return@setOnClickListener
                }
            }

            binding.evaluation?.let {
                it.hasObserved?.set(binding.swQuestionTitle.isChecked)
            }

            binding.evaluation?.let { evaluation ->

                if (evaluation.id == 0) {
                    viewModel.insertEntity(binding.evaluation!!)
                    binding.evaluation = viewModel.localRepo.getLastEvaluation()
                } else {
                    viewModel.updateEntity(binding.evaluation!!)
                }
            }

            ToastUtil.ShowToast(activity!!, getString(R.string.msg_info_information_saved_successfully), EnumUtil.ToastActionState.Success, Toast.LENGTH_LONG)

            binding.btnSave.tag = "sfd"

            activity?.onBackPressed()
        }



    }

    override fun onDestroy() {
        if (binding.btnSave.tag == null)
            binding.evaluation?.rowGuid?.let {
                viewModel.deleteAttachments4Feature(it)
            }
        super.onDestroy()
    }


}
