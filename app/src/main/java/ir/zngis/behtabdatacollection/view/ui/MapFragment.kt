package ir.zngis.behtabdatacollection.view.ui


import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import com.google.android.material.bottomsheet.BottomSheetBehavior
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.*
import com.google.android.gms.maps.model.Polygon
import com.google.android.gms.tasks.OnSuccessListener
import ir.zngis.behtabdatacollection.repository.dataModel.*
import ir.zngis.behtabdatacollection.util.*
import ir.zngis.behtabdatacollection.view.MapItem
import ir.zngis.behtabdatacollection.view.adaptor.MapsItemsRvAdaptor
import ir.zngis.behtabdatacollection.view.callback.FeatureInfoCallback
import ir.zngis.behtabdatacollection.view.callback.MapItemsRvAdaptorCallback
import ir.zngis.behtabdatacollection.view.sub_view.MapVertex
import ir.zngis.behtabdatacollection.viewModel.MapVm
import ir.zngis.geojson.*
import kotlinx.android.synthetic.main.btm_sheet_map.*
import kotlinx.android.synthetic.main.fragment_map.*
import javax.inject.Inject


class MapFragment : androidx.fragment.app.Fragment(), Injectable, OnSuccessListener<Location>, FeatureInfoCallback {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: MapVm


    private var mapView: MapView? = null
    private var map: GoogleMap? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    private val MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey"

    var action4GetLocation = ""

    private var mTempPolygon: Polygon? = null
    private var mPolygonOptions: PolygonOptions? = null

    private var mVertexes = mutableListOf<Marker>()

    var mTempMarker: Marker? = null
    var mMarkerOption: MarkerOptions? = null


    var mMapEditType: EnumUtil.MapEditType? = null

    private lateinit var mMapsItemsRvAdaptor: MapsItemsRvAdaptor


    lateinit var mBottomSheetBehavior: BottomSheetBehavior<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MapVm::class.java)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment


        val rootView = inflater.inflate(R.layout.fragment_map, container, false)

        mapView = rootView.findViewById(R.id.map_view)

        return rootView
    }


    companion object {
        const val REQUEST_CODE_LOCATION = 100

        const val GET_LOCATION_4_ACTION_ZOOM = "GET_LOCATION_4_ACTION_ZOOM"
        const val GET_LOCATION_4_ACTION_DRAW_ENTRANCE = "GET_LOCATION_4_ACTION_DRAW_ENTRANCE"

        @JvmStatic
        fun newInstance(hospital: Hospital) =
                MapFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(Constants.HOSPITAL, hospital)

                    }
                }
    }

    private fun setFrgTitle() {
        val mainActivity = activity as MainActivity

        val hospital = arguments!![Constants.HOSPITAL] as Hospital
        mainActivity.mToolbar.title = getString(R.string.label_draw_on_map)
        mainActivity.mToolbar.subtitle = hospital.name.get()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFrgTitle()

        mBottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)

        mBottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {

            override fun onSlide(p0: View, p1: Float) {

            }

            override fun onStateChanged(p0: View, state: Int) {

//                if (state == BottomSheetBehavior.STATE_COLLAPSED) {
//                    loadInfo()
//                }


            }

        })

        mBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        val hospital = arguments!![Constants.HOSPITAL] as Hospital

        mapView?.onCreate(savedInstanceState)
        mapView?.let { mapView ->
            mapView.isClickable = true
            mapView.getMapAsync(this::onMapReady)
        }

        fab_show_features_list.setOnClickListener {
            mBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }

        fab_layers.setOnClickListener {
            onFabLayersClicked()
        }

        fab_my_location.setOnClickListener {

            action4GetLocation = GET_LOCATION_4_ACTION_ZOOM

            requestLocation4Action()
        }

        fab_add_feature.setOnClickListener {

            showFeatureTypeDialog()
        }

        img_map_center_indicator.setOnClickListener {
            btn_add_vertex.callOnClick()
        }

        btn_add_vertex.setOnClickListener {

            val entraces = viewModel.localRepo.getHospitalEntrances(hospital.id).filter { it.status != EnumUtil.EntityStatus.DELETE }

            if (entraces.isEmpty()) {

                mMapEditType = EnumUtil.MapEditType.HospitalEntrance
                mMarkerOption = getHospitalEntrance()

                btn_undo.visibility = View.GONE

                showEditControls("${getString(R.string.label_drawing)} \n ${getString(R.string.label_hospital_entrance)}")


                if (viewModel.shouldControlLocation()) {

                    action4GetLocation = GET_LOCATION_4_ACTION_DRAW_ENTRANCE

                    requestLocation4Action()

                    return@setOnClickListener
                }


            }

            when (mMapEditType) {

                EnumUtil.MapEditType.HospitalEntrance -> {

                    addEntrance()
                }
                else -> {

                    mPolygonOptions?.let { polygonOptions ->

                        var mapCenter = map!!.cameraPosition.target

                        mPolygonOptions?.add(mapCenter)

                        val tag = mTempPolygon?.tag

                        mTempPolygon?.remove()

                        mTempPolygon = map?.addPolygon(polygonOptions)

                        mTempPolygon?.tag = tag

                        cleanVertexes()

                        drawVertexes()
                    }
                }
            }
        }

        btn_undo.setOnClickListener {

            if (mMapEditType != EnumUtil.MapEditType.HospitalEntrance) {

                mPolygonOptions?.let { polygonOptions ->

                    val points = polygonOptions.points.toMutableList()

                    if (points.isNotEmpty())
                        points.removeAt(points.size - 1)

                    val tag = mTempPolygon?.tag

                    mTempPolygon?.remove()

                    polygonOptions.points?.clear()

                    if (points.isNotEmpty()) {

                        polygonOptions.addAll(points)

                        mTempPolygon = map?.addPolygon(mPolygonOptions)

                        cleanVertexes()

                        drawVertexes()
                    }
                    mTempPolygon?.tag = tag
                }
            }
        }

        btn_cancel.setOnClickListener {

            if (mTempPolygon != null || mTempMarker != null) {
                activity?.let { fragmentActivity ->

                    DialogUtil.showInfo(fragmentActivity, getString(R.string.msg_warning_are_you_sure_to_cancel_drawing),
                            getString(R.string.label_ok),
                            getString(R.string.label_cancel),
                            View.OnClickListener {

                                val dialogInfo = it.tag as Dialog
                                if (it.id == R.id.btnOk) {

                                    cancelEdit()

                                }

                                dialogInfo.dismiss()

                            })


                }
            } else {
                cancelEdit()
            }
        }

        btn_finish_drawing.setOnClickListener {

            when (mMapEditType) {
                EnumUtil.MapEditType.HospitalEntrance -> {

                    if (mTempMarker == null)
                        ToastUtil.ShowToast(activity!!, getString(R.string.msg_error_nothing_drawn_yet), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)

                    mTempMarker?.let { marker ->

                        var hospitalEntrance = HospitalEntrance()

                        hospitalEntrance.hospitalId = hospital.id

                        if (marker.tag != null) {
                            hospitalEntrance = marker.tag as HospitalEntrance
                        }

                        hospitalEntrance.accessPoint = Point(marker.position.latitude, marker.position.longitude)


                        if (hospitalEntrance.id == 0) {
                            showHospitalEntranceFrg(hospitalEntrance, hospital)
                        } else {
                            viewModel.localRepo.updateEntity(hospitalEntrance)
                            showToastSuccess()
                            stopEdit()
                            loadInfo()
                        }

                    }
                }
                EnumUtil.MapEditType.HospitalArea -> {

                    hospital.boundary = getPolygonFromEditFeature()

                    viewModel.localRepo.updateEntity(hospital)

                    showToastSuccess()

                    stopEdit()

                }
                EnumUtil.MapEditType.Building -> {

                    if (mTempPolygon == null) {
                        ToastUtil.ShowToast(activity!!, getString(R.string.msg_error_nothing_drawn_yet), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                        return@setOnClickListener
                    }
                    mTempPolygon?.let {
                        if (it.points.size < 4) {
                            ToastUtil.ShowToast(activity!!, getString(R.string.msg_error_nothing_drawn_yet), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                            return@let
                        }


                        var building = Building()
                        building.hospitalId = hospital.id
                        if (mTempPolygon?.tag != null) {
                            building = mTempPolygon?.tag as Building
                        }
                        building.geom = getPolygonFromEditFeature()

                        if (building.id == 0) {
                            showBuildingFrg(building, hospital)
                        } else {
                            viewModel.localRepo.updateEntity(building)
                            showToastSuccess()
                            stopEdit()
                            loadInfo()
                        }
                    }
                }
                EnumUtil.MapEditType.CandidateLocation -> {

                    if (mTempPolygon == null) {
                        ToastUtil.ShowToast(activity!!, getString(R.string.msg_error_nothing_drawn_yet), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                        return@setOnClickListener
                    }
                    mTempPolygon?.let {
                        if (it.points.size < 4) {
                            ToastUtil.ShowToast(activity!!, getString(R.string.msg_error_nothing_drawn_yet), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                            return@let
                        }

                        var candidateLocation = CandidateLocation()
                        candidateLocation.hospitalId = hospital.id

                        if (mTempPolygon?.tag != null) {
                            candidateLocation = mTempPolygon?.tag as CandidateLocation
                        }
                        candidateLocation.geom = getPolygonFromEditFeature()

                        if (candidateLocation.id == 0) {
                            showCandidateFrg(candidateLocation, hospital)
                        } else {
                            viewModel.localRepo.updateEntity(candidateLocation)
                            showToastSuccess()
                            stopEdit()
                            loadInfo()
                        }
                    }
                }
            }
        }

    }

    private fun addEntrance() {
        var mapCenter = map!!.cameraPosition.target

        mapCenter?.let { mapCenter ->

            mMarkerOption?.position(mapCenter)

            val tag = mTempMarker?.tag

            mTempMarker?.remove()

            mTempMarker = map?.addMarker(mMarkerOption)

            mTempMarker?.tag = tag
        }
    }

    private fun cancelEdit() {
        if (mTempPolygon?.tag == null) {
            mTempPolygon?.remove()
        }
        if (mTempMarker?.tag == null) {
            mTempMarker?.remove()
        }
        mTempPolygon = null
        mTempMarker = null

        hideEditControls()

        fab_add_feature.show()

        fab_show_features_list.show()

        loadInfo()
    }

    private fun stopEdit() {

        mTempPolygon = null
        mTempMarker = null

        cleanVertexes()

        hideEditControls()

        fab_add_feature.show()

        fab_show_features_list.show()
    }

    private fun showCandidateFrg(candidateLocation: CandidateLocation, hospital: Hospital) {
        var frg = CandidateLocationFragment.newInstance(candidateLocation, hospital, this)
        frg.show(childFragmentManager, CandidateLocationFragment::class.java.simpleName)
    }

    private fun showBuildingFrg(building: Building, hospital: Hospital) {
        val frg = BuildingFragment.newInstance(building, hospital, this)
        frg.show(childFragmentManager, BuildingFragment::class.java.simpleName)
    }

    private fun showHospitalEntranceFrg(hospitalEntrance: HospitalEntrance, hospital: Hospital) {
        var frg = HospitalEntranceFragment.newInstance(hospitalEntrance, hospital, this)
        frg.show(childFragmentManager, HospitalEntranceFragment::class.java.simpleName)

    }

    fun showToastSuccess() {
        ToastUtil.ShowToast(activity!!, getString(R.string.msg_info_information_saved_successfully), EnumUtil.ToastActionState.Success, Toast.LENGTH_LONG)
    }


    private fun drawVertexes() {

        mTempPolygon?.points?.let { points ->

            var countPosition = points.size

            if (countPosition > 2)
                countPosition--

            for (i in 0 until countPosition) {

                val position = points[i]

                val markerOptions = MarkerOptions()

                markerOptions.draggable(false)

                markerOptions.position(position)

                var bitmap = GeneralUtil.drawableToBitmap(MapVertex(activity!!, (i + 1).toString(), mTempPolygon!!.strokeColor))

                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                markerOptions.anchor(0.5F, 0.5F)


                var marker = map?.addMarker(markerOptions)

                marker?.let {
                    mVertexes.add(it)
                }

            }
        }

    }

    fun cleanVertexes() {
        mVertexes.forEach {
            it.remove()
        }
        mVertexes.clear()
    }


    @SuppressLint("SetTextI18n")
    private fun showFeatureTypeDialog() {
        activity?.let { activity ->

            var dialog = Dialog(activity)

            mMapEditType = null
            mPolygonOptions = null

            val hospital = arguments!![Constants.HOSPITAL] as Hospital

            val entraces = viewModel.localRepo.getHospitalEntrances(hospital.id).filter { it.status != EnumUtil.EntityStatus.DELETE }

            var types = activity.resources.getStringArray(R.array.drawing_type)

            var icons = intArrayOf(R.drawable.ic_building_map_symbol,
                    R.drawable.ic_hospital_location_pin,
                    R.drawable.ic_hospital_area_map_symbole,
                    R.drawable.ic_candidate_location_map_symbol)

            if (entraces.isEmpty()) {
                types = arrayOf(types[1])
                icons = intArrayOf(R.drawable.ic_hospital_location_pin)
            }

            DialogUtil.showActions(activity, dialog,
                    getString(
                            R.string.label_choose_draw_type),
                    types,
                    icons,
                    View.OnClickListener {
                        it?.let { view ->

                            var textView: TextView = view.findViewById(R.id.action_name)

                            var action = textView.text.toString()

                            val index = types.indexOf(action)

                            btn_undo.visibility = View.VISIBLE

                            when (index) {
                                0 -> {
                                    mMapEditType = EnumUtil.MapEditType.Building
                                    mPolygonOptions = getBuildingPolygonOptions()
                                }
                                1 -> {
                                    mMapEditType = EnumUtil.MapEditType.HospitalEntrance
                                    mMarkerOption = getHospitalEntrance()

                                    btn_undo.visibility = View.GONE
                                }
                                2 -> {
                                    mMapEditType = EnumUtil.MapEditType.HospitalArea
                                    mPolygonOptions = getHospitalPolygonOptions()

                                    viewModel.getHospitalBoudry()?.let {
                                        mTempPolygon = it
                                        zoomToGeom(it)
                                        drawVertexes()
                                    }

                                }
                                3 -> {
                                    mMapEditType = EnumUtil.MapEditType.CandidateLocation
                                    mPolygonOptions = getCandidatePolygonOptions()
                                }
                            }

                            showEditControls("${getString(R.string.label_drawing)} \n $action")

                            dialog.dismiss()
                        }
                    })
        }
    }

    private fun hideEditControls() {

        fab_add_feature.show()

        fab_show_features_list.show()

        layout_map_controls.visibility = View.GONE

        img_map_center_indicator.visibility = View.GONE

        tv_map_title.visibility = View.GONE
    }

    private fun showEditControls(actionType: String) {

        fab_add_feature.hide()

        fab_show_features_list.hide()

        layout_map_controls.visibility = View.VISIBLE

        mBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        img_map_center_indicator.visibility = View.VISIBLE

        tv_map_title.text = actionType

        tv_map_title.visibility = View.VISIBLE

        img_map_center_indicator.visibility = View.VISIBLE
    }

    private fun onMapReady(map: GoogleMap?) {

        this.map = map

        this.map?.mapType = PreferencesUtil.getMapPref(Constants.MAP_TYPE, activity)

        this.map?.apply {

            uiSettings.isMapToolbarEnabled = false
            uiSettings.isMyLocationButtonEnabled = false
            uiSettings.isCompassEnabled = true

            //returning true is for disabling map from auto centering map after onMarkerClick event fired
            setOnMarkerClickListener {
                true
            }
        }

        try {
            val parent = mapView?.findViewWithTag<ImageView>("GoogleMapMyLocationButton")?.parent as ViewGroup
            parent.post {
                try {
                    val r = getResources()
                    //convert our dp margin into pixels
                    val marginPixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20F, r.displayMetrics).toInt()
                    // Get the map compass view
                    val mapCompass = parent.getChildAt(4)

                    // create layoutParams, giving it our wanted width and height(important, by default the width is "match parent")
                    val rlp = RelativeLayout.LayoutParams(mapCompass.height, mapCompass.height)
                    // position on top right
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT)
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0)
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
//                    rlp.addRule(RelativeLayout.CENTER_VERTICAL,0)
                    //give compass margin
                    rlp.setMargins(marginPixels, marginPixels, marginPixels, marginPixels)
                    mapCompass.layoutParams = rlp

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) run {
            requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                    REQUEST_CODE_LOCATION)
        } else {
            map?.isMyLocationEnabled = true
            action4GetLocation = GET_LOCATION_4_ACTION_ZOOM
            requestLocation4Action()
        }
        loadInfo()
    }

    private fun requestLocation4Action() {
        if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_CODE_LOCATION)

            return
        }
        mFusedLocationClient?.lastLocation?.addOnSuccessListener(this)

    }

    override fun onSuccess(location: Location?) {

        val manager = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if (!statusOfGPS) {

            activity!!.runOnUiThread {
                DialogUtil.showInfo(activity!!, getString(R.string.msg_info_enable_gps), "", "", View.OnClickListener { v ->
                    when (v.id) {
                        R.id.btnOk -> enableGps()
                        R.id.btnCancel -> {
                        }
                    }
                    val dialog = v.tag as Dialog
                    dialog.dismiss()
                })
            }
            return
        }

        if (location == null) {
            ToastUtil.ShowToast(activity, getString(R.string.msg_info_location_not_received_yet_pls_waite), EnumUtil.ToastActionState.Info, Toast.LENGTH_LONG)
        } else {
            when (action4GetLocation) {
                GET_LOCATION_4_ACTION_DRAW_ENTRANCE -> {

                    val hospital = arguments!![Constants.HOSPITAL] as Hospital

                    if (hospital.centerPoint != null) {

                        val hospitalCenter = Location("")

                        hospitalCenter.latitude = hospital.centerPoint!!.position.latitude
                        hospitalCenter.longitude = hospital.centerPoint!!.position.longitude


                        val mapCenter = map!!.cameraPosition.target

                        //for hospitals with no location ignore location constraints
                        if (hospitalCenter.latitude == 0.0 && hospitalCenter.longitude == 0.0) {
                            hospitalCenter.latitude = mapCenter.latitude
                            hospitalCenter.longitude = mapCenter.longitude
                        }


                        val mapCenterLocation = Location("")

                        mapCenterLocation.longitude = mapCenter.longitude
                        mapCenterLocation.latitude = mapCenter.latitude


                        val distHospitalCenter2UserLocation = location.distanceTo(hospitalCenter)

                        val distClickedLocation2UserLocation = location.distanceTo(mapCenterLocation)

                        if (distHospitalCenter2UserLocation < viewModel.getDistanceToFeature() && distClickedLocation2UserLocation < viewModel.getDistanceToFeature()) {

                            addEntrance()

                        } else {
                            ToastUtil.ShowToast(activity, getString(R.string.msg_info_you_are_not_in_alowded_distance_to_entrance), EnumUtil.ToastActionState.Info, Toast.LENGTH_LONG)
                        }

                    } else {
                        ToastUtil.ShowToast(activity, getString(R.string.msg_error_hospital_center_have_not_been_drawn), EnumUtil.ToastActionState.Info, Toast.LENGTH_LONG)
                    }
                }
                GET_LOCATION_4_ACTION_ZOOM -> {
                    zoomToCuLoc(location)
                }
            }
        }
    }


    private fun enableGps() {
        val gpsOptionsIntent = Intent(
                Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        startActivityForResult(gpsOptionsIntent, REQUEST_CODE_LOCATION)
    }


    private fun zoomToCuLoc(location: Location?) {
        if (location != null) {
            val sydney = LatLng(location.latitude, location.longitude)
            val cameraPosition = CameraPosition.Builder().target(sydney).zoom(17f).build()
            map?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        var mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY)
        if (mapViewBundle == null) {
            mapViewBundle = Bundle()
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle)
        }

        mapView?.onSaveInstanceState(mapViewBundle)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CODE_LOCATION -> {
                val manager = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                val statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                if (statusOfGPS) {
                    if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return
                    }
                    mFusedLocationClient?.getLastLocation()?.addOnSuccessListener(this)
                } else {
                    ToastUtil.ShowToast(activity, R.string.msg_info_gps_required_for_showing_current_location, EnumUtil.ToastActionState.Failure)
                }
            }
        }

    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()

    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
        setFrgTitle()
    }

    override fun onStart() {
        super.onStart()
        mapView?.onStart()

    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

    private fun onFabLayersClicked() {

        map?.let { googleMap ->

            when (googleMap.mapType) {
                GoogleMap.MAP_TYPE_SATELLITE -> {
                    map?.mapType = GoogleMap.MAP_TYPE_NORMAL
                    ToastUtil.ShowToast(activity, getString(R.string.label_map_type_basic), EnumUtil.ToastActionState.Info)

                }
                GoogleMap.MAP_TYPE_NORMAL -> {
                    map?.mapType = GoogleMap.MAP_TYPE_SATELLITE
                    ToastUtil.ShowToast(activity, getString(R.string.label_map_type_satelite), EnumUtil.ToastActionState.Info)

                }
            }
            PreferencesUtil.putInteger(Constants.MAP_TYPE, map?.mapType!!, activity!!)

        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    @SuppressLint("SetTextI18n")
    fun loadInfo() {

        arguments?.let { arg ->

            val hospital = arguments!![Constants.HOSPITAL] as Hospital


            val entraces = viewModel.localRepo.getHospitalEntrances(hospital.id).filter { it.status != EnumUtil.EntityStatus.DELETE }
            val building = viewModel.localRepo.getBuildings(hospital.id).filter { it.status != EnumUtil.EntityStatus.DELETE }
            val candidateLocations = viewModel.localRepo.getCandiadateLocations(hospital.id).filter { it.status != EnumUtil.EntityStatus.DELETE }

            val allObject: ArrayList<BaseClass> = arrayListOf()

            allObject.addAll(building)
            allObject.addAll(entraces)
            allObject.addAll(candidateLocations)

            var sortedDate = allObject.sortedByDescending { it.modifyDate }

            val mapItems = mutableListOf<MapItem>()

            sortedDate.forEach { mapItems.add(MapItem(it, null, false)) }

            drawOnMap(mapItems, hospital)


            val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
            layoutManager.isSmoothScrollbarEnabled = true
            layoutManager.orientation = androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL

            rv_map_items.layoutManager = layoutManager

            mMapsItemsRvAdaptor = MapsItemsRvAdaptor(mapItems, object : MapItemsRvAdaptorCallback {

                override fun onItemClicked(mapItem: MapItem) {
                    mapItem.geometry?.let { geom ->
                        when (geom) {
                            is Polygon -> {
                                zoomToGeom(geom)
                            }
                            is Marker -> {
                                map?.animateCamera(CameraUpdateFactory.newLatLng(LatLng(geom.position.latitude, geom.position.longitude)))
                            }
                            else -> {

                            }
                        }
                    }
                }

                override fun onDeleteMenuClicked(mapItem: MapItem) {
                    DialogUtil.showInfo(activity!!, getString(R.string.msg_warning_are_you_sure_2_delete_selected_items), "", "", View.OnClickListener { v ->
                        when (v.id) {
                            R.id.btnOk -> {
                                viewModel.localRepo.deleteEntity(mapItem.type)
                                loadInfo()
                            }
                        }
                        val dialog = v.tag as Dialog
                        dialog.dismiss()
                    })
                }

                override fun onEditAttributeMenuClicked(mapItem: MapItem) {
                    val type = mapItem.type
                    when (type) {
                        is CandidateLocation -> {
                            showCandidateFrg(type, hospital)
                        }
                        is Building -> {
                            showBuildingFrg(type, hospital)
                        }
                        is HospitalEntrance -> {
                            showHospitalEntranceFrg(type, hospital)
                        }
                    }
                }

                override fun onEditGeomMenuClicked(mapItem: MapItem) {
                    startEdit(mapItem)
                }

            })

            rv_map_items.adapter = mMapsItemsRvAdaptor

            tv_map_features.text = getString(R.string.label_map_features) + "(" + mapItems.size + ")"

            hospital.centerPoint?.let { center ->

                var markerOptions = MarkerOptions()
                markerOptions.draggable(false)
                markerOptions.position(LatLng(center.position.latitude, center.position.longitude))
                markerOptions.title(getString(R.string.label_hospital_center))
                markerOptions.icon(GeneralUtil.bitmapDescriptorFromVector(context!!, R.drawable.ic_hospital))


                map?.addMarker(markerOptions)

            }
        }


    }

    private fun startEdit(mapItem: MapItem) {

        val type = mapItem.type

        btn_undo.visibility = View.VISIBLE

        var action = ""
        when (type) {
            is HospitalEntrance -> {
                mMapEditType = EnumUtil.MapEditType.HospitalEntrance
                mMarkerOption = getHospitalEntrance()
                btn_undo.visibility = View.GONE
                action = getString(R.string.label_hospital_entrance)
            }
            is Building -> {
                mMapEditType = EnumUtil.MapEditType.Building
                mPolygonOptions = getBuildingPolygonOptions()

                action = getString(R.string.label_building)
            }
            is CandidateLocation -> {
                mMapEditType = EnumUtil.MapEditType.CandidateLocation
                mPolygonOptions = getCandidatePolygonOptions()

                action = getString(R.string.label_candidate_locations)
            }
        }

        var geom = mapItem.geometry

        when (geom) {
            is Polygon -> {

                mTempPolygon = geom

                mTempPolygon?.points?.let {
                    for (i in 0 until it.size - 1)
                        mPolygonOptions?.add(it[i])
                }
                zoomToGeom(geom)

                drawVertexes()
            }
            is Marker -> mTempMarker = geom
        }

        showEditControls("${getString(R.string.label_drawing)} \n $action")
    }

    private fun zoomToGeom(geom: Polygon) {
        var boundBuilder = LatLngBounds.Builder()

        geom.points.forEach { boundBuilder.include(it) }

        var bound = boundBuilder.build()

        map?.animateCamera(CameraUpdateFactory.newLatLngBounds(bound, 200))
    }

    private fun drawOnMap(allObject: MutableList<MapItem>, hospital: Hospital) {

        map?.clear()

        hospital.boundary?.let {

            val geom = drawPolygon(it, getHospitalPolygonOptions())

            geom?.let { g ->
                g.tag = hospital
                viewModel.setHospitalGeom(g)
            }


        }

        allObject.forEach { mapItem ->
            when (mapItem.type) {
                is Building -> {
                    val building = (mapItem.type as Building)
                    val mapGeom = drawPolygon(building.geom, getBuildingPolygonOptions())
                    mapGeom?.tag = building
                    mapItem.geometry = mapGeom
                }
                is HospitalEntrance -> drawEntrance(mapItem)
                is CandidateLocation -> {
                    val candidateLocation = (mapItem.type as CandidateLocation)
                    val mapGeom = drawPolygon(candidateLocation.geom, getCandidatePolygonOptions())
                    mapGeom?.tag = candidateLocation
                    mapItem.geometry = mapGeom
                }
            }
        }
    }

    private fun drawPolygon(polygon: ir.zngis.geojson.Polygon?, polygonOptions: PolygonOptions): Polygon? {

        polygon?.let {
            it.rings?.let {

                it.forEach {

                    it?.let {

                        val latLongs = it.positions.map { LatLng(it.latitude, it.longitude) }

                        polygonOptions.add(*latLongs.toTypedArray())

                    }
                }
            }
        }
        return if (polygonOptions.points.isNotEmpty())
            map?.addPolygon(polygonOptions)
        else
            null
    }

    private fun drawEntrance(mapItem: MapItem) {

        var pointOption = MarkerOptions()

        val geoJsonPoint = (mapItem.type as HospitalEntrance).accessPoint

        geoJsonPoint?.let {
            pointOption.position(LatLng(it.position.latitude, it.position.longitude))
        }

        pointOption.draggable(false)
        pointOption.flat(false)

        pointOption.icon(GeneralUtil.bitmapDescriptorFromVector(activity!!, R.drawable.ic_hospital_location_pin))

        map?.let { map ->
            val marker = map.addMarker(pointOption)
            marker.tag = mapItem.type
            mapItem.geometry = marker
        }

    }


    private fun getHospitalEntrance(): MarkerOptions {

        var markerOption = MarkerOptions()

        markerOption.icon(GeneralUtil.bitmapDescriptorFromVector(context!!, R.drawable.ic_hospital_location_pin))

        markerOption.draggable(false)

        return markerOption
    }

    private fun getBuildingPolygonOptions(): PolygonOptions {
        var polygonOptions = PolygonOptions()
        polygonOptions.fillColor(ContextCompat.getColor(context!!, R.color.color_building_fill))
        polygonOptions.strokeColor(ContextCompat.getColor(context!!, R.color.color_building_stroke))
        polygonOptions.zIndex(2F)
        polygonOptions.strokeWidth(3F)
        polygonOptions.strokeJointType(JointType.ROUND)


        return polygonOptions
    }

    private fun getCandidatePolygonOptions(): PolygonOptions {
        var polygonOptions = PolygonOptions()
        polygonOptions.fillColor(ContextCompat.getColor(context!!, R.color.color_candidate_location_fill))
        polygonOptions.strokeColor(ContextCompat.getColor(context!!, R.color.color_candidate_location_stroke))
        polygonOptions.zIndex(2F)
        polygonOptions.strokeWidth(3F)
        polygonOptions.strokeJointType(JointType.BEVEL)

        mTempPolygon?.points?.forEach { latLng ->
            polygonOptions.add(latLng)
        }

        return polygonOptions
    }

    private fun getHospitalPolygonOptions(): PolygonOptions {
        var polygonOptions = PolygonOptions()
        polygonOptions.fillColor(ContextCompat.getColor(context!!, R.color.color_hospital_fill))
        polygonOptions.strokeColor(ContextCompat.getColor(context!!, R.color.color_hospital_stroke))
        polygonOptions.zIndex(2F)
        polygonOptions.strokeWidth(3F)
        polygonOptions.strokeJointType(JointType.ROUND)

        mTempPolygon?.points?.forEach { latLng ->
            polygonOptions.add(latLng)
        }

        return polygonOptions
    }

    private fun getPolygonFromEditFeature(): ir.zngis.geojson.Polygon {

        val ring = Ring()
        mTempPolygon?.points?.forEach {
            it?.let {
                ring.addPosition(Position(it.latitude, it.longitude))
            }
        }
        val rings = mutableListOf<Ring>()
        rings.add(ring)
        val boundry = ir.zngis.geojson.Polygon()

        boundry.rings = rings

        return boundry

    }

    override fun featureSave() {
        showToastSuccess()
        stopEdit()
        loadInfo()
    }

    override fun featureCanceled() {
        cancelEdit()
    }
}
