package ir.zngis.behtabdatacollection.view.sub_view

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.util.TypedValue


class MapVertex(var context: Context, var text: String, var color: Int) : Drawable() {


    private var mWidth: Int = 0
    private var mHeight: Int = 0

    private var mCircleWidth = 0

    private var mPaint: Paint = Paint()

    private val mPaddingHeight = 7F

    init {

        mCircleWidth = dp2px(context, 8F)

        val textWidth = dp2px(context, 20F)

        mWidth = (mCircleWidth + (textWidth + mPaddingHeight)*2).toInt()

        mHeight = (mCircleWidth + mPaddingHeight).toInt()

    }

    override fun draw(canvas: Canvas) {

        mPaint.color = color

        mPaint.textSize = sp2px(context, 10F).toFloat()

        mPaint.style = Paint.Style.FILL

        val radius = (mCircleWidth / 2).toFloat()

        val x = mWidth / 2

        val y = mHeight / 2

        canvas.drawCircle(x.toFloat(), y.toFloat(), radius, mPaint)

        canvas.drawText(text, x + radius , y + radius, mPaint)


    }

    override fun setAlpha(alpha: Int) {

    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSPARENT;
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {

    }

    fun dp2px(context: Context, dpValue: Float): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, context.resources.displayMetrics).toInt()
    }

    fun sp2px(context: Context, spValue: Float): Int {
        return (spValue * context.resources.displayMetrics.scaledDensity + 0.5f).toInt()
    }

    fun px2sp(context: Context, pxValue: Float): Int {
        return (pxValue / context.resources.displayMetrics.scaledDensity + 0.5f).toInt()
    }

    override fun getIntrinsicWidth(): Int {
        return mWidth
    }

    override fun getIntrinsicHeight(): Int {
        return mHeight
    }

}