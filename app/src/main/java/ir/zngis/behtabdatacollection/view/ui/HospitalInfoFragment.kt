package ir.zngis.behtabdatacollection.view.ui


import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.widget.NestedScrollView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.databinding.FragmentHospitalInfoBinding
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.Hospital
import ir.zngis.behtabdatacollection.repository.dataModel.HospitalComments
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.util.ToastUtil
import ir.zngis.behtabdatacollection.viewModel.SurveyActivityVm
import kotlinx.android.synthetic.main.fragment_hospital_info.*
import javax.inject.Inject


class HospitalInfoFragment : androidx.fragment.app.Fragment(), Injectable {

    lateinit var binding: FragmentHospitalInfoBinding
    lateinit var viewModel: SurveyActivityVm


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(SurveyActivityVm::class.java)


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_hospital_info, container, false);

        val hospital = arguments!![Constants.HOSPITAL] as Hospital

        var comments = HospitalComments()

        comments.hospitalId = hospital.id

        val commentsList = viewModel.localRepo.getHospitalCommentByHospital(hospital.id).blockingGet()

        commentsList?.let { if (it.isNotEmpty()) comments = it.first() }

        binding.hospital = viewModel.localRepo.findHospitalById(hospital.id)?.clone()

        binding.comment = comments.clone()

//        binding.layoutRoot.setOnScrollChangeListener { nestedScrollView: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
//            run {
//                if (scrollY > 0) {
//                    binding.btnSaveChanges.hide()
//                } else {
//                    binding.btnSaveChanges.show()
//                }
//            }
//        }


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnSaveChanges.setOnClickListener {

            viewModel.updateEntity(binding.hospital!!)

            binding.comment?.let { comment ->

                if (comment.id == 0) {
                    viewModel.insertEntity(comment)
                } else {
                    viewModel.updateEntity(comment)
                }

            }

            ToastUtil.ShowToast(activity!!, getString(R.string.msg_info_information_saved_successfully), EnumUtil.ToastActionState.Success, Toast.LENGTH_LONG)
        }


        var items = arrayListOf<SelectableListDialogFragment.SelectableItem>()

        val structureTypes = resources.getStringArray(R.array.structure_type)

        for (i in 0 until structureTypes.size) {
            items.add(SelectableListDialogFragment.SelectableItem(i.toString(), structureTypes[i]))
        }

        tv_structure_type.text = items.filter { it.checked }.map { it.displayValue }.toString().replace("]", "").replace("[", "")

        layout_structure_type.setOnClickListener {

            binding.hospital?.structureType?.get()?.let {
                if (it.isNotEmpty()) {
                    it.split(",").forEach { t: String? ->
                        items.find { it.displayValue == t }?.let {
                            it.checked = true
                        }
                    }
                }
            }


            val frg = SelectableListDialogFragment.newInstance(items, object : SelectableListDialogFragment.Callback {
                override fun onDialogClose() {
                    tv_structure_type.text = items.filter { it.checked }.map { it.displayValue }.toString().replace("]", "").replace("[", "")
                }
            })

            frg.show(childFragmentManager, SelectableListDialogFragment::class.java.simpleName)
        }



        btn_attachments.setOnClickListener {
            val attachments = AttachmentsFragment.newInstance(getString(R.string.label_attachments), Hospital::class.java.simpleName, binding.hospital?.id.toString())
            attachments.show(childFragmentManager, AttachmentsFragment::class.java.simpleName)
        }
    }


    companion object {

        @JvmStatic
        fun newInstance(hospital: Hospital) =
                HospitalInfoFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(Constants.HOSPITAL, hospital)
                    }
                }
    }
}
