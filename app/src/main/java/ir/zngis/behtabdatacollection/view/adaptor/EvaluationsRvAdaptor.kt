package ir.zngis.behtabdatacollection.view.adaptor

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.repository.dataModel.Evaluation
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.view.callback.GeneralRvCallback
import ir.zngis.behtabdatacollection.view.ui.MainActivity
import kotlinx.android.synthetic.main.item_evaluation.view.*

class EvaluationsRvAdaptor(val evaluations: List<Evaluation>, val callback: Callback) : androidx.recyclerview.widget.RecyclerView.Adapter<EvaluationsRvAdaptor.ViewHolder>() {


    override fun getItemCount() = evaluations.size

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val inflator = LayoutInflater.from(viewGroup.context)

        val view = inflator.inflate(R.layout.item_evaluation, viewGroup, false)

        return ViewHolder(view, callback)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(evaluations[position], position)
    }


    class ViewHolder(viewItem: View, val callback: Callback) : androidx.recyclerview.widget.RecyclerView.ViewHolder(viewItem) {


        fun bind(evaluation: Evaluation, position: Int) = with(itemView) {


            setOnClickListener { callback.onItemClicked(evaluation) }

            setOnLongClickListener {
                callback.onEvaluationLongClick(evaluation, tv_evaluation.text.toString())
                true
            }

            if (evaluation.hasObserved!!.get()) {
                when (evaluation.riskLevel) {
                    Constants.NON_RISK_LEVEL -> {
                        if (MainActivity.IS_RTL) {
                            tv_evaluation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.success, 0)
                        } else {
                            tv_evaluation.setCompoundDrawablesWithIntrinsicBounds(R.drawable.success, 0, 0, 0)
                        }
                        tv_evaluation.text = "${position + 1} - ${tv_evaluation.context.getText(R.string.label_non_occurrence)}"
                    }
                    Constants.HIGH_RISK_LEVEL -> {
                        if (MainActivity.IS_RTL) {
                            tv_evaluation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_high_risk, 0)
                        } else {
                            tv_evaluation.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_high_risk, 0, 0, 0)
                        }
                        tv_evaluation.text = "${position + 1} - ${tv_evaluation.context.getText(R.string.label_hazard_level)} ${tv_evaluation.context.getText(R.string.label_high_occurrence)}"

                    }
                    Constants.MEDIUM_RISK_LEVEL -> {
                        if (MainActivity.IS_RTL) {
                            tv_evaluation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_medioum_risk, 0)
                        } else {
                            tv_evaluation.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_medioum_risk, 0, 0, 0)
                        }
                        tv_evaluation.text = "${position + 1} - ${tv_evaluation.context.getText(R.string.label_hazard_level)} ${tv_evaluation.context.getText(R.string.label_medium_occurrence)}"

                    }
                    Constants.LOW_RISK_LEVEL -> {
                        if (MainActivity.IS_RTL) {
                            tv_evaluation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_low_risk, 0)
                        } else {
                            tv_evaluation.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_low_risk, 0, 0, 0)
                        }
                        tv_evaluation.text = "${position + 1} - ${tv_evaluation.context.getText(R.string.label_hazard_level)} ${tv_evaluation.context.getText(R.string.label_low_occurrence)}"
                    }
                }

            } else {
                tv_evaluation.text = "${position + 1} - ${tv_evaluation.context.getString(R.string.label_unable_to_record)} "
                if (MainActivity.IS_RTL) {
                    tv_evaluation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_visibility_off_black_24dp, 0)
                } else {
                    tv_evaluation.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_visibility_off_black_24dp, 0, 0, 0)
                }
            }




            evaluation.comment?.let {
                if (!it.get().isNullOrEmpty()) {
                    tv_comment.visibility = View.VISIBLE
                    tv_comment.text = it.get()
                }
            }

            btn_edit.setOnClickListener { callback.onItemClicked(evaluation) }
            btn_show_attachments.setOnClickListener { callback.onBtnShowAttachmentsClicked(evaluation) }
        }

    }

    interface Callback : GeneralRvCallback {
        fun onEvaluationLongClick(evaluation: Evaluation, title: String)
        fun onBtnShowAttachmentsClicked(evaluation: Evaluation)

    }
}