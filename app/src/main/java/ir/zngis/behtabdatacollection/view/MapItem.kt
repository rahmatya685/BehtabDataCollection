package ir.zngis.behtabdatacollection.view


import ir.zngis.behtabdatacollection.repository.dataModel.BaseClass

class MapItem(
        var type: BaseClass,
        var geometry: Any?,
        var isSelected: Boolean
) {

}