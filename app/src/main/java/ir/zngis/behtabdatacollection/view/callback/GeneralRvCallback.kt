package ir.zngis.behtabdatacollection.view.callback

import ir.zngis.behtabdatacollection.repository.dataModel.BaseClass

interface GeneralRvCallback {

    fun onItemClicked(unit: Any)

    fun onItemLongClicked(unit: Any)


}