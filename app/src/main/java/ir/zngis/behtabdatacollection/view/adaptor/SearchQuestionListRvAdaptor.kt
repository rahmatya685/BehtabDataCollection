package ir.zngis.behtabdatacollection.view.adaptor

import android.annotation.SuppressLint
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.edsab.gedat.edsgeneral.util.inflate
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.repository.dataModel.BaseClass
import ir.zngis.behtabdatacollection.repository.dataModel.Question
import ir.zngis.behtabdatacollection.repository.dataModel.QuestionCategory
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.util.PreferencesUtil
import ir.zngis.behtabdatacollection.view.callback.GeneralRvCallback
import kotlinx.android.synthetic.main.item_question_cat.view.*
import kotlinx.android.synthetic.main.item_question_search_rv.view.*

class SearchQuestionListRvAdaptor(var listener: GeneralRvCallback) :
        androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {


    private var mComparator: Comparator<BaseClass>? = null

    private val mSortedList =
            SortedList(BaseClass::class.java, object : SortedList.Callback<BaseClass>() {
                override fun compare(a: BaseClass, b: BaseClass): Int {
                    return mComparator!!.compare(a, b)
                }

                override fun onInserted(position: Int, count: Int) {
                    notifyItemRangeInserted(position, count)
                }

                override fun onRemoved(position: Int, count: Int) {
                    notifyItemRangeRemoved(position, count)
                }

                override fun onMoved(fromPosition: Int, toPosition: Int) {
                    notifyItemMoved(fromPosition, toPosition)
                }

                override fun onChanged(position: Int, count: Int) {
                    notifyItemRangeChanged(position, count)
                }

                override fun areContentsTheSame(
                        oldItem: BaseClass,
                        newItem: BaseClass
                ): Boolean {
                    if (oldItem is Question && newItem is Question) {
                        return oldItem.id == newItem.id
                    } else if (oldItem is QuestionCategory && newItem is QuestionCategory) {
                        return oldItem.id == newItem.id
                    } else
                        return false
                }

                override fun areItemsTheSame(
                        item1: BaseClass,
                        item2: BaseClass
                ): Boolean {
                    if (item1 is Question && item2 is Question) {
                        return item1.id == item2.id
                    } else if (item1 is QuestionCategory && item2 is QuestionCategory) {
                        return item1.id == item2.id
                    } else
                        return false


                }
            })

    init {
        mComparator = Comparator { item1, item2 ->

            if (item1 is Question && item2 is Question) {
                item1.id.compareTo(item2.id)
            } else if (item1 is QuestionCategory && item2 is QuestionCategory) {
                item1.id.compareTo(item2.id)
            }else
                0
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_QUESTION -> ViewHolderQuestion(parent.inflate(R.layout.item_question_search_rv))
            VIEW_CATEGORY -> ViewHolderCategory(parent.inflate(R.layout.item_question_cat))
            else -> ViewHolderCategory(parent.inflate(R.layout.item_question_cat))
        }
    }

    override fun getItemCount() = mSortedList.size()

    override fun getItemViewType(position: Int): Int {
        return when (mSortedList[position]) {
            is Question -> VIEW_QUESTION
            is QuestionCategory -> VIEW_CATEGORY
            else -> super.getItemViewType(position)
        }

    }

    override fun onBindViewHolder(p: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        when (p) {
            is ViewHolderQuestion -> p.bind(mSortedList[position] as Question, listener, position)
            is ViewHolderCategory -> p.bind(mSortedList[position] as QuestionCategory)
        }
    }


    inner class ViewHolderCategory(private val viewItem: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(viewItem) {
        @SuppressLint("SetTextI18n")
        fun bind(
                questionCategory : QuestionCategory
        ) = with(viewItem) {

            tv_question_cat_title.text = "${questionCategory.number}-" + if (PreferencesUtil.getCurrentLanguage(context) == Constants.ENGLISH) {
                questionCategory.categoryTitleEn

            } else {
                questionCategory.categoryTitleFa
            }

        }
    }


    class ViewHolderQuestion(private val viewItem: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(viewItem) {
        @SuppressLint("SetTextI18n")
        fun bind(
                question: Question,
                itemClickListener: GeneralRvCallback,
                position: Int
        ) = with(viewItem) {


            tv_question_title.text = "${question.number}-${
            if (PreferencesUtil.getCurrentLanguage(context) == Constants.ENGLISH) {
                question.questionTitleEn
            } else {
                question.questionTitleFa
            }
            }"

            tv_question_count.text = "${
            if (PreferencesUtil.getCurrentLanguage(context) == Constants.ENGLISH) {
                "Evaluation Count: "
            } else {
                "تعداد پاسخ : "
            }}${question.evaluations.count { it.status != EnumUtil.EntityStatus.DELETE }}"


            if (question.evaluations.count { it.status != EnumUtil.EntityStatus.DELETE } > 0)
                root.setBackgroundColor(ContextCompat.getColor(root.context, R.color.green))
            else
                root.setBackgroundColor(ContextCompat.getColor(root.context, R.color.icons))



            root.setOnClickListener { itemClickListener.onItemClicked(question) }

            PreferencesUtil.putString(Constants.SEARCH_POSITION, position.toString(), viewItem.context)
        }
    }


    fun add(model: Question) {
        mSortedList.add(model)
    }


    fun add(models: List<BaseClass>) {
        mSortedList.addAll(models)
    }

    fun replaceAll(models: List<BaseClass>) {
        mSortedList.beginBatchedUpdates()
        mSortedList.clear()
//        for (i in mSortedList.size() - 1 downTo 0) {
//            val model = mSortedList.get(i)
//            if (model.type == 0 && !models.contains(model)) {
//                mSortedList.remove(model)
//            }
//        }
        mSortedList.addAll(models)
        mSortedList.endBatchedUpdates()
    }

    companion object {
        const val VIEW_QUESTION = 1
        const val VIEW_CATEGORY = 4
    }

}