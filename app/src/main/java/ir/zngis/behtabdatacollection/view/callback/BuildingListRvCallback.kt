package ir.zngis.behtabdatacollection.view.callback

 import ir.zngis.behtabdatacollection.repository.dataModel.BaseClass
 import ir.zngis.behtabdatacollection.repository.dataModel.Building
 import ir.zngis.behtabdatacollection.repository.dataModel.BuildingWithUnits

interface BuildingListRvCallback {
    fun onUnitItemClicked(building:Building, unit:Any)
    fun onBuildingItemClicked(building:Building )
    fun onUnitItemLongClicked(building: Building, unit: Any)
    fun onBuildingItemLongClicked(building:Building )


}