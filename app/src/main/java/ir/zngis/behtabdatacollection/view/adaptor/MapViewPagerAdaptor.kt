package ir.zngis.behtabdatacollection.view.adaptor

import android.annotation.SuppressLint
import android.os.Build
import androidx.viewpager.widget.PagerAdapter
import android.view.View
import android.view.ViewGroup
import android.widget.TextClock
import android.widget.TextView
import com.edsab.gedat.edsgeneral.util.inflate
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.repository.dataModel.BaseClass
import ir.zngis.behtabdatacollection.repository.dataModel.Building
import ir.zngis.behtabdatacollection.repository.dataModel.CandidateLocation
import ir.zngis.behtabdatacollection.repository.dataModel.HospitalEntrance
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.GeneralUtil
import ir.zngis.behtabdatacollection.util.PreferencesUtil

class MapViewPagerAdaptor(var data: ArrayList<BaseClass>) : androidx.viewpager.widget.PagerAdapter() {
    override fun isViewFromObject(view: View, p1: Any): Boolean {
        return view === `p1`
    }

    override fun getCount(): Int = data.size


    @SuppressLint("SetTextI18n")
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        var view = container.inflate(R.layout.item_map_rv)

        val tvItemName = view.findViewById<TextView>(R.id.tv_item_name)
        val tvItemType = view.findViewById<TextView>(R.id.tv_item_type)

        val language = PreferencesUtil.getCurrentLanguage(container.context)

        tvItemName?.let {
            val dataItem = data[position]

            when (dataItem) {
                is Building -> {
                    tvItemType.text = container.context.getString(R.string.label_building)
                    it.text = dataItem.name.get() + "\n" + dataItem.comment?.get()
                }
                is HospitalEntrance -> {
                    tvItemType.text = container.context.getString(R.string.label_hospital_entrance)

                    it.text = dataItem.name.get() + "\n" + dataItem

                }
                is CandidateLocation -> {
                    tvItemType.text = container.context.getString(R.string.label_candidate_locations)
                    it.text = dataItem.name.get()
                }

            }
        }

        container.addView(view)

        return view
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }
}