package ir.zngis.behtabdatacollection.view.adaptor

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import ir.zngis.behtabdatacollection.repository.dataModel.Hospital
import ir.zngis.behtabdatacollection.view.ui.ModulesFragment

class HospitalFrgAdaptor(fragmentManager: androidx.fragment.app.FragmentManager, var hospitals: Array<Hospital>) : androidx.fragment.app.FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        val frg = ModulesFragment.newInstance(hospitals[position])
        return frg
    }

    override fun getCount() = hospitals.size
}