package ir.zngis.behtabdatacollection.view.ui


import android.app.AlarmManager
import android.app.Dialog
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Process
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.DialogUtil
import ir.zngis.behtabdatacollection.util.PreferencesUtil
import kotlinx.android.synthetic.main.fragment_setting.*
import android.os.Process.myPid
import android.os.Process.killProcess
import android.widget.RadioGroup
import com.zeugmasolutions.localehelper.Locales
import java.util.*


class SettingFragment : FullScreenDialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_setting, container, false)
    }


    companion object {
        @JvmStatic
        fun newInstance() =
                SettingFragment().apply {
                    arguments = Bundle().apply {
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.navigationIcon = ContextCompat.getDrawable(activity!!, R.drawable.ic_close_black_24dp)
        if (MainActivity.IS_RTL) {
            toolbar.navigationIcon = ContextCompat.getDrawable(activity!!, R.drawable.ic_close_black_24dp)
        }
        toolbar.setNavigationOnClickListener {
            dismiss()
        }

        val lang = PreferencesUtil.getString(Constants.LANGUAGE, activity)
        when (lang) {
            Constants.PERSIAN -> rg_change_language.check(R.id.rb_lang_persian)
            Constants.ENGLISH -> rg_change_language.check(R.id.rb_lang_english)
            else -> rg_change_language.check(R.id.rb_lang_persian)
        }


        var onCheckedChangeListener = object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
//                DialogUtil.showInfo(activity!!, getString(R.string.msg_warning_are_you_sure_you_want_change_language), "", "", View.OnClickListener {
//                    val dialog = it.tag as Dialog
//                    if (it.id == R.id.btnOk) {
//
//                        dialog.dismiss()
//                        //restartApp()
//                    } else {
//                        rg_change_language.setOnCheckedChangeListener(null)
//                        when (checkedId) {
//                            R.id.rb_lang_english -> rg_change_language.check(R.id.rb_lang_persian)
//                            R.id.rb_lang_persian -> rg_change_language.check(R.id.rb_lang_english)
//                        }
//                        rg_change_language.setOnCheckedChangeListener(this)
//
//                    }
//                    dialog.dismiss()
//                })

                when (checkedId) {
                    R.id.rb_lang_english ->{
                        PreferencesUtil.putString(Constants.LANGUAGE, Constants.ENGLISH, activity)
                        activity?.let {
                            (it as MainActivity).updateLocale(Locale.ENGLISH)
                        }
                    }
                    R.id.rb_lang_persian ->{
                        PreferencesUtil.putString(Constants.LANGUAGE, Constants.PERSIAN, activity)
                        val iran = Locale("fa", "IRA")
                        activity?.let {
                            (it as MainActivity).updateLocale(iran)
                        }
                    }
                }
            }

        }

        rg_change_language.setOnCheckedChangeListener(onCheckedChangeListener)

    }


    fun restartApp() {

        try {
            Thread.sleep(2000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        val mStartActivity = Intent(getActivity(), SplashActivity::class.java)
        val mPendingIntentId = 123456
        val mPendingIntent = PendingIntent.getActivity(getActivity(), mPendingIntentId, mStartActivity,
                PendingIntent.FLAG_CANCEL_CURRENT);
        val mgr = activity!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent)

        android.os.Process.killProcess(Process.myPid())

    }
}
