package ir.zngis.behtabdatacollection.view.callback

import ir.zngis.behtabdatacollection.view.MapItem

interface MapItemsRvAdaptorCallback {

    fun onDeleteMenuClicked(mapItem: MapItem)
    fun onEditAttributeMenuClicked(mapItem: MapItem)
    fun onEditGeomMenuClicked(mapItem: MapItem)

    fun onItemClicked(mapItem: MapItem)

}