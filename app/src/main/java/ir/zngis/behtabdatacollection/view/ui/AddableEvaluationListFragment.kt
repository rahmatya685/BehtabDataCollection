package ir.zngis.behtabdatacollection.view.ui

import android.app.Dialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.*
import ir.zngis.behtabdatacollection.util.*
import ir.zngis.behtabdatacollection.view.adaptor.QuestionRvAdaptor
import ir.zngis.behtabdatacollection.view.callback.GeneralRvCallback
import ir.zngis.behtabdatacollection.viewModel.QuestionVm
import kotlinx.android.synthetic.main.fragment_evaluation_list.*
import javax.inject.Inject


class AddableEvaluationListFragment : androidx.fragment.app.Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: QuestionVm

    lateinit var mainActivity: MainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(QuestionVm::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_evaluation_list, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(building: Building, unit: Any?, moduleName: String) =
                AddableEvaluationListFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(Constants.BUILDING, building)
                        putString(Constants.MODULE, moduleName)
                        unit?.let {
                            putParcelable(Constants.UNIT, it as Parcelable?)
                        }

                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainActivity = activity as MainActivity

        val building = arguments!![Constants.BUILDING] as Building
        val module = arguments!![Constants.MODULE] as String

        setFrgTitle()

        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
        layoutManager.isSmoothScrollbarEnabled = true
        layoutManager.orientation = androidx.recyclerview.widget.LinearLayoutManager.VERTICAL

        rv_questions.layoutManager = layoutManager





        var unit: Any? = null

        var observer = Observer<List<Any>> {

            if (it == null || it.isEmpty()) {
                ToastUtil.ShowToast(activity!!, getString(R.string.msg_error_no_data_found), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
            } else {

                val filteredEvaluation= mutableListOf<Any>()

                it.forEach {
                    if ((it is EvaluationWithQuestion && it.evaluation?.status != EnumUtil.EntityStatus.DELETE) || it is QuestionCategory){
                        filteredEvaluation.add(it)
                    }
                }

                rv_questions.adapter = QuestionRvAdaptor( filteredEvaluation , object : GeneralRvCallback {
                    override fun onItemLongClicked(e: Any) {
                        val questionWithEvaluations = e as EvaluationWithQuestion

                        doDelete(questionWithEvaluations)
                    }

                    override fun onItemClicked(t: Any) {
                        val questionWithEvaluations = t as EvaluationWithQuestion
//                        val frg = AnswerFragment.newInstance2(building, unit, module, questionWithEvaluations)
//                        (activity as MainActivity).showFrg(frg, AnswerFragment::class.java.simpleName)
                    }
                })
            }

        }


        arguments?.let { arg ->
            if (arg.containsKey(Constants.UNIT)) {
                unit = arguments!![Constants.UNIT] as Any
                viewModel.getUnitEvaluations(module, unit!!).observe(this, observer)


            } else {
                viewModel.getBuildingEvaluations(module, building.rowGuid).observe(this, observer)
            }
        }


        btn_add_evaluation.setOnClickListener {
            val frg = SearchQuestionsFragment.newInstance(module,building,unit, object : GeneralRvCallback {
                override fun onItemLongClicked(unit: Any) {

                }

                override fun onItemClicked(unit: Any) {
                    showAnswerFrg(unit)
                }
            })
            frg.show(childFragmentManager, SearchQuestionsFragment::class.java.simpleName)
        }



        rv_questions.addOnScrollListener(object : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    btn_add_evaluation.hide()
                } else {
                    btn_add_evaluation.show()
                }
            }
        })
    }

    private fun doDelete(questionWithEvaluations: EvaluationWithQuestion) {
        questionWithEvaluations.evaluation?.let { evaluation ->
            DialogUtil.showInfo(activity!!, "${getString(R.string.msg_warning_are_you_sure_2_delete_selected_items)} \n ${
            if (PreferencesUtil.getCurrentLanguage(context) == Constants.ENGLISH) {
                questionWithEvaluations.questionTitleEn
            } else {
                questionWithEvaluations.questionTitleFa
            }
            }", "", "", View.OnClickListener {
                val dialogInfo = it.tag as Dialog
                if (it.id == R.id.btnOk)
                    viewModel.deleteEntity(evaluation)
                dialogInfo.dismiss()
            })


        }

    }

    private fun showAnswerFrg(q: Any) {
        arguments?.let { arg ->

            val building = arg[Constants.BUILDING] as Building
            val module = arg[Constants.MODULE] as String
            var unit: Any? = null
            if (arg.containsKey(Constants.UNIT)){
                unit = arg[Constants.UNIT]
            }

//            val frg = AnswerFragment.newInstance(building, unit, module, q as Question)
//            mainActivity.showFrg(frg, AnswerFragment::class.java.simpleName)

        }


    }


    override fun onResume() {
        super.onResume()
        setFrgTitle()
    }

    private fun setFrgTitle() {
        val module = arguments!![Constants.MODULE] as String
        mainActivity.mToolbar.title = GeneralUtil.getModuleFaName(activity, module)


        val building = arguments!![Constants.BUILDING] as Building

        arguments?.let { arg ->
            if (arg.containsKey(Constants.UNIT)) {
                val unit: Any = arg[Constants.UNIT] as Any
                when (unit) {
                    is SurgeryDepartment -> {
                        mainActivity.mToolbar.subtitle = "${building.name.get()} || ${unit.id}-${unit.nameTemp}"
                    }
                    is NonClinicalUnit -> {
                        mainActivity.mToolbar.subtitle = "${building.name.get()} || ${unit.id}-${unit.nameTemp}"
                    }
                    is ClinicalUnit -> {
                        mainActivity.mToolbar.subtitle = "${building.name.get()} || ${unit.id}-${unit.nameTemp}"
                    }
                }
            }
        }
    }


}
