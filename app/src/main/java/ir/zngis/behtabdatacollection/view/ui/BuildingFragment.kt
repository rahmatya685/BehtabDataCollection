package ir.zngis.behtabdatacollection.view.ui


import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.databinding.FragmentBuildingBinding
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.Building
import ir.zngis.behtabdatacollection.repository.dataModel.Hospital
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.util.ToastUtil
import ir.zngis.behtabdatacollection.view.callback.FeatureInfoCallback
import ir.zngis.behtabdatacollection.viewModel.BuildingVm
import kotlinx.android.synthetic.main.fragment_building.*
import javax.inject.Inject


class BuildingFragment : FullScreenDialogFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: BuildingVm
    lateinit var binding: FragmentBuildingBinding

    lateinit var mFeatureInfoCallback: FeatureInfoCallback

    private var mIsSaved = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(BuildingVm::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_building, container, false)
        return binding.root
    }


    companion object {
        /**
         * @param building Building to be edited.
         * @return A new instance of fragment BuildingFragment.
         */
        @JvmStatic
        fun newInstance(building: Building, hospital: Hospital, featureInfoCallback: FeatureInfoCallback) =
                BuildingFragment().apply {
                    this.mFeatureInfoCallback = featureInfoCallback
                    arguments = Bundle().apply {
                        putParcelable(Constants.BUILDING, building)
                        putParcelable(Constants.HOSPITAL, hospital)

                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.navigationIcon = ContextCompat.getDrawable(activity!!, R.drawable.ic_close_black_24dp)
        if (MainActivity.IS_RTL) {
            toolbar.navigationIcon = ContextCompat.getDrawable(activity!!, R.drawable.ic_close_black_24dp)
        }
        toolbar.setNavigationOnClickListener { dismiss() }

        val hospital = arguments!![Constants.HOSPITAL] as Hospital

        toolbar.title = hospital.name.get()


        arguments?.let {
            val building = it[Constants.BUILDING] as Building
            toolbar.subtitle = building.name.get()

            binding.building = building.clone()

        }


        binding.btnSaveChanges.setOnClickListener { it ->
            if (binding.building?.name?.get().isNullOrEmpty()) {
                ToastUtil.ShowToast(activity!!, getString(R.string.msg_error_invalid_name), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                return@setOnClickListener
            }
            ToastUtil.ShowToast(activity!!, getString(R.string.msg_info_information_saved_successfully), EnumUtil.ToastActionState.Success, Toast.LENGTH_LONG)

            binding.building?.let { b->
                if (b.id == 0){
                    viewModel.localRepo.insertEntity(b)
                }else{
                    viewModel.localRepo.updateEntity(b)
                }
            }


            mFeatureInfoCallback.featureSave()

            mIsSaved = true

            dismiss()
        }



        binding.btnAttachments.setOnClickListener {
            val attachments = AttachmentsFragment.newInstance(getString(R.string.label_attachments), Building::class.java.simpleName, binding.building!!.rowGuid)
            attachments.show(childFragmentManager, AttachmentsFragment::class.java.simpleName)
        }


    }

    override fun onDestroy() {
        if (!mIsSaved) {
            mFeatureInfoCallback.featureCanceled()

        }

        super.onDestroy()


    }
}
