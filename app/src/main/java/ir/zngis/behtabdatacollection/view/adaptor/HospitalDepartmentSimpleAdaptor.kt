package ir.zngis.behtabdatacollection.view.adaptor

import android.annotation.SuppressLint
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.edsab.gedat.edsgeneral.util.inflate
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.repository.dataModel.BaseClass
import ir.zngis.behtabdatacollection.repository.dataModel.ClinicalUnit
import ir.zngis.behtabdatacollection.repository.dataModel.NonClinicalUnit
import ir.zngis.behtabdatacollection.repository.dataModel.SurgeryDepartment
import ir.zngis.behtabdatacollection.view.callback.GeneralRvCallback
import kotlinx.android.synthetic.main.item_hospital_unit.view.*

class HospitalDepartmentSimpleAdaptor(val units: List<BaseClass>, private val generalRvCallback: GeneralRvCallback) : androidx.recyclerview.widget.RecyclerView.Adapter<HospitalDepartmentSimpleAdaptor.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.item_hospital_unit), generalRvCallback)

    override fun getItemCount() = units.size

    override fun onBindViewHolder(viewHolder: ViewHolder, postion: Int) {
        viewHolder.bind(units[postion])
    }


    class ViewHolder(viewItem: View, private val generalRvCallback: GeneralRvCallback) : androidx.recyclerview.widget.RecyclerView.ViewHolder(viewItem) {

        @SuppressLint("SetTextI18n")
        fun bind(dataItem: BaseClass) = with(itemView) {
            when (dataItem) {
                is ClinicalUnit -> {
                    tv_type.text = "${dataItem.nameTemp}"
                    tv_count_unobservable.text = dataItem.countUnVisited.toString()
                    tv_count_observed.text = dataItem.countVisited.toString()
                    tv_count_remained_questions.text = dataItem.countRemainedQuestions.toString()
                }
                is SurgeryDepartment -> {
                    tv_type.text = "${dataItem.nameTemp}"
                    tv_count_unobservable.text = dataItem.countUnVisited.toString()
                    tv_count_observed.text = dataItem.countVisited.toString()
                    tv_count_remained_questions.text = dataItem.countRemainedQuestions.toString()
                }
                is NonClinicalUnit -> {
                    tv_type.text = "${dataItem.nameTemp}"
                    tv_count_unobservable.text = dataItem.countUnVisited.toString()
                    tv_count_observed.text = dataItem.countVisited.toString()
                    tv_count_remained_questions.text = dataItem.countRemainedQuestions.toString()
                }
                else -> {
                }
            }
            setOnClickListener {
                generalRvCallback.onItemClicked(dataItem)
            }
            setOnLongClickListener {
                generalRvCallback.onItemLongClicked(dataItem)
                true
            }
        }
    }
}