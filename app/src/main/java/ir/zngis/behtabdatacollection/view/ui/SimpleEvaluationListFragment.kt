package ir.zngis.behtabdatacollection.view.ui


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.Building
import ir.zngis.behtabdatacollection.repository.dataModel.EvaluationWithQuestion
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.util.GeneralUtil
import ir.zngis.behtabdatacollection.util.ToastUtil
import ir.zngis.behtabdatacollection.view.adaptor.QuestionRvAdaptor
import ir.zngis.behtabdatacollection.view.callback.GeneralRvCallback
import ir.zngis.behtabdatacollection.viewModel.QuestionVm
import kotlinx.android.synthetic.main.fragment_simple_evaluation_list.*
import javax.inject.Inject


class SimpleEvaluationListFragment : androidx.fragment.app.Fragment(), Injectable {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: QuestionVm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(QuestionVm::class.java)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_simple_evaluation_list, container, false)
    }


    companion object {
        @JvmStatic
        fun newInstance(building: Building) =
                SimpleEvaluationListFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(Constants.BUILDING, building)
                    }
                }
    }
    private fun setFrgTitle() {
        val mainActivity = activity as MainActivity


        val building = arguments!![Constants.BUILDING] as Building

        val module = Constants.MODULE_HAZARD

        mainActivity.mToolbar.title = GeneralUtil.getModuleFaName(activity, module)

        mainActivity.mToolbar.subtitle = building.name.get()
    }

    override fun onResume() {
        super.onResume()
        setFrgTitle()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFrgTitle()
        val building = arguments!![Constants.BUILDING] as Building

        val module = Constants.MODULE_HAZARD

        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
        layoutManager.isSmoothScrollbarEnabled = true
        layoutManager.orientation = androidx.recyclerview.widget.LinearLayoutManager.VERTICAL

        rv_questions.layoutManager = layoutManager

        viewModel.getQuestionsWithEvaluation4Hazard(building.rowGuid, module).observe(this, Observer {

            if (it == null || it.isEmpty()) {
                ToastUtil.ShowToast(activity!!, getString(R.string.msg_error_no_data_found), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
            } else {
                rv_questions.adapter = QuestionRvAdaptor(it, object : GeneralRvCallback {
                    override fun onItemLongClicked(unit: Any) {

                    }
                    override fun onItemClicked(unit: Any) {
                        val questionWithEvaluations = unit as EvaluationWithQuestion
//                        if (questionWithEvaluations.evaluation == null) {
//                            val question = viewModel.localRepo.getQuestion(questionWithEvaluations.questionId)
//
//                            val frg = AnswerFragment.newInstance(building, unit, module, question)
//                            (activity as MainActivity).showFrg(frg, AnswerFragment::class.java.simpleName)
//                        } else {
//                            val frg = AnswerFragment.newInstance2(building, unit, module, questionWithEvaluations)
//                            (activity as MainActivity).showFrg(frg, AnswerFragment::class.java.simpleName)
//                        }

                    }
                })
            }


        })


    }
}
