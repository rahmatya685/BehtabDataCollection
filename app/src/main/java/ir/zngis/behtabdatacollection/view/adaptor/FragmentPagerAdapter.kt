package cloudgis.mihanblog.com.datacollection.adaptor

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

/**
 * Created by alireza on 3/26/2018.
 */
class FragmentPagerAdapter(frgManager: androidx.fragment.app.FragmentManager, mFragmentList: MutableList<androidx.fragment.app.Fragment>): androidx.fragment.app.FragmentStatePagerAdapter(frgManager) {
    private val mFragmentList =mFragmentList

    var mFragmentTitleList:MutableList<String>  = mutableListOf()

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return  mFragmentList[position]
    }

    override fun getCount(): Int {
        return  mFragmentList.size
    }
    override fun getPageTitle(position: Int): CharSequence? {
        return if (mFragmentTitleList.isEmpty()){
            super.getPageTitle(position)
        }else{
            mFragmentTitleList[position]
        }
    }


}