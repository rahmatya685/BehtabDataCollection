package ir.zngis.behtabdatacollection.view.adaptor

import androidx.recyclerview.widget.RecyclerView
import android.view.View

open class BaseViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view)
