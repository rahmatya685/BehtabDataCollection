package ir.zngis.behtabdatacollection.view.ui

import android.app.Dialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.Building
import ir.zngis.behtabdatacollection.repository.dataModel.Hospital
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.DialogUtil
import ir.zngis.behtabdatacollection.view.adaptor.BuildingListRvAdaptor
import ir.zngis.behtabdatacollection.view.callback.BuildingListRvCallback
import ir.zngis.behtabdatacollection.viewModel.BuildingVm
import kotlinx.android.synthetic.main.fragment_builing_list.*
import javax.inject.Inject
import androidx.recyclerview.widget.DividerItemDecoration
import ir.zngis.behtabdatacollection.repository.dataModel.BaseClass
import ir.zngis.behtabdatacollection.repository.dataModel.SurgeryDepartment
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.util.GeneralUtil


class BuildingListFragment : androidx.fragment.app.Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: BuildingVm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(BuildingVm::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_builing_list, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param hospital Parameter 1.
         * @param moduleName Parameter 2.
         * @return A new instance of fragment BuildingListFragment.
         */
        @JvmStatic
        fun newInstance(hospital: Hospital, moduleName: String) =
                BuildingListFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(Constants.HOSPITAL, hospital)
                        putString(Constants.MODULE, moduleName)

                    }
                }
    }

    private fun setFrgTitle() {
        val mainActivity = activity as MainActivity

        val hospital = arguments!![Constants.HOSPITAL] as Hospital
        val moduleName = arguments!![Constants.MODULE] as String

        mainActivity.mToolbar.title = GeneralUtil.getModuleFaName(activity!!, moduleName)
        mainActivity.mToolbar.subtitle = hospital.name.get()

    }

    override fun onResume() {
        super.onResume()
        setFrgTitle()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val hospital = arguments!![Constants.HOSPITAL] as Hospital
        val moduleName = arguments!![Constants.MODULE] as String

        setFrgTitle()

        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
        layoutManager.isSmoothScrollbarEnabled = true
        layoutManager.orientation = androidx.recyclerview.widget.LinearLayoutManager.VERTICAL

        val decoration = androidx.recyclerview.widget.DividerItemDecoration(activity!!, androidx.recyclerview.widget.DividerItemDecoration.VERTICAL)
        activity?.let {
            val divider = ContextCompat.getDrawable(it, R.drawable.rv_simple_divider)
            divider?.let {
                decoration.setDrawable(it)
            }
            rv_building.addItemDecoration(decoration)
        }

        rv_building.layoutManager = layoutManager


        viewModel.getBuildings(hospital.id, moduleName).observe(this, Observer {
            it?.let {
                if (it.isEmpty()) {
                    DialogUtil.showInfo(activity!!, getString(R.string.msg_error_no_building_found_pls_draw_hospital_buildings_in_map))
                }
                rv_building.adapter = BuildingListRvAdaptor(it.filter { it.building.status != EnumUtil.EntityStatus.DELETE }, object : BuildingListRvCallback {


                    override fun onBuildingItemClicked(building: Building) {

                        when (moduleName) {
                            Constants.MODULE_STRUCTURAL,
                            Constants.MODULE_INSTITUTIONAL,
                            Constants.MODULE_HAZARD -> {
                                val questionsPagerFrg = QuestionsFragment.newInstance(moduleName,hospital, building, null)
                                (activity as MainActivity).showFrg(questionsPagerFrg, QuestionsFragment::class.java.simpleName)

                            }
                        }
                    }

                    override fun onBuildingItemLongClicked(building: Building) {
                        DialogUtil.showInfo(activity!!,
                                "${getString(R.string.msg_warning_are_you_sure_for_deleting_selected_item)} \n" +
                                        "${building.name.get()}", getString(R.string.label_delete), getString(R.string.label_cancel), View.OnClickListener {
                            val dialogInfo = it.tag as Dialog
                            if (it.id == R.id.btnOk)
                                viewModel.deleteEntity(building)
                            dialogInfo.dismiss()
                        })
                    }

                    override fun onUnitItemLongClicked(building: Building, unit: Any) {
                        DialogUtil.showInfo(activity!!,
                                "${getString(R.string.msg_warning_are_you_sure_for_deleting_selected_item)} \n" +
                                        "${(unit as SurgeryDepartment).nameTemp}", getString(R.string.label_delete), getString(R.string.label_cancel), View.OnClickListener {
                            val dialogInfo = it.tag as Dialog
                            if (it.id == R.id.btnOk)
                                viewModel.deleteEntity(unit as BaseClass)
                            dialogInfo.dismiss()
                        })
                    }

                    override fun onUnitItemClicked(building: Building, unit: Any) {

                        val questionsPagerFrg = QuestionsFragment.newInstance(moduleName,hospital, building, unit)
                        (activity as MainActivity).showFrg(questionsPagerFrg, QuestionsFragment::class.java.simpleName)


//                        val frg = AddableEvaluationListFragment.newInstance(building, unit, arguments!![Constants.MODULE] as String)
//                        (activity as MainActivity).showFrg(frg, AddableEvaluationListFragment::class.java.simpleName)
                    }

                }, moduleName)
            }
        })
    }
}
