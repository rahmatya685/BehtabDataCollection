package ir.zngis.behtabdatacollection.view.ui


import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import cloudgis.mihanblog.com.datacollection.adaptor.FragmentPagerAdapter
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.Hospital
import ir.zngis.behtabdatacollection.repository.dataModel.HospitalComments
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.Constants.HOSPITAL
import ir.zngis.behtabdatacollection.util.Constants.HOSPITAL_COMMENT
import ir.zngis.behtabdatacollection.util.GeneralUtil
import kotlinx.android.synthetic.main.fragment_general_module.*


class GeneralModuleFragment : androidx.fragment.app.Fragment(), Injectable {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_general_module, container, false)
    }


    companion object {

        @JvmStatic
        fun newInstance(hospital: Hospital ) =
                GeneralModuleFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(HOSPITAL, hospital)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setFrgTitle()

        tb_hospital_general_info.setupWithViewPager(vp_hospital_general_info)
        tb_hospital_general_info.tabMode = TabLayout.MODE_FIXED
        setupViewPager(vp_hospital_general_info)


    }

    private fun setFrgTitle() {
        val hospital = arguments!![HOSPITAL] as Hospital
        val mainActivity = activity as MainActivity
        mainActivity.mToolbar.title = getString(R.string.label_general_info)
        mainActivity.mToolbar.subtitle = hospital.name.get()
    }

    private fun setupViewPager(vp: androidx.viewpager.widget.ViewPager?) {

        val hospital = arguments!![HOSPITAL] as Hospital


        val hospitalInfoFrg = HospitalInfoFragment.newInstance(hospital  )
        val hospitalUnits = HospitalDepartmentListFragment.newInstance(hospital)

        val frgs: MutableList<androidx.fragment.app.Fragment> = mutableListOf(hospitalInfoFrg, hospitalUnits)
        val adapter = FragmentPagerAdapter(childFragmentManager, frgs)
        adapter.mFragmentTitleList = mutableListOf(getString(R.string.label_characteristics), getString(R.string.label_departments))
        vp?.adapter = adapter
        vp!!.currentItem = 0
    }

    override fun onResume() {
        super.onResume()
        setFrgTitle()
    }
}
