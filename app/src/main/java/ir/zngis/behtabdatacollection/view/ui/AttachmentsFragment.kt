package ir.zngis.behtabdatacollection.view.ui


import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.github.angads25.filepicker.model.DialogConfigs
import com.github.angads25.filepicker.model.DialogProperties
import com.github.angads25.filepicker.view.FilePickerDialog
import io.reactivex.Single
import io.reactivex.SingleEmitter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.Attachment
import ir.zngis.behtabdatacollection.util.*
import ir.zngis.behtabdatacollection.view.adaptor.PicRvAdaptor
import ir.zngis.behtabdatacollection.viewModel.AttachmentsVm
import kotlinx.android.synthetic.main.fragment_attachments.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class AttachmentsFragment : FullScreenDialogFragment(), PicRvAdaptor.Listener, Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: AttachmentsVm

    private val deleteQueue: MutableList<Attachment> = mutableListOf()
    private lateinit var adapter: PicRvAdaptor
    private val PERMISSION_CAMERA = 325
    private var currentPhotoPath: String? = null
    internal val REQUEST_IMAGE_CAPTURE = 658
    internal val REQUEST_ALBUM_PHOTO = 454

    val subscriptions = CompositeDisposable()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(AttachmentsVm::class.java)
    }

    override fun onDestroy() {
        subscriptions.clear()
        super.onDestroy()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_attachments, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context_toolbar.inflateMenu(R.menu.menu_pic_frg_callback)

        toolbar.navigationIcon = ContextCompat.getDrawable(activity!!, R.drawable.ic_close_black_24dp)
        if (MainActivity.IS_RTL) {
            toolbar.navigationIcon = ContextCompat.getDrawable(activity!!, R.drawable.ic_close_black_24dp)
        }
        toolbar.setNavigationOnClickListener {
            dismiss()
        }

        toolbar.inflateMenu(R.menu.menu_pic_frg)
        toolbar.setOnMenuItemClickListener(this::onMenuItemClick2)

        toolbar.title = arguments?.getString(FRAGMENT_TITLE)

        context_toolbar.setOnMenuItemClickListener {

            when (it.itemId) {
                R.id.action_delete_photos -> {
                    DialogUtil.showInfo(activity!!,
                            getString(R.string.msg_warning_are_you_sure_2_delete_selected_items),
                            getString(R.string.label_ok),
                            getString(R.string.label_cancel), View.OnClickListener {

                        val dialog = it.tag as Dialog
                        if (it.id == R.id.btnOk) {
                            deleteFileInfos()
                        }
                        dialog.dismiss()
                    }
                    )
                }
                R.id.action_close_menu -> {
                    closeActionMenu()
                }
            }

            true
        }

        val manager = androidx.recyclerview.widget.GridLayoutManager(activity, 2, androidx.recyclerview.widget.GridLayoutManager.VERTICAL, false)
        manager.isSmoothScrollbarEnabled = true

        rv_photos?.layoutManager = manager
        rv_photos?.isVerticalScrollBarEnabled = true

        arguments?.let {

            val category = it.getString(Constants.ATTACHMENT_CATEGORY)
            val linkedId = it.getString(Constants.LINKED_ID)

            viewModel.getAttachments(category, linkedId).observe(this, androidx.lifecycle.Observer { attachments ->
                attachments?.let {
                    adapter = PicRvAdaptor(it.filter { it.status != EnumUtil.EntityStatus.DELETE }, this, false)
                    rv_photos.adapter = adapter
                }
            })
        }

    }

    @SuppressLint("CheckResult")
    private fun deleteFileInfos() {
        var dialog: Dialog? = null

        val subsctiption = Single.create { e: SingleEmitter<MutableList<Attachment>> ->
            e.onSuccess(
                    deleteFiles(deleteQueue)
            )
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    dialog = DialogUtil.showProgressBar(activity!!, getString(R.string.msg_progress_deleting_files))
                }
                .subscribe(
                        {
                            dialog!!.dismiss()
                            viewModel.deleteAttachments(deleteQueue)
                            closeActionMenu()

                        },
                        {
                            showError(it.stackTrace.toString())
                        })

        subscriptions.add(subsctiption)
    }

    private fun closeActionMenu() {
        toolbar.visibility = View.VISIBLE
        context_toolbar.visibility = View.GONE
        deleteQueue.clear()
        adapter.setItemsUnselected()
    }

    override fun onAttachmentCommnetChanged(attachment: Attachment) {
        viewModel.updateAttachment(attachment)
    }

    private fun deleteFiles(deleteQueue: MutableList<Attachment>): MutableList<Attachment> {
        val dir = GeneralUtil.getPicsDirectory(activity!!)
        deleteQueue.forEach {
            try {
                val file = File(dir!!.absolutePath)
                if (file.exists())
                    file.delete()
            } finally {

            }

        }
        return deleteQueue
    }

    companion object {
        const val FRAGMENT_TITLE = "frgTitle"

        fun newInstance(
                frgTitle: String,
                category: String,
                linkedId: String
        ): AttachmentsFragment {
            val fragment = AttachmentsFragment()
            var args = Bundle()
            args.putString(FRAGMENT_TITLE, frgTitle)
            args.putString(Constants.ATTACHMENT_CATEGORY, category)
            args.putString(Constants.LINKED_ID, linkedId)
            fragment.arguments = args
            return fragment
        }
    }

    private fun onMenuItemClick2(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_take_photo -> {
                startCameraActivity()
            }
            R.id.action_add_file -> {
                startFilePickerDialog()
            }
            R.id.action_show_album -> {
                showAlbum()
            }
        }
        return true
    }

    private fun showAlbum() {
//        val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
////            putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
//            putExtra(Intent.EXTRA_LOCAL_ONLY, true)
//            type = "image/*"
//        }
//        startActivityForResult(intent, REQUEST_ALBUM_PHOTO)


        val i = Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI).apply {
            putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        }

        startActivityForResult(i, REQUEST_ALBUM_PHOTO)
    }


    private fun startFilePickerDialog() {
        var properties = DialogProperties()
        properties.selection_mode = DialogConfigs.MULTI_MODE
        properties.selection_type = DialogConfigs.FILE_SELECT
        properties.root = File(DialogConfigs.DEFAULT_DIR)
        properties.error_dir = File(DialogConfigs.DEFAULT_DIR)
        properties.offset = File(DialogConfigs.DEFAULT_DIR)
        properties.extensions = null

        var dialog = FilePickerDialog(activity!!, properties)
        dialog.setNegativeBtnName(activity!!.getString(R.string.label_cancel))
        dialog.setPositiveBtnName(getString(R.string.label_ok))
        dialog.setTitle(getString(R.string.label_select_file))

        dialog.setDialogSelectionListener {
            var files: MutableList<File> = mutableListOf()
            it.forEach {
                var file = File(it)
                files.add(file)
            }
            handleFileAttachments(files)
        }
        dialog.show()
    }

    private fun handleFileAttachments(files: MutableList<File>) {
        var dialog: Dialog? = null

        val subscription = Single.create { e: SingleEmitter<MutableList<File>> ->
            e.onSuccess(
                    copyFiles(files)
            )
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    dialog = DialogUtil.showProgressBar(activity!!, getString(R.string.msg_info_copying_file))
                }
                .subscribe(
                        {
                            dialog?.dismiss()

                            it?.let {
                                if (it.isEmpty()) {

                                    showError(getString(R.string.msg_error_no_file_found))

                                } else {

                                    arguments?.let {

                                        val category = it.getString(Constants.ATTACHMENT_CATEGORY)
                                        val linkedId = it.getString(Constants.LINKED_ID)

                                        viewModel.addAttachments(files, category, linkedId)
                                    }

                                }
                            }

                        },
                        {
                            dialog!!.dismiss()
                            showError(it!!.stackTrace.toString())
                        })

        subscriptions.add(subscription)


    }

    private fun copyFiles(files: MutableList<File>): MutableList<File> {
        var featureDocs: MutableList<File> = mutableListOf()
        files.forEach {
            val fileToCopy = File("${GeneralUtil.getPicsDirectory(activity!!)}//${it.name}")
            if (!fileToCopy.exists()) {
                val copiedFile = it.copyTo(fileToCopy, true)
                if (copiedFile.exists()) {
                    featureDocs.add(copiedFile)
                }
            } else {
                featureDocs.add(fileToCopy)
            }

        }
        return featureDocs
    }


    private fun showError(error: String) {
        ToastUtil.ShowToast(activity!!, error, EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
    }

    private fun checkCameraHardware(context: Context): Boolean {
        return context.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
    }


    private fun startCameraActivity() {
        if (ContextCompat.checkSelfPermission(
                        activity!!,
                        android.Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(arrayOf(android.Manifest.permission.CAMERA), PERMISSION_CAMERA)
            }
        } else {
            openCamera()
        }

    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CAMERA -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera()
            } else {
                ToastUtil.ShowToast(
                        activity!!,
                        getString(R.string.msg_info_no_access_to_camera_provided), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG
                )
            }
        }
    }

    private fun openCamera() {
        //        if (GeneralUtil.isExternalStorageWritable()){
        //            DialogUtil.error(getActivity(),getString(R.string.msg_error_sdcard_unavilable));
        //            return;
        //        }
        if (!checkCameraHardware(activity!!))
            return

        var photoFile: File? = null

        try {
            photoFile = createImageFile()
        } catch (e: IOException) {

        }

        if (photoFile != null) {
            val photoURI = FileProvider.getUriForFile(
                    activity!!,
                    "ir.zngis.behtabdatacollection.fileprovider",
                    photoFile
            )
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
            if (takePictureIntent.resolveActivity(activity!!.packageManager) != null) {
                startActivityForResult(
                        takePictureIntent,
                        REQUEST_IMAGE_CAPTURE
                )
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
        val storageDir = GeneralUtil.getPicsDirectory(activity!!)
        val imageFileName = "behtab_" + timeStamp + "_"
        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        )
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.absolutePath
        return image
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("currentPhotoPath", currentPhotoPath)
        // outState.putSerializable("TbFeatures", tbFeatures)
        super.onSaveInstanceState(outState)
    }

    private fun GetDataFromBundle(savedInstanceState: Bundle) {
        if (savedInstanceState.containsKey("currentPhotoPath")) {
            currentPhotoPath = savedInstanceState.getString("currentPhotoPath")
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState == null) {
            return
        }
        GetDataFromBundle(savedInstanceState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {

            when (requestCode) {
                REQUEST_IMAGE_CAPTURE -> {
                    val img = File(currentPhotoPath)

                    arguments?.let {
                        val category = it.getString(Constants.ATTACHMENT_CATEGORY)
                        val linkedId = it.getString(Constants.LINKED_ID)

                        viewModel.addAttachments(mutableListOf(img), category, linkedId)
                    }
                }
                REQUEST_ALBUM_PHOTO -> {
                    val uri = data?.getData()
                    val filepath = arrayOf(MediaStore.Images.Media.DATA)
                    val cursor = activity?.contentResolver?.query(uri, filepath, null, null, null)

                    val columnIndex = cursor?.getColumnIndex(filepath[0])

                    cursor?.let { cur ->

                        cur.moveToFirst()

                        val files = mutableListOf<File>()

                        arguments?.let {


                            val nonReplasedPath = cursor.getString(columnIndex!!)

                            val img = File(nonReplasedPath)

                            if (img.exists())
                                files.add(img)
                        }

                        handleFileAttachments(files)

                        cursor.close()
                    }


                }
            }

        }
    }


    override fun showContextualMenu(): Boolean {
        toolbar.visibility = View.GONE
        context_toolbar.visibility = View.VISIBLE

//        toolbar.removeView(customNav)
//        context_toolbar.addView(customNav)

        return true
    }

    override fun addItem2DeleteQueue(fileInfo: Attachment, position: Int) {
        if (fileInfo != null) {
            deleteQueue.add(fileInfo)
        }
    }

    override fun removeItemFromDeleteQueue(fileInfo: Attachment, position: Int) {
        deleteQueue.remove(fileInfo)
    }

    override fun showPicViewer(fileName: String, imgTitle: String, fileDownloadUri: String) {
        val filePath =
                String.format("%s/%s", GeneralUtil.getPicsDirectory(activity!!), fileName)

        val picViewerFragment =
                PicViewerFragment.newInstance(filePath, imgTitle, fileDownloadUri)
        picViewerFragment.show(childFragmentManager, PicViewerFragment::class.java.simpleName)

    }

    override fun openFileIntent(name: String) {
        GeneralUtil.openFile(File("${GeneralUtil.getPicsDirectory(activity!!)}//$name"), activity!!)
    }


}
