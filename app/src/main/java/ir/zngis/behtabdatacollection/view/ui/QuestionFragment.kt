package ir.zngis.behtabdatacollection.view.ui


import android.annotation.SuppressLint
import android.app.Dialog
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.Toast

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.*
import ir.zngis.behtabdatacollection.util.*
import ir.zngis.behtabdatacollection.view.adaptor.EvaluationsRvAdaptor
import ir.zngis.behtabdatacollection.view.callback.GeneralRvCallback
import ir.zngis.behtabdatacollection.viewModel.QuestionVm
import kotlinx.android.synthetic.main.fragment_question.*
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 * Use the [QuestionFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class QuestionFragment : androidx.fragment.app.Fragment(), Injectable {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: QuestionVm


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(QuestionVm::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_question, container, false)
    }


    companion object {
        const val POSITION = "POSITION"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment QuestionFragment.
         */
         @JvmStatic
        fun newInstance(question: Question,hospital:Hospital, building: Building, unit: Any?, moduleName: String, position: Int) =
                QuestionFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(Constants.HOSPITAL,hospital)
                        putParcelable(Constants.QUESTION, question)
                        putInt(POSITION, position)

                        putParcelable(Constants.BUILDING, building)
                        putString(Constants.MODULE, moduleName)
                        unit?.let {
                            putParcelable(Constants.UNIT, it as Parcelable?)
                        }
                    }
                }
    }


    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var unit: Any? = null

        arguments?.let { arg ->

            val hospital = arg[Constants.HOSPITAL] as Hospital

            val building = arguments!![Constants.BUILDING] as Building
            val module = arguments!![Constants.MODULE] as String


            if (arg.containsKey(Constants.UNIT)) {
                unit = arguments!![Constants.UNIT] as Any
            }

            val question = arg[Constants.QUESTION] as Question

            if (PreferencesUtil.getCurrentLanguage(context) == Constants.ENGLISH) {

                tv_category.text = "${question.questionCategory.number}-${ question.questionCategory.categoryTitleEn}"
                tv_question_title.text = "${question.number}-${question.questionTitleEn}"
                tv_help.text = question.briefDescriptionEn

            } else {

                tv_category.text = "${question.questionCategory.number}-${ question.questionCategory.categoryTitleFa}"
                tv_question_title.text = "${question.number}-${question.questionTitleFa}"
                tv_help.text = question.briefDescriptionFa

            }


            val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
            layoutManager.isSmoothScrollbarEnabled = true
            layoutManager.orientation = androidx.recyclerview.widget.LinearLayoutManager.VERTICAL

            rv_evaluations.layoutManager = layoutManager


            val filteredEvaluations = question.evaluations.filter { it.status != EnumUtil.EntityStatus.DELETE }

            rv_evaluations.adapter = EvaluationsRvAdaptor(filteredEvaluations, object : EvaluationsRvAdaptor.Callback {

                override fun onBtnShowAttachmentsClicked(evaluation: Evaluation) {
                    val attachments = AttachmentsFragment.newInstance(getString(R.string.label_attachments), Evaluation::class.java.simpleName,evaluation.rowGuid)
                    attachments.show(childFragmentManager, AttachmentsFragment::class.java.simpleName)

                }

                override fun onEvaluationLongClick(evaluation: Evaluation, title: String) {

                    doDelete(evaluation, title)
                }

                override fun onItemLongClicked(e: Any) {

                }

                override fun onItemClicked(t: Any) {
                    val e = t as Evaluation

                    val questionWithEvaluations = EvaluationWithQuestion()
                    questionWithEvaluations.evaluation = e
                    questionWithEvaluations.questionTitleEn = question.questionTitleEn
                    questionWithEvaluations.questionTitleFa = question.questionTitleFa

                    questionWithEvaluations.briefDescriptionEn = question.briefDescriptionEn
                    questionWithEvaluations.briefDescriptionFa = question.briefDescriptionFa

                    questionWithEvaluations.questionId = question.id
                    questionWithEvaluations.categoryId = question.categoryId
                    questionWithEvaluations.helpLink = question.helpLink
                    questionWithEvaluations.weight = question.weight

                    val frg = AnswerFragment.newInstance2(hospital,building, unit, module, questionWithEvaluations)
                    (activity as MainActivity).showFrg(frg, AnswerFragment::class.java.simpleName)
                }

            })

            btn_add.setOnClickListener {
                showAnswerFrg(question)
            }

            when (module) {
                /*
               * for this module there should be only one answer for each question
                 */
                Constants.MODULE_STRUCTURAL,Constants.MODULE_HAZARD,Constants.MODULE_INSTITUTIONAL -> {
                    if (question.evaluations.filter { it.status != EnumUtil.EntityStatus.DELETE }.isNotEmpty()) {
                        btn_add.visibility = View.GONE
                    }
                }
            }


            tv_answers.text = tv_answers.text.toString() + " (${filteredEvaluations.filter { it.status != EnumUtil.EntityStatus.DELETE }.size})"




            tv_help_link.setOnClickListener {
                activity?.let {
                    if (!URLUtil.isValidUrl(question.helpLink)){
                        ToastUtil.ShowToast(activity,getString(R.string.msg_error_invalid_help_url),EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                    }else{
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = Uri.parse(question.helpLink)
                        activity?.startActivity(intent)
                    }
                }
            }
        }



        tv_more_help.setOnClickListener {

            when (layout_help.visibility) {
                View.VISIBLE -> {

                    layout_help.visibility = View.GONE

                    activity?.let {
                        tv_more_help.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_more_black_24dp, 0);
                        tv_more_help.text = getString(R.string.label_extra_help)
                    }
                }
                View.GONE -> {
                    layout_help.visibility = View.VISIBLE
                    activity?.let {
                        tv_more_help.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_less_black_24dp, 0);
                        tv_more_help.text = getString(R.string.label_hide_description)

                    }
                }
            }

        }




    }

    private fun doDelete(evaluation: Evaluation, title: String) {
        DialogUtil.showInfo(activity!!, "${getString(R.string.msg_warning_are_you_sure_2_delete_selected_items)} \n $title", "", "", View.OnClickListener {
            val dialogInfo = it.tag as Dialog
            if (it.id == R.id.btnOk)
                viewModel.deleteEntity(evaluation)
            dialogInfo.dismiss()
        })

    }

    private fun showAnswerFrg(q: Any) {
        arguments?.let { arg ->

            val hospital = arg[Constants.HOSPITAL] as Hospital

            val building = arg[Constants.BUILDING] as Building
            val module = arg[Constants.MODULE] as String
            var unit: Any? = null
            if (arg.containsKey(Constants.UNIT)) {
                unit = arg[Constants.UNIT]
            }

            val frg = AnswerFragment.newInstance(hospital, building, unit, module, q as Question)

            activity?.let {
                (it as MainActivity).showFrg(frg, AnswerFragment::class.java.simpleName)

            }


        }


    }
}
