package ir.zngis.behtabdatacollection.view.callback

import androidx.lifecycle.Observer


class ApiObserver<T> constructor(var changeListener: ChangeListener<T>) : Observer<DataWrapper<T>> {

    override fun onChanged(t: DataWrapper<T>?) {

        t?.let { dataWrapper ->

            if (dataWrapper.exception != null) {
                changeListener.onError(dataWrapper.exception!!)
            } else {
                changeListener.onSuccess(dataWrapper.data!!)
            }

        }

    }


    interface ChangeListener<T> {
        fun onSuccess(dataWrapper: T)
        fun onError(exception: Throwable)
    }
}