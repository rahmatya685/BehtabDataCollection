package ir.zngis.behtabdatacollection.view.adaptor

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import ir.zngis.behtabdatacollection.R

class ActionDialogAdapter(private val items: Array<String>?, private val icons: IntArray?, private val listener: View.OnClickListener) : androidx.recyclerview.widget.RecyclerView.Adapter<ActionDialogAdapter.ActionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActionViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_action_dialog_rv, parent, false)
        v.setOnClickListener(listener)
        v.setMinimumHeight(parent.measuredHeight / 4)

        return ActionViewHolder(v)
    }

    override fun onBindViewHolder(holder: ActionViewHolder, position: Int) {
        holder.name.text = this.items!![position]

        if (this.icons != null && position < this.icons.size && this.icons[position] != -1) {
            holder.image.setImageResource(this.icons[position])
        }else{
            holder.image.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    class ActionViewHolder internal constructor(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        internal var name: TextView
        internal var image: ImageView

        init {

            name = itemView.findViewById<View>(R.id.action_name) as TextView
            image = itemView.findViewById<View>(R.id.action_img) as ImageView
        }
    }
}
