package ir.zngis.behtabdatacollection.view.ui


import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.databinding.FragmentCandidateLocationBinding
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.CandidateLocation
import ir.zngis.behtabdatacollection.repository.dataModel.Hospital
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.util.ToastUtil
import ir.zngis.behtabdatacollection.view.callback.FeatureInfoCallback
import ir.zngis.behtabdatacollection.viewModel.BuildingVm
import kotlinx.android.synthetic.main.fragment_candidate_location.*
import javax.inject.Inject


class CandidateLocationFragment : FullScreenDialogFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: BuildingVm
    lateinit var binding: FragmentCandidateLocationBinding

    lateinit var mFeatureInfoCallback: FeatureInfoCallback

    private var mIsSaved = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(BuildingVm::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate<FragmentCandidateLocationBinding>(inflater, R.layout.fragment_candidate_location, container, false)
        return binding.root
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CandidateLocationFragment.
         */
        @JvmStatic
        fun newInstance(candidateLocation: CandidateLocation, hospital: Hospital, featureInfoCallback: FeatureInfoCallback) =
                CandidateLocationFragment().apply {
                    this.mFeatureInfoCallback = featureInfoCallback
                    arguments = Bundle().apply {
                        putParcelable(Constants.HOSPITAL, hospital)
                        putParcelable(Constants.CANDIDATE_LOCATION, candidateLocation)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mainActivity = activity as MainActivity
        mainActivity.setSupportActionBar(toolbar)

        toolbar.navigationIcon = ContextCompat.getDrawable(activity!!, R.drawable.ic_close_black_24dp)
        if (MainActivity.IS_RTL) {
            toolbar.navigationIcon = ContextCompat.getDrawable(activity!!, R.drawable.ic_close_black_24dp)
        }
        toolbar.setNavigationOnClickListener { dismiss() }

        val hospital = arguments!![Constants.HOSPITAL] as Hospital


        mainActivity.supportActionBar?.title = hospital.name.get()


        arguments?.let {
            val candidateLocation = it[Constants.CANDIDATE_LOCATION] as CandidateLocation
            mainActivity.supportActionBar?.subtitle = candidateLocation.name.get()

            binding.candidateLocation = candidateLocation.clone()

        }


        binding.btnSaveChanges.setOnClickListener {

            if (binding.candidateLocation?.name?.get().isNullOrEmpty()) {
                ToastUtil.ShowToast(activity!!, getString(R.string.msg_error_invalid_name), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                return@setOnClickListener
            }

            binding.candidateLocation?.let { c ->

                if (c.id == 0) {
                    viewModel.localRepo.insertEntity(c)
                } else {
                    viewModel.localRepo.updateEntity(c)
                }

            }

            mFeatureInfoCallback.featureSave()

            mIsSaved = true

            dismiss()

        }


        binding.btnAttachments.setOnClickListener {
            val attachments = AttachmentsFragment.newInstance(getString(R.string.label_attachments), CandidateLocation::class.java.simpleName, binding.candidateLocation!!.rowGuid)
            attachments.show(childFragmentManager, AttachmentsFragment::class.java.simpleName)

        }

    }

    override fun onDestroy() {

        if (!mIsSaved) {
            mFeatureInfoCallback.featureCanceled()
        }

        super.onDestroy()
    }
}
