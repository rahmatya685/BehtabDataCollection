package ir.zngis.behtabdatacollection.view.ui


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import android.text.Editable
import android.text.TextWatcher
import android.view.*

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.*
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.GeneralUtil
import ir.zngis.behtabdatacollection.util.PreferencesUtil
import ir.zngis.behtabdatacollection.view.adaptor.QuestionsFrgAdaptor
import ir.zngis.behtabdatacollection.view.callback.GeneralRvCallback
import ir.zngis.behtabdatacollection.viewModel.QuestionVm
import kotlinx.android.synthetic.main.fragment_questions.*
import java.lang.Exception
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 * create an instance of this fragment.
 *
 */
class QuestionsFragment : androidx.fragment.app.Fragment(), Injectable {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: QuestionVm


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(QuestionVm::class.java)

        setHasOptionsMenu(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        return when (item!!.itemId) {
//            R.id.app_bar_search -> {
//                true
//            }
//            else -> super.onOptionsItemSelected(item)
//        }

        return true

    }

    private fun showSearchFrg(v: View) {
        val module = arguments!![Constants.MODULE] as String
        var building: Building? = null
        var unit: Any? = null

        arguments?.let {
            building = it[Constants.BUILDING] as Building

            if (it.containsKey(Constants.UNIT))
                unit = it[Constants.UNIT] as Any
        }

        val frg = SearchQuestionsFragment.newInstance(module, building!!, unit, object : GeneralRvCallback {
            override fun onItemLongClicked(q: Any) {

            }

            override fun onItemClicked(q: Any) {
                if (q is Question) {

                    val questionPosition = (vp_questions.adapter as QuestionsFrgAdaptor).questions.indexOfFirst { it.id == q.id }

                    vp_questions.setCurrentItem(questionPosition, true)
                }
            }
        })
        frg.show(childFragmentManager, SearchQuestionsFragment::class.java.simpleName)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater?.inflate(R.menu.question_frg, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_questions, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment QuestionsFragment.
         */
        @JvmStatic
        fun newInstance(moduleName: String, hospital: Hospital, building: Building, unit: Any?) =
                QuestionsFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(Constants.HOSPITAL, hospital)
                        putString(Constants.MODULE, moduleName)
                        unit?.let {
                            putParcelable(Constants.UNIT, unit as Parcelable)
                        }
                        putParcelable(Constants.BUILDING, building)
                    }
                }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val module = arguments!![Constants.MODULE] as String
        var building: Building? = null
        var unit: Any? = null

        setFrgTitle()

        var indexKey = ""

        arguments?.let {

            building = it[Constants.BUILDING] as Building

            if (it.containsKey(Constants.UNIT)) {
                unit = it[Constants.UNIT] as Any

                when (unit) {
                    is SurgeryDepartment -> {
                        indexKey = Constants.CURRENT_PAGE_QUESTIONS_VIEW_PAGER + (unit as SurgeryDepartment).rowGuid
                    }
                    is NonClinicalUnit -> {
                        indexKey = Constants.CURRENT_PAGE_QUESTIONS_VIEW_PAGER + (unit as NonClinicalUnit).rowGuid
                    }
                    is ClinicalUnit -> {
                        indexKey = Constants.CURRENT_PAGE_QUESTIONS_VIEW_PAGER + (unit as ClinicalUnit).rowGuid
                    }
                }
            } else {
                indexKey = Constants.CURRENT_PAGE_QUESTIONS_VIEW_PAGER + building!!.rowGuid
            }

        }


        viewModel.getQuestionsWithCategoryAndEvaluations(module, building, unit).observe(viewLifecycleOwner, Observer {
            it?.let {
                vp_questions.addOnPageChangeListener(object : androidx.viewpager.widget.ViewPager.OnPageChangeListener {
                    override fun onPageScrollStateChanged(p0: Int) {

                    }

                    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
                    }

                    override fun onPageSelected(curre: Int) {
                        PreferencesUtil.putInteger(indexKey, vp_questions.currentItem, activity)
                        setCurrentPage()
                    }

                })

                val hospital = arguments!![Constants.HOSPITAL] as Hospital

                vp_questions.adapter = QuestionsFrgAdaptor(childFragmentManager, hospital, it, module, building, unit)


                val currentPageIndex = PreferencesUtil.getInteger(indexKey, activity)

                if (currentPageIndex != -1) {
                    vp_questions.currentItem = currentPageIndex
                }

                setCurrentPage()
            }


            btn_next.setOnClickListener(this::onBtnNextClicked)
            btn_previous.setOnClickListener(this::onBtnPreviousClicked)
            btn_last.setOnClickListener(this::onLastBtnClicked)
            btn_first.setOnClickListener(this::onFirstBtnClicked)

            btn_search.setOnClickListener(this::showSearchFrg)
        })


    }

    fun setCurrentPage() {
        vp_questions.adapter?.let {
            tv_current_page.text = "${vp_questions.currentItem + 1}/ ${it.count}"
        }
    }

    fun onBtnNextClicked(view: View) {
        vp_questions.adapter?.let {
            if (vp_questions.currentItem < it.count)
                vp_questions.currentItem = ++vp_questions.currentItem
        }
    }

    fun onLastBtnClicked(view: View) {
        vp_questions.adapter?.let {
            if (it.count > 0)
                vp_questions.currentItem = it.count - 1

        }
    }

    fun onFirstBtnClicked(view: View) {
        vp_questions.adapter?.let {
            if (it.count > 0)
                vp_questions.currentItem = 0

        }
    }

    fun onBtnPreviousClicked(view: View) {
        vp_questions.adapter?.let {
            if (vp_questions.currentItem > 0)
                vp_questions.currentItem = --vp_questions.currentItem

        }
    }


    private fun setFrgTitle() {
        arguments?.let { bundle ->


            val building = bundle[Constants.BUILDING] as Building
            val module = bundle[Constants.MODULE] as String
            val mainActivity = activity as MainActivity
            val hospital = bundle[Constants.HOSPITAL] as Hospital

            mainActivity.mToolbar.title = GeneralUtil.getModuleFaName(activity, module)


            when (module) {
                Constants.MODULE_HAZARD, Constants.MODULE_STRUCTURAL -> {
                    mainActivity.supportActionBar?.subtitle = "${hospital.name.get()} || ${building.name.get()}"

                }
                Constants.MODULE_NON_STRUCTURAL -> {

                    arguments?.let { arg ->

                        if (arg.containsKey(Constants.UNIT)) {

                            val unit: Any = arg[Constants.UNIT] as Any

                            when (unit) {
                                is ClinicalUnit -> {
                                    mainActivity.supportActionBar?.subtitle = "${hospital.name.get()} || ${building.name.get()} || ${unit.id}-${unit.nameTemp}"
                                }
                                is NonClinicalUnit -> {
                                    mainActivity.supportActionBar?.subtitle = "${hospital.name.get()} || ${building.name.get()} || ${unit.id}-${unit.nameTemp}"
                                }
                                is SurgeryDepartment -> {
                                    mainActivity.supportActionBar?.subtitle = "${hospital.name.get()} || ${building.name.get()} || ${unit.id}-${unit.nameTemp}"

                                }
                            }

                        }
                    }

                }
                else -> {

                }
            }

        }

    }


    override fun onResume() {
        super.onResume()
        setFrgTitle()
    }

}
