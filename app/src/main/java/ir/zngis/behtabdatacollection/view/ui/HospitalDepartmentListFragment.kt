package ir.zngis.behtabdatacollection.view.ui


import android.app.Dialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.baseBinding.BindableInt
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.*
import ir.zngis.behtabdatacollection.util.*
import ir.zngis.behtabdatacollection.view.adaptor.HospitalDepartmentAdaptor
import ir.zngis.behtabdatacollection.view.callback.GeneralRvCallback
import ir.zngis.behtabdatacollection.viewModel.HospitalDepVm
import kotlinx.android.synthetic.main.fragment_hospital_department_list.*
import javax.inject.Inject


class HospitalDepartmentListFragment : androidx.fragment.app.Fragment(), Injectable, HospitalDepartmentAdaptor.CallBack {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: HospitalDepVm
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(HospitalDepVm::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hospital_department_list, container, false)
    }


    companion object {

        @JvmStatic
        fun newInstance(hospital: Hospital) =
                HospitalDepartmentListFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(Constants.HOSPITAL, hospital)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val hospital = arguments!![Constants.HOSPITAL] as Hospital


        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
        layoutManager.isSmoothScrollbarEnabled = true
        layoutManager.orientation = androidx.recyclerview.widget.LinearLayoutManager.VERTICAL

        rv_hospital_units.layoutManager = layoutManager


        viewModel.getUnits(hospital.id).observe(this, Observer {
            it?.let {
                rv_hospital_units.adapter = HospitalDepartmentAdaptor(it.filter { it.status != EnumUtil.EntityStatus.DELETE }, this)
            }
        })

//        rv_hospital_units.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                super.onScrolled(recyclerView, dx, dy)
//                if (dy > 0) {
//                    btn_add_unit.hide()
//                } else {
//                    btn_add_unit.show()
//                }
//            }
//        })
    }

    override fun onAddUnitClicked(building: Building) {

        val hospital = arguments!![Constants.HOSPITAL] as Hospital
        val buildings = viewModel.getBuildings(hospital.id)
        if (buildings.isEmpty()) {
            DialogUtil.showInfo(activity!!, getString(R.string.msg_error_no_building_found_for_adding_new_department))
            return
        }

        viewModel.getUnitCategories().observe(this, Observer {

            it?.let {
                val dialog = Dialog(activity)

                var subCategories = mutableListOf<UnitSubCategory>()

                it.forEach { subCategories.addAll(it.unitSubCategories) }


                val cates = subCategories.map {
                    if (PreferencesUtil.getCurrentLanguage(activity) == Constants.ENGLISH) {
                        it.titleEn
                    } else {
                        it.titleFa
                    }

                }.toTypedArray()

                if (cates.isEmpty()) {
                    ToastUtil.ShowToast(activity!!, getString(R.string.msg_error_no_data_found_for_department_definition), EnumUtil.ToastActionState.Failure, Toast.LENGTH_LONG)
                    return@let
                }

                DialogUtil.showActions(context!!, dialog, getString(R.string.label_pls_choose_department_type), cates, null, View.OnClickListener { v ->
                    val textView: TextView = v!!.findViewById(R.id.action_name)
                    val action = textView.text.toString()

                    var unit: BaseClass? = null

                    val index = cates.indexOf(action)

                    var subCategory =subCategories[index]

                    val catId =subCategory.categoryId

                    var cat = it.find { it.unitCategory.id == catId }

                    when (cat!!.unitCategory.type) {
                        ClinicalUnit::class.java.simpleName -> {
                            unit = ClinicalUnit()
                            unit.name = BindableInt(subCategory.id)
                            unit.category = BindableInt(cat.unitCategory.id)
                            unit.nameTemp = action
                        }
                        SurgeryDepartment::class.java.simpleName -> {
                            unit = SurgeryDepartment()
                            unit.name = BindableInt(subCategory.id)
                            unit.category = BindableInt(cat.unitCategory.id)
                            unit.nameTemp = action
                        }
                        NonClinicalUnit::class.java.simpleName -> {
                            unit = NonClinicalUnit()
                            unit.name = BindableInt(subCategory.id)
                            unit.category = BindableInt(cat.unitCategory.id)
                            unit.nameTemp = action
                        }
                    }
                    unit?.let {
                        val frg = HospitalDepartmentFragment.newInstance(it, building)
                        frg.show(childFragmentManager, HospitalDepartmentFragment::class.java.simpleName)
                    }
                    dialog.dismiss()
                })

            }
        })
    }

    override fun onItemClicked(unit: Any) {

        var building: Building? = null

        var buildingRowGuid: String? = null
        when (unit) {
            is SurgeryDepartment -> {
                buildingRowGuid = unit.buildingId?.get()
            }
            is NonClinicalUnit -> {
                buildingRowGuid = unit.buildingId?.get()

            }
            is ClinicalUnit -> {
                buildingRowGuid = unit.buildingId?.get()
            }
        }
        buildingRowGuid?.let {

            building = viewModel.getBuilding(it)

            building?.let {

                val frg = HospitalDepartmentFragment.newInstance(unit as BaseClass, it)
                frg.show(childFragmentManager, HospitalDepartmentFragment::class.java.simpleName)
            }
        }


    }

    override fun onItemLongClicked(unit: Any) {
        DialogUtil.showInfo(activity!!, getString(R.string.msg_warning_are_you_sure_for_deleting_selected_item), getString(R.string.label_delete), getString(R.string.label_cancel), View.OnClickListener {
            val dialogInfo = it.tag as Dialog
            if (it.id == R.id.btnOk)
                viewModel.deleteEntity(unit as BaseClass)
            dialogInfo.dismiss()
        })
    }
}
