package ir.zngis.behtabdatacollection.view.ui

import android.app.Dialog
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.widget.Toast
import dagger.android.DispatchingAndroidInjector
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.util.*
import ir.zngis.behtabdatacollection.viewModel.UserVm
import kotlinx.android.synthetic.main.activity_splash.*
import javax.inject.Inject
import dagger.android.AndroidInjector
import dagger.android.HasAndroidInjector


class SplashActivity : BaseActivity(), HasAndroidInjector {
    private val SPLASH_TIME_OUT: Long = 2000



    @Inject
    lateinit var supFragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Any>


    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<androidx.fragment.app.Fragment>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: UserVm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(UserVm::class.java)

        supportActionBar?.hide()

        txtAppVersion.text = "V ${GeneralUtil.getApplicationVersionString(this)}"

        getUserInfo()

    }

    private fun getUserInfo() {
        runOnUiThread {
            Handler().postDelayed({

                val token = PreferencesUtil.getString(Constants.TOKEN, this)

                if (!token.isNullOrEmpty()) {

                    startMainActivity(getString(R.string.app_name))

//                    viewModel.getUserInfo().observe(this, ApiObserver(object : ApiObserver.ChangeListener<User> {
//                        override fun onSuccess(user: User) {
//
//                            startMainActivity(user.name)
//                        }
//
//                        override fun onError(exception: Throwable) {
//                            exception.message?.let {
//                                if (it.contentEquals(Constants.ERROR_SERVER_IS_NOT_AVAILABLE)){
//                                    progressBar2.visibility = View.INVISIBLE
//                                    val snackbarNoProfile = Snackbar.make(rootView, getString(R.string.msg_error_connecting_server), Snackbar.LENGTH_INDEFINITE )
//                                    snackbarNoProfile.setAction(getString(R.string.label_try_again)) {
//                                        progressBar2.visibility = View.VISIBLE
//                                        getUserInfo()
//
//                                    }
//                                    snackbarNoProfile.show()
//                                    return
//                                }
//                            }
//                            startLoginActivity()
//                        }
//
//                    }))
                } else {
                    startLoginActivity()
                }

            }, SPLASH_TIME_OUT)

        }
    }

    private fun startMainActivity(name: String?) {
        runOnUiThread {
            val b = Handler().postDelayed({
                val i = Intent(this, MainActivity::class.java)
                i.putExtra(Constants.USER_NAME, name)
                startActivity(i)
                try {
                    overridePendingTransition(R.anim.slide_up, R.anim.slide_down)
                } catch (e: Exception) {
                }
                finish()
                //mProgress.setVisibility(View.GONE);
            }, 100)
        }
    }

    private fun startLoginActivity() {
        runOnUiThread {
            val b = Handler().postDelayed({
                val i = Intent(this, LoginActivity::class.java)
                startActivity(i)
                try {
                    //overridePendingTransition(R.anim.s, R.anim.fadeout)
                } catch (e: Exception) {
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    overridePendingTransition(R.anim.slide_up, R.anim.slide_down)
                }
                finish()
                //mProgress.setVisibility(View.GONE);
            }, 100)
        }
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return supFragmentDispatchingAndroidInjector
    }

}
