package ir.zngis.behtabdatacollection.view.ui


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Parcelable
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.dependencyInjection.Injectable
import ir.zngis.behtabdatacollection.repository.dataModel.*
import ir.zngis.behtabdatacollection.util.Constants
import ir.zngis.behtabdatacollection.util.PreferencesUtil
import ir.zngis.behtabdatacollection.view.adaptor.SearchQuestionListRvAdaptor
import ir.zngis.behtabdatacollection.view.adaptor.SearchQuestionsExListAdaptor
import ir.zngis.behtabdatacollection.view.adaptor.SimpleDividerItemDecoration
import ir.zngis.behtabdatacollection.view.callback.GeneralRvCallback
import ir.zngis.behtabdatacollection.viewModel.QuestionVm
import kotlinx.android.synthetic.main.fragment_search_questions.*
import javax.inject.Inject


class SearchQuestionsFragment : BaseDialogFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: QuestionVm

    lateinit var callback: GeneralRvCallback

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(QuestionVm::class.java)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_questions, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param moduleName Parameter 2.
         * @return A new instance of fragment SearchQuestionsFragment.
         */
        @JvmStatic
        fun newInstance(moduleName: String, building: Building, unit: Any?, callback: GeneralRvCallback) =
                SearchQuestionsFragment().apply {
                    this.callback = callback
                    arguments = Bundle().apply {
                        putString(Constants.MODULE, moduleName)
                        unit?.let {
                            putParcelable(Constants.UNIT, unit as Parcelable)
                        }
                        putParcelable(Constants.BUILDING, building)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val module = arguments!![Constants.MODULE] as String


//        val layoutManager = LinearLayoutManager(activity)
//        layoutManager.isSmoothScrollbarEnabled = true
//        layoutManager.orientation = LinearLayoutManager.VERTICAL

//        rv_questions.layoutManager = layoutManager
//
//        val itemDecor = SimpleDividerItemDecoration(activity)
//        rv_questions.addItemDecoration(itemDecor)

        //       val adaptor = SearchQuestionListRvAdaptor(object : GeneralRvCallback {
//            override fun onItemClicked(unit: Any) {
//                callback.onItemClicked(unit)
//                dismiss()
//            }
//
//            override fun onItemLongClicked(unit: Any) {
//
//            }
//        })
//        rv_questions.adapter = adaptor

        var building: Building? = null
        var unit: Any? = null

        arguments?.let {

            building = it[Constants.BUILDING] as Building
            if (it.containsKey(Constants.UNIT))
                unit = it[Constants.UNIT] as Any

        }

        viewModel.getQuestionsWithCategoriesAsFlatList(module, building, unit, PreferencesUtil.getCurrentLanguage(activity)).observe(this, Observer {
            it?.let {

                val adaptor = SearchQuestionsExListAdaptor(it, object : GeneralRvCallback {
                    override fun onItemClicked(unit: Any) {
                        if (::callback.isInitialized)
                            callback.onItemClicked(unit)
                        dismiss()
                    }

                    override fun onItemLongClicked(unit: Any) {

                    }

                })
                exp_lv_questions.setAdapter(adaptor)

                sv_questions.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(p0: String?): Boolean {
                        return false
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {

                        if (newText.isNullOrEmpty()) {
                            adaptor.replaceAll(it)
                        } else {
                            var filteredItems: MutableList<QuestionCategoryWithQuestion> = mutableListOf()
                            it.forEach {

                                val questionCat = QuestionCategoryWithQuestion()
                                questionCat.allQuestions = it.allQuestions.filter { it.questionTitleEn.contains(newText!!, true) || it.questionTitleFa.contains(newText, true) || it.number!!.contains(newText, true) }.toMutableList()
                                questionCat.questionCategory = it.questionCategory

                                filteredItems.add(questionCat)

                            }

                            adaptor.replaceAll(filteredItems)
                        }
                        return false
                    }

                })

                adaptor.add(it)


                //===========expndable List view ===========


            }
        })


    }
}
