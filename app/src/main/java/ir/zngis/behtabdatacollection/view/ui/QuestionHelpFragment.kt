package ir.zngis.behtabdatacollection.view.ui


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.Toast

import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.util.EnumUtil
import ir.zngis.behtabdatacollection.util.ToastUtil
import kotlinx.android.synthetic.main.fragment_question_help.*


class QuestionHelpFragment : FullScreenDialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_question_help, container, false)
    }


    companion object {
        const val HELP_TEXT = "HEPLL_TEXT"
        const val HELP_LINK = "HELP_LINK"
        @JvmStatic
        fun newInstance(helpText: String, helpLink: String) =
                QuestionHelpFragment().apply {
                    arguments = Bundle().apply {
                        putString(HELP_LINK, helpLink)
                        putString(HELP_TEXT, helpText)
                    }
                }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.navigationIcon = ContextCompat.getDrawable(activity!!, R.drawable.ic_close_black_24dp)
        if (MainActivity.IS_RTL) {
            toolbar.navigationIcon = ContextCompat.getDrawable(activity!!, R.drawable.ic_close_black_24dp)
        }
        toolbar.setNavigationOnClickListener { dismiss() }


        toolbar?.title = getString(R.string.label_guid)

        arguments?.let { arg ->

            val helpText = arg.getString(HELP_TEXT)
            val helpUrl = arg.getString(HELP_LINK)

            tv_help_text.text = helpText

            tv_help_link.setOnClickListener {
                activity?.let {
                    if (!URLUtil.isValidUrl(helpUrl)){
                        ToastUtil.ShowToast(activity,getString(R.string.msg_error_invalid_help_url),EnumUtil.ToastActionState.Failure,Toast.LENGTH_LONG)
                    }else{
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = Uri.parse(helpUrl)
                        activity?.startActivity(intent)
                    }
                }
            }
        }
    }
}
