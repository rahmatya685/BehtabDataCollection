package ir.zngis.behtabdatacollection.view.adaptor

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import ir.zngis.behtabdatacollection.repository.dataModel.Building
import ir.zngis.behtabdatacollection.repository.dataModel.Hospital
import ir.zngis.behtabdatacollection.repository.dataModel.Question
import ir.zngis.behtabdatacollection.view.ui.QuestionFragment

class QuestionsFrgAdaptor(fragmentManager: androidx.fragment.app.FragmentManager, var hospital: Hospital, var questions: List<Question>, var module: String, var building: Building?, var unit: Any?) : androidx.fragment.app.FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        val frg = QuestionFragment.newInstance(questions[position],hospital,building!! ,unit,module,position)
        return frg
    }

    override fun getCount() = questions.size
}