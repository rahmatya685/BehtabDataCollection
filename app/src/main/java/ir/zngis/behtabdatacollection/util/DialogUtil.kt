package ir.zngis.behtabdatacollection.util

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import ir.zngis.behtabdatacollection.R
import ir.zngis.behtabdatacollection.view.adaptor.ActionDialogAdapter
import ir.zngis.behtabdatacollection.view.sub_view.ActionDialogListItemDecoration

object DialogUtil {


    fun showActions(context: Context, dialog: Dialog, dialogTitle: String, strList: Array<String>, icons: IntArray?, onClickListener: View.OnClickListener) {
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.dailog_action_layout, null, false)
        layout.verticalScrollbarPosition = View.FOCUS_UP


        val tvDialogTitle = layout.findViewById<TextView>(R.id.tv_title)
        tvDialogTitle.text = dialogTitle

        val recyclerView = layout.findViewById(R.id.action_list_view) as androidx.recyclerview.widget.RecyclerView

        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(layout)
        dialog.window!!.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        recyclerView.setHasFixedSize(true)
        val gridLayoutManager = androidx.recyclerview.widget.GridLayoutManager(context, 1, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, true)
        recyclerView.layoutManager = gridLayoutManager
        val itemDecoration = ActionDialogListItemDecoration(context, R.dimen.action_dialog_rv_items_padding)
        recyclerView.addItemDecoration(itemDecoration)
        recyclerView.scrollToPosition(View.FOCUS_UP)
        recyclerView.adapter = ActionDialogAdapter(strList, icons, onClickListener)
        dialog.show()
    }


    fun showGetFileNameDialog(context: Context, clickListener: View.OnClickListener, title: String): Dialog {
        val inflater = LayoutInflater.from(context)
        val alertDialogBuilder = AlertDialog.Builder(context, R.style.MyDialogTheme)

        val promptsView = inflater.inflate(R.layout.dialog_file_archiving, null)

        promptsView.findViewById<TextView>(R.id.tv_title).text = title
        val editText = promptsView.findViewById(R.id.file_name) as EditText
        alertDialogBuilder.setView(promptsView)
        val alertDialog = alertDialogBuilder.create()
        alertDialog.window!!.setWindowAnimations(R.style.DialogAnimation)
        alertDialog.show()
        val btn_alert_dialog_ok = promptsView.findViewById(R.id.alert_dialog_ok) as Button
        btn_alert_dialog_ok.setTag(R.integer.tag_id_get_name_dialog_layout, alertDialog)
        btn_alert_dialog_ok.setTag(R.integer.tag_id_get_name_dialog_edittext, editText)
        btn_alert_dialog_ok.setOnClickListener(clickListener)
        return alertDialog
    }


    /*
    * non cancelable progressbar
     */
    fun showProgressBar(context: Context, msg: String): Dialog {
        return showProgressBar(context, msg, false)
    }

    fun showProgressBar(context: Context, msg: String, cancelable: Boolean): Dialog {
        val activity = context as Activity
        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_progress, null)
        val txtViewMsg = view.findViewById<TextView>(R.id.msg)
        txtViewMsg.setText(msg)
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(cancelable)
        dialog.setContentView(view)
        return dialog
    }

//    fun showError(context: Context, msg: String) {
//
//        val activity = context as Activity
//
//        val inflater = activity.layoutInflater
//
//        val alertDialogBuilder = AlertDialog.Builder(activity, R.style.MyDialogTheme)
//
//
//        val alertBtns = inflater.inflate(R.layout.dialog_error, null)
//        alertDialogBuilder.setView(alertBtns)
//
//        alertDialogBuilder.setCancelable(false)
//
//        val alertDialog = alertDialogBuilder.show()
//        alertDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
//        //alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        val textView = alertDialog.findViewById<TextView>(R.id.message)
//        val face = Typeface.createFromAsset(context.getAssets(), "font/b_yekan.ttff")
//        textView!!.typeface = face
//        textView.text = msg
//        val btnCancel = alertBtns.findViewById(R.id.btnCancel)
//        btnCancel.setOnClickListener({ view -> alertDialog.dismiss() })
//        val btn_alert_dialog_ok = alertBtns.findViewById(R.id.btnOk) as Button
////        btn_alert_dialog_ok.setOnClickListener { view -> ACRA.getErrorReporter().handleException(Throwable(msg), false) }
//
//
//    }


    fun showInfo(context: Context, msg: String, btnOkTxt: String, btnCancelText: String?, clickListener: View.OnClickListener) {
        val activity = context as Activity
        val inflater = activity.layoutInflater

        val alertDialogBuilder = AlertDialog.Builder(activity, R.style.MyDialogTheme)
        val alertBtns = inflater.inflate(R.layout.dialog_info, null)
        alertDialogBuilder.setView(alertBtns)
        alertDialogBuilder.setMessage(msg).setCancelable(false)
        val txtViewMsg = alertBtns.findViewById<TextView>(R.id.msg)
        txtViewMsg.text = msg

        val btnOk = alertBtns.findViewById<Button>(R.id.btnOk)
        btnOk.setOnClickListener(clickListener)
        if (btnOkTxt.isNotEmpty())
            btnOk.text = btnOkTxt

        val btnCancel = alertBtns.findViewById<Button>(R.id.btnCancel)
        btnCancel.setOnClickListener(clickListener)
        if (btnCancelText == null) {
            btnCancel.visibility = View.GONE
        } else {
            if (btnCancelText.isNotEmpty())
                btnCancel.text = btnCancelText
            btnCancel.visibility = View.VISIBLE
        }

        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(alertBtns)
        btnOk.tag = dialog
        btnCancel.tag = dialog
        dialog.show()
    }


    fun showInfo(context: Context, msg: String) {
        val inflater = LayoutInflater.from(context)

        val alertDialogBuilder = AlertDialog.Builder(context, R.style.MyDialogTheme)
        val alertBtns = inflater.inflate(R.layout.dialog_info, null)
        alertDialogBuilder.setView(alertBtns)
        alertDialogBuilder.setMessage(msg).setCancelable(false)
        val txtViewMsg = alertBtns.findViewById<TextView>(R.id.msg)
        txtViewMsg.setText(msg)

        val btnOk = alertBtns.findViewById<Button>(R.id.btnOk)


        val btnCancel = alertBtns.findViewById<Button>(R.id.btnCancel)
        btnCancel.setVisibility(View.GONE)

        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(alertBtns)
        btnOk.setTag(dialog)
        btnCancel.setTag(dialog)
        dialog.show()
        btnOk.setText(R.string.label_got_it)
        btnOk.setOnClickListener({ view -> dialog.dismiss() })

    }

}