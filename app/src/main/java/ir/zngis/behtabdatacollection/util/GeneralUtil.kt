package ir.zngis.behtabdatacollection.util

import android.content.Context
import android.content.Intent
import androidx.fragment.app.FragmentActivity
import ir.zngis.behtabdatacollection.R
import android.graphics.Typeface
import android.net.Uri
import android.os.Environment
import java.io.File
import android.app.ActivityManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.util.TypedValue
import java.util.*
import java.util.UUID.randomUUID
import android.webkit.MimeTypeMap
import android.content.ContentResolver
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import androidx.core.content.ContextCompat
import androidx.annotation.DrawableRes
import com.google.android.gms.maps.model.BitmapDescriptor


object GeneralUtil {


//    fun replaceNonstandardDigits(input: String?): String? {
//        if (input == null || input.isEmpty()) {
//            return input
//        }
//
//        val builder = StringBuilder()
//        for (i in 0 until input.length) {
//            val ch = input[i]
//            if (isNonstandardDigit(ch)) {
//                val numericValue = Character.getNumericValue(ch)
//                if (numericValue >= 0) {
//                    builder.append(numericValue)
//                }
//            } else {
//                builder.append(ch)
//            }
//        }
//        return builder.toString()
//    }
//
//    private fun isNonstandardDigit(ch: Char): Boolean {
//        return Character.isDigit(ch) && !(ch >= '0' && ch <= '9')
//    }
    fun Now(): Date {
        val timeZone = TimeZone.getTimeZone("Asia/Tehran")
        val calendar = GregorianCalendar(timeZone)
        return calendar.time
    }

    fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    fun drawableToBitmap(drawable: Drawable): Bitmap {
        var bitmap: Bitmap;

        if (drawable is BitmapDrawable) {
            var bitmapDrawable = drawable as BitmapDrawable
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        var canvas = Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }


    fun openFile(url: File, context: Context) {
        val uri = Uri.fromFile(url)
        val intent = Intent(Intent.ACTION_VIEW)
        if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword")
        } else if (url.toString().contains(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf")
        } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint")
        } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel")
        } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
            // WAV audio file
            intent.setDataAndType(uri, "application/x-wav")
        } else if (url.toString().contains(".rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf")
        } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav")
        } else if (url.toString().contains(".gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif")
        } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
            // JPG file
            intent.setDataAndType(uri, "image/jpeg")
        } else if (url.toString().contains(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain")
        } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
            // Video files
            intent.setDataAndType(uri, "video/*")
        } else {
            //if you want you can also define the intent type for any other file
            //additionally use else clause below, to manage other unknown extensions
            //in this case, Android will show all applications installed on the device
            //so you can choose which application to use
            intent.setDataAndType(uri, "*/*")
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)

    }

    fun getFileExtensionImagePath(context: Context, extension: String): Uri {
        val extenson = extension.replace(".", "")
        val assetManager = context.assets

        var filePath = ""
        try {
            val fileExtensionsImagesPaths = assetManager.list("file_extensions")

            for (f in fileExtensionsImagesPaths) {
                if (f.toUpperCase().substring(0, f.indexOf(".")).equals(extenson, ignoreCase = true)) {
                    filePath = f
                }
            }

        } catch (e: Exception) {

        }

        return Uri.parse("file:///android_asset/file_extensions/$filePath")
    }

    fun getPicsDirectory(activity: Context): File? {
        val path = File(getAppsDirectory()+ File.separator + "Pics")
        if (!path.exists()) path.mkdir()
        return path.absoluteFile
    }

    fun createDbFolder(): String {
        val dbFileDirectory = GeneralUtil.getAppsDirectory() + File.separator + "Db"

        val file = File(dbFileDirectory)
        if (!file.exists())
            file.mkdir()
         return dbFileDirectory
    }

    fun getAppsDirectory():String{
        val path = File(Environment.getExternalStorageDirectory().absolutePath + File.separator + "Behtab")
        if (!path.exists()) path.mkdir()
        return  path.absolutePath
    }

    fun getTypeFace(activity: Context): Typeface {
        return Typeface.createFromAsset(activity.assets, Constants.FONT_PATH)
    }

    fun getApplicationVersionString(context: Context): String {
        try {
            val pm = context.packageManager;
            val info = pm.getPackageInfo(context.packageName, 0);
            return info.versionName;
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }

    fun getModuleFaName(activity: androidx.fragment.app.FragmentActivity?, module: String): CharSequence? {
        return when (module) {
            Constants.MODULE_HAZARD -> {
                activity!!.getString(R.string.label_hazard_info)
            }
            Constants.MODULE_INSTITUTIONAL -> {
                activity!!.getString(R.string.label_institutional_info)
            }
            Constants.MODULE_NON_STRUCTURAL -> {
                activity!!.getString(R.string.label_non_structural_information)
            }
            Constants.MODULE_STRUCTURAL -> {
                activity!!.getString(R.string.label_structural_information)
            }
            else -> ""
        }
    }

    fun isServiceRunning(activity: Context, serviceClass: Class<*>): Boolean {
        val manager = activity.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    fun convertDiptoPix(context: Context, dip: Float): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, context.resources.displayMetrics).toInt()
    }


    fun getFontBitmap(context: Context, text: String, txtColor: Int, backgroundColor: Int, fontSizeSP: Float): Bitmap {
        val fontSizePX = convertDiptoPix(context, fontSizeSP)
        val pad = fontSizePX / 9
        val paint = Paint()

        val bitmap = Bitmap.createBitmap(150, 150, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        val radius = Math.min(canvas.width, canvas.height / 2)


        paint.color = backgroundColor
        paint.style = Paint.Style.FILL
        canvas.drawCircle(
                (canvas.width / 2).toFloat(), // cx
                (canvas.height / 2).toFloat(), // cy
                radius.toFloat(), // Radius
                paint // Paint
        )

        val typeface = Typeface.createFromAsset(context.assets, Constants.FONT_PATH)
        paint.isAntiAlias = true
        paint.typeface = typeface
        paint.textSize = fontSizePX.toFloat()
        val textWidth = (paint.measureText(text) + pad * 2).toInt()
        val height = (fontSizePX / 0.2).toInt()
        paint.color = txtColor
        canvas.drawText(text, pad.toFloat(), (canvas.height / 2).toFloat(), paint)
        return bitmap
    }

    fun generateGuid(): String {
        return UUID.randomUUID().toString().toUpperCase()
    }


    fun getMimeType(uri: Uri, context: Context): String? {
        var mimeType: String? = null
        if (uri.scheme == ContentResolver.SCHEME_CONTENT) {
            val cr = context.getContentResolver()
            mimeType = cr.getType(uri)
        } else {
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString())
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase())
        }
        return mimeType
    }

    fun getModuleNumber(module: String): Any {

        return when (module) {
            Constants.MODULE_HAZARD -> 1
            Constants.MODULE_STRUCTURAL -> 2
            Constants.MODULE_NON_STRUCTURAL -> 3
            Constants.MODULE_INSTITUTIONAL -> 4
            else -> -1
        }

    }
}