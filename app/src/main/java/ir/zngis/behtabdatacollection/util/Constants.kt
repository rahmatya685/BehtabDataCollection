package ir.zngis.behtabdatacollection.util

import ir.zngis.behtabdatacollection.repository.dataModel.User

object Constants {
    const val NOTIFICATION_FONT_SIZE = 30F
    var FONT_PATH = "fonts/b_yekan.ttf"
    const val SHARED_PREF_FILE_NAME = "PreferencesUtil"
    const val isDbPopulated = "isDbPopulated"
    const val HOSPITAL = "HOSPITAL"
    const val HOSPITAL_COMMENT = "HOSPITAL_COMMENT"
    const val BUILDING = "BUILDING"
    const val MODULE = "MODULE"
    const val VERTEXT = "VERTEXT"
    const val HOSPITAL_ENTRANCE = "HOSPITAL_ENTRANCE"
    const val CANDIDATE_LOCATION = "CANDIDATE_LOCATION"
    const val QUESTION = "QUESTION"
    const val EVALUATION = "EVALUATION"
    const val UNIT = "UNIT"
    const val SEARCH_POSITION = "SEARCH_POSITION"
    const val EVALUATION_WITH_QUESTION = "EVALUATION_WITH_QUESTION"

    const val MAP_TYPE = "MAP_TYPE"

    const val LINKED_ID = "LINKED_ID"
    const val ATTACHMENT_CATEGORY = "ATTACHMENT_CATEGORY"


    const val MODULE_STRUCTURAL = "STRUCTURAL"
    const val MODULE_NON_STRUCTURAL = "NON_STRUCTURAL"
    const val MODULE_HAZARD = "HAZARD"
    const val MODULE_INSTITUTIONAL = "INSTITUTIONAL"


    const val LANGUAGE = "LANGUAGE"
    const val PERSIAN = "fa"
    const val ENGLISH = "en"

    val HIGH_RISK_LEVEL = 4
    val MEDIUM_RISK_LEVEL = 3
    val LOW_RISK_LEVEL = 2
    val NON_RISK_LEVEL = 1

    const val TOKEN = "sjdfgsidgfjosdjfgoipsdfj"


    const val ERROR_USER_NOT_FOUND = "User not found"
    const val ERROR_SERVER_IS_NOT_AVAILABLE = "SERVER IS NOT AVAILABLE"
    const val ERROR_MORE_THAN_ONE_USER_LOGIN_IN_NOT_ALLOWED = "MORE_THAN_ONE_USER_LOGIN_IN_NOT_ALLOWED"

    const val USER_NAME = "USERNAME"
    const val USER_IDENTIFIER = "uniqNum"
    const val MSG_RELOGIN = "MSG_RELOGIN"
    const val GET_DATA_RECIVER_KEY = "GET_DATA_RECIVER_KEY"

    const val DOWNLOAD_COMPLETED_HOSPITALS = 1000
    const val DOWNLOAD_COMPLETED_QUESTIONS = 2000
    const val ERROR = "ERROR"
    const val ERROR_CODE = 548
    const val STATUS_OK = 100
    const val STATUS_ERROR = 100

    const val SYNC_STATUS_OK = "OK"
    const val SYNC_ATTACHMENTS_STATUS_OK = "SYNC_ATTACHMENTS_STATUS_OK"
    const val NO_ATTACHMENT_FOUND_FOR_UPLOAD = "NO_ATTACHMENT_FOUND_FOR_UPLOAD"
    const val SYNC_STATUS_PROGRESS = "OK"


    const val AUTHORIZATION_LABEL = "Authorization"


    const val ACTION_START_SYNC_DATA_SERVICE = "ACTION_START_SYNC_DATA_SERVICE"
    const val ACTION_STOP_SYNC_DATA_SERVICE = "ACTION_STOP_SYNC_DATA_SERVICE"
    const val FOREGROUND_SERVICE = 548644
    const val FINISH_NOTIFICATION_ID = 546


    const val USER_ROLE_EVALUATOR = "2"
    const val USER_ROLE_SURVERYOR = "1"
    const val DB_NAME = "DB"
    const val CURRENT_PAGE_HOSPITAL_VIEW_PAGER = "CURRENT_PAGE_HOSPITAL_VIEW_PAGER"
    const val CURRENT_PAGE_QUESTIONS_VIEW_PAGER = "CURRENT_PAGE_HOSPITAL_VIEW_PAGER"


    const val KEY_SETTING_SHOULD_CONTROL_LOCATION = "5a13ac64-1971-4c9a-bf0c-c9e887713e1fdf5"
    const val KEY_SETTING_USER_DISTANCE_TO_FEATURE = "5a13ac64-1971-4c9a-bf0c-c9e887713e15"
    const val KEY_INSTALLATION_ID = "KEY_INSTALLATION_ID"

}