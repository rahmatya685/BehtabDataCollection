package ir.zngis.behtabdatacollection.util;

import com.google.maps.android.data.geojson.GeoJsonPointStyle;

public class GeoJsonPointStyleCustom  extends GeoJsonPointStyle {
    public GeoJsonPointStyleCustom(int zLevel) {
        this.mMarkerOptions.zIndex( zLevel);
    }
}
