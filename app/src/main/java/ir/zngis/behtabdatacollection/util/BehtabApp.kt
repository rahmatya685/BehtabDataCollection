//package ir.zngis.behtabdatacollection.util
//
//import android.app.Activity
//import android.app.Application
//import android.app.Service
//import android.content.BroadcastReceiver
//import android.content.Context
//import android.support.multidex.MultiDexApplication
//import dagger.android.*
//import ir.zngis.behtabdatacollection.dependencyInjection.AppInjector
//import javax.inject.Inject
//import android.os.Build
//import android.os.Build.BRAND
//import android.support.multidex.MultiDex
//import com.crashlytics.android.Crashlytics
//import com.crashlytics.android.answers.Answers
//import io.fabric.sdk.android.Fabric
//import ir.zngis.behtabdatacollection.BuildConfig
//
//
//class BehtabApp :  Application(), HasActivityInjector, HasServiceInjector, HasBroadcastReceiverInjector {
//
//
//    @Inject
//     lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
//
//
//    @Inject
//    lateinit var dispatchingServiceInjector: DispatchingAndroidInjector<Service>
//
//    @Inject
//    lateinit var broadcastReceiverInjector: DispatchingAndroidInjector<BroadcastReceiver>
//
//    override fun onCreate() {
//        super.onCreate()
//        AppInjector.init(this)
//
//        val crashlytics = Crashlytics()
//
//
//        Fabric.with(this, crashlytics)
//        Fabric.with(this, Answers())
//
//        Crashlytics.setString("VERSION_NAME",  BuildConfig.VERSION_NAME)
//        Crashlytics.setString("DEVICE", Build.DEVICE)
//        Crashlytics.setString("MODEL", Build.MODEL)
//        Crashlytics.setString("BRAND", Build.BRAND)
//    }
//
//    override fun attachBaseContext(base: Context) {
//        var lang = PreferencesUtil.getString(Constants.LANGUAGE, base)
//        if (lang.isNullOrEmpty()) {
//            lang = Constants.PERSIAN
//            PreferencesUtil.putString(Constants.LANGUAGE,Constants.PERSIAN,base)
//        }
//        super.attachBaseContext(LocaleHelper.onAttach(base, lang))
//        MultiDex.install(this)
//
//    }
//
//    override fun activityInjector(): AndroidInjector<Activity> {
//        return activityDispatchingAndroidInjector
//    }
//
//    override fun serviceInjector(): AndroidInjector<Service> {
//        return dispatchingServiceInjector
//    }
//
//    override fun broadcastReceiverInjector(): AndroidInjector<BroadcastReceiver> {
//        return broadcastReceiverInjector
//    }
//
//
//}