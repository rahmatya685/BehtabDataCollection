package ir.zngis.behtabdatacollection.util

object EnumUtil {

    enum class MapEditType{
        HospitalArea,
        HospitalEntrance,
        Building,
        CandidateLocation
    }


    enum class ToastActionState {
        Success,
        Failure,
        Info

    }



    enum class FileDownloadStatus{
        Exist,
        Download,
        ErrorDownloading
    }


    enum class EntityStatus constructor(private val code: Int) {
        DELETE(1),
        INSERT(2),
        SYNCED(3);


        companion object {

            fun fromInteger(x: Int): EntityStatus? {
                return when (x) {
                    1 -> DELETE
                    2 -> INSERT
                    3 -> SYNCED
                    else -> INSERT
                }
            }

            fun toInteger(value: EnumUtil.EntityStatus): Int {
                return when (value) {
                    DELETE -> 1
                    INSERT -> 2
                    SYNCED -> 3
                }
            }
        }
    }

    enum class HealthLocationType constructor(private val code :Int){
        HOSPITAL(1),
        HEALTH_HOUSE(2);

        companion object {
            fun toInt(healthLocationType: HealthLocationType): Int {
                when (healthLocationType) {
                    HOSPITAL -> return 1
                    HEALTH_HOUSE -> return 2
                    else -> return 0
                }
            }

            fun toHealthLocationType(type: Int): HealthLocationType {
                when (type) {
                    1 -> return HealthLocationType.HOSPITAL
                    2 -> return HealthLocationType.HEALTH_HOUSE
                    else -> return HealthLocationType.HOSPITAL
                }
            }
        }
    }
}