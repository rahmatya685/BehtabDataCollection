package com.edsab.gedat.edsgeneral.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by rahmati on 1/30/2018.
 */
fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}


