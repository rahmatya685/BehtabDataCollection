package ir.zngis.behtabdatacollection.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PreferencesUtil {

    public static void putString(String key, String value, Context context) {


        try {
            SharedPreferences.Editor editor = getEditor(context);
            editor.putString(key, value);
            editor.commit();
        } catch (Exception ex) {

        }
    }

    private static SharedPreferences.Editor getEditor(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).edit();
    }

    public static void putBoolean(String key, Boolean value, Context context) {
        try {
            SharedPreferences.Editor editor = getEditor(context);
            editor.putBoolean(key, value);
            editor.commit();
        } catch (Exception ex) {

        }
    }

    public static String getString(String key, Context context) {
        try {
            return PreferenceManager.getDefaultSharedPreferences(context).getString(key, "");
        } catch (Exception ex) {
            return "";
        }


    }

    public static String getCurrentLanguage(Context context) {
        return getString(Constants.LANGUAGE, context);
    }


    public static Boolean getBoolean(String key, Context context) {
        try {
            return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, false);
        } catch (Exception ex) {
            return false;
        }
    }

    public static int getInteger(String key, Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(key, -1);
    }

    public static void putInteger(String key, int value, Context context){
        try {
            SharedPreferences.Editor editor = getEditor(context);
            editor.putInt(key, value);
            editor.commit();
        } catch (Exception ex) {

        }
    }
    public static int getMapPref(String key, Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(key, 1);

    }

    public static void putArray(String key, List<String> list, Context context) {
        Set<String> set = new HashSet<String>(list);

        try {
            SharedPreferences.Editor editor = getEditor(context);
            editor.putStringSet(key, set);
            editor.commit();
        } catch (Exception ex) {

        }
    }

    public static List<String> getArray(String key, Context context) {
        Set<String> set = new HashSet<String>();
        set = PreferenceManager.getDefaultSharedPreferences(context).getStringSet(key, set);

        return new ArrayList<>(set);
    }
}
