package ir.zngis.behtabdatacollection.util;


import android.app.Application;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.zeugmasolutions.localehelper.LocaleHelperApplicationDelegate;
import javax.inject.Inject;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import io.fabric.sdk.android.Fabric;
import ir.zngis.behtabdatacollection.BuildConfig;
import ir.zngis.behtabdatacollection.dependencyInjection.AppInjector;

public class BehtabApp extends Application implements HasAndroidInjector {

    @Inject
    DispatchingAndroidInjector<Object> activityDispatchingAndroidInjector;


    private LocaleHelperApplicationDelegate localeAppDelegate =new LocaleHelperApplicationDelegate();



    @Inject
    public DispatchingAndroidInjector<Service>  dispatchingServiceInjector;

    @Inject
    public DispatchingAndroidInjector<BroadcastReceiver> broadcastReceiverInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        AppInjector.init(this);

        Crashlytics crashlytics =new Crashlytics();


        Fabric.with(this, crashlytics);
        Fabric.with(this,new Answers());

        Crashlytics.setString("VERSION_NAME",  BuildConfig.VERSION_NAME);
        Crashlytics.setString("DEVICE", Build.DEVICE);
        Crashlytics.setString("MODEL", Build.MODEL);
        Crashlytics.setString("BRAND", Build.BRAND);

    }

    @Override
    protected void attachBaseContext(Context base) {

        String lang = PreferencesUtil.getString(Constants.LANGUAGE, base);
        if (lang.isEmpty()) {
            PreferencesUtil.putString(Constants.LANGUAGE,Constants.PERSIAN,base);
        }
        super.attachBaseContext(localeAppDelegate.attachBaseContext(base));

    }


    @Override
    public AndroidInjector<Object> androidInjector() {
        return activityDispatchingAndroidInjector;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        localeAppDelegate.onConfigurationChanged(this);
    }
}
