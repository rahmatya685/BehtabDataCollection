package ir.zngis.behtabdatacollection.baseBinding;

import androidx.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by rahmati on 7/16/2018.
 */

public class BindableInt extends BaseObservable implements Parcelable, Cloneable {

    int value;

    protected BindableInt(Parcel in) {
        value = in.readInt();
    }

    public BindableInt(int value) {
        this.value = value;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BindableInt> CREATOR = new Creator<BindableInt>() {
        @Override
        public BindableInt createFromParcel(Parcel in) {
            return new BindableInt(in);
        }

        @Override
        public BindableInt[] newArray(int size) {
            return new BindableInt[size];
        }
    };

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        if (this.value != value) {
            notifyChange();
        }
        this.value = value;

    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BindableInt)) return false;

        BindableInt that = (BindableInt) o;

        return getValue() == that.getValue();
    }

    @Override
    public int hashCode() {
        return getValue();
    }
}
