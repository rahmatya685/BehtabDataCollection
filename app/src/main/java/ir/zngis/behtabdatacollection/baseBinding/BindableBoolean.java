package ir.zngis.behtabdatacollection.baseBinding;


import androidx.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;


public class BindableBoolean extends BaseObservable implements Parcelable, Cloneable {
    boolean mValue;

    public BindableBoolean(boolean mValue) {
        this.mValue = mValue;
    }

    protected BindableBoolean(Parcel in) {
        mValue = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (mValue ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BindableBoolean> CREATOR = new Creator<BindableBoolean>() {
        @Override
        public BindableBoolean createFromParcel(Parcel in) {
            return new BindableBoolean(in);
        }

        @Override
        public BindableBoolean[] newArray(int size) {
            return new BindableBoolean[size];
        }
    };

    public boolean get() {
        return mValue;
    }

    public void set(boolean value) {
        if (mValue != value) {
            this.mValue = value;
            notifyChange();
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BindableBoolean)) return false;

        BindableBoolean that = (BindableBoolean) o;

        return mValue == that.mValue;
    }

    @Override
    public int hashCode() {
        return (mValue ? 1 : 0);
    }
}