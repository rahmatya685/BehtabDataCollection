package ir.zngis.behtabdatacollection.baseBinding;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import androidx.databinding.BindingAdapter;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import androidx.core.util.Pair;
import androidx.core.widget.CompoundButtonCompat;
import android.text.Editable;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;
import java.text.DecimalFormat;

import ir.zngis.behtabdatacollection.R;
import ir.zngis.behtabdatacollection.repository.dataModel.Attachment;
import ir.zngis.behtabdatacollection.util.GeneralUtil;
import ir.zngis.behtabdatacollection.view.sub_view.TextWatcherAdapter;

/**
 * Created by rahmati on 7/15/2018.
 */

public class AttributeDefs {


    @BindingAdapter({"app:binding"})
    public static void bindEditText2(EditText view, final BindableInt bindableInt) {
        Pair<BindableInt, TextWatcherAdapter> pair = (Pair) view.getTag(R.id.bound_observable);
        if (pair == null || pair.first != bindableInt) {
            if (pair != null) {
                view.removeTextChangedListener(pair.second);
            }

            TextWatcherAdapter watcher = new TextWatcherAdapter() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    try {
                        bindableInt.setValue(Integer.parseInt(s.toString()));
                    } catch (java.lang.NumberFormatException e) {

                    }

                }
            };
            view.setTag(R.id.bound_observable, new Pair<>(bindableInt, watcher));
            view.addTextChangedListener(watcher);

        }
        if (bindableInt != null && bindableInt.value != -1) {
            String newValue = String.valueOf(bindableInt.getValue());
            if (!view.getText().toString().equals(newValue)) {
                view.setText(newValue);
            }
        }
    }

    @BindingAdapter({"app:binding"})
    public static void bindEditText(TextInputEditText view, final BindableInt bindableInt) {
        Pair<BindableInt, TextWatcherAdapter> pair = (Pair) view.getTag(R.id.bound_observable);
        if (pair == null || pair.first != bindableInt) {
            if (pair != null) {
                view.removeTextChangedListener(pair.second);
            }

            TextWatcherAdapter watcher = new TextWatcherAdapter() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    try {
                        bindableInt.setValue(Integer.parseInt(s.toString()));
                    } catch (Exception e) {
                    }
                }
            };
            view.setTag(R.id.bound_observable, new Pair<>(bindableInt, watcher));
            view.addTextChangedListener(watcher);

        }
        if (bindableInt != null && bindableInt.getValue() != -1) {
            String newValue = String.valueOf(bindableInt.getValue());
            if (!view.getText().toString().equals(newValue)) {
                view.setText(newValue);
            }
        }

    }


    @BindingAdapter({"app:binding"})
    public static void bindEditText(EditText view, final BindableString bindableString) {
        Pair<BindableString, TextWatcherAdapter> pair = (Pair) view.getTag(R.id.bound_observable);
        if (pair == null || pair.first != bindableString) {
            if (pair != null) {
                view.removeTextChangedListener(pair.second);
            }
            TextWatcherAdapter watcher = new TextWatcherAdapter() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    bindableString.set(s.toString());
                }
            };
            view.setTag(R.id.bound_observable, new Pair<>(bindableString, watcher));
            view.addTextChangedListener(watcher);
        }
        if (bindableString != null && !bindableString.get().isEmpty()) {
            String newValue = bindableString.get();
            if (!view.getText().toString().equals(newValue)) {
                view.setText(newValue);
            }
        }
    }

    @BindingAdapter({"app:binding"})
    public static void bindEditText(EditText editText, final BindableDouble bindableDouble) {
        Pair<BindableDouble, TextWatcherAdapter> pair = (Pair) editText.getTag(R.id.bound_observable);
        if (pair == null || pair.first != bindableDouble) {
            if (pair != null) {
                editText.removeTextChangedListener(pair.second);
            }
            TextWatcherAdapter watcher = new TextWatcherAdapter() {
                @Override
                public void afterTextChanged(Editable s) {
                    super.afterTextChanged(s);
                    String val = s.toString();
                    try {
                        if (!val.isEmpty())
                            bindableDouble.set(Double.parseDouble(val));
                        else {
                            bindableDouble.set(0);
                        }
                    } catch (Exception e) {
                        // bindableDouble.set(0);
                    }
                }
            };

            editText.setTag(R.id.bound_observable, new Pair<>(bindableDouble, watcher));
            editText.addTextChangedListener(watcher);
        }

        double curVal = -1;
        try {
            if (!editText.getText().toString().trim().isEmpty())
                curVal = Double.parseDouble(editText.getText().toString());
        } catch (NumberFormatException e) {
        }

        if (curVal != bindableDouble.get()) {
            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(2); //Sets the maximum number of digits after the decimal point
            df.setMinimumFractionDigits(0); //Sets the minimum number of digits after the decimal point
            df.setGroupingUsed(false); //If false thousands separator such ad 1,000 wont work so it will display 1000

            editText.setText(df.format(bindableDouble.get()));
        }
    }

    @BindingAdapter("app:checkState")
    public static void bindCheckboxCheckState(androidx.appcompat.widget.AppCompatCheckBox view, BindableBoolean bindableBoolean) {
        view.setTypeface(GeneralUtil.INSTANCE.getTypeFace(view.getContext()));
        Pair<BindableBoolean, CompoundButton.OnCheckedChangeListener> pair = (Pair<BindableBoolean, CompoundButton.OnCheckedChangeListener>) view.getTag(R.id.bound_observable);
        if (pair == null || pair.first != bindableBoolean) {
            if (pair != null)
                view.setOnCheckedChangeListener(null);

            CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    bindableBoolean.set(b);
                }
            };
            view.setTag(R.id.bound_observable, new Pair(bindableBoolean, onCheckedChangeListener));
            view.setOnCheckedChangeListener(onCheckedChangeListener);
        }
        if (view.isChecked() != bindableBoolean.get())
            view.setChecked(bindableBoolean.get());
    }

    @BindingAdapter("app:checkState")
    public static void bindSwitch(android.widget.Switch view, BindableBoolean bindableBoolean) {
        Pair<BindableBoolean, CompoundButton.OnCheckedChangeListener> pair = (Pair<BindableBoolean, CompoundButton.OnCheckedChangeListener>) view.getTag(R.id.bound_observable);
        if (pair == null || pair.first != bindableBoolean) {
            if (pair != null)
                view.setOnCheckedChangeListener(null);

            CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    bindableBoolean.set(b);
                }
            };
            view.setTag(R.id.bound_observable, new Pair(bindableBoolean, onCheckedChangeListener));
            view.setOnCheckedChangeListener(onCheckedChangeListener);
        }
        if (view.isChecked() != bindableBoolean.get())
            view.setChecked(bindableBoolean.get());
    }

    @BindingAdapter({"app:binding"})
    public static void bindCheckboxColor(androidx.appcompat.widget.AppCompatCheckBox view, BindableInt bindableInt) {
        BindableInt tagVal = (BindableInt) view.getTag(R.id.bound_observable2);
        if (tagVal == null) {
            view.setTag(bindableInt);
        }
        int newValue = bindableInt.getValue();
        CompoundButtonCompat.setButtonTintList(view, ColorStateList.valueOf(newValue));
    }


//    @BindingAdapter({"app:binding"})
//    public static void bindFloatingActionButtonColor(FloatingActionButton view, BindableInt bindableInt) {
//        Pair<BindableInt, View.OnClickListener> pair = (Pair<BindableInt, View.OnClickListener>) view.getTag(R.id.bound_observable);
//
//        if (pair == null || pair.first != bindableInt) {
//            if (pair != null) {
//                view.setOnClickListener(null);
//            }
//            View.OnClickListener onClickListener = view1 -> ColorPickerHandler.show(view1.getContext(), bindableInt.getValue(), (ColorPicker.OnColorChangedListener) color -> bindableInt.setValue(color));
//            view.setTag(R.id.bound_observable, new Pair<>(bindableInt, onClickListener));
//            view.setOnClickListener(onClickListener);
//        }
//        int newValue = bindableInt.getValue();
//        int oldColor = view.getBackgroundTintList().getColorForState(new int[]{0}, newValue++);
//        if (oldColor != newValue) {
//            view.setBackgroundTintList(ColorStateList.valueOf(newValue));
//        }
//    }

    @BindingAdapter({"app:binding"})
    public static void bindImageView(ImageView imageView, final String fileName) {

        File imagePath = GeneralUtil.INSTANCE.getPicsDirectory(imageView.getContext());

        String filePath = imagePath.getAbsolutePath() + File.separatorChar + fileName;

        Glide.with(imageView.getContext()).load(filePath).apply(
                new RequestOptions().override(imageView.getMeasuredWidth(), imageView.getMeasuredHeight()).centerCrop()
                        .dontAnimate().error(R.drawable.error).diskCacheStrategy(DiskCacheStrategy.ALL)
        ).into(imageView);
    }

    @BindingAdapter({"app:binding"})
    public static void bindAttachmentToImageView(ImageView imageView, Attachment attachment) {

        File imagePath = GeneralUtil.INSTANCE.getPicsDirectory(imageView.getContext());

        String filePath = imagePath.getAbsolutePath() + File.separatorChar + attachment.getFileName();

        File file = new File(filePath);
        if (file.exists()) {
            bindImageView(imageView, attachment.getFileName());
        } else {
            setImageUrl(imageView, attachment.getFileDownloadUri());
        }


    }

    @BindingAdapter({"app:binding"})
    public static void bindImageView(ImageView imageView, final Uri filePath) {

        Glide.with(imageView.getContext()).load(filePath).apply(
                new RequestOptions().override(imageView.getMeasuredWidth(), imageView.getMeasuredHeight()).centerCrop()
                        .dontAnimate().error(R.drawable.error).diskCacheStrategy(DiskCacheStrategy.ALL)
        ).into(imageView);
    }

    @SuppressLint("CheckResult")
    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        if (!url.isEmpty()) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.error(R.drawable.error);
            requestOptions.downsample(DownsampleStrategy.NONE);
            requestOptions.placeholder(R.drawable.ic_photo_black_24dp);

            Context context = imageView.getContext();

            Glide.with(context).asBitmap().load(url).apply(requestOptions).into(imageView);


        }
    }


    @BindingAdapter({"app:binding"})
    public static void bindImageView(ImageView imageView, final Boolean setVisible) {
        if (setVisible) {
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.GONE);
        }
    }

    @BindingAdapter({"app:binding"})
    public static void bindSpinner(Spinner view, final BindableString bindableString) {
        Pair<BindableString, AdapterView.OnItemSelectedListener> pair = (Pair) view.getTag(R.id.bound_observable);
        if (pair == null || pair.first != bindableString) {
            if (pair != null) {
                view.setOnItemSelectedListener(null);
            }
            AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    bindableString.set(adapterView.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    bindableString.set(null);

                }
            };
            view.setTag(R.id.bound_observable, new Pair<>(bindableString, onItemSelectedListener));
            view.setOnItemSelectedListener(onItemSelectedListener);
        }

        String newValue = "";
        if (bindableString != null)
            newValue = bindableString.get();
        if (view.getSelectedItem() != null && !view.getSelectedItem().toString().equals(newValue)) {
            Adapter adapter = view.getAdapter();

            int position = -1;

            for (int i = 0; i < adapter.getCount(); i++) {
                if (adapter.getItem(i).toString().equalsIgnoreCase(newValue)) {
                    position = i;
                }
            }
            if (position != -1)
                view.setSelection(position);
        }
    }

    @BindingAdapter({"app:binding"})
    public static void bindSpinner(Spinner view, final BindableInt bindableInt) {
        Pair<BindableInt, AdapterView.OnItemSelectedListener> pair = (Pair) view.getTag(R.id.bound_observable);
        if (pair == null || pair.first != bindableInt) {
            if (pair != null) {
                view.setOnItemSelectedListener(null);
            }
            AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (bindableInt != null) {
                        bindableInt.setValue(i);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    if (bindableInt == null) {
                        bindableInt.setValue(-1);
                    }
                }
            };
            view.setTag(R.id.bound_observable, new Pair<>(bindableInt, onItemSelectedListener));
            view.setOnItemSelectedListener(onItemSelectedListener);
        }

        if (bindableInt != null)
            view.setSelection(bindableInt.getValue());

    }

    @BindingAdapter({"app:binding"})
    public static void bindEditText(TextView view, final BindableString bindableString) {
        Pair<BindableString, TextWatcherAdapter> pair = (Pair) view.getTag(R.id.bound_observable);
        if (pair == null || pair.first != bindableString) {
            if (pair != null) {
                view.removeTextChangedListener(pair.second);
            }
            TextWatcherAdapter watcher = new TextWatcherAdapter() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    bindableString.set(s.toString());
                }
            };
            view.setTag(R.id.bound_observable, new Pair<>(bindableString, watcher));
            view.addTextChangedListener(watcher);

        }

        String newValue = "";
        if (bindableString != null)
            newValue = bindableString.get();
        if (!view.getText().toString().equals(newValue)) {
            view.setText(newValue);
        }
    }


    @BindingAdapter({"app:binding"})
    public static void bindRadioGroup(RadioGroup view, final BindableBoolean bindableBoolean) {
        if (view.getTag(R.id.bound_observable) != bindableBoolean) {
            view.setTag(R.id.bound_observable, bindableBoolean);
            view.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    bindableBoolean.set(checkedId == group.getChildAt(1).getId());
                }
            });
        }
        Boolean newValue = bindableBoolean.get();
        ((RadioButton) view.getChildAt(newValue ? 1 : 0)).setChecked(true);
    }

    @BindingAdapter({"app:onClick"})
    public static void bindOnClick(View view, final Runnable runnable) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runnable.run();
            }
        });
    }
}
