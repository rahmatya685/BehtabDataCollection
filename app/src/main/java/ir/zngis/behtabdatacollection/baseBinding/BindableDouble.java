package ir.zngis.behtabdatacollection.baseBinding;

import androidx.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by rahmati on 7/16/2018.
 */

public class BindableDouble extends BaseObservable implements Parcelable, Cloneable {


    double value;

    public BindableDouble(double value) {
        this.value = value;
    }

    protected BindableDouble(Parcel in) {
        value = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BindableDouble> CREATOR = new Creator<BindableDouble>() {
        @Override
        public BindableDouble createFromParcel(Parcel in) {
            return new BindableDouble(in);
        }

        @Override
        public BindableDouble[] newArray(int size) {
            return new BindableDouble[size];
        }
    };

    public double get() {
        return value;
    }

    public void set(double value) {
        if (this.value != value) {
            this.value = value;
            notifyChange();
        }
    }

    public boolean isEmpty() {
        return value == -1;
    }


    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BindableDouble)) return false;

        BindableDouble that = (BindableDouble) o;

        return Double.compare(that.value, value) == 0;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(value);
        return (int) (temp ^ (temp >>> 32));
    }
}
