package ir.zngis.behtabdatacollection.baseBinding;

import androidx.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by rahmati on 7/15/2018.
 */
public class BindableString extends BaseObservable implements Parcelable, Cloneable {
    private String value;

    public BindableString(String value) {
        this.value = value;
    }

    protected BindableString(Parcel in) {
        value = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BindableString> CREATOR = new Creator<BindableString>() {
        @Override
        public BindableString createFromParcel(Parcel in) {
            return new BindableString(in);
        }

        @Override
        public BindableString[] newArray(int size) {
            return new BindableString[size];
        }
    };

    public String get() {
        return value != null ? value : "";
    }

    public static boolean equals(Object a, Object b) {
        return (a == b) || (a != null && a.equals(b));
    }

    public void set(String value) {
        if (!equals(this.value, value)) {
            this.value = value;
            notifyChange();
        }
    }

    public boolean isEmpty() {
        return value == null || value.isEmpty();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BindableString)) return false;

        BindableString that = (BindableString) o;

        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }
}
