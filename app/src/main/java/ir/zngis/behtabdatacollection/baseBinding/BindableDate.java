package ir.zngis.behtabdatacollection.baseBinding;

import androidx.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by rahmati on 7/16/2018.
 */

public class BindableDate extends BaseObservable implements Parcelable, Cloneable {
    Date value;

    protected BindableDate(Parcel in) {
        in.writeSerializable(getValue());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        this.value = (Date) dest.readSerializable();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BindableDate> CREATOR = new Creator<BindableDate>() {
        @Override
        public BindableDate createFromParcel(Parcel in) {
            return new BindableDate(in);
        }

        @Override
        public BindableDate[] newArray(int size) {
            return new BindableDate[size];
        }
    };

    public Date getValue() {
        return value;
    }

    public void setValue(Date value) {
        if (this.value.equals(value)) {
            notifyChange();
        }
        this.value = value;
    }


    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BindableDate)) return false;

        BindableDate that = (BindableDate) o;

        return getValue() != null ? getValue().equals(that.getValue()) : that.getValue() == null;
    }

    @Override
    public int hashCode() {
        return getValue() != null ? getValue().hashCode() : 0;
    }
}
