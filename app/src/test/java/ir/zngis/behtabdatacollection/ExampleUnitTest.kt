package ir.zngis.behtabdatacollection

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        val url ="http://192.168.1.2:8080/downloadFile/behtab_20181212_221644_-1228177678.jpg"
        val fileName = url.substring(url.lastIndexOf("/")+1, url.length)

        assertEquals("behtab_20181212_221644_-1228177678.jpg" ,fileName  )
    }
}
